<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_has_books', function (Blueprint $table) {
            $table->integer('product_id')->after('book_id')->unsigned();

            $table->foreign('product_id')->references('id')->on('book_products')->onDelete('restrict');
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();

            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('patronimic')->nullable();
            $table->string('comment')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->tinyInteger('shipping_method')->unsigned()->nullable();
            $table->integer('delivery_point')->unsigned()->nullable();
            $table->string('country_code')->nullable();
            $table->string('region')->nullable();
            $table->string('settlement')->nullable();
            $table->string('address')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('kladr_id')->nullable();
            $table->float('weight')->nullable();
            $table->float('length')->nullable();
            $table->float('width')->nullable();
            $table->float('height')->nullable();
            $table->string('shipping_status')->nullable();
            $table->decimal('price');
            $table->decimal('delivery_price')->nullable();
            $table->decimal('total_price');
            $table->tinyInteger('payment_method')->unsigned();
            $table->string('payment_id')->nullable();
            $table->tinyInteger('status')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
        });

        Schema::create('orders_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('book_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('article');
            $table->tinyInteger('type')->unsigned();
            $table->tinyInteger('quantity')->unsigned();
            $table->decimal('original_price');
            $table->tinyInteger('discount')->unsigned();
            $table->decimal('price');
            $table->float('length');
            $table->float('width');
            $table->float('height');
            $table->float('weight');
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('restrict');
            $table->foreign('product_id')->references('id')->on('book_products')->onDelete('restrict');
            $table->foreign('book_id')->references('id')->on('books')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
