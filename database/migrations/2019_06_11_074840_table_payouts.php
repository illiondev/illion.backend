<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablePayouts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('payouts');

        Schema::create('payouts', function (Blueprint $table) {
            $table->increments('id');

            $table->decimal('amount');

            $table->integer('author_id')->unsigned();
            $table->integer('moderator_id')->unsigned();

            $table->foreign('author_id')->references('id')->on('authors')->onDelete('restrict');
            $table->foreign('moderator_id')->references('id')->on('users')->onDelete('restrict');

            $table->date('payout_date')->nullable();
            $table->timestamps();
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
