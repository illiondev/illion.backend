<?php

use App\Models\OrderBill;
use App\Models\OrderItem;
use App\Models\Promocode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Moderator\Models\Price;

class CreateTableBundle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bundles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('book_id');
            $table->string('slug')->unique();
            $table->boolean('enabled')->default(0);
            $table->boolean('hidden')->default(0);
            $table->boolean('pre_order')->default(0);
            $table->string('title')->default('');
            $table->text('description')->nullable();

            $table->foreign('book_id')->references('id')->on('books')->onDelete('restrict');
            $table->timestamps();
        });

        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('model');
            $table->unsignedTinyInteger('type');
            $table->string('currency');
            $table->decimal('amount');
        });

        Schema::create('bundles_has_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bundle_id');
            $table->unsignedInteger('product_id');
            $table->boolean('is_surprise')->default(0);
            $table->foreign('bundle_id')->references('id')->on('bundles')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('book_products')->onDelete('cascade');
        });

        $products = \App\Models\Product::with(['book'])->get();

        foreach ($products as $product) {
            $bundle = new \App\Models\Bundle();

            if ($product->article == '008-e') {
                $bundle->slug = 'solodar';
            } else {
                $bundle->slug = uniqid();
            }

            $bundle->book_id = $product->book_id;
            $bundle->enabled = $product->enabled;
            $bundle->hidden = $product->hidden;
            $bundle->pre_order = $product->pre_order;
            $bundle->title = $product->book->title;
            $bundle->description = $product->description;

            // мигрировать картинку

            $bundle->saveOrFail();

            $price = new Price();
            $price->type = Price::TYPE_WEB;
            $price->currency = Price::CURRENCY_RUB;
            $price->amount = $product->price_web_rub;

            $bundle->prices()->save($price);
            $bundle->products()->syncWithoutDetaching($product->id);
        }

        Schema::table('book_products', function (Blueprint $table) {
            $table->dropColumn('enabled');
            $table->dropColumn('hidden');
            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('pre_order');
            $table->dropColumn('price_web_rub');
            $table->dropColumn('price_web_eur');
            $table->dropColumn('price_web_uah');
            $table->dropColumn('price_mob_rub');
            $table->dropColumn('price_mob_eur');
            $table->dropColumn('price_mob_uah');
        });

        Schema::table('orders_bills', function (Blueprint $table) {
            $table->unsignedInteger('bundle_id')->after('order_id')->nullable();
            $table->foreign('bundle_id')->references('id')->on('bundles')->onDelete('restrict');
        });

        // мигрировать счета
        $bills = OrderBill::all();
        foreach ($bills as $bill) {
            $bundleId = DB::table('bundles_has_products')->where('product_id', $bill->product_id)->value('bundle_id');

            $bill->bundle_id = $bundleId;
            $bill->saveOrFail();
        }

        Schema::table('orders_bills', function (Blueprint $table) {
            $table->dropForeign('orders_bills_product_id_foreign');
            $table->dropColumn('product_id');
            $table->dropColumn('article');
            $table->dropColumn('title');
        });

        Schema::table('promocodes', function (Blueprint $table) {
            $table->unsignedInteger('bundle_id')->after('code')->nullable();
            $table->foreign('bundle_id')->references('id')->on('bundles')->onDelete('restrict');
        });

        // мигрировать промокоды
        $promocodes = Promocode::all();
        foreach ($promocodes as $promocode) {
            $bundleId = DB::table('bundles_has_products')->where('product_id', $promocode->product_id)->value('bundle_id');

            $promocode->bundle_id = $bundleId;
            $promocode->saveOrFail();
        }

        Schema::table('promocodes', function (Blueprint $table) {
            $table->dropForeign('promocodes_product_id_foreign');
            $table->dropColumn('product_id');
        });

        Schema::table('orders_items', function (Blueprint $table) {
            $table->unsignedInteger('bundle_id')->after('order_id')->nullable();
            $table->foreign('bundle_id')->references('id')->on('bundles')->onDelete('restrict');
        });

        Schema::create('orders_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('order_item_id')->unsigned();
            $table->integer('book_id')->unsigned();
            $table->decimal('prime_cost');
            $table->integer('bundle_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('article');
            $table->string('book_title');
            $table->tinyInteger('type')->unsigned();
            $table->float('length');
            $table->float('width');
            $table->float('height');
            $table->float('weight');
            $table->boolean('is_surprise')->default(0);

            $table->foreign('order_item_id')->references('id')->on('orders_items')->onDelete('restrict');
            $table->foreign('bundle_id')->references('id')->on('bundles')->onDelete('restrict');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('restrict');
            $table->foreign('product_id')->references('id')->on('book_products')->onDelete('restrict');
            $table->foreign('book_id')->references('id')->on('books')->onDelete('restrict');
        });

        // мигрировать айтемы
        $bundleHasProducts = DB::table('bundles_has_products')->get();
        foreach ($bundleHasProducts as $bundleHasProduct) {
            OrderItem::where(['product_id' => $bundleHasProduct->product_id])
                ->update(['bundle_id' => $bundleHasProduct->bundle_id]);
        }

        $orderItems = OrderItem::all();
        foreach ($orderItems as $orderItem) {
            \App\Models\OrderProduct::insert([
                'order_id' => $orderItem->order_id,
                'order_item_id' => $orderItem->id,
                'bundle_id' => $orderItem->bundle_id,
                'prime_cost' => $orderItem->prime_cost,
                'book_title' => $orderItem->title,
                'book_id' => $orderItem->book_id,
                'product_id' => $orderItem->product_id,
                'article' => $orderItem->article,
                'type' => $orderItem->type,
                'length' => $orderItem->length,
                'width' => $orderItem->width,
                'height' => $orderItem->height,
                'weight' => $orderItem->weight,
                'is_surprise' => 0,
            ]);
        }

        Schema::table('orders_items', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->dropForeign('orders_items_product_id_foreign');
            $table->dropColumn('product_id');
            $table->dropColumn('article');
            $table->dropColumn('type');
            $table->dropColumn('length');
            $table->dropColumn('width');
            $table->dropColumn('height');
            $table->dropColumn('weight');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        die(__CLASS__ . ' migration cannot be reverted' . PHP_EOL);
    }
}
