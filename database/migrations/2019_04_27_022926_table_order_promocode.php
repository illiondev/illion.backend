<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableOrderPromocode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('promocode')->after('total_price')->nullable();
            $table->float('discount')->after('price')->unsigned()->nullable();
            $table->float('discount_flat')->unsigned()->after('discount')->nullable();

            $table->float('delivery_discount')->after('delivery_price')->unsigned()->nullable();
            $table->float('delivery_discount_flat')->unsigned()->after('delivery_discount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
