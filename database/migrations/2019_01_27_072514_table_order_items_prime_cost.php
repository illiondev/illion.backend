<?php

use App\Models\OrderItem;
use App\Models\Product;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableOrderItemsPrimeCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_items', function (Blueprint $table) {
            $table->decimal('prime_cost')->after('book_id');
        });

        $products = Product::all();

        foreach ($products as $product) {
            OrderItem::where([
                'product_id' => $product->id,
                'type' => $product->type,
            ])
                ->update(['prime_cost' => $product->prime_cost]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
