<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductAuthorId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_products', function (Blueprint $table) {
            $table->integer('author_id')->after('book_id')->unsigned()->nullable();
        });

        $products = \App\Models\Product::with([
            'book',
        ])->get();

        /** @var \App\Models\Product $product */
        foreach ($products as $product) {
            $product->author_id = $product->book->author_id;
            $product->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
