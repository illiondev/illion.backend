<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableExternalOrderBundles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('external_orders_items', function (Blueprint $table) {
            $table->unsignedInteger('bundle_id')->after('product_id')->nullable();
            $table->foreign('bundle_id')->references('id')->on('bundles')->onDelete('restrict');
        });

        $externalOrdersItems = \App\Models\ExternalOrderItem::all();
        foreach ($externalOrdersItems as $externalOrdersItem) {
            $bundleId = DB::table('bundles_has_products')->where('product_id', $externalOrdersItem->product_id)->value('bundle_id');

            $externalOrdersItem->bundle_id = $bundleId;
            $externalOrdersItem->saveOrFail();
        }

        Schema::table('external_orders_items', function (Blueprint $table) {
            $table->dropColumn('product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
