<?php

use App\Models\BillProduct;
use App\Models\Order;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BillsProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('orders_bills', 'bills');
        Schema::drop('book_products_has_includes');

        Schema::create('bills_products', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('quantity')->unsigned();
            $table->integer('bill_id')->unsigned();
            $table->integer('book_id')->unsigned();
            $table->integer('author_id')->unsigned();
            $table->decimal('prime_cost');
            $table->integer('bundle_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('article');
            $table->string('book_title');
            $table->tinyInteger('type')->unsigned();
            $table->float('length');
            $table->float('width');
            $table->float('height');
            $table->float('weight');
            $table->boolean('is_surprise');
            $table->float('authors_percent');
            $table->decimal('price');
            $table->decimal('net_profit')->nullable();

            $table->foreign('bill_id')->references('id')->on('bills')->onDelete('restrict');
            $table->foreign('book_id')->references('id')->on('books')->onDelete('restrict');
            $table->foreign('bundle_id')->references('id')->on('bundles')->onDelete('restrict');
            $table->foreign('product_id')->references('id')->on('book_products')->onDelete('restrict');
            $table->foreign('author_id')->references('id')->on('authors')->onDelete('restrict');
        });

        $bills = \App\Models\OrderBill::whereNotNull('bundle_id')->get();

        foreach ($bills as $bill) {
            /** @var \App\Models\Bundle $bundle */
            $bundle = $bill->bundle;
            foreach ($bundle->products as $product) {
                $billProduct = new BillProduct();

                $billProduct->bill_id = $bill->id;
                $billProduct->book_id = $product->book_id;
                $billProduct->product_id = $product->id;
                $billProduct->prime_cost = $product->prime_cost;
                $billProduct->author_id = $product->author_id;
                $billProduct->quantity = 1;
                $billProduct->authors_percent = $product->pivot->authors_percent;
                $billProduct->article = $product->article;
                $billProduct->book_title = $product->book->title;
                $billProduct->type = $product->type;
                $billProduct->bundle_id = $bundle->id;
                $billProduct->length = $product->length;
                $billProduct->width = $product->width;
                $billProduct->height = $product->height;
                $billProduct->weight = $product->weight;
                $billProduct->is_surprise = $product->pivot->is_surprise;
                $billProduct->price = $product->pivot->price;

                if ($bill->isPaid()) {
                    $fee = 0;
                    if (!$bill->external_kassa) {
                        if (isset(Order::$paymentTypeFee[$bill->payment_type])) {
                            $fee = Order::$paymentTypeFee[$bill->payment_type];
                        }
                    }

                    $billProduct->net_profit = $billProduct->price * (1 - ($fee / 100));
                }

                $billProduct->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
