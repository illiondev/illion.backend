<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePublishedBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('published_books', function (Blueprint $table) {
            $table->increments('id');

            $table->string('filename');

            $table->unsignedInteger('book_id');

            $table->unsignedInteger('size');

            $table->string('secret_key');

            $table->timestamps();

            $table->foreign('book_id')->references('id')->on('books')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('published_books');
    }
}
