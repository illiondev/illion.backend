<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promocodes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('type');
            $table->string('code');
            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('book_products')->onDelete('restrict');

            $table->float('discount')->unsigned();
            $table->float('discount_flat')->unsigned();
            $table->unsignedInteger('limit');
            $table->unsignedInteger('used');
            $table->timestamp('expired_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promocode');
    }
}
