<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExternalSaleNetProfit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('external_sales', function (Blueprint $table) {
            $table->decimal('net_profit')->after('commission_flat')->default(0);
        });

        $externalSales = \App\Models\ExternalSale::all();

        foreach ($externalSales as $externalSale) {
            if ($externalSale->commission_flat) {
                $externalSale->net_profit = $externalSale->price - $externalSale->commission_flat;
            } else if ($externalSale->commission) {
                $externalSale->net_profit = $externalSale->price * (1 - ($externalSale->commission / 100));
            }

            $externalSale->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
