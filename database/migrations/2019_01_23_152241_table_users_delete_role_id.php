<?php

use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;

class TableUsersDeleteRoleId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Role::create(['name' => User::ROLE_USER]);
        Role::create(['name' => User::ROLE_MODERATOR]);
        Role::create(['name' => User::ROLE_AUTHOR]);
        Role::create(['name' => User::ROLE_ADMIN]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
