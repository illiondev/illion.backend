<?php

use App\Models\Order;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewOrderProductsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_products', function (Blueprint $table) {
            $table->integer('author_id')->after('book_id')->unsigned()->nullable();
            $table->tinyInteger('quantity')->after('id')->unsigned()->nullable();
            $table->float('authors_percent')->after('is_surprise')->nullable();
            $table->decimal('price')->after('authors_percent')->nullable();
            $table->decimal('net_profit')->after('price')->nullable();

            $table->foreign('author_id')->references('id')->on('authors')->onDelete('restrict');
        });

        $orderProducts = \App\Models\OrderProduct::with([
            'orderItem',
            'book',
            'order',
        ])->get();

        /** @var \App\Models\OrderProduct $orderProduct */
        foreach ($orderProducts as $orderProduct) {
            $orderProduct->quantity = $orderProduct->orderItem->quantity;
            $orderProduct->author_id = $orderProduct->book->author_id;

            $bundle = \Modules\Moderator\Models\Bundle::findOrFail($orderProduct->bundle_id);
            $product = $bundle->products()->findOrFail($orderProduct->product_id);
            $orderProduct->authors_percent = $product->pivot->authors_percent;
            $orderProduct->price = $product->pivot->price;

            if ($orderProduct->order->isPaid()) {
                $fee = 0;
                if (!$orderProduct->order->external_kassa) {
                    if (isset(Order::$paymentTypeFee[$orderProduct->order->payment_type])) {
                        $fee = Order::$paymentTypeFee[$orderProduct->order->payment_type];
                    }
                }

                $orderProduct->net_profit = $orderProduct->price * (1 - ($fee / 100));
            }

            $orderProduct->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
