<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExternalOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('token');
        });

        Schema::create('external_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('external_id');

            $table->string('country_code');
            $table->string('region');
            $table->string('city');

            $table->unsignedTinyInteger('status');

            $table->timestamp('sold_at');

            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('external_clients')->onDelete('restrict');
            $table->unique(['client_id', 'external_id']);
        });

        Schema::create('external_orders_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('external_order_id');
            $table->unsignedInteger('external_id');

            $table->unsignedInteger('product_id');
            $table->unsignedTinyInteger('quantity');
            $table->decimal('prime_cost');
            $table->decimal('price');
            $table->decimal('net_profit');

            $table->foreign('client_id')->references('id')->on('external_clients')->onDelete('restrict');
            $table->unique(['client_id', 'external_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_orders_items');
        Schema::dropIfExists('external_orders');
        Schema::dropIfExists('external_clients');
    }
}
