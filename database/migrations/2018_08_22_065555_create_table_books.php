<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();

            $table->string('title');
            $table->integer('author_id')->unsigned();

            $table->float('price_web');
            $table->float('price_mob');

            $table->mediumText('preview');

            $table->date('publication_date')->nullable();

            $table->timestamp('completed_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('author_id')->references('id')->on('authors')->onDelete('restrict');
        });

        Schema::create('book_chapters', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('book_id')->unsigned();

            $table->integer('number')->unsigned();

            $table->string('title')->default('');
            $table->mediumText('content');

            $table->boolean('completed')->default(0);

            $table->timestamps();

            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_chapters');
        Schema::dropIfExists('books');
    }
}
