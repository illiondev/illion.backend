<?php

use App\Models\Order;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersAndBillsFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bills', function (Blueprint $table) {
            $table->float('payment_fee')->after('payment_type')->nullable();
        });

        $bills = \App\Models\OrderBill::whereNotNull('bundle_id')->get();

        foreach ($bills as $bill) {
            if ($bill->isPaid()) {
                $fee = 0;
                if (!$bill->external_kassa) {
                    if (isset(Order::$paymentTypeFee[$bill->payment_type])) {
                        $fee = Order::$paymentTypeFee[$bill->payment_type];
                    }
                }

                $bill->payment_fee = $fee;
                $bill->save();
            }
        }

        Schema::table('orders', function (Blueprint $table) {
            $table->float('payment_fee')->after('payment_type')->nullable();
        });

        $orders = \App\Models\Order::all();

        foreach ($orders as $order) {
            if ($order->isPaid()) {
                $fee = 0;
                if (!$order->external_kassa) {
                    if (isset(Order::$paymentTypeFee[$order->payment_type])) {
                        $fee = Order::$paymentTypeFee[$order->payment_type];
                    }
                }

                $order->payment_fee = $fee;
                $order->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
