<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableProductTypeVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_products', function (Blueprint $table) {
            $table->boolean('hidden')->default(0)->after('enabled');
        });

        Schema::create('book_products_has_includes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('book_products')->onDelete('restrict');

            $table->integer('included_product_id')->unsigned();
            $table->foreign('included_product_id')->references('id')->on('book_products')->onDelete('restrict');

            $table->timestamp('expire_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
