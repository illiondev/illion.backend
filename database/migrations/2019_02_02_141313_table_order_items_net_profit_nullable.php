<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Order;

class TableOrderItemsNetProfitNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_items', function (Blueprint $table) {
            $table->decimal('net_profit')->after('price')->nullable()->change();
        });

        $orders = Order::whereIn('status', [Order::STATUS_PROCESSING, Order::STATUS_COMPLETED])->get();

        foreach ($orders as $order) {
            if (isset(Order::$paymentTypeFee[$order->payment_type])) {
                $fee = Order::$paymentTypeFee[$order->payment_type];
            } else {
                $fee = 0;
                app('sentry')->captureMessage('order ' . $order->id .  ' unknown payment type ' . $order->payment_type);
            }

            foreach ($order->items as $item) {
                $item->net_profit = $item->price * (1 - ($fee / 100));
                $item->saveOrFail();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
