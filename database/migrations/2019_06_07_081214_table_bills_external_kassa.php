<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableBillsExternalKassa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bundles', function (Blueprint $table) {
            $table->unsignedInteger('shop_id')->nullable()->after('description');
            $table->string('secret_key')->nullable()->after('shop_id');
        });

        Schema::table('orders_bills', function (Blueprint $table) {
            $table->boolean('external_kassa')->default(0)->after('status');
        });

        $books = \App\Models\Book::all();

        foreach ($books as $book) {
            if ($book->shop_id) {
                \App\Models\Bundle::where([
                    'book_id' => $book->id,
                ])->update([
                    'shop_id' => $book->shop_id,
                    'secret_key' => $book->secret_key,
                ]);

            }
         }

        Schema::table('books', function (Blueprint $table) {
            $table->dropColumn('shop_id');
            $table->dropColumn('secret_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
