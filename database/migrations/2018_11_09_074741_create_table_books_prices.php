<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBooksPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('books', function (Blueprint $table) {
            $table->string('tagline')->default('')->after('title');

            $table->dropColumn('epub');
            $table->dropColumn('price_mob');
            $table->dropColumn('price_web');
        });

        Schema::create('book_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku')->unique();
            $table->boolean('enabled')->default(0);

            $table->unsignedTinyInteger('type');
            $table->unsignedInteger('book_id');

            $table->decimal('price_web_rub');
            $table->decimal('price_web_eur');
            $table->decimal('price_web_uah');
            $table->decimal('price_mob_rub');
            $table->decimal('price_mob_eur');
            $table->decimal('price_mob_uah');

            $table->boolean('pre_order')->default(0);

            // epub, вес
            $table->text('meta');

            $table->timestamps();

            $table->foreign('book_id')->references('id')->on('books')->onDelete('restrict');
        });

        Schema::create('book_advantages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('book_id');
            $table->text('text');

            $table->foreign('book_id')->references('id')->on('books')->onDelete('restrict');
        });

        Schema::create('book_features', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('book_id');
            $table->string('text');

            $table->foreign('book_id')->references('id')->on('books')->onDelete('restrict');
        });

        Schema::create('book_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('book_id');
            $table->string('name');
            $table->string('text');

            $table->timestamps();

            $table->foreign('book_id')->references('id')->on('books')->onDelete('restrict');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('book_products')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
