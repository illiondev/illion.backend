<?php

use App\Models\Price;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableBundleMinusBookId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bundles_has_products', function (Blueprint $table) {
            $table->decimal('price')->default(0);
            $table->float('authors_percent')->default(50);
        });

        $bundles = \App\Models\Bundle::all();

        foreach ($bundles as $bundle) {
            $products = $bundle->products;

            foreach ($products as $product) {
                $pivot = $product->pivot;
                $pivot->price = $bundle->priceByCurrency(Price::CURRENCY_RUB);
                $pivot->save();
            }
        }

        Schema::create('books_has_bundles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('book_id');
            $table->unsignedInteger('bundle_id');
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->foreign('bundle_id')->references('id')->on('bundles')->onDelete('cascade');
        });

        /** @var \App\Models\Book[] $books */
        $books = \App\Models\Book::all();
        foreach ($books as $book) {
            $bundles = \App\Models\Bundle::where(['book_id' => $book->id]);
            $book->bundles()->syncWithoutDetaching($bundles->pluck('id')->toArray());
        }

        Schema::table('bundles', function (Blueprint $table) {
            $table->dropForeign('bundles_book_id_foreign');
            $table->dropColumn('book_id');
        });
        Schema::table('orders_items', function (Blueprint $table) {
            $table->dropForeign('orders_items_book_id_foreign');
            $table->dropColumn('book_id');
        });
        Schema::table('orders_bills', function (Blueprint $table) {
            $table->dropForeign('orders_bills_book_id_foreign');
            $table->dropColumn('book_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
