<?php

use App\Models\ExternalSaleProduct;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExternalSaleProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_sales_products', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('quantity')->unsigned();
            $table->integer('external_sale_id')->unsigned();
            $table->integer('book_id')->unsigned();
            $table->integer('author_id')->unsigned();
            $table->decimal('prime_cost');
            $table->integer('bundle_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('article');
            $table->string('book_title');
            $table->tinyInteger('type')->unsigned();
            $table->float('length');
            $table->float('width');
            $table->float('height');
            $table->float('weight');
            $table->boolean('is_surprise');
            $table->float('authors_percent');
            $table->decimal('price');
            $table->decimal('net_profit')->nullable();

            $table->foreign('external_sale_id')->references('id')->on('external_sales')->onDelete('restrict');
            $table->foreign('book_id')->references('id')->on('books')->onDelete('restrict');
            $table->foreign('bundle_id')->references('id')->on('bundles')->onDelete('restrict');
            $table->foreign('product_id')->references('id')->on('book_products')->onDelete('restrict');
            $table->foreign('author_id')->references('id')->on('authors')->onDelete('restrict');
        });

        $externalSales = \App\Models\ExternalSale::all();

        foreach ($externalSales as $externalSale) {
            /** @var \App\Models\Bundle $bundle */
            $bundle = $externalSale->bundle;

            foreach ($bundle->products as $product) {
                $externalSaleProduct = new ExternalSaleProduct();

                $externalSaleProduct->external_sale_id = $externalSale->id;
                $externalSaleProduct->book_id = $product->book_id;
                $externalSaleProduct->product_id = $product->id;
                $externalSaleProduct->prime_cost = $product->prime_cost;
                $externalSaleProduct->author_id = $product->author_id;
                $externalSaleProduct->quantity = 1;
                $externalSaleProduct->authors_percent = $externalSale->author_fee;
                $externalSaleProduct->article = $product->article;
                $externalSaleProduct->book_title = $product->book->title;
                $externalSaleProduct->type = $product->type;
                $externalSaleProduct->bundle_id = $bundle->id;
                $externalSaleProduct->length = $product->length;
                $externalSaleProduct->width = $product->width;
                $externalSaleProduct->height = $product->height;
                $externalSaleProduct->weight = $product->weight;
                $externalSaleProduct->is_surprise = $product->pivot->is_surprise;
                $externalSaleProduct->price = $product->pivot->price;

                if ($externalSale->commission_flat) {
                    $externalSaleProduct->net_profit = $externalSaleProduct->price - $externalSale->commission_flat;
                } else if ($externalSale->commission) {
                    $externalSaleProduct->net_profit = $externalSaleProduct->price * (1 - ($externalSale->commission / 100));
                } else {
                    $externalSaleProduct->net_profit = $externalSaleProduct->price;
                }

                $externalSaleProduct->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
