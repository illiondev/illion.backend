<?php

use App\Models\Preference;
use App\Models\PreferenceOption;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePreferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
        });

        Schema::create('preference_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('preference_id')->unsigned();

            $table->string('title');

            $table->foreign('preference_id')->references('id')->on('preferences')->onDelete('cascade');
        });

        Schema::create('user_has_preference_option', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();

            $table->integer('preference_option_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('preference_option_id')->references('id')->on('preference_options')->onDelete('cascade');
        });

        $data = [
            'Много ли читаете?' => [
                'книгу в неделю',
                'книгу в месяц',
                'книгу в три месяца',
                'книгу в полгода',
                'книгу в год',
            ],
            'Вы предпочитаете электронные книги или бумажные?' => [
                'электронные',
                'бумажные',
            ],
            'Какую литературу вы предпочитаете?' => [
                'фантастика',
                'художественная',
                'научно-популярная',
                'профессиональная',
                'психология',
                'детская',
                'образовательная',
                'историческая',
            ],
            'Дарите ли вы книги?' => [
                'на',
                'нет',
            ],
            'Пользуетесь электронной книгой или моб.устройством?' => [
                'электронной книгой',
                'моб.устройством',
            ],
        ];

        foreach ($data as $key => $value) {
            $preference = new Preference();
            $preference->title = $key;
            $preference->save();

            foreach ($value as $option) {
                $preferenceOption = new PreferenceOption();
                $preferenceOption->preference_id = $preference->id;
                $preferenceOption->title = $option;
                $preferenceOption->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_has_preference_option');
        Schema::dropIfExists('preference_options');
        Schema::dropIfExists('preferences');
    }
}
