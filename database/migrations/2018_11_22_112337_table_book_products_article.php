<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableBookProductsArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_products', function (Blueprint $table) {
            $table->string('article')->after('id');
        });

        $products = \App\Models\Product::all();

        foreach ($products as $product) {
            $product->article = uniqid();
            $product->save();
        }

        Schema::table('book_products', function (Blueprint $table) {
            $table->unique('article');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
