<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payouts', function (Blueprint $table) {
            $table->increments('id');

            $table->decimal('amount');

            $table->integer('author_user_id')->unsigned();
            $table->integer('moderator_user_id')->unsigned();

            $table->foreign('author_user_id')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('moderator_user_id')->references('id')->on('users')->onDelete('restrict');

            $table->timestamp('payout_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payouts');
    }
}
