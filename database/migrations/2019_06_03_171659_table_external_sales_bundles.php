<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableExternalSalesBundles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('external_sales', function (Blueprint $table) {
            $table->unsignedInteger('bundle_id')->after('product_id')->nullable();
            $table->foreign('bundle_id')->references('id')->on('bundles')->onDelete('restrict');
        });

        $externalSales = \App\Models\ExternalSale::all();
        foreach ($externalSales as $externalSale) {
            $bundleId = DB::table('bundles_has_products')->where('product_id', $externalSale->product_id)->value('bundle_id');

            $externalSale->bundle_id = $bundleId;
            $externalSale->saveOrFail();
        }

        Schema::table('external_sales', function (Blueprint $table) {
            $table->dropForeign('external_sales_product_id_foreign');
            $table->dropColumn('product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
