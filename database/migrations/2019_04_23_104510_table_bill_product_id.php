<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableBillProductId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_bills', function (Blueprint $table) {
            $table->integer('book_id')->after('order_id')->nullable()->unsigned();
            $table->foreign('book_id')->references('id')->on('books')->onDelete('restrict');

            $table->integer('product_id')->after('book_id')->nullable()->unsigned();
            $table->foreign('product_id')->references('id')->on('book_products')->onDelete('restrict');

            $table->string('article')->after('product_id')->nullable();
            $table->string('title')->after('article')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
