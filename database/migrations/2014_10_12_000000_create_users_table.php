<?php

use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('external_id')->unique();

            $table->unsignedTinyInteger('status')->default(User::STATUS_NEW);

            $table->string('email')->unique();
            $table->timestamp('email_confirmed_at')->nullable();

            $table->boolean('subscription_email')->default(0);

            $table->boolean('agreements_accepted')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
