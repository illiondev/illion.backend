<?php

use Faker\Generator as Faker;
use App\Models\OrderBill;

$factory->define(OrderBill::class, function (Faker $faker) {
    return [
        'hash' => $faker->unique()->md5,
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
        'order_id' => function () {
            return factory(\App\Models\Order::class)->create()->id;
        },
        'price' => $faker->numberBetween(10, 100),
        'payment_id' => $faker->text,
        'status' => OrderBill::PAYMENT_STATUS_NEW,
    ];
});
