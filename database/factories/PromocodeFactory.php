<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Promocode::class, function (Faker $faker) {
    return [
        'type' => $faker->randomDigit,
        'code' => $faker->md5,
        'product_id' => function () {
            return factory(\App\Models\Product::class)->create()->id;
        },
        'discount' => $faker->randomFloat(2, 1, 99),
        'discount_flat' => $faker->randomFloat(2, 1, 99),
        'limit' => 0,
        'used' => $faker->randomNumber(1, true),
    ];
});
