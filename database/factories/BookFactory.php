<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Book::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
        'title' => $faker->text,
        'author_id' => function () {
            return factory(\App\Models\Author::class)->create()->id;
        },
        'preview' => $faker->text,
    ];
});
