<?php

use App\Models\Profile;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
        'name' => $faker->firstName,
        'gender' => $faker->randomElement([
            Profile::GENDER_FEMALE,
            Profile::GENDER_MALE,
            Profile::GENDER_UNKNOWN,
        ]),
        'birthday' => \Carbon\Carbon::parse($faker->date),
    ];
});
