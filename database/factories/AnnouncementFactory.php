<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Announcement::class, function (Faker $faker) {
    return [
        'enabled' => $faker->boolean,
        'content' => $faker->text,
        'page_type' => $faker->randomElement([
            \App\Models\Announcement::PAGE_TYPE_MAIN,
            \App\Models\Announcement::PAGE_TYPE_ALL
        ]),
    ];
});
