<?php

use Faker\Generator as Faker;
use App\Models\OrderItem;
use App\Models\Product;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'order_id' => function () {
            return factory(\App\Models\Order::class)->create()->id;
        },
        'book_id' => function () {
            return factory(\App\Models\Book::class)->create()->id;
        },
        'product_id' => function () {
            return factory(Product::class)->create()->id;
        },
        'type' => $faker->randomElement([
            Product::PRODUCT_TYPE_PAPER_BOOK,
            Product::PRODUCT_TYPE_E_BOOK,
        ]),
        'prime_cost' => $faker->numberBetween(10, 100),
        'article' => $faker->text,
        'title' => $faker->title,
        'quantity' => $faker->numberBetween(1, 9),
        'price' => $faker->numberBetween(10, 100),
        'original_price' => $faker->numberBetween(10, 100),
        'discount' => $faker->numberBetween(1, 99),
        'length' => $faker->numberBetween(20, 500),
        'width' => $faker->numberBetween(20, 500),
        'height' => $faker->numberBetween(5, 50),
        'weight' => $faker->numberBetween(100, 400),
    ];
});
