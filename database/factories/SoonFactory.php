<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Soon::class, function (Faker $faker) {
    return [
        'enabled' => $faker->boolean,
        'title' => $faker->title,
        'author_id' => function () {
            return factory(\App\Models\Author::class)->create()->id;
        },
    ];
});
