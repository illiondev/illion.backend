<?php

use Faker\Generator as Faker;

$factory->define(App\Models\PreferenceOption::class, function (Faker $faker) {
    return [
        'preference_id' => function () {
            return factory(\App\Models\Preference::class)->create()->id;
        },
        'title' => $faker->text,
    ];
});
