<?php
namespace Helper;

use App\Models\Order as OrderModel;

class Order extends \Codeception\Module
{
    public function getShippingStatuses(): array
    {
        return [
            OrderModel::SHIPPING_STATUS_NEW,
            OrderModel::SHIPPING_STATUS_PICKUP,
            OrderModel::SHIPPING_STATUS_ON_ASSEMBLE,
            OrderModel::SHIPPING_STATUS_ASSEMBLED,
            OrderModel::SHIPPING_STATUS_IN_STORE,
            OrderModel::SHIPPING_STATUS_PACKED,
            OrderModel::SHIPPING_STATUS_SENT,
            OrderModel::SHIPPING_STATUS_WAITING_ON_DELIVERY_POINT,
            OrderModel::SHIPPING_STATUS_RETURNING_TO_WAREHOUSE,
            OrderModel::SHIPPING_STATUS_DELIVERED,
            OrderModel::SHIPPING_STATUS_RETURNED_TO_WAREHOUSE,
            OrderModel::SHIPPING_STATUS_RESENT,
            OrderModel::SHIPPING_STATUS_TO_DISBAND,
            OrderModel::SHIPPING_STATUS_DISBANDED,
            OrderModel::SHIPPING_STATUS_TO_RETURN,
            OrderModel::SHIPPING_STATUS_RETURNED,
            OrderModel::SHIPPING_STATUS_LOST,
            OrderModel::SHIPPING_STATUS_REMOVED,
            OrderModel::SHIPPING_STATUS_RECYCLED,
        ];
    }

    public function getPaymentTypes(): array
    {
        return [
            OrderModel::PAYMENT_TYPE_PAYPAL,
            OrderModel::PAYMENT_TYPE_YANDEX_MONEY,
            OrderModel::PAYMENT_TYPE_BANK_CARD,
            OrderModel::PAYMENT_TYPE_WEBMONEY,
            OrderModel::PAYMENT_TYPE_SBERBANK,
            OrderModel::PAYMENT_TYPE_ALFABANK,
            OrderModel::PAYMENT_TYPE_QIWI,
            OrderModel::PAYMENT_TYPE_APPLE_PAY,
            OrderModel::PAYMENT_TYPE_GOOGLE_PAY,
            OrderModel::PAYMENT_TYPE_CASH,
            OrderModel::PAYMENT_TYPE_PSB,
            OrderModel::PAYMENT_TYPE_MOBILE_BALANCE,
            OrderModel::PAYMENT_TYPE_INSTALLMENTS,
            OrderModel::PAYMENT_TYPE_FREE,
        ];
    }

    public function getStatuses(): array
    {
        return [
            OrderModel::STATUS_NEW,
            OrderModel::STATUS_WAITING_FOR_PAYMENT,
            OrderModel::STATUS_PROCESSING,
            OrderModel::STATUS_COMPLETED,
            OrderModel::STATUS_CANCELED,
            OrderModel::STATUS_RETURNED,
        ];
    }
}
