<?php
namespace Helper;

use App\Models\User;
use Illuminate\Support\Str;

class Dingo extends \Codeception\Module
{
    public function getRouteByName(string $name, array $params = []): string
    {
        $root = config('app.url');
        $scheme = Str::startsWith($root, 'http://') ? 'http' : 'https';
        $urlGenerator = app(\Dingo\Api\Routing\UrlGenerator::class);

        $urlGenerator->forceRootUrl($root);
        $urlGenerator->forceScheme($scheme);

        return $urlGenerator
            ->version('v1')
            ->route($name, $params);
    }

    public function loggedAs(User $user)
    {
        $auth = app(\Dingo\Api\Auth\Auth::class);
        $auth->setUser($user);
    }
}
