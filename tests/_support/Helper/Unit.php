<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use AspectMock\Test as test;

class Unit extends \Codeception\Module
{
    public function _beforeSuite($settings = array())
    {
        test::clean();
    }

    public function _afterSuite()
    {
        test::clean();
    }
}
