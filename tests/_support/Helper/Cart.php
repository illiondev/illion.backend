<?php
namespace Helper;

use App\Models\Product;
use App\Models\Redis\Cart as RedisCart;

class Cart extends \Codeception\Module
{
    public function setProduct(string $hash, Product $product)
    {
        $cart = new RedisCart($hash);
        $cart->set($product->id, 1);
    }

    public function setProducts(string $hash, int $amount = 1)
    {
        $cart = new RedisCart($hash);
        $products = factory(Product::class, $amount)->states('enabled', 'paperBook')->create()->all();

        foreach ($products as $product) {
            $cart->set($product->id, rand(1, 10));
        }
    }
}
