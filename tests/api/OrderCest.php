<?php

use App\Models\User;
use App\Models\Order;
use App\Services\OrderService;
use App\Services\ShiptorService;
use AspectMock\Test as test;
use Carbon\Carbon;
use Codeception\Util\HttpCode;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Redis;
use Faker\Factory;

class OrderCest
{
    /** @var \Helper\Dingo $dingo */
    protected $dingo;

    /** @var \Helper\Cart $cart */
    protected $cart;

    /** @var \App\Models\User $user */
    protected $user;

    protected $faker;

    protected $hash;

    protected function _inject(\Helper\Dingo $dingo, \Helper\Cart $cart)
    {
        $this->dingo = $dingo;
        $this->cart = $cart;
    }

    public function _before(ApiTester $I)
    {
        Redis::flushDB();
        Queue::fake();

        $this->user = factory(User::class)->create()->first();

        \Auth::setUser($this->user);
        $this->dingo->loggedAs($this->user);
        test::double(\Auth::class, ['getUser' => $this->user]);

        $this->faker = Factory::create();
        $this->hash = $this->faker->md5;
    }

    public function _after(ApiTester $I)
    {
        test::cleanInvocations();
    }

    // tests
    public function trySeeOrders(ApiTester $I)
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
        ])->first();
        $url = $this->dingo->getRouteByName('profile/orders');

        $I->sendGET($url);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'data' => 'array',
            'status' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'data' => [
                0 => [
                    'id' => $order->id,
                ],
            ],
            'status' => 'success'
        ]);

        Queue::assertNotPushed(\App\Jobs\YandexCheckPaymentJob::class);
    }

    public function trySeeWaitingOrders(ApiTester $I)
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'status' => Order::STATUS_WAITING_FOR_PAYMENT,
        ])->first();
        $url = $this->dingo->getRouteByName('profile/orders');

        $I->sendGET($url);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'data' => 'array',
            'status' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'data' => [
                0 => [
                    'id' => $order->id,
                ],
            ],
            'status' => 'success'
        ]);

        Queue::assertPushed(\App\Jobs\YandexCheckPaymentJob::class, 1);
    }

    public function tryGetConfirmationUrl(ApiTester $I)
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'status' => Order::STATUS_WAITING_FOR_PAYMENT,
        ])->first();
        $url = $this->dingo->getRouteByName('orders/confirmation_url', ['orderId' => $order->id]);
        $orderService = test::double(OrderService::class, [
            'getConfirmation' => ['id' => $order->id]
        ]);

        $I->sendPOST($url);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseContainsJson([
            'id' => $order->id,
            'status' => 'success'
        ]);

        $orderService->verifyInvokedOnce('getConfirmation');
    }

    public function tryCheckPayment(ApiTester $I)
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
        ])->first();
        $url = $this->dingo->getRouteByName('orders/payed', ['orderId' => $order->id]);
        $orderService = test::double(OrderService::class, [
            'checkPayment' => $order
        ]);

        $I->sendPOST($url);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseMatchesJsonType([
            'data' => 'array',
            'status' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'data' => [
                'id' => $order->id,
            ],
            'status' => 'success'
        ]);

        $orderService->verifyInvokedOnce('checkPayment');
    }

    public function trySubmit(ApiTester $I)
    {
        $url = $this->dingo->getRouteByName('orders/submit');
        $params = $this->getSubmitRequestParams();
        $amount = $this->faker->numberBetween(1, 5);
        $this->cart->setProducts($this->hash, $amount);
        $shiptorService = test::double(ShiptorService::class, [
            'calculateDimensions' => [
                'length' => $this->faker->numberBetween(20, 500),
                'width' => $this->faker->numberBetween(20, 500),
                'height' => $this->faker->numberBetween(5, 50),
                'weight' => $this->faker->numberBetween(100, 400),
            ],
            'calculateShipping' => $this->faker->numberBetween(10, 100),
            'send' => true,
        ]);

        $I->sendPOST($url, $params);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseMatchesJsonType([
            'data' => 'array',
            'status' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'status' => 'success'
        ]);

        $shiptorService->verifyInvokedOnce('calculateDimensions');
        $shiptorService->verifyInvokedOnce('calculateShipping');

        Queue::assertPushed(\App\Jobs\Sendpulse\SendPulseSendFail::class, 1);
    }

    public function trySubmitInvalidPaymentMethod(ApiTester $I)
    {
        $url = $this->dingo->getRouteByName('orders/submit');
        $params = $this->getSubmitRequestParams();
        $product = factory(\App\Models\Product::class)->states('enabled', 'eBook')->create()->first();
        $this->cart->setProduct($this->hash, $product);
        $order = test::double(Order::class, [
            'isPaymentOnline' => false,
        ]);

        $I->sendPOST($url, $params);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
        $I->seeResponseMatchesJsonType([
            'data' => 'array',
            'status' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'status' => 'fail',
            'message' => 'Invalid payment method',
        ]);

        $order->verifyInvokedOnce('isPaymentOnline');
    }

    public function trySubmitEmptyCart(ApiTester $I)
    {
        $url = $this->dingo->getRouteByName('orders/submit');
        $params = $this->getSubmitRequestParams();

        $I->sendPOST($url, $params);
        $I->seeResponseEmptyCart();
    }

    public function tryConfirm(ApiTester $I)
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
        ])->first();
        $url = $this->dingo->getRouteByName('orders/confirm', ['id' => $order->id]);
        $orderService = test::double(OrderService::class, [
            'process' => ['id' => $order->id]
        ]);

        $I->sendPOST($url);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseContainsJson([
            'id' => $order->id,
            'status' => 'success'
        ]);

        $orderService->verifyInvokedOnce('process');
    }

    public function tryConfirmFailed(ApiTester $I)
    {
        $expiredDate = (Carbon::now())->subMinutes(Order::LIFETIME_MINUTES);
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'created_at' => $expiredDate,
        ])->first();
        $url = $this->dingo->getRouteByName('orders/confirm', ['id' => $order->id]);
        $orderService = test::double(OrderService::class, [
            'process' => []
        ]);

        $I->sendPOST($url);
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);

        $orderService->verifyNeverInvoked('process');
    }

    public function tryConfirmAndroid(ApiTester $I)
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
        ])->first();
        $url = $this->dingo->getRouteByName('orders/confirm/android', ['id' => $order->id]);
        $orderService = test::double(OrderService::class, [
            'process' => ['id' => $order->id]
        ]);

        $I->sendPOST($url);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseContainsJson([
            'id' => $order->id,
            'status' => 'success'
        ]);

        $orderService->verifyInvokedOnce('process');
    }

    public function tryConfirmAndroidFailed(ApiTester $I)
    {
        $expiredDate = (Carbon::now())->subMinutes(Order::LIFETIME_MINUTES);
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'created_at' => $expiredDate,
        ])->first();
        $url = $this->dingo->getRouteByName('orders/confirm/android', ['id' => $order->id]);
        $orderService = test::double(OrderService::class, [
            'process' => []
        ]);

        $I->sendPOST($url);
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);

        $orderService->verifyNeverInvoked('process');
    }

    protected function getSubmitRequestParams()
    {
        return [
            'payment_method' => Order::PAYMENT_METHOD_ONLINE,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'hash' => $this->hash,
        ];
    }
}
