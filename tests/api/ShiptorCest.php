<?php

use App\Services\ShiptorService;
use AspectMock\Test as test;
use Codeception\Util\HttpCode;
use Faker\Factory;
use Illuminate\Support\Facades\Redis;

class ShiptorCest
{
    /** @var \Helper\Dingo $dingo */
    protected $dingo;

    /** @var \Helper\Cart $cart */
    protected $cart;

    protected $faker;

    protected $hash;

    protected function _inject(\Helper\Dingo $dingo, \Helper\Cart $cart)
    {
        $this->dingo = $dingo;
        $this->cart = $cart;
    }

    public function _before(ApiTester $I)
    {
        Redis::flushDB();
        test::clean();

        $this->faker = Factory::create();
        $this->hash = $this->faker->md5;
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function trySeeCountries(ApiTester $I)
    {
        $url = $this->dingo->getRouteByName('shiptor/countries');

        $I->sendGET($url);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'data' => 'array',
            'status' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'status' => 'success'
        ]);
    }

    public function tryGetExportShippingMethods(ApiTester $I)
    {
        $responseData = ['methods' => ['test1', 'test2']];
        test::double(ShiptorService::class, [
            'getExportShippingMethods' => $responseData
        ]);

        $url = $this->dingo->getRouteByName('orders/export/shipping_methods');
        $countryCode = $this->faker->randomElement(array_keys(ShiptorService::COUNTRIES));
        $amount = $this->faker->numberBetween(1, 5);

        $this->cart->setProducts($this->hash, $amount);

        $I->sendPOST($url, ['hash' => $this->hash, 'country_code' => $countryCode]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'data' => 'array',
            'status' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'data' => $responseData,
            'status' => 'success'
        ]);
    }

    public function tryGetExportShippingMethodsEmptyCart(ApiTester $I)
    {
        $url = $this->dingo->getRouteByName('orders/export/shipping_methods');
        $countryCode = $this->faker->randomElement(array_keys(ShiptorService::COUNTRIES));

        $I->sendPOST($url, ['hash' => $this->hash, 'country_code' => $countryCode]);
        $I->seeResponseEmptyCart();
    }

    public function tryCalculateShipping(ApiTester $I)
    {
        $price = $this->faker->randomFloat(2, 0, 1000000);
        test::double(ShiptorService::class, [
            'calculateShipping' => $price
        ]);

        $url = $this->dingo->getRouteByName('orders/calculate_shipping');
        $countryCode = $this->faker->randomElement(array_keys(ShiptorService::COUNTRIES));
        $shippingMethod = $this->faker->numberBetween(16, 20);
        $amount = $this->faker->numberBetween(1, 5);

        $this->cart->setProducts($this->hash, $amount);

        $I->sendPOST($url,
            ['hash' => $this->hash, 'country_code' => $countryCode, 'shipping_method' => $shippingMethod]
        );
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'delivery_price' => 'float',
            'status' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'delivery_price' => $price,
            'status' => 'success'
        ]);
    }

    public function tryCalculateShippingEmptyCart(ApiTester $I)
    {
        $url = $this->dingo->getRouteByName('orders/calculate_shipping');
        $countryCode = $this->faker->randomElement(array_keys(ShiptorService::COUNTRIES));
        $shippingMethod = $this->faker->numberBetween(16, 20);

        $I->sendPOST($url,
            ['hash' => $this->hash, 'country_code' => $countryCode, 'shipping_method' => $shippingMethod]
        );
        $I->seeResponseEmptyCart();
    }
}
