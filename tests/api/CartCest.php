<?php

use Codeception\Util\HttpCode;
use Faker\Factory;
use Illuminate\Support\Facades\Redis;

class CartCest
{
    /** @var \Helper\Dingo $dingo */
    protected $dingo;

    protected $faker;

    protected $hash;

    protected function _inject(\Helper\Dingo $dingo)
    {
        $this->dingo = $dingo;
    }

    public function _before(ApiTester $I)
    {
        Redis::flushDB();

        $this->faker = Factory::create();
        $this->hash = $this->faker->md5;
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function trySeeCart(ApiTester $I)
    {
        $url = $this->dingo->getRouteByName('cart');

        $I->sendGET($url, ['hash' => $this->hash]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'data' => 'array',
            'status' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'status' => 'success'
        ]);
    }

    public function trySetProduct(ApiTester $I)
    {
        $product = factory(\App\Models\Product::class)->states('enabled')->create()->first();
        $quantity = $this->faker->numberBetween(1, 10);
        $url = $this->dingo->getRouteByName('cart/set/bundle', [
            'productId' => $product->id,
            'quantity' => $quantity
        ]);

        $I->sendPOST($url, ['hash' => $this->hash]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'data' => 'array',
            'status' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'data' => [
                'bundle' => [
                    'data' => [
                        'product_id' => $product->id,
                        'quantity' => $quantity,
                    ]
                ],
            ],
            'status' => 'success'
        ]);
    }

    public function tryFlushCart(ApiTester $I)
    {
        $url = $this->dingo->getRouteByName('cart/flush');

        $I->sendPOST($url, ['hash' => $this->hash]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'data' => 'array',
            'status' => 'string'
        ]);
        $I->seeResponseContainsJson([
            'status' => 'success'
        ]);
    }
}
