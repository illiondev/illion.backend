<?php namespace Modules\Moderator\Repositories;

use Modules\Moderator\Models\Vacancy;

class VacancyRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'vacancies';

    /** @var VacancyRepository */
    protected $repository;

    protected function _before()
    {
        $this->repository = \App::make(VacancyRepository::class, [new Vacancy()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testFindOrFail()
    {
        $vacancy = factory(Vacancy::class)->create()->first();

        $foundVacancy = $this->repository->findOrFail($vacancy->id);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, ['id' => $vacancy->id]);
        $this->tester->assertEquals($vacancy->id, $foundVacancy->id);
    }

    public function testCreate()
    {
        $params = factory(Vacancy::class)->make()->toArray();
        $fillable = with(new Vacancy)->getFillable();
        $fillableParams = array_intersect_key($params, array_flip($fillable));

        $this->tester->seeNumRecords(0, $this->tableName);

        $createdVacancy = $this->repository->create($fillableParams);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, $fillableParams);
        $this->tester->assertEquals($fillableParams['title'], $createdVacancy->title);
    }

    public function testUpdate()
    {
        $vacancy = factory(Vacancy::class)->create()->first();
        $params = factory(Vacancy::class)->make()->toArray();

        $this->tester->seeRecord($this->tableName, ['id' => $vacancy->id]);

        $updatedVacancy = $this->repository->update($vacancy, $params);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->assertEquals($params['title'], $updatedVacancy->title);
    }

    public function testAll()
    {
        $count = rand(1, 10);
        factory(Vacancy::class, $count)->create();

        $vacancies = $this->repository->all();
        $recordsCount = $this->tester->grabNumRecords($this->tableName);

        $this->tester->assertCount($recordsCount, $vacancies);
    }
}
