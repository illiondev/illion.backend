<?php namespace Modules\Moderator\Repositories;

use Modules\Moderator\Models\Author;

class AuthorRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'authors';

    /** @var AuthorRepository */
    protected $repository;

    protected function _before()
    {
        $this->repository = \App::make(AuthorRepository::class, [new Author()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testFindByUserId()
    {
        $author = factory(Author::class)->create()->first();

        $foundAuthor = $this->repository->findByUserId($author->user_id);

        $this->tester->assertEquals($author, $foundAuthor);
    }

    public function testFindOrFail()
    {
        $author = factory(Author::class)->create()->first();

        $foundAuthor = $this->repository->findOrFail($author->id);

        $this->tester->assertEquals($author, $foundAuthor);
    }

    public function testAllPaginate()
    {
        $perPage = rand(1, 10);
        $num = rand(1, 10);
        factory(Author::class, $num)->create();

        /** @var \Illuminate\Contracts\Pagination\LengthAwarePaginator $paginatedData */
        $paginatedData = $this->repository->allPaginate($perPage);
        $this->tester->assertEquals($perPage, $paginatedData->perPage());
        $this->tester->assertEquals($num, $paginatedData->total());
    }

    public function testCreate()
    {
        $params = factory(Author::class)->make()->toArray();
        $fillable = with(new Author)->getFillable();
        $fillableParams = array_intersect_key($params, array_flip($fillable));

        $this->tester->seeNumRecords(0, $this->tableName);

        $createdAuthor = $this->repository->create($fillableParams);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, $fillableParams);
        $this->tester->assertEquals($params['name'], $createdAuthor->name);
        $this->tester->assertEquals($params['surname'], $createdAuthor->surname);
    }

    public function testUpdate()
    {
        $author = factory(Author::class)->create()->first();
        $params = factory(Author::class)->make()->toArray();

        $this->tester->seeRecord($this->tableName, ['id' => $author->id]);

        $updatedAnnouncement = $this->repository->update($author, $params);

        $this->tester->seeNumRecords(1, $this->tableName);

        $this->tester->assertEquals($params['name'], $updatedAnnouncement->name);
        $this->tester->assertEquals($params['surname'], $updatedAnnouncement->surname);
    }

    public function testAutocomplete()
    {
        $limit = rand(1, 5);
        factory(Author::class)->create(['name' => 'Eric', 'surname' => 'Blair']);
        factory(Author::class)->create(['name' => 'Edgar', 'surname' => 'Poe']);
        factory(Author::class)->create(['name' => 'Randy', 'surname' => 'White']);
        factory(Author::class)->create(['name' => 'Ayn', 'surname' => 'Rand']);
        factory(Author::class)->create(['name' => 'Oscar', 'surname' => 'Wilde']);

        $query = 'Rand';
        $result = $this->repository->autocomplete($query, $limit);

        $found = $result->count() >= 1;
        $this->tester->assertTrue($found);

        $query = 'William';
        $result = $this->repository->autocomplete($query, $limit);
        $notFound = $result->count() === 0;
        $this->tester->assertTrue($notFound);
    }
}