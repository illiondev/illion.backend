<?php namespace Modules\Moderator\Repositories;

use Modules\Moderator\Models\Order;

class OrderRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'orders';

    /** @var OrderRepository */
    protected $repository;
    
    protected function _before()
    {
        $this->repository = \App::make(OrderRepository::class, [new Order()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testFindOrFail()
    {
        $order = factory(Order::class)->create()->first();

        $foundOrder = $this->repository->findOrFail($order->id);

        $this->tester->assertEquals($order, $foundOrder);
    }

    public function testAllPaginate()
    {
        $perPage = rand(1, 10);
        $num = rand(1, 10);
        $params = [];
        factory(Order::class, $num)->create();

        /** @var \Illuminate\Contracts\Pagination\LengthAwarePaginator $paginatedData */
        $paginatedData = $this->repository->allPaginate($params, $perPage);
        $this->tester->assertEquals($perPage, $paginatedData->perPage());
        $this->tester->assertEquals($num, $paginatedData->total());
    }
}