<?php namespace Modules\Moderator\Repositories;

use Carbon\Carbon;
use Modules\Moderator\Models\Payout;

class PayoutRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'payouts';

    /** @var PayoutRepository */
    protected $repository;

    protected function _before()
    {
        $this->repository = \App::make(PayoutRepository::class, [new Payout()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    public function testFindOrFail()
    {
        $payout = factory(Payout::class)->create()->first();

        $foundPayout = $this->repository->findOrFail($payout->id);

        $this->tester->assertEquals($payout, $foundPayout);
    }

    public function testAllPaginate()
    {
        $perPage = rand(1, 10);
        $num = rand(1, 10);
        factory(Payout::class, $num)->create();

        /** @var \Illuminate\Contracts\Pagination\LengthAwarePaginator $paginatedData */
        $paginatedData = $this->repository->allPaginate($perPage);
        $this->tester->assertEquals($perPage, $paginatedData->perPage());
        $this->tester->assertEquals($num, $paginatedData->total());
    }

    public function testCreate()
    {
        $params = factory(Payout::class)->make()->toArray();
        $params['payout_date'] = Carbon::now()->toDateString();

        $createdPayout = $this->repository->create($params['author_id'], $params['moderator_id'],
            $params['amount'], $params['payout_date']);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, $params);
        $this->tester->assertEquals($params['amount'], $createdPayout->amount);
        $this->tester->assertEquals($params['author_id'], $createdPayout->author_id);
    }

    public function testUpdate()
    {
        $payout = factory(Payout::class)->create()->first();
        $params = factory(Payout::class)->make()->toArray();
        $params['payout_date'] = Carbon::now()->toDateString();

        $this->tester->seeRecord($this->tableName, ['id' => $payout->id]);

        $updatedPayout = $this->repository->update($payout, $params['author_id'], $params['moderator_id'],
            $params['amount'], $params['payout_date']);

        $this->tester->seeNumRecords(1, $this->tableName);

        $this->tester->assertEquals($params['author_id'], $updatedPayout->author_id);
        $this->tester->assertEquals($params['moderator_id'], $updatedPayout->moderator_id);
        $this->tester->assertEquals($params['amount'], $updatedPayout->amount);
    }
}
