<?php namespace Modules\Moderator\Repositories;

use Modules\Moderator\Models\Announcement;

class AnnouncementRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'announcements';

    /** @var AnnouncementRepository */
    protected $repository;

    protected function _before()
    {
        $this->repository = \App::make(AnnouncementRepository::class, [new Announcement()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testFirstOrNewFound()
    {
        $announcement = factory(Announcement::class)->create()->first();

        $foundAnnouncement = $this->repository->firstOrNew();

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, ['id' => $announcement->id]);
        $this->tester->assertEquals($announcement->id, $foundAnnouncement->id);
    }

    public function testFirstOrNewCreated()
    {
        $createdAnnouncement = $this->repository->firstOrNew();

        $this->tester->assertNull($createdAnnouncement->id);
        $this->tester->seeNumRecords(0, $this->tableName);
    }

    public function testFirstOrFail()
    {
        $announcement = factory(Announcement::class)->create()->first();

        $foundAnnouncement = $this->repository->firstOrFail();

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, ['id' => $announcement->id]);
        $this->tester->assertEquals($announcement->id, $foundAnnouncement->id);
    }

    public function testCreate()
    {
        $params = factory(Announcement::class)->make()->toArray();
        $fillable = with(new Announcement)->getFillable();
        $fillableParams = array_intersect_key($params, array_flip($fillable));

        $this->tester->seeNumRecords(0, $this->tableName);

        $createdAnnouncement = $this->repository->create($fillableParams);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, $fillableParams);
        $this->tester->assertEquals($fillableParams['content'], $createdAnnouncement->content);
    }

    public function testUpdate()
    {
        $announcement = factory(Announcement::class)->create()->first();
        $params = factory(Announcement::class)->make()->toArray();

        $this->tester->seeRecord($this->tableName, ['id' => $announcement->id]);

        $updatedAnnouncement = $this->repository->update($announcement, $params);

        $this->tester->seeNumRecords(1, $this->tableName);

        $this->tester->assertEquals($params['content'], $updatedAnnouncement->content);
    }
}