<?php namespace Modules\Moderator\Repositories;

use Modules\Moderator\Models\Soon;

class SoonRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'soon';

    /** @var SoonRepository */
    protected $repository;

    protected function _before()
    {
        $this->repository = \App::make(SoonRepository::class, [new Soon()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testFindOrFail()
    {
        $soon = factory(Soon::class)->create()->first();

        $foundSoon = $this->repository->findOrFail($soon->id);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, ['id' => $soon->id]);
        $this->tester->assertEquals($soon->id, $foundSoon->id);
    }

    public function testCreate()
    {
        $params = factory(Soon::class)->make()->toArray();
        $fillable = with(new Soon)->getFillable();
        $fillableParams = array_intersect_key($params, array_flip($fillable));

        $this->tester->seeNumRecords(0, $this->tableName);

        $createdSoon = $this->repository->create($fillableParams);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, $fillableParams);
        $this->tester->assertEquals($fillableParams['title'], $createdSoon->title);
    }

    public function testUpdate()
    {
        $soon = factory(Soon::class)->create()->first();
        $params = factory(Soon::class)->make()->toArray();

        $this->tester->seeRecord($this->tableName, ['id' => $soon->id]);

        $updatedSoon = $this->repository->update($soon, $params);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->assertEquals($params['title'], $updatedSoon->title);
    }

    public function testAll()
    {
        $count = rand(1, 10);
        factory(Soon::class, $count)->create();

        $all = $this->repository->all();
        $recordsCount = $this->tester->grabNumRecords($this->tableName);

        $this->tester->assertCount($recordsCount, $all);
    }
}
