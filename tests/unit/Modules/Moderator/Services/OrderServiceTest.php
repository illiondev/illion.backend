<?php namespace Modules\Moderator\Services;

use App\Models\OrderBill;
use App\Models\User;
use Faker\Factory;
use Modules\Moderator\Models\Order;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class OrderServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $faker;

    /** @var OrderService $service */
    protected $service;

    /** @var User $user */
    protected $user;

    protected function _before()
    {
        $this->faker = Factory::create();
        $this->service = \App::make(OrderService::class);
        $this->user = factory(User::class)->create()->first();
    }

    protected function _after()
    {
        $this->service = null;
    }

    // tests
    public function testUpdate()
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
        ])->first();
        $params = [
            'name' => $this->faker->firstName,
            'surname' => $this->faker->lastName,
        ];

        $updatedOrder = $this->service->update($order, $params);

        $this->tester->assertEquals($updatedOrder->id, $order->id);
        $this->tester->assertEquals($updatedOrder->name, $params['name']);
        $this->tester->assertNotEquals($updatedOrder->name, $order->name);
        $this->tester->assertEquals($updatedOrder->surname, $params['surname']);
        $this->tester->assertNotEquals($updatedOrder->surname, $order->surname);
    }

    public function testUpdateShippedOrder()
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'shiptor_id' => $this->faker->randomNumber(2, true),
        ])->first();
        $params = [];

        $this->tester->expectThrowable(BadRequestHttpException::class, function() use ($order, $params) {
            $this->service->update($order, $params);
        });
    }

    public function testPay()
    {
        $statuses = [Order::STATUS_CANCELED, Order::STATUS_WAITING_FOR_PAYMENT];
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'status' => $this->faker->randomElement($statuses),
        ])->first();

        $paidOrder = $this->service->pay($order, $this->user);

        $this->tester->assertEquals($paidOrder->status, Order::STATUS_PROCESSING);
    }

    public function testPayFailedByStatus()
    {
        $wrongStatuses = [
            Order::STATUS_NEW,
            Order::STATUS_COMPLETED,
            Order::STATUS_PROCESSING,
            Order::STATUS_RETURNED,
        ];
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'status' => $this->faker->randomElement($wrongStatuses),
        ])->first();

        $this->tester->expectThrowable(BadRequestHttpException::class, function() use ($order) {
            $this->service->pay($order, $this->user);
        });
    }

    public function testCreateBill()
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
        ])->first();
        $params = [
            'price' => $this->faker->randomNumber(2, true),
            'description' => $this->faker->text,
        ];

        $orderBill = $this->service->createBill($order, $params, $this->user);

        $this->tester->assertInstanceOf(\Modules\Moderator\Models\OrderBill::class, $orderBill);
        $this->tester->assertEquals($orderBill->order_id, $order->id);
        $this->tester->assertEquals($orderBill->user_id, $this->user->id);
        $this->tester->assertEquals($orderBill->status, OrderBill::PAYMENT_STATUS_NEW);
    }
}
