<?php namespace Modules\Moderator\Services;

use App\Models\User;
use AspectMock\Test as test;
use Faker\Factory;
use Illuminate\Support\Facades\Queue;
use Modules\Moderator\Jobs\BookGenerateJob;
use Modules\Moderator\Models\BookChapter;
use Modules\Moderator\Models\Product;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $faker;

    /** @var ProductService $service */
    protected $service;

    /** @var User $user */
    protected $user;

    protected function _before()
    {
        Queue::fake();
        $this->faker = Factory::create();
        $this->service = \App::make(ProductService::class);
        $this->user = factory(User::class)->create()->first();
    }

    protected function _after()
    {
        $this->service = null;
        test::cleanInvocations();
    }

    // tests
    public function testPublish()
    {
        $numChapters = $this->faker->numberBetween(1, 9);
        $product = factory(Product::class)->states('enabled', 'eBook')->create()->first();
        factory(BookChapter::class, $numChapters)->state('completed')->create([
            'book_id' => $product->book_id,
        ]);

        $this->service->publish($product);

        Queue::assertPushed(BookGenerateJob::class, 1);
    }

    public function testPublishUnfinishedChaptersFailed()
    {
        $numChapters = $this->faker->numberBetween(1, 9);
        $product = factory(Product::class)->states('enabled', 'eBook')->create()->first();
        factory(BookChapter::class, $numChapters)->create([
            'book_id' => $product->book_id,
        ]);

        $this->tester->expectThrowable(
            new BadRequestHttpException('Book contains unfinished chapters'),
            function () use ($product) {
                $this->service->publish($product);
            });

        Queue::assertNotPushed(BookGenerateJob::class);
    }

    public function testPublishWithoutChaptersFailed()
    {
        $product = factory(Product::class)->states('enabled', 'eBook')->create()->first();

        $this->tester->expectThrowable(
            new BadRequestHttpException('Book do not have any chapters'),
            function () use ($product) {
                $this->service->publish($product);
            });

        Queue::assertNotPushed(BookGenerateJob::class);
    }
}
