<?php

require_once __DIR__ . '/../../vendor/autoload.php'; // composer autoload

$kernel = \AspectMock\Kernel::getInstance();
$kernel->init([
    'debug' => true,
    'appDir' => __DIR__ . '/../../app',
    'cacheDir' => __DIR__ . '/../_data/cache',
    'includePaths' => [
        __DIR__ . '/../../vendor/laravel',
        __DIR__ . '/../../app',
    ],
    'excludePaths' => [__DIR__ . '/../'], // tests dir should be excluded
]);
