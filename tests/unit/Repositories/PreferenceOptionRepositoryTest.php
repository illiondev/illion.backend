<?php namespace Repositories;

use App\Models\PreferenceOption;
use App\Repositories\PreferenceOptionRepository;

class PreferenceOptionRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'preference_options';

    protected $repository = null;
    
    protected function _before()
    {
        $this->repository = \App::make(PreferenceOptionRepository::class, [new PreferenceOption()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testFind()
    {
        $option = factory(PreferenceOption::class)->create()->first();

        $foundOption = $this->repository->find($option->preference_id, $option->id);

        $this->tester->assertEquals($option, $foundOption);
    }

    public function testFindFailed()
    {
        $optionId = rand(100, 200);
        $optionPreferenceId = rand(100, 200);

        $foundOption = $this->repository->find($optionPreferenceId, $optionId);

        $this->tester->dontSeeRecord($this->tableName, [
            'id' => $optionId,
            'preference_id' => $optionPreferenceId,
        ]);
        $this->tester->assertNull($foundOption);
    }
}