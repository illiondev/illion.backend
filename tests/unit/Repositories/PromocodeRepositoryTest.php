<?php namespace Repositories;

use App\Models\Promocode;
use App\Repositories\PromocodeRepository;

class PromocodeRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'promocodes';

    protected $repository = null;

    protected function _before()
    {
        $this->repository = \App::make(PromocodeRepository::class, [new Promocode()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testFirst()
    {
        $promocode = factory(Promocode::class)->create()->first();

        $foundPromocode = $this->repository->first($promocode->code);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, ['id' => $promocode->id]);
        $this->tester->assertEquals($promocode->id, $foundPromocode->id);
    }
}
