<?php namespace Repositories;

use App\Models\OrderBill;
use App\Repositories\OrderBillRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrderBillRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'orders_bills';

    protected $repository = null;

    protected function _before()
    {
        $this->repository = \App::make(OrderBillRepository::class, [new OrderBill()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testFind()
    {
        $orderBill = factory(OrderBill::class)->create()->first();

        $foundOrderBill = $this->repository->findOrFail($orderBill->hash);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, ['hash' => $orderBill->hash]);
        $this->tester->assertEquals($orderBill->id, $foundOrderBill->id);
    }

    public function testFindFailed()
    {
        $orderBill = factory(OrderBill::class)->create()->first();
        $wrongHash = md5($orderBill->hash);

        $this->tester->expectThrowable(ModelNotFoundException::class, function() use ($wrongHash) {
            $this->repository->findOrFail($wrongHash);
        });
    }

    protected function testFindWithStatus(int $status, string $methodName)
    {
        $orderBill = factory(OrderBill::class)->create([
            'status' => $status,
        ])->first();

        $foundOrderBill = $this->repository->$methodName($orderBill->hash);

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, ['hash' => $orderBill->hash]);
        $this->tester->assertEquals($orderBill->id, $foundOrderBill->id);
    }

    public function testFindNew()
    {
        $this->testFindWithStatus(OrderBill::PAYMENT_STATUS_NEW, 'findOrFailNew');
    }

    public function testFindWaiting()
    {
        $this->testFindWithStatus(OrderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT, 'findOrFailWaitingForPayment');
    }
}
