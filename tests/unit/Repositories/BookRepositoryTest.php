<?php namespace Repositories;

use App\Models\Book;
use App\Models\Product;
use App\Repositories\BookRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class BookRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $repository = null;

    protected function _before()
    {
        Redis::flushDB();

        $this->repository = \App::make(BookRepository::class, [new Book()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testGetNew()
    {
        $date = Carbon::now()->subSecond();
        $book = factory(Book::class)->create()->first();
        factory(Product::class)
            ->states('enabled')
            ->create([
                'book_id' => $book->id
            ])->first();

        $foundBooks = $this->repository->getNew($date);
        $foundBookIds = $foundBooks->pluck('id')->all();

        $this->tester->assertTrue(\in_array($book->id, $foundBookIds));
    }

    public function testFindOrFail()
    {
        $book = factory(Book::class)->create()->first();

        factory(Product::class)
            ->states('enabled')
            ->create([
                'book_id' => $book->id
            ])->first();

        $foundBook = $this->repository->findOrFail($book->id);

        $this->tester->assertEquals($book->id, $foundBook->id);
    }

    public function testFindFromHash()
    {
        $book = factory(Book::class)->create()->first();
        $hash = Str::random(32);

        Redis::set(Book::BUNDLE_PREVIEW_KEY_ . $hash, $book->id);

        $foundBook = $this->repository->findFromHash($hash);

        $this->tester->assertEquals($book->id, $foundBook->id);
    }

    public function testFindFromHashFailed()
    {
        $hash = Str::random(32);

        $this->tester->expectThrowable(ModelNotFoundException::class, function() use ($hash) {
            $this->repository->findFromHash($hash);
        });
    }
}
