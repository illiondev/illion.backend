<?php namespace Repositories;

use App\Models\Vacancy;
use App\Repositories\VacancyRepository;

class VacancyRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'vacancies';

    protected $repository = null;

    protected function _before()
    {
        $this->repository = \App::make(VacancyRepository::class, [new Vacancy()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testFindOrFail()
    {
        $vacancy = factory(Vacancy::class)->create()->first();

        $foundVacancy = $this->repository->findOrFail($vacancy->id);

        $this->tester->assertEquals($vacancy->id, $foundVacancy->id);
    }

    public function testAll()
    {
        $numEnabled = rand(1, 5);
        $numDisabled = rand(1, 5);
        $enabledVacancies = factory(Vacancy::class, $numEnabled)->create(['enabled' => true])->all();
        factory(Vacancy::class, $numDisabled)->create(['enabled' => false])->all();

        $foundVacancies = $this->repository->all();

        $this->tester->seeNumRecords($numEnabled + $numDisabled, $this->tableName);
        $this->tester->assertCount(count($enabledVacancies), $foundVacancies);
        $this->tester->assertEquals(array_get($enabledVacancies, 'id'), array_get($foundVacancies->all(), 'id'));
    }
}
