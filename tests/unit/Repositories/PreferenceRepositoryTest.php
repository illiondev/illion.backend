<?php namespace Repositories;

use App\Models\Preference;
use App\Repositories\PreferenceRepository;

class PreferenceRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'preferences';

    protected $repository = null;
    
    protected function _before()
    {
        $this->repository = \App::make(PreferenceRepository::class, [new Preference()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testAll()
    {
        $count = $this->tester->grabNumRecords($this->tableName);
        $preferences = $this->repository->all();

        $this->tester->assertCount($count, $preferences);
    }

    public function testAllWithCreated()
    {
        $count = rand(1, 10);
        factory(Preference::class, $count)->create();

        $preferences = $this->repository->all();
        $recordsCount = $this->tester->grabNumRecords($this->tableName);

        $this->tester->assertCount($recordsCount, $preferences);
    }
}