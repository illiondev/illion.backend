<?php namespace Repositories;

use App\Models\Soon;
use App\Repositories\SoonRepository;

class SoonRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'soon';

    protected $repository = null;

    protected function _before()
    {
        $this->repository = \App::make(SoonRepository::class, [new Soon()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testAll()
    {
        $numEnabled = rand(1, 5);
        $numDisabled = rand(1, 5);
        $enabledSoon = factory(Soon::class, $numEnabled)->create(['enabled' => true])->all();
        factory(Soon::class, $numDisabled)->create(['enabled' => false])->all();

        $foundVacancies = $this->repository->all();

        $this->tester->seeNumRecords($numEnabled + $numDisabled, $this->tableName);
        $this->tester->assertCount(count($enabledSoon), $foundVacancies);
        $this->tester->assertEquals(array_get($enabledSoon, 'id'), array_get($foundVacancies->all(), 'id'));
    }
}
