<?php namespace Repositories;

use App\Models\Announcement;
use App\Repositories\AnnouncementRepository;

class AnnouncementRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'announcements';

    protected $repository = null;
    
    protected function _before()
    {
        $this->repository = \App::make(AnnouncementRepository::class, [new Announcement()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testFirstOrNewFound()
    {
        $announcement = factory(Announcement::class)->create()->first();

        $foundAnnouncement = $this->repository->firstOrNew();

        $this->tester->seeNumRecords(1, $this->tableName);
        $this->tester->seeRecord($this->tableName, ['id' => $announcement->id]);
        $this->tester->assertEquals($announcement->id, $foundAnnouncement->id);
    }

    public function testFirstOrNewCreated()
    {
        $createdAnnouncement = $this->repository->firstOrNew();

        $this->tester->assertNull($createdAnnouncement->id);
        $this->tester->seeNumRecords(0, $this->tableName);
    }
}