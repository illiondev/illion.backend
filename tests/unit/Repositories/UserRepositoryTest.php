<?php namespace Repositories;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $tableName = 'users';

    protected $repository = null;

    protected function _before()
    {
        $this->repository = \App::make(UserRepository::class, [new User()]);
    }

    protected function _after()
    {
        $this->repository = null;
    }

    // tests
    public function testFind()
    {
        $user = factory(User::class)->create()->first();
        $userId = $user->id;

        $foundUser = $this->repository->findOrFail($userId);

        $this->tester->assertEquals($userId, $foundUser->id);
        $this->tester->assertEquals($user->email, $foundUser->email);
        $this->tester->seeRecord($this->tableName, ['id' => $userId]);
    }

    public function testFindFailed()
    {
        $user = factory(User::class)->create()->first();
        $wrongUserId = $user->id + 1;

        $this->tester->expectThrowable(ModelNotFoundException::class, function() use ($wrongUserId) {
            $this->repository->findOrFail($wrongUserId);
        });
    }

    public function testFindByExternalId()
    {
        $user = factory(User::class)->create()->first();
        $externalId = $user->external_id;

        $foundUser = $this->repository->findByExternalId($externalId);

        $this->tester->assertEquals($externalId, $foundUser->external_id);
        $this->tester->assertEquals($user->email, $foundUser->email);
        $this->tester->seeRecord($this->tableName, ['external_id' => $externalId]);
    }

    public function testFindByExternalIdFailed()
    {
        $user = factory(User::class)->create()->first();
        $externalId = $user->external_id + 1;

        $foundUser = $this->repository->findByExternalId($externalId);

        $this->tester->assertNull($foundUser);
        $this->tester->dontSeeRecord($this->tableName, ['external_id' => $externalId]);
    }
}
