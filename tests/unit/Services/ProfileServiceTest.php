<?php namespace Services;

use App\Models\Profile;
use App\Models\User;
use App\Services\ProfileService;
use Carbon\Carbon;
use Faker\Factory;
use Illion\Service\Models\User as IllionUser;

class ProfileServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $faker;

    /** @var ProfileService $service */
    protected $service;

    /** @var Profile $profile */
    protected $profile;

    protected function _before()
    {
        $this->faker = Factory::create();
        $this->service = \App::make(ProfileService::class);
        $this->profile = factory(Profile::class)->create()->first();
    }

    protected function _after()
    {
        $this->service = null;
    }

    // tests
    public function testUpdate()
    {
        $profileData = factory(Profile::class)->make()->first();

        $params = [
            'name' => $profileData->name,
            'gender' => $profileData->gender,
            'birthday' => $profileData->birthday,
        ];

        $this->service->update($this->profile, $params, null);
        $this->profile = $this->profile->fresh();

        $this->tester->assertEquals($this->profile->name, $params['name']);
        $this->tester->assertEquals($this->profile->gender, $params['gender']);
        $this->tester->assertEquals($this->profile->birthday, $params['birthday']);
    }

    public function testUpdateWithUser()
    {
        $profileData = factory(Profile::class)->make()->first();

        $params = [
            'name' => $profileData->name,
            'gender' => $profileData->gender,
            'birthday' => $profileData->birthday,
        ];
        $user = factory(User::class)->create()->first();
        $illionUser = new IllionUser([
            'id' => $user->external_id,
            'socialNetworkAccounts' => [],
            'name' => $this->faker->firstName,
            'gender' => $this->faker->randomElement([
                Profile::GENDER_FEMALE,
                Profile::GENDER_MALE,
                Profile::GENDER_UNKNOWN,
            ]),
            'birthday' => Carbon::parse($this->faker->date),
        ]);

        $this->service->update($this->profile, $params, $illionUser);
        $this->profile = $this->profile->fresh();

        $this->tester->assertEquals($this->profile->name, $illionUser->name);
        $this->tester->assertEquals($this->profile->gender, $illionUser->gender);
        $this->tester->assertEquals($this->profile->birthday, $illionUser->birthday);
    }
}
