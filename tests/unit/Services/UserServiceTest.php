<?php namespace Services;

use App\Models\User;
use App\Services\UserService;
use Faker\Factory;
use Illion\Service\Models\User as IllionUser;

class UserServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $faker;

    /** @var UserService $service */
    protected $service;

    /** @var User $user */
    protected $user;

    protected function _before()
    {
        $this->faker = Factory::create();
        $this->service = \App::make(UserService::class);
        $this->user = factory(User::class)->create()->first();
    }

    protected function _after()
    {
        $this->service = null;
    }

    // tests
    public function testUpdate()
    {
        $params = [
            'email' => $this->faker->unique()->email,
            'subscription_email' => $this->faker->boolean,
        ];
        $user = factory(User::class)->create()->first();

        $this->service->update($user, $params, null);
        $user = $user->fresh();

        $this->tester->assertEquals($user->email, $params['email']);
        $this->tester->assertEquals($user->subscription_email, $params['subscription_email']);
    }

    public function testUpdateWithUser()
    {
        $params = [
            'email' => $this->faker->unique()->email,
            'subscription_email' => $this->faker->boolean,
        ];
        $user = factory(User::class)->create()->first();
        $illionUser = new IllionUser([
            'id' => $user->external_id,
            'email' => $this->faker->unique()->email,
            'email_confirmed_at' => date('Y-m-d H:i:s', $this->faker->unixTime),
            'socialNetworkAccounts' => [],
        ]);

        $this->service->update($user, $params, $illionUser);
        $user = $user->fresh();

        $this->tester->assertEquals($user->email, $illionUser->email);
        $this->tester->assertEquals(date('Y-m-d H:i:s', $user->email_confirmed_at), $illionUser->email_confirmed_at);
        $this->tester->assertEquals($user->subscription_email, $params['subscription_email']);
    }

    public function testCreate()
    {
        $id = $this->faker->unique()->randomNumber(3, true);
        $name = $this->faker->firstName;
        $email = $this->faker->unique()->email;

        $user = $this->service->create($name, $email, $id);

        $this->tester->assertEquals($user->external_id, $id);
        $this->tester->assertEquals($user->email, $email);
    }
}
