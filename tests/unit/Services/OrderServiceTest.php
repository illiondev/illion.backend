<?php namespace Services;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use App\Services\OrderService;
use App\Services\ShiptorService;
use App\Services\YandexMoneyService;
use AspectMock\Test as test;
use Illuminate\Support\Facades\Queue;
use Faker\Factory;
use YandexCheckout\Model\PaymentMethod\PaymentMethodCash;
use YandexCheckout\Request\Payments\PaymentResponse;

class OrderServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $faker;

    /** @var OrderService $service */
    protected $service;

    /** @var User $user */
    protected $user;

    /** @var \Helper\Order $order */
    protected $order;

    protected function _inject(\Helper\Order $order)
    {
        $this->order = $order;
    }

    protected function _before()
    {
        Queue::fake();
        $this->faker = Factory::create();
        $this->service = \App::make(OrderService::class);
        $this->user = factory(User::class)->create()->first();
    }

    protected function _after()
    {
        $this->service = null;
        test::cleanInvocations();
    }

    // tests
    public function testBought()
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
        ])->first();
        $numItems = $this->faker->numberBetween(1, 10);
        factory(OrderItem::class, $numItems)->create([
            'order_id' => $order->id,
            'type' => Product::PRODUCT_TYPE_PAPER_BOOK,
        ])->all();
        $order = $order->fresh();
        $paymentType = $this->faker->randomElement($this->order->getPaymentTypes());

        $this->service->bought($order, $paymentType);

        $this->tester->assertEquals($order->status, Order::STATUS_PROCESSING);
        $this->tester->assertEquals($order->payment_type, $paymentType);

        Queue::assertPushed(\App\Jobs\ShiptorSendJob::class, 1);
        Queue::assertPushed(\App\Jobs\Sendpulse\SendPulseSendSuccess::class, 1);
    }

    public function testSendPaperBooks()
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
        ])->first();
        $numItems = $this->faker->numberBetween(1, 10);
        factory(OrderItem::class, $numItems)->create([
            'order_id' => $order->id,
            'type' => Product::PRODUCT_TYPE_PAPER_BOOK,
        ])->all();
        $order = $order->fresh();
        $shiptorService = test::double(ShiptorService::class, [
            'send' => null,
        ]);

        $this->service->send($order);

        $shiptorService->verifyInvokedOnce('send');
    }

    public function testSendEbooks()
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
        ])->first();

        $this->service->send($order);

        $this->tester->assertEquals($order->status, Order::STATUS_COMPLETED);
    }

    protected function testCheckPaymentStatus(string $status)
    {
        $response = \Codeception\Stub::make(PaymentResponse::class, [
            'getStatus' => $status,
            'getPaymentMethod' => \Codeception\Stub::make(PaymentMethodCash::class, ['getType' => 'test']),
        ]);
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'status' => Order::STATUS_WAITING_FOR_PAYMENT,
            'payment_method' => Order::PAYMENT_METHOD_CASH,
            'payment_id' => $this->faker->text,
        ])->first();
        $yandexMoneyService = test::double(YandexMoneyService::class, [
            'getPaymentInfo' => $response,
            'capturePayment' => true,
        ]);
        $orderService = test::double($this->service, [
            'bought' => null,
        ]);

        $this->service->checkPayment($order);

        $yandexMoneyService->verifyInvokedOnce('getPaymentInfo');
        $orderService->verifyInvokedOnce('bought');
    }

    protected function testCheckPaymentCanceled()
    {
        $response = \Codeception\Stub::make(PaymentResponse::class, [
            'getStatus' => YandexMoneyService::STATUS_CANCELED,
        ]);
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'status' => Order::STATUS_WAITING_FOR_PAYMENT,
            'payment_id' => $this->faker->text,
        ])->first();
        $yandexMoneyService = test::double(YandexMoneyService::class, [
            'getPaymentInfo' => $response,
        ]);

        $order = $this->service->checkPayment($order);

        $this->tester->assertEquals($order->status, Order::STATUS_CANCELED);

        $yandexMoneyService->verifyInvokedOnce('getPaymentInfo');
    }

    public function testCheckPayment()
    {
        $this->testCheckPaymentStatus(YandexMoneyService::STATUS_WAITING_FOR_CAPTURE);
        test::cleanInvocations();
        $this->testCheckPaymentStatus(YandexMoneyService::STATUS_SUCCEEDED);
        test::cleanInvocations();
        $this->testCheckPaymentCanceled();
    }

    public function testProcess()
    {
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'status' => Order::STATUS_NEW,
            'payment_method' => $this->faker->randomElement([
                Order::PAYMENT_METHOD_CASH,
                Order::PAYMENT_METHOD_CARD,
            ]),
        ])->first();

        $result = $this->service->process($order);
        $order = $order->fresh();

        $this->tester->assertEquals($order->status, Order::STATUS_PROCESSING);
        $this->tester->assertEquals($result, []);
    }

    protected function testProcessPayment(int $paymentMethod, string $methodName)
    {
        $response = [
            'id' => $this->faker->word,
            'confirmation_url' => $this->faker->url,
            'message' => 'test',
        ];
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'status' => Order::STATUS_NEW,
            'payment_method' => $paymentMethod,
        ])->first();
        $yandexMoneyService = test::double(YandexMoneyService::class, [
            $methodName => $response,
        ]);

        $result = $this->service->process($order);
        $order = $order->fresh();

        $this->tester->assertEquals($order->status, Order::STATUS_WAITING_FOR_PAYMENT);
        $this->tester->assertEquals($result['confirmation_url'], $response['confirmation_url']);

        $yandexMoneyService->verifyInvokedOnce($methodName);
    }

    public function testProcessOnlinePayment()
    {
        $this->testProcessPayment(Order::PAYMENT_METHOD_ONLINE, 'createPaymentFromOrder');
    }

    public function testProcessGooglePlayPayment()
    {
        $this->testProcessPayment(Order::PAYMENT_METHOD_GOOGLE_PLAY, 'createPaymentFromToken');
    }

    public function testGetConfirmation()
    {
        $response = [
            'id' => $this->faker->word,
            'confirmation_url' => $this->faker->url,
            'message' => 'test',
        ];
        $paymentInfo = \Codeception\Stub::makeEmpty(PaymentResponse::class);
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'status' => Order::STATUS_WAITING_FOR_PAYMENT,
            'payment_method' => Order::PAYMENT_METHOD_ONLINE,
            'payment_id' => $this->faker->text,
        ])->first();
        $yandexMoneyService = test::double(YandexMoneyService::class, [
            'getPaymentInfo' => $paymentInfo,
            'getConfirmation' => $response,
        ]);

        $result = $this->service->getConfirmation($order);
        unset($response['id']);

        $this->tester->assertEquals($result, $response);

        $yandexMoneyService->verifyInvokedOnce('getPaymentInfo');
        $yandexMoneyService->verifyInvokedOnce('getConfirmation');
    }

    public function testSetDeliveryStatus()
    {
        $oldStatus = Order::SHIPPING_STATUS_NEW;
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'shipping_status' => $oldStatus,
        ])->first();

        $newStatus = $this->faker->randomElement(
            array_diff($this->order->getShippingStatuses(), [$oldStatus])
        );

        $this->service->setDeliveryStatus($order, $newStatus);

        $this->tester->assertEquals($order->shipping_status, $newStatus);
    }

    public function testSetDeliveryStatusFailed()
    {
        $oldStatus = Order::SHIPPING_STATUS_DELIVERED;
        $order = factory(Order::class)->create([
            'user_id' => $this->user->id,
            'shipping_status' => $oldStatus,
        ])->first();

        $newStatus = $this->faker->randomElement(
            array_diff($this->order->getShippingStatuses(), [$oldStatus])
        );

        $this->service->setDeliveryStatus($order, $newStatus);

        $this->tester->assertEquals($order->shipping_status, $oldStatus);
    }
}
