<?php namespace Services;

use App\Models\OrderBill;
use App\Models\User;
use App\Services\OrderBillService;
use App\Services\YandexMoneyService;
use AspectMock\Test as test;
use Illuminate\Support\Facades\Queue;
use Faker\Factory;
use YandexCheckout\Request\Payments\PaymentResponse;

class OrderBillServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $faker;

    /** @var OrderBillService $service */
    protected $service;

    /** @var User $user */
    protected $user;

    protected function _before()
    {
        Queue::fake();
        $this->faker = Factory::create();
        $this->service = \App::make(OrderBillService::class);
        $this->user = factory(User::class)->create()->first();
    }

    protected function _after()
    {
        $this->service = null;
        test::cleanInvocations();
    }

    // tests
    public function testProcess()
    {
        $response = [
            'id' => $this->faker->word,
            'confirmation_url' => $this->faker->url,
            'message' => 'test',
        ];
        $orderBill = factory(OrderBill::class)->create([
            'user_id' => $this->user->id,
            'status' => OrderBill::PAYMENT_STATUS_NEW,
        ])->first();

        $yandexMoneyService = test::double(YandexMoneyService::class, [
            'createPaymentFromBill' => $response,
        ]);

        $result = $this->service->process($orderBill);
        $orderBill = $orderBill->fresh();

        $this->tester->assertEquals($orderBill->status, OrderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT);
        $this->tester->assertEquals($orderBill->payment_id, $response['id']);
        $this->tester->assertEquals($result['confirmation_url'], $response['confirmation_url']);

        $yandexMoneyService->verifyInvokedOnce('createPaymentFromBill');
    }

    public function testGetConfirmation()
    {
        $response = [
            'id' => $this->faker->word,
            'confirmation_url' => $this->faker->url,
            'message' => 'test',
        ];
        $paymentInfo = \Codeception\Stub::makeEmpty(PaymentResponse::class);
        $orderBill = factory(OrderBill::class)->create([
            'user_id' => $this->user->id,
            'status' => OrderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT,
        ])->first();

        $yandexMoneyService = test::double(YandexMoneyService::class, [
            'getPaymentInfo' => $paymentInfo,
            'getConfirmation' => $response,
        ]);

        $result = $this->service->getConfirmation($orderBill);
        unset($response['id']);

        $this->tester->assertEquals($result, $response);

        $yandexMoneyService->verifyInvokedOnce('getPaymentInfo');
        $yandexMoneyService->verifyInvokedOnce('getConfirmation');
    }

    protected function testCheckPaymentStatus(string $paymentStatus, int $status)
    {
        $response = \Codeception\Stub::make(PaymentResponse::class, [
            'getStatus' => $paymentStatus,
        ]);
        $orderBill = factory(OrderBill::class)->create([
            'user_id' => $this->user->id,
            'status' => OrderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT,
        ])->first();
        $yandexMoneyService = test::double(YandexMoneyService::class, [
            'getPaymentInfo' => $response,
            'capturePayment' => true,
        ]);

        $updatedOrderBill = $this->service->checkPayment($orderBill);

        $this->tester->assertEquals($updatedOrderBill->status, $status);

        $yandexMoneyService->verifyInvokedOnce('getPaymentInfo');
        if ($paymentStatus === YandexMoneyService::STATUS_WAITING_FOR_CAPTURE) {
            $yandexMoneyService->verifyInvokedOnce('capturePayment');
        }
    }

    public function testCheckPaymentPending()
    {
        $this->testCheckPaymentStatus(YandexMoneyService::STATUS_PENDING,
            OrderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT);
    }

    public function testCheckPaymentWaiting()
    {
        $this->testCheckPaymentStatus(YandexMoneyService::STATUS_WAITING_FOR_CAPTURE, OrderBill::PAYMENT_STATUS_PAYED);
    }

    public function testCheckPaymentSucceeded()
    {
        $this->testCheckPaymentStatus(YandexMoneyService::STATUS_SUCCEEDED, OrderBill::PAYMENT_STATUS_PAYED);
    }

    public function testCheckPaymentCanceled()
    {
        $this->testCheckPaymentStatus(YandexMoneyService::STATUS_CANCELED, OrderBill::PAYMENT_STATUS_CANCELED);
    }
}
