<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * @mixin Builder
 */
class Review extends Model implements HasMedia
{
    use HasMediaTrait;

    const MEDIA_CATEGORY_IMAGE = 'image';
    const MEDIA_CONVERSION_WEB = 'web';

    const MEDIA_MAX_WIDTH = 1500;
    const MEDIA_MAX_HEIGHT = 1000;

    protected $with = [
        'media'
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::MEDIA_CATEGORY_IMAGE)
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion(self::MEDIA_CONVERSION_WEB)
            ->keepOriginalImageFormat()
            ->optimize();
    }

    protected $table = 'book_reviews';

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'instagram' => 'string',
        'text' => 'string',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
