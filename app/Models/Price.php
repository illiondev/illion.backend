<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin Builder
 */
class Price extends Model
{
    public $timestamps = false;

    const TYPE_WEB = 1;
    const TYPE_MOB = 2;

    const CURRENCY_RUB = 'rub';
    const CURRENCY_EUR = 'eur';
    const CURRENCY_UAH = 'uah';

    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'currency' => 'string',
        'amount' => 'float',
    ];

    public static function getTypes() : array
    {
        return [
            self::TYPE_WEB,
            self::TYPE_MOB,
        ];
    }

    public static function getCurrencies() : array
    {
        return [
            self::CURRENCY_RUB,
            self::CURRENCY_EUR,
            self::CURRENCY_UAH,
        ];
    }
}
