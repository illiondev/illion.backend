<?php

namespace App\Models\Vendor;

use App\Models\Advantage;
use App\Models\Announcement;
use App\Models\Author;
use App\Models\Book;
use App\Models\BookChapter;
use App\Models\Bundle;
use App\Models\Comment;
use App\Models\Feature;
use App\Models\Order;
use App\Models\OrderBill;
use App\Models\Payout;
use App\Models\Price;
use App\Models\Product;
use App\Models\Quote;
use App\Models\Review;
use App\Models\Soon;
use App\Models\User;
use App\Models\Vacancy;
use App\Models\ExternalSale;

class Activity extends \Spatie\Activitylog\Models\Activity
{
    public const DESCRIPTION_PAID = 'paid';

    protected $appends = ['subject_name', 'subject_label'];

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    protected static $classes = [
        'advantage' => [
            'name' => 'Преимущество книги',
            'class' => Advantage::class,
        ],
        'announcement' => [
            'name' => 'Объявление',
            'class' => Announcement::class,
        ],
        'author' => [
            'name' => 'Автор',
            'class' => Author::class,
        ],
        'book' => [
            'name' => 'Книга',
            'class' => Book::class,
        ],
        'book_chapter' => [
            'name' => 'Глава книги',
            'class' => BookChapter::class,
        ],
        'external_sale' => [
            'name' => 'Внешняя продажа',
            'class' => ExternalSale::class,
        ],
        'feature' => [
            'name' => 'Особенность книги',
            'class' => Feature::class,
        ],
        'order' => [
            'name' => 'Заказ',
            'class' => Order::class,
        ],
        'payout' => [
            'name' => 'Выплата',
            'class' => Payout::class,
        ],
        'product' => [
            'name' => 'Продукт',
            'class' => Product::class,
        ],
        'quote' => [
            'name' => 'Цитата книги',
            'class' => Quote::class,
        ],
        'review' => [
            'name' => 'Обзор',
            'class' => Review::class,
        ],
        'user' => [
            'name' => 'Пользователь',
            'class' => User::class,
        ],
        'vacancy' => [
            'name' => 'Вакансия',
            'class' => Vacancy::class,
        ],
        'bill' => [
            'name' => 'Счет',
            'class' => OrderBill::class,
        ],
        'soon' => [
            'name' => 'Скоро',
            'class' => Soon::class,
        ],
        'bundle' => [
            'name' => 'Комплект',
            'class' => Bundle::class,
        ],
        'price' => [
            'name' => 'Цена',
            'class' => Price::class,
        ],
        'comment' => [
            'name' => 'Комментарий',
            'class' => Comment::class,
        ],
    ];

    public function getSubjectNameAttribute(): string
    {
        return self::$classes[$this->subject_label]['name'] ?? $this->subject_label;
    }

    public static function getClassName(string $classLabel): string
    {
        return self::$classes[$classLabel]['class'];
    }

    public function getSubjectLabelAttribute(): string
    {
        foreach (self::$classes as $key => $class) {
            if ($class['class'] === $this->subject_type) {
                return $key;
            }
        }

        throw new \Exception('Label not found: ' . $this->subject_type);
    }
}
