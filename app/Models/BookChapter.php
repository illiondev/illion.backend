<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * @deprecated
 * @mixin Builder
 */
class BookChapter extends Model implements HasMedia
{
    use HasMediaTrait;

    const MEDIA_CATEGORY_IMAGE = 'image';
    const MEDIA_CONVERSION_WEB = 'web';

    const MEDIA_MAX_WIDTH = 1500;
    const MEDIA_MAX_HEIGHT = 1000;

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::MEDIA_CATEGORY_IMAGE);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion(self::MEDIA_CONVERSION_WEB)
            ->nonQueued()
            ->keepOriginalImageFormat()
            ->optimize();
    }

    protected $casts = [
        'id' => 'integer',
        'number' => 'integer',

        'book_id' => 'integer',

        'title' => 'string',
        'content' => 'string',

        'completed' => 'boolean',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

}
