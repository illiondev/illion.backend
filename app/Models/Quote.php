<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin Builder
 */
class Quote extends Model
{
    protected $table = 'book_quotes';

    public $timestamps = false;

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'text' => 'string',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
