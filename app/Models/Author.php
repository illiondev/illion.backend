<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $about
 * @property string $instagram
 * @property int $user_id
 *
 * @property-read User $user
 *
 * @mixin Builder
 */
class Author extends Model implements HasMedia
{
    use HasMediaTrait;

    const MEDIA_CATEGORY_PHOTO = 'photo';
    const MEDIA_CONVERSION_WEB = 'web';

    const MEDIA_MAX_WIDTH = 1500;
    const MEDIA_MAX_HEIGHT = 1000;

    protected $with = [
        'media'
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::MEDIA_CATEGORY_PHOTO)
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion(self::MEDIA_CONVERSION_WEB)
            ->keepOriginalImageFormat()
            ->optimize();
    }

    public $timestamps = false;

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'surname' => 'string',
        'about' => 'string',
        'instagram' => 'string',
        'user_id' => 'integer',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
