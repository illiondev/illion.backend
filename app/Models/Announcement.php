<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property boolean $enabled
 * @property string $content
 * @property integer $page_type
 *
 * @mixin Builder
 */
class Announcement extends Model
{
    public $timestamps = false;

    const PAGE_TYPE_MAIN = 1;
    const PAGE_TYPE_ALL = 2;

    protected $casts = [
        'id' => 'integer',
        'enabled' => 'boolean',
        'content' => 'string',
        'page_type' => 'integer',
    ];
}
