<?php

namespace App\Models;

use App\Interfaces\PriceableInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * @property int $id
 * @property int $book_id
 * @property string $slug
 * @property boolean $enabled
 * @property boolean $hidden
 * @property boolean $pre_order
 * @property string $notification
 * @property string $title
 * @property string $description
 * @property int $shop_id
 * @property string $secret_key
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read float $prime_cost
 * @property-read Collection|Price[] $prices
 * @property-read Collection|Product[] $products
 *
 * @mixin Builder
 */
class Bundle extends Model implements HasMedia, PriceableInterface
{
    use HasMediaTrait;

    const PRODUCT_TYPE_PAPER_BOOK = 1;
    const PRODUCT_TYPE_E_BOOK = 2;
    const PRODUCT_TYPE_VIDEO = 3;
    const PRODUCT_TYPE_MIXED = 4;

    const MEDIA_CATEGORY_COVER = 'cover';
    const MEDIA_CONVERSION_WEB = 'web';

    protected $with = [
        'media',
        'prices',
        'products',
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::MEDIA_CATEGORY_COVER)
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion(self::MEDIA_CONVERSION_WEB)
            ->keepOriginalImageFormat()
            ->optimize();
    }

    protected $casts = [
        'id' => 'integer',
        'book_id' => 'integer',

        'slug' => 'string',

        'enabled' => 'boolean',
        'hidden' => 'boolean',

        'pre_order' => 'boolean',
        'notification' => 'string',

        'title' => 'string',
        'description' => 'string',

        'shop_id' => 'integer',
        'secret_key' => 'string',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function products(): BelongsToMany
    {
        return $this
            ->belongsToMany(Product::class, 'bundles_has_products')
            ->withPivot(['is_surprise', 'price', 'authors_percent']);
    }

    public function prices(): MorphMany
    {
        return $this->morphMany(Price::class, 'model')->where(['type' => Price::TYPE_WEB]);
    }

    public function priceByCurrency(string $currency): float
    {
        return $this->prices->keyBy('currency')[$currency]->amount;
    }

    /**
     * @return Collection|Product[]
     */
    public function getPaperProducts(): Collection
    {
        return $this->products->filter(function ($product) {
            return $product->type === Product::PRODUCT_TYPE_PAPER_BOOK;
        });
    }

    public function getPrimeCostAttribute(): float
    {
        return $this->products->sum('prime_cost');
    }

    public function getType(): ?int
    {
        $type = null;
        foreach ($this->products as $product) {
            if (!$product->pivot->is_surprise) {
                if ($type === null) {
                    $type = $product->type;
                } else {
                    if ($type !== $product->type) {
                        return self::PRODUCT_TYPE_MIXED;
                    }
                }
            }
        }

        return $type;
    }
}
