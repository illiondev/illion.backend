<?php

namespace App\Models;

use Illion\Service\Models\City;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\File;

/**
 * @mixin Builder
 */
class Profile extends Model implements HasMedia
{
    use HasMediaTrait;

    const GENDER_FEMALE = 0;
    const GENDER_MALE = 1;
    const GENDER_UNKNOWN = 2;

    const READ_BOOK_PER_WEEK = 1;          // Книгу в неделю
    const READ_BOOK_PER_MONTH = 2;         // Книгу в месяц
    const READ_BOOK_PER_THREE_MONTHS = 3;  // Книгу в три месяца
    const READ_BOOK_PER_HALF_YEAR = 4;     // Книгу в полгода
    const READ_BOOK_PER_YEAR = 5;          // Книгу в год

    const PREFER_EBOOK = 1; // e-book
    const PREFER_PAPER = 2; // paper

    const MEDIA_CATEGORY_AVATAR = 'avatar';

    const MEDIA_ACCEPTABLE_MIME = [
        'image/jpeg',
        'image/png',
    ];

    const MEDIA_MIN_WIDTH  = 200;
    const MEDIA_MIN_HEIGHT = 200;
    const MEDIA_MAX_WIDTH = 1500;
    const MEDIA_MAX_HEIGHT = 1000;

    const MEDIA_CONVERSION_WEB = 'web';

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::MEDIA_CATEGORY_AVATAR)
            ->acceptsFile(function (File $file) {
                return in_array($file->mimeType, self::MEDIA_ACCEPTABLE_MIME, true);
            })
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion(self::MEDIA_CONVERSION_WEB)
            ->keepOriginalImageFormat()
            ->optimize();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'gender',
        'birthday',
        'city_id',
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'name' => 'string',
        'gender' => 'integer',
        'birthday' => 'date',

        'country_id' => 'integer',
        'country_names' => 'array',
        'city_id' => 'integer',
        'city_names' => 'array',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function preferenceOptions(): BelongsToMany
    {
        return $this
            ->belongsToMany(PreferenceOption::class, 'user_has_preference_option', 'user_id', 'preference_option_id', 'user_id')
            ->withPivot('preference_id');
    }

    public function getCityAttribute(): ?City
    {
        if (!$this->city_id) {
            return null;
        }

        $locale = app()->getLocale();

        return new City([
            'id' => $this->city_id,
            'name' => $this->city_names[$locale] ?? null,
            'country_id' => $this->country_id,
            'country_name' => $this->country_names[$locale] ?? null,
            'locale' => $locale,
        ]);
    }

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }
}
