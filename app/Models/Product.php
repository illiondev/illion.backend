<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property string $article
 * @property integer $type
 * @property integer $book_id
 * @property integer $author_id
 * @property float $prime_cost
 * @property array $meta
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read string $epub
 * @property-read string $pdf
 * @property-read string $epub_preview
 * @property-read string $video
 * @property-read float $length
 * @property-read float $width
 * @property-read float $height
 * @property-read float $weight
 * @property-read string $description
 *
 * @property-read Book $book
 *
 * @mixin Builder
 */
class Product extends Model
{
    protected $table = 'book_products';

    const PRODUCT_TYPE_PAPER_BOOK = 1;
    const PRODUCT_TYPE_E_BOOK = 2;
    const PRODUCT_TYPE_VIDEO = 3;
    const PRODUCT_TYPE_COURSE = 4;

    protected $casts = [
        'id' => 'integer',
        'article' => 'string',

        'type' => 'integer',
        'book_id' => 'integer',
        'author_id' => 'integer',

        'prime_cost' => 'float',

        // epub, вес
        'meta' => 'array',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function book() : BelongsTo
    {
        return $this->belongsTo(Book::class);
    }

    public function getLengthAttribute(): float
    {
        return (float) $this->meta['length'] ?? 0;
    }

    public function getWidthAttribute(): float
    {
        return (float) $this->meta['width'] ?? 0;
    }

    public function getHeightAttribute(): float
    {
        return (float) $this->meta['height'] ?? 0;
    }

    public function getWeightAttribute(): float
    {
        return (float) $this->meta['weight'] ?? 0;
    }

    public function getEpubAttribute(): string
    {
        return $this->meta['epub'] ?? '';
    }

    public function getPdfAttribute(): string
    {
        return $this->meta['pdf'] ?? '';
    }

    public function getEpubPreviewAttribute(): string
    {
        return $this->meta['epub_preview'] ?? '';
    }

    public function getVideoAttribute(): string
    {
        return $this->meta['video'] ?? '';
    }

    public function getDescriptionAttribute(): string
    {
        return $this->meta['description'] ?? '';
    }
}
