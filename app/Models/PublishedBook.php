<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $type
 * @property string $filename
 * @property integer $book_id
 * @property integer $size
 * @property string $secret_key
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @mixin Builder
 */
class PublishedBook extends Model
{
    const TYPE_BOOK = 1;
    const TYPE_PREVIEW = 2;

    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'filename' => 'string',

        'book_id' => 'integer',
        'size' => 'integer',

        'secret_key' => 'string',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
