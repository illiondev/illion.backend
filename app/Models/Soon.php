<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * @mixin Builder
 */
class Soon extends Model implements HasMedia
{
    protected $table = 'soon';

    use HasMediaTrait;

    const MEDIA_CATEGORY_COVER = 'cover';

    const MEDIA_CONVERSION_WEB = 'web';

    const MEDIA_MAX_WIDTH = 1500;
    const MEDIA_MAX_HEIGHT = 1000;

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::MEDIA_CATEGORY_COVER)
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion(self::MEDIA_CONVERSION_WEB)
            ->nonQueued()
            ->keepOriginalImageFormat()
            ->optimize();
    }

    protected $casts = [
        'id' => 'integer',
        'enabled' => 'boolean',
        'author_id' => 'integer',
        'title' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function author(): BelongsTo
    {
        return $this->belongsTo(Author::class);
    }
}
