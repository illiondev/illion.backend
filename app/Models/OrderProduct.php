<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $order_item_id
 * @property integer $book_id
 * @property integer $bundle_id
 * @property integer $product_id
 * @property float $prime_cost
 * @property string $article
 * @property string $book_title
 * @property integer $type
 * @property float $length
 * @property float $width
 * @property float $height
 * @property float $weight
 * @property boolean $is_surprise
 *
 * @property integer $author_id
 * @property integer $quantity
 * @property float $authors_percent
 * @property float $price
 * @property float $net_profit
 *
 * @property-read Book $book
 * @property-read OrderItem $orderItem
 * @property-read Order $order
 * @property-read Bundle $bundle
 *
 * @mixin Builder
 */
class OrderProduct extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders_products';
    public $timestamps = false;

    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'order_item_id' => 'integer',
        'book_id' => 'integer',
        'bundle_id' => 'integer',
        'product_id' => 'integer',
        'prime_cost' => 'float',
        'article' => 'string',
        'book_title' => 'string',
        'type' => 'integer',
        'length' => 'float',
        'width' => 'float',
        'height' => 'float',
        'weight' => 'float',
        'is_surprise' => 'boolean',

        'author_id' => 'integer',
        'quantity' => 'integer',
        'authors_percent' => 'float',
        'price' => 'float',
        'net_profit' => 'float',
    ];

    public function book() : BelongsTo
    {
        return $this->belongsTo(Book::class);
    }

    public function order() : BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function orderItem() : BelongsTo
    {
        return $this->belongsTo(OrderItem::class);
    }

    public function bundle() : BelongsTo
    {
        return $this->belongsTo(Bundle::class);
    }
}
