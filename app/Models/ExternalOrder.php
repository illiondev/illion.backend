<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @mixin Builder
 */
class ExternalOrder extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'client_id',
        'external_id',
        'country_code',
        'region',
        'city',
        'status',
        'price',
        'delivery_price',
        'total_price',
        'sold_at',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'id' => 'integer',
        'client_id' => 'integer',
        'external_id' => 'integer',

        'country_code' => 'string',
        'region' => 'string',
        'city' => 'string',

        'status' => 'integer',

        'price' => 'float',
        'delivery_price' => 'float',
        'total_price' => 'float',

        'sold_at' => 'datetime',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public static function getStatuses()
    {
        return [
            Order::STATUS_NEW,
            Order::STATUS_WAITING_FOR_PAYMENT,
            Order::STATUS_PROCESSING,
            Order::STATUS_COMPLETED,
            Order::STATUS_CANCELED,
            Order::STATUS_RETURNED,
        ];
    }

    public function items(): HasMany
    {
        return $this->hasMany(ExternalOrderItem::class, 'external_order_id', 'external_id');
    }

    public function getLockKey(): string
    {
        return 'external_order_' . $this->id;
    }
}
