<?php

namespace App\Models;

use Cache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $type
 * @property string $code
 * @property integer $bundle_id
 * @property float $discount
 * @property float $discount_flat
 * @property integer $limit
 * @property integer $used
 * @property Carbon $expired_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @mixin Builder
 */
class Promocode extends Model
{
    const TYPE_BUNDLE = 1;
    const TYPE_OVERALL = 2;
    const TYPE_DELIVERY = 3;

    protected $redisKey = 'promocode_lock_';

    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'code' => 'string',
        'bundle_id' => 'integer',
        'discount' => 'float',
        'discount_flat' => 'float',
        'limit' => 'integer',
        'used' => 'integer',
        'expired_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function use()
    {
        if (Cache::lock($this->redisKey . $this->id, 60 * 10)->get()) {
            try {
                $this->refresh();

                $this->used++;

                $this->save();
            } finally {
                Cache::lock($this->redisKey . $this->id)->release();
            }
        }
    }

    public function isBundleType(): bool
    {
        return $this->type === self::TYPE_BUNDLE;
    }

    public function isOverallType(): bool
    {
        return $this->type === self::TYPE_OVERALL;
    }

    public function isDeliveryType(): bool
    {
        return $this->type === self::TYPE_DELIVERY;
    }
}
