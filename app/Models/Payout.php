<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property integer $id
 * @property float $amount
 * @property integer $author_id
 * @property integer $moderator_id
 * @property Carbon $payout_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read User $moderator
 * @mixin Builder
 */
class Payout extends Model
{
    protected $casts = [
        'id' => 'integer',
        'amount' => 'float',
        'author_id' => 'integer',
        'moderator_id' => 'integer',
        'payout_date' => 'date',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function moderator(): HasOne
    {
        return $this->HasOne(User::class, 'id', 'moderator_id');
    }
}
