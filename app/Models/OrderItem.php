<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property integer $id
 * @property integer $book_id
 * @property integer $bundle_id
 * @property integer $order_id
 * @property float $prime_cost
 * @property string $title
 * @property integer $quantity
 * @property float $original_price
 * @property float $discount
 * @property float $discount_flat
 * @property float $price
 * @property string $promocode
 * @property float $net_profit
 *
 * @property-read Bundle $bundle
 * @property-read OrderProduct[] $products
 *
 * @mixin Builder
 */
class OrderItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders_items';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'book_id',
        'bundle_id',
        'title',
        'prime_cost',
        'quantity',
        'original_price',
        'discount',
        'discount_flat',
        'price',
    ];

    protected $casts = [
        'id' => 'integer',
        'book_id' => 'integer',
        'bundle_id' => 'integer',
        'order_id' => 'integer',
        'prime_cost' => 'float',
        'title' => 'string',
        'quantity' => 'integer',
        'original_price' => 'float',
        'discount' => 'float',
        'discount_flat' => 'float',
        'price' => 'float',
        'promocode' => 'string',
        'net_profit' => 'float',
    ];

    protected $with = [
        'products',
        'bundle',
    ];

    /**
     * @return Collection|OrderProduct[]
     */
    public function getPaperProducts(): Collection
    {
        if ($this->exists) {
            return $this->products->filter(function ($product) {
                return $product->type === Product::PRODUCT_TYPE_PAPER_BOOK;
            });
        } else {
            return $this->bundle->getPaperProducts();
        }
    }

    public function bundle(): BelongsTo
    {
        return $this->belongsTo(Bundle::class);
    }

    public function products(): HasMany
    {
        return $this->hasMany(OrderProduct::class);
    }
}
