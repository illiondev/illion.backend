<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * @property integer $id
 * @property integer $priority
 * @property integer $user_id
 * @property string $theme_id
 * @property integer $author_id
 * @property string $title
 * @property string $tagline
 * @property string $preview
 * @property Carbon $publication_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read PublishedBook|null $publishedEbook
 * @property-read PublishedBook|null $previewEbook
 * @property-read Author $author
 * @property-read Bundle[]|Collection $bundles
 *
 * @mixin Builder
 */
class Book extends Model implements HasMedia
{
    use HasMediaTrait;

    const REDIS_BOOK_PREVIEW_KEY_ = 'book_preview_key';
    const REDIS_BOOK_PREVIEW_KEY_EXPIRE = 60 * 60; // seconds, 1h

    const MEDIA_CATEGORY_COVER = 'cover';
    const MEDIA_CATEGORY_HARDCOVER = 'hardcover';
    const MEDIA_CATEGORY_COVER_SMALL = 'cover-small';
    const MEDIA_CATEGORY_PHOTO = 'photo';
    const MEDIA_CATEGORY_FRAGMENT = 'fragment';

    const MEDIA_CONVERSION_WEB = 'web';

    const MEDIA_MAX_WIDTH = 1500;
    const MEDIA_MAX_HEIGHT = 1000;

    protected $with = [
        'author',
        'media',

        'bundles',
        'advantages',
        'features',
        'reviews',
        'quotes',

        'bundles',
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::MEDIA_CATEGORY_COVER)
            ->singleFile();
        $this->addMediaCollection(self::MEDIA_CATEGORY_HARDCOVER)
            ->singleFile();
        $this->addMediaCollection(self::MEDIA_CATEGORY_COVER_SMALL)
            ->singleFile();

        $this->addMediaCollection(self::MEDIA_CATEGORY_PHOTO);

        $this->addMediaCollection(self::MEDIA_CATEGORY_FRAGMENT)
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion(self::MEDIA_CONVERSION_WEB)
            ->nonQueued()
            ->keepOriginalImageFormat()
            ->optimize();
    }

    const GENRE_FANTASY = 1; // фантастика
    const GENRE_FICTION = 2; // художественная
    const GENRE_SCIENCE = 3; // научно-популярная
    const GENRE_PROFESSIONAL = 4; // профессиональная
    const GENRE_PSYCHOLOGY = 5; // психология
    const GENRE_FOR_CHILDREN = 6; // детская
    const GENRE_EDUCATIONAL = 7; // образовательная
    const GENRE_HISTORICAL = 8; // историческая

    protected $casts = [
        'id' => 'integer',
        'priority' => 'integer',
        'user_id' => 'integer',
        'theme_id' => 'string',
        'author_id' => 'integer',

        'title' => 'string',
        'tagline' => 'string',
        'preview' => 'string',

        'publication_date' => 'date',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function author(): BelongsTo
    {
        return $this->belongsTo(Author::class);
    }

    public function publishedEbook(): HasOne
    {
        return $this
            ->hasOne(PublishedBook::class)
            ->where(['type' => PublishedBook::TYPE_BOOK])
            ->latest()
            ->limit(1);
    }

    public function previewEbook(): HasOne
    {
        return $this
            ->hasOne(PublishedBook::class)
            ->where(['type' => PublishedBook::TYPE_PREVIEW])
            ->latest()
            ->limit(1);
    }

    public function features(): HasMany
    {
        return $this->hasMany(Feature::class);
    }

    public function advantages(): HasMany
    {
        return $this->hasMany(Advantage::class);
    }

    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    public function bundles(): BelongsToMany
    {
        return $this
            ->belongsToMany(Bundle::class, 'books_has_bundles')
            ->where(['enabled' => 1, 'hidden' => 0]);
    }

    public function quotes(): HasMany
    {
        return $this->hasMany(Quote::class);
    }
}
