<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property integer $external_sale_id
 * @property integer $book_id
 * @property integer $product_id
 * @property float $prime_cost
 * @property integer $author_id
 * @property integer $quantity
 * @property float $net_profit
 * @property float $authors_percent
 * @property string $article
 * @property string $book_title
 * @property integer $type
 * @property integer $bundle_id
 * @property float $length
 * @property float $width
 * @property float $height
 * @property float $weight
 * @property boolean $is_surprise
 * @property float $price
 *
 * @property-read ExternalSale|null $externalSale
 *
 * @mixin Builder
 */
class ExternalSaleProduct extends Model
{
    protected $table = 'external_sales_products';

    public $timestamps = false;

    protected $casts = [
        'id' => 'integer',
        'external_sale_id' => 'integer',
        'book_id' => 'integer',
        'product_id' => 'integer',
        'prime_cost' => 'float',
        'author_id' => 'integer',
        'quantity' => 'integer',
        'net_profit' => 'float',
        'authors_percent' => 'float',
        'article' => 'string',
        'book_title' => 'string',
        'type' => 'integer',
        'bundle_id' => 'integer',
        'length' => 'float',
        'width' => 'float',
        'height' => 'float',
        'weight' => 'float',
        'is_surprise' => 'boolean',
        'price' => 'float',
    ];

    public function externalSale() : BelongsTo
    {
        return $this->belongsTo(ExternalSale::class);
    }
}
