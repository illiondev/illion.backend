<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * @property-read Comment $comment
 *
 * @mixin Builder
 */
class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;

    const ROLE_USER = 'user';
    const ROLE_MODERATOR = 'moderator';
    const ROLE_AUTHOR = 'author';
    const ROLE_ADMIN = 'admin';

    const STATUS_NEW = 1;
    const STATUS_REGISTERED = 2;
    const STATUS_APPROVED = 3;
    const STATUS_BLOCKED = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'subscription_email',
    ];

    protected $casts = [
        'id' => 'integer',
        'external_id' => 'integer',
        'status' => 'integer',
        'email' => 'string',
        'email_confirmed_at' => 'timestamp',
        'subscription_email' => 'boolean',
        'agreements_accepted' => 'boolean',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    function isAuthor(): bool
    {
        return $this->hasRole(self::ROLE_AUTHOR);
    }

    function isModerator(): bool
    {
        return $this->hasRole([self::ROLE_MODERATOR, self::ROLE_ADMIN]);
    }

    public function isAdmin(): bool
    {
        return $this->hasRole(self::ROLE_ADMIN);
    }

    public function getAuthIdentifierName()
    {
        return 'external_id';
    }

    public function profile(): HasOne
    {
        return $this->hasOne(Profile::class);
    }

    public function ownedProducts(): BelongsToMany
    {
        return $this
            ->belongsToMany(Product::class, 'user_has_books')
            ->withTimestamps();
    }

    public function ownedBooks(): BelongsToMany
    {
        return $this
            ->belongsToMany(Book::class, 'user_has_books')
            ->groupBy('user_has_books.user_id', 'user_has_books.book_id');
    }

    public function isEmailConfirmed(): bool
    {
        return $this->email_confirmed_at !== null;
    }

    public function isBlocked(): bool
    {
        return $this->status === self::STATUS_BLOCKED;
    }

    public function comment(): MorphOne
    {
        return $this->morphOne(Comment::class, 'model');
    }
}
