<?php

namespace App\Models\Redis;

use App\Models\Bundle;
use App\Models\OrderItem;
use App\Models\Price;
use App\Models\Promocode;
use App\Repositories\PromocodeRepository;
use App\Services\PromocodeService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redis;
use Ramsey\Uuid\Uuid;
use Request;

/**
 * @property-read string $hash
 */
class Cart
{
    const REDIS_KEY_PREFIX = 'illion_cart';
    const REDIS_KEY_EXPIRE = 24 * 60 * 60 * 70; // 70 days

    const LIMIT = 1000; // items limit per key

    public $hash;
    protected $ip;

    public function __construct(string $hash = null)
    {
        if (mb_strlen($hash) === 0) {
            $hash = null;
        }

        if (Redis::exists('illion_cart_hash_' . $hash)) {
            $this->hash = $hash;
        } else {
            if ($hash === null) {
                $this->hash = self::generateHash();
            } else {
                $this->hash = $hash;
                $this->ip = Request::ip();
            }
        }
    }

    public static function generateHash()
    {
        $hash = Uuid::uuid4();

        Redis::set('illion_cart_hash_' . $hash, 1);
        Redis::expire('illion_cart_hash_' . $hash, self::REDIS_KEY_EXPIRE);

        return $hash;
    }

    private function getKey()
    {
        return self::REDIS_KEY_PREFIX . '_' . ($this->ip ? $this->ip . '_' : '') . $this->hash;
    }

    public function setPromocode(string $promocode): bool
    {
        $promocodeRepository = app()->make(PromocodeRepository::class);

        /** @var Promocode $promocode */
        $promocode = $promocodeRepository->first($promocode);

        if (!$promocode) {
            return false;
        }

        if ($promocode->isBundleType() && $this->get($promocode->bundle_id) == null) {
            return false;
        }

        Redis::set($this->getKey() . '_promocode', $promocode->code);
        Redis::expire($this->getKey() . '_promocode', self::REDIS_KEY_EXPIRE);

        return true;
    }

    public function getPromocode(): ?Promocode
    {
        $promocodeRepository = app()->make(PromocodeRepository::class);
        $code = Redis::get($this->getKey() . '_promocode');

        return $code ? $promocodeRepository->first($code) : null;
    }

    public function removePromocode(): void
    {
        Redis::del($this->getKey() . '_promocode');
    }

    /**
     * Get all.
     *
     * @return array
     */
    protected function all(): array
    {
        return Redis::hGetAll($this->getKey());
    }

    /**
     * Get count.
     */
    protected function count(): int
    {
        $cart = $this->all();
        $count = 0;

        foreach ($cart as $elem) {
            $count += (int) unserialize($elem)['quantity'];
        }

        return $count;
    }

    /**
     * Get item from cart.
     */
    protected function get(int $id): ?int
    {
        $current = Redis::hGet($this->getKey(), $id);

        if ($current) {
            return (int) unserialize($current)['quantity'];
        }

        return null;
    }

    /**
     * Set cart quantity.
     */
    public function set(int $id, int $quantity): int
    {
        // todo костыли на книгу эстера, выпилить позже
        if ($id != 15 && $quantity > 0) {
            $this->remove(15);
        }
        if ($id == 15 && $quantity > 0) {
            $this->flush();
        }
        // todo костыли на книгу эстера, выпилить позже

        if ((int) $quantity <= 0) {
            return $this->remove($id);
        }

        // can change old or add new
        if ($this->get($id) !== null || Redis::hLen($this->getKey()) < self::LIMIT) {
            Redis::hSet(
                $this->getKey(),
                $id,
                serialize(['id' => (int) $id, 'quantity' => (int) $quantity])
            );
            Redis::expire($this->getKey(), self::REDIS_KEY_EXPIRE);
        }

        return $this->count();
    }

    /**
     * Remove from cart.
     */
    protected function remove(int $id): int
    {
        $promocode = $this->getPromocode();

        if ($promocode && $promocode->isBundleType() && $promocode->bundle_id === $id) {
            $this->removePromocode();
        }

        Redis::hDel($this->getKey(), $id);

        return $this->count();
    }

    /**
     * Flush by key.
     */
    public function flush(): int
    {
        $this->removePromocode();

        return Redis::del($this->getKey());
    }

    /**
     * @return OrderItem[]|Collection
     */
    public function items(): Collection
    {
        $cart = $this->all();

        if (count($cart) > 0) {
            $promocode = $this->getPromocode();

            $cartItems = array_map(function (string $cartString) {
                return unserialize($cartString);
            }, $cart);

            $orderItems = [];
            $paperBookCount = 0;

            /** @var Bundle[] $bundles */
            $bundles = Bundle::with([
                'products',
                'prices',
            ])
                ->whereIn('id', array_keys($cartItems))
                ->where(['enabled' => 1])
                ->get();

            foreach ($bundles as $bundle) {
                $quantity = (int) $cartItems[$bundle->id]['quantity'];
                $orderItems[] = self::createOrderItem($bundle, $quantity);

                if ($bundle->getPaperProducts()->count() > 0) {
                    $paperBookCount += $quantity;
                }
            }

            /** @var PromocodeService $promocodeService */
            $promocodeService = app(PromocodeService::class);
            foreach ($orderItems as $orderItem) {
                if ($promocode) {
                    $orderItem = $promocodeService->applyPromocodeToOrderItem($promocode, $orderItem);
                }

                if ($paperBookCount >= 10 && $orderItem->bundle->getPaperProducts()->count() > 0) {
                    if ($orderItem->discount == 0 && $orderItem->discount_flat == 0) {
                        $orderItem->discount = 10;
                    }
                }

                if ($orderItem->discount_flat) {
                    $orderItem->price = $orderItem->original_price - $orderItem->discount_flat;
                } elseif ($orderItem->discount) {
                    $orderItem->price = $orderItem->original_price * (1 - ($orderItem->discount / 100));
                }
            }

            return collect($orderItems);
        }

        return collect();
    }

    public static function createOrderItem(Bundle $bundle, int $quantity): OrderItem
    {
        $orderItem = new OrderItem([
            'title' => $bundle->title,
            'prime_cost' => $bundle->prime_cost,
            'bundle_id' => $bundle->id,
            'quantity' => $quantity,
            'original_price' => $bundle->priceByCurrency(Price::CURRENCY_RUB),
            'discount' => 0,
            'discount_flat' => 0,
            'price' => $bundle->priceByCurrency(Price::CURRENCY_RUB),
        ]);

        $orderItem->setRelation('bundle', $bundle);

        return $orderItem;
    }
}
