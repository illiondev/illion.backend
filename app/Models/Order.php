<?php

namespace App\Models;

use App\Services\ShiptorService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property integer $id
 * @property integer $ml_crm_id
 * @property integer $user_id
 * @property string $name
 * @property string $surname
 * @property string $patronimic
 * @property string $comment
 * @property string $system_comment
 * @property string $email
 * @property string $phone
 * @property integer $shiptor_id
 * @property integer $shipping_method
 * @property integer $delivery_point
 * @property string $country_code
 * @property string $region
 * @property string $settlement
 * @property string $address
 * @property string $address2
 * @property string $postal_code
 * @property string $kladr_id
 * @property float $weight
 * @property float $length
 * @property float $width
 * @property float $height
 * @property string $shipping_status
 * @property string $tracking_number
 * @property Carbon $sent_date
 * @property string $shiptor_error
 * @property float $price
 * @property float $promocode
 * @property float $discount
 * @property float $discount_flat
 * @property float $delivery_price
 * @property float $delivery_discount
 * @property float $delivery_discount_flat
 * @property float $total_price
 * @property integer $payment_method
 * @property string $payment_id
 * @property string $payment_type
 * @property float $payment_fee
 * @property integer $status
 * @property boolean $external_kassa
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read OrderItem[]|Collection $items
 * @property-read OrderProduct[]|Collection $products
 * @property-read User $user
 *
 * @mixin Builder
 */
class Order extends Model
{
    const LIFETIME_MINUTES = 60 * 24 * 7 * 2;

    const STATUS_NEW = 1;
    const STATUS_WAITING_FOR_PAYMENT = 2;
    const STATUS_PROCESSING = 3;
    const STATUS_COMPLETED = 4;
    const STATUS_CANCELED = 5;
    const STATUS_RETURNED = 7;

    const SHIPPING_STATUS_NEW = 'new';
    const SHIPPING_STATUS_PICKUP = 'pickup';
    const SHIPPING_STATUS_ON_ASSEMBLE = 'on_assemble';
    const SHIPPING_STATUS_ASSEMBLED = 'assembled';
    const SHIPPING_STATUS_IN_STORE = 'in_store';
    const SHIPPING_STATUS_PACKED = 'packed';
    const SHIPPING_STATUS_SENT = 'sent';
    const SHIPPING_STATUS_WAITING_ON_DELIVERY_POINT = 'waiting_on_delivery_point';
    const SHIPPING_STATUS_RETURNING_TO_WAREHOUSE = 'returning_to_warehouse';
    const SHIPPING_STATUS_DELIVERED = 'delivered';
    const SHIPPING_STATUS_RETURNED_TO_WAREHOUSE = 'returned_to_warehouse';
    const SHIPPING_STATUS_RESENT = 'resent';
    const SHIPPING_STATUS_TO_DISBAND = 'to_disband';
    const SHIPPING_STATUS_DISBANDED = 'disbanded';
    const SHIPPING_STATUS_TO_RETURN = 'to_return';

    const SHIPPING_STATUS_RETURNED = 'returned';
    const SHIPPING_STATUS_LOST = 'lost';
    const SHIPPING_STATUS_REMOVED = 'removed';
    const SHIPPING_STATUS_RECYCLED = 'recycled';

    const PAYMENT_METHOD_CASH = 1; // наличными при получении
    const PAYMENT_METHOD_CARD = 2; // картой при получении
    const PAYMENT_METHOD_ONLINE = 3; // онлайн
    const PAYMENT_METHOD_GOOGLE_PLAY = 4; // google pay

    const PAYMENT_TYPE_PAYPAL = 'paypal';
    const PAYMENT_TYPE_YANDEX_MONEY = 'yandex_money'; // — Яндекс.Деньги;
    const PAYMENT_TYPE_BANK_CARD = 'bank_card'; // — банковская карта;
    const PAYMENT_TYPE_WEBMONEY = 'webmoney'; // — Webmoney;
    const PAYMENT_TYPE_SBERBANK = 'sberbank'; // — Сбербанк Онлайн (интернет-банк Сбербанка для частных лиц);
    const PAYMENT_TYPE_ALFABANK = 'alfabank'; // — Альфа-Клик (интернет-банк Альфа-Банка);
    const PAYMENT_TYPE_QIWI = 'qiwi'; // — QIWI Кошелек;
    const PAYMENT_TYPE_APPLE_PAY = 'apple_pay'; // — криптограмма Apple Pay;
    const PAYMENT_TYPE_GOOGLE_PAY = 'google_pay'; // — криптограмма Google Pay;
    const PAYMENT_TYPE_CASH = 'cash'; // — оплата наличными в терминале;
    const PAYMENT_TYPE_PSB = 'psb'; // — интернет-банк Промсвязьбанка (этот способ можно выбрать только на странице Яндекс.Кассы).
    const PAYMENT_TYPE_FREE = 'free'; // — автооплата при нулевой стоимости;

    const PAYMENT_TYPE_MOBILE_BALANCE = 'mobile_balance'; // — баланс мобильного телефона;
    const PAYMENT_TYPE_INSTALLMENTS = 'installments'; // — оплата через сервис «Заплатить по частям» (в кредит или рассрочку);

    public static $paymentTypeFee = [
        self::PAYMENT_TYPE_PAYPAL => 0,
        self::PAYMENT_TYPE_FREE => 0,

        self::PAYMENT_TYPE_YANDEX_MONEY => 4.9, // Яндекс.Деньги
        self::PAYMENT_TYPE_BANK_CARD => 2.75, // Банковские карты
        self::PAYMENT_TYPE_WEBMONEY => 4.5, // WebMoney
        self::PAYMENT_TYPE_SBERBANK => 3.5, // Сбербанк Онлайн
        self::PAYMENT_TYPE_ALFABANK => 3.5, // Альфа-Клик
        self::PAYMENT_TYPE_PSB => 3.5, // Промсвязьбанк
        self::PAYMENT_TYPE_QIWI => 5.9, // QIWI Wallet

        self::PAYMENT_TYPE_APPLE_PAY => 30,
        self::PAYMENT_TYPE_GOOGLE_PAY => 30,
    ];

    protected $casts = [
        'id' => 'integer',
        'ml_crm_id' => 'integer', // @deprecated
        'user_id' => 'integer',
        'name' => 'string',
        'surname' => 'string',
        'patronimic' => 'string',
        'comment' => 'string',
        'system_comment' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'shiptor_id' => 'integer',
        'shipping_method' => 'integer',
        'delivery_point' => 'integer',
        'country_code' => 'string',
        'region' => 'string',
        'settlement' => 'string',
        'address' => 'string',
        'address2' => 'string',
        'postal_code' => 'string',
        'kladr_id' => 'string',
        'weight' => 'float',
        'length' => 'float',
        'width' => 'float',
        'height' => 'float',
        'shipping_status' => 'string',
        'tracking_number' => 'string',
        'sent_date' => 'datetime',
        'price' => 'float',
        'promocode' => 'float',
        'discount' => 'float',
        'discount_flat' => 'float',
        'delivery_price' => 'float',
        'delivery_discount' => 'float',
        'delivery_discount_flat' => 'float',
        'total_price' => 'float',
        'payment_method' => 'integer',
        'payment_id' => 'string',
        'payment_type' => 'string',
        'status' => 'integer',
        'external_kassa' => 'boolean',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected $with = [
        'items',
    ];

    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    public function products(): HasMany
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function isPaid(): bool
    {
        return in_array($this->status, [self::STATUS_PROCESSING, self::STATUS_COMPLETED]);
    }

    public function isPaymentOnline(): bool
    {
        return ($this->payment_method === self::PAYMENT_METHOD_ONLINE) ||
            ($this->payment_method === self::PAYMENT_METHOD_GOOGLE_PLAY);
    }

    /**
     * @return Collection|Product[]|OrderProduct[]
     */
    public function getPaperProducts(Collection $orderItems = null): Collection
    {
        if (!$orderItems) {
            $orderItems = $this->items;
        }

        $paperProducts = new Collection();

        if ($this->exists) {
            $paperProducts = $this->products->filter(function ($product) {
                return $product->type === Product::PRODUCT_TYPE_PAPER_BOOK;
            });
        } else {
            foreach ($orderItems as $orderItem) {
                $paperProducts = $paperProducts->merge($orderItem->bundle->getPaperProducts());
            }
        }

        return $paperProducts;
    }

    public function getPrice(Collection $orderItems = null): float
    {
        if (!$orderItems) {
            $orderItems = $this->items;
        }

        $price = 0;
        foreach ($orderItems as $orderItem) {
            $price += ($orderItem->price * $orderItem->quantity);
        }

        return $price;
    }

    public function getLockKey(): string
    {
        return 'order_' . $this->id;
    }

    public function getCountryName()
    {
        switch ($this->country_code) {
            case 'RU':
                return 'Россия';
            case 'KZ':
                return 'Казахстан';
            case 'BY':
                return 'Беларусь';
            default:
                return $this->country_code ? ShiptorService::COUNTRIES[$this->country_code] : '';
        }
    }

    public function getWeight(Collection $orderItems = null): float
    {
        if (!$orderItems) {
            $orderItems = $this->items;
        }

        $weight = 0;

        foreach ($orderItems as $orderItem) {
            if ($this->exists) {
                $paperProducts = $orderItem->getPaperProducts();
            } else {
                $paperProducts = $orderItem->bundle->getPaperProducts();
            }

            $weight += $paperProducts->sum('weight') * $orderItem->quantity;
        }

        return $weight;
    }
}
