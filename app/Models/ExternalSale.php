<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

/**
 * @property integer $id
 * @property boolean $is_paid
 * @property integer $user_id
 * @property integer $quantity
 * @property float $price
 * @property float $prime_cost
 * @property float $author_fee
 * @property float $commission
 * @property float $commission_flat
 * @property float $net_profit
 * @property string $comment
 * @property integer $bundle_id
 *
 * @property Carbon $sold_at
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Bundle $bundle
 * @property-read User $moderator
 * @property-read ExternalSaleProduct[]|Collection $products
 *
 * @mixin Builder
 */
class ExternalSale extends Model
{
    protected $casts = [
        'id' => 'integer',
        'is_paid' => 'boolean',
        'user_id' => 'integer',
        'quantity' => 'integer',
        'price' => 'float',
        'prime_cost' => 'float',
        'author_fee' => 'float',
        'commission' => 'float',
        'commission_flat' => 'float',
        'net_profit' => 'float',
        'comment' => 'string',
        'bundle_id' => 'integer',
        'sold_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function bundle(): BelongsTo
    {
        return $this->belongsTo(Bundle::class);
    }

    public function moderator(): HasOne
    {
        return $this->HasOne(User::class, 'id', 'user_id');
    }

    public function isPaid(): bool
    {
        return $this->is_paid;
    }

    public function products(): HasMany
    {
        return $this->hasMany(ExternalSaleProduct::class);
    }
}
