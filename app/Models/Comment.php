<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Moderator\Traits\Loggable;

/**
 * @property integer $id
 * @property string $text
 * @property integer $user_id
 *
 * @property-read User $user
 *
 * @mixin Builder
 */
class Comment extends Model
{
    use Loggable;

    protected $table = 'moderator_comments';

    protected $casts = [
        'id' => 'integer',
        'text' => 'string',
        'user_id' => 'integer',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
