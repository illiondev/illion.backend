<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 *
 * @property integer $id
 * @property string $hash
 * @property string $email
 * @property integer $order_id
 * @property integer $bundle_id
 * @property integer $user_id
 * @property float $price
 * @property float $net_profit
 * @property string $description
 * @property string $payment_id
 * @property string $payment_type
 * @property float $payment_fee
 * @property integer $status
 * @property boolean $external_kassa
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Bundle|null $bundle
 * @property-read Order|null $order
 * @property-read User|null $user
 * @property-read BillProduct[]|Collection $products
 *
 * @mixin Builder
 *
 * todo rename to Bill
 */
class OrderBill extends Model
{
    protected $table = 'bills';

    protected $casts = [
        'id' => 'integer',
        'hash' => 'string',

        'email' => 'string',

        'order_id' => 'integer',

        'bundle_id' => 'integer',

        'user_id' => 'integer',
        'price' => 'float',
        'net_profit' => 'float',

        'description' => 'string',

        'payment_id' => 'string',
        'payment_type' => 'string',
        'status' => 'integer',
        'external_kassa' => 'boolean',

        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    const PAYMENT_STATUS_NEW = 1;
    const PAYMENT_STATUS_WAITING_FOR_PAYMENT = 2;
    const PAYMENT_STATUS_PAYED = 3;
    const PAYMENT_STATUS_CANCELED = 5;

    public function getLockKey(): string
    {
        return 'order_bill_' . $this->id;
    }

    public function bundle() : BelongsTo
    {
        return $this->belongsTo(Bundle::class);
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function isPaid(): bool
    {
        return $this->status === self::PAYMENT_STATUS_PAYED;
    }

    public function products(): HasMany
    {
        return $this->hasMany(BillProduct::class, 'bill_id', 'id');
    }
}
