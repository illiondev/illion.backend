<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @mixin Builder
 */
class Preference extends Model
{
    public $timestamps = false;

    public function preferenceOptions(): HasMany
    {
        return $this->hasMany(PreferenceOption::class);
    }
}
