<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property integer $client_id
 * @property integer $external_id
 * @property integer $external_order_id
 * @property integer $bundle_id
 * @property integer $quantity
 * @property integer $prime_cost
 * @property integer $price
 * @property integer $net_profit
 *
 * @property integer $referral_user_id
 * @property float $referral_amount
 *
 * @property-read Bundle $bundle
 *
 * @mixin Builder
 */
class ExternalOrderItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'external_orders_items';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'client_id',
        'external_id',
        'external_order_id',

        'bundle_id',

        'quantity',
        'prime_cost',
        'price',
        'net_profit',
    ];

    protected $casts = [
        'id' => 'integer',
        'client_id' => 'integer',
        'external_id' => 'integer',
        'external_order_id' => 'integer',

        'bundle_id' => 'integer',

        'quantity' => 'integer',
        'prime_cost' => 'integer',
        'price' => 'integer',
        'net_profit' => 'integer',
    ];

    public function externalOrder(): BelongsTo
    {
        return $this->belongsTo(ExternalOrder::class, 'external_order_id', 'external_id');
    }

    public function bundle(): BelongsTo
    {
        return $this->belongsTo(Bundle::class);
    }
}
