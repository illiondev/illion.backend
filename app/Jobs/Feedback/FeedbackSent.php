<?php

namespace App\Jobs\Feedback;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;

abstract class FeedbackSent extends Mailable implements ShouldQueue
{
    use Queueable;

    public $section;
    public $name;
    public $phone;
    public $text;

    public function __construct(string $name, string $phone, string $text)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->text = $text;

        $this->subject('Illion. Письмо из раздела "' . $this->section . '" от пользователя "' . $this->name . '"');
    }

    public function build()
    {
        return $this->view('emails.feedback.deal');
    }
}
