<?php

namespace App\Jobs\Feedback;

class SupportSent extends FeedbackSent
{
    public $section = 'Обратиться в поддержку';
}
