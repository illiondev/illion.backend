<?php

namespace App\Jobs;

use App\Models\OrderBill;
use App\Services\OrderBillService;

class YandexCheckPaymentBillJob extends Job
{
    protected $orderBill;

    public function __construct(OrderBill $orderBill)
    {
        $this->orderBill = $orderBill;
    }

    public function handle(OrderBillService $orderBillService): void
    {
        $orderBillService->checkPayment($this->orderBill);
    }
}
