<?php

namespace App\Jobs\Sendpulse;

use App\Jobs\Job;
use App\Models\Order;
use App\Services\SendpulseService;
use Cache;

class SendPulseSendSuccess extends Job
{
    protected $order;
    protected $redisKey;

    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->redisKey = 'sendpulse_' . $order->email;
    }

    public function handle(): void
    {
        if (!app()->environment('production')) {
            return;
        }

        if (Cache::lock($this->redisKey, 60 * 10)->block(5)) {
            try {
                $sendpulseService = app()->make(SendpulseService::class);
                $sendpulseService->addEmailToSuccess($this->order, $this->order->email);
            } finally {
                Cache::lock($this->redisKey)->release();
            }
        }
    }
}
