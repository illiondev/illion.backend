<?php

namespace App\Jobs\Sendpulse;

use App\Jobs\Job;
use App\Models\Order;
use App\Services\SendpulseService;
use Cache;

class SendPulseSendFail extends Job
{
    protected $order;
    protected $redisKey;

    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->redisKey = 'sendpulse_' . $order->email;
    }

    public function handle(): void
    {
        if (!app()->environment('production')) {
            return;
        }

        if (!in_array($this->order->status, [Order::STATUS_NEW, Order::STATUS_WAITING_FOR_PAYMENT])) {
            return;
        }

        if (Cache::lock($this->redisKey, 60 * 10)->get()) {
            try {
                if (in_array($this->order->status, [Order::STATUS_NEW, Order::STATUS_WAITING_FOR_PAYMENT])) {
                    $sendpulseService = app()->make(SendpulseService::class);
                    $sendpulseService->addEmailToFail($this->order, $this->order->email);
                }
            } finally {
                Cache::lock($this->redisKey)->release();
            }
        }
    }
}
