<?php

namespace App\Jobs\Sendpulse;

use App\Jobs\Job;
use App\Services\SendpulseService;

class SendPulseSubscribe extends Job
{
    protected $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public function handle(): void
    {
        if (!app()->environment('production')) {
            return;
        }

        $sendpulseService = app()->make(SendpulseService::class);
        $sendpulseService->subscribe($this->email);
    }
}
