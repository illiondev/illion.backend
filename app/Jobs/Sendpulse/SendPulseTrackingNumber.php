<?php

namespace App\Jobs\Sendpulse;

use App\Jobs\Job;
use App\Services\SendpulseService;

class SendPulseTrackingNumber extends Job
{
    protected $email;
    protected $trackingNumber;
    protected $sentDate;

    public function __construct(string $email, string $trackingNumber, string $sentDate)
    {
        $this->email = $email;
        $this->trackingNumber = $trackingNumber;
        $this->sentDate = $sentDate;
    }

    public function handle(): void
    {
        if (!app()->environment('production')) {
            return;
        }

        $sendpulseService = app()->make(SendpulseService::class);
        $sendpulseService->track($this->email, $this->trackingNumber, $this->sentDate);
    }
}
