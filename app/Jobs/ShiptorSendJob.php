<?php

namespace App\Jobs;

use App\Models\Order;
use App\Services\OrderService;
use Cache;

class ShiptorSendJob extends Job
{
    protected $order;
    protected $redisKey;

    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->redisKey = 'shiptor_' . $order->id;
    }

    public function handle(OrderService $orderService): void
    {
        if ($this->order->shiptor_id) {
            return;
        }

        if ($this->order->status !== Order::STATUS_PROCESSING) {
            return;
        }

        if (Cache::lock($this->redisKey, 60 * 10)->get()) {
            try {
                $this->order = $this->order->fresh();

                if (!$this->order->shiptor_id) { // еще не отправлен?
                    if ($this->order->status === Order::STATUS_PROCESSING) { // оплачен
                        $orderService->send($this->order);
                    }
                }
            } finally {
                Cache::lock($this->redisKey)->release();
            }
        }
    }
}
