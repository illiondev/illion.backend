<?php

namespace App\Jobs;

use App\Models\Order;
use App\Services\ShiptorService;
use Cache;
use ShiptorRussiaApiClient\Client\Core\Response\ErrorResponse;
use ShiptorRussiaApiClient\Client\Shiptor;

class ShiptorEditJob extends Job
{
    protected $order;
    protected $redisKey;

    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->redisKey = 'shiptor_edit2_' . $order->id;
    }

    public function handle(ShiptorService $shiptorService): void
    {
        // todo doesnt working after bundles update
        return;
        $order = $this->order;
        $paperProducts = $order->getPaperProducts();

        $export = !in_array($order->country_code, ['RU', 'KZ', 'BY']);

        if (Cache::has($this->redisKey)) {
            return;
        }

        $shiptor = new Shiptor(['API_KEY' => getenv('SHIPTOR_API_KEY')]);
        $request = $shiptor->ShippingEndpoint();

        if ($export) {
            $request = $request->editPackageExport();
        } else {
            $request = $request->editPackage();
        }

        $request->setId($order->shiptor_id);

        if ($export) {
            $request->setAddressLine2($order->address2);

            $request
                ->setReciever($order->name . ' ' . $order->surname . ' ' . $order->patronimic);
        } else {
            $request = $request
                ->setKladrId($order->kladr_id)
                ->setAdditionalService('additional-pack')
                ->setCashlessPayment($order->payment_method === Order::PAYMENT_METHOD_CARD);

            $request
                ->setName($order->name)
                ->setSurname($order->surname)
                ->setPatronimic($order->patronimic);

            $request->newService()->setShopArticle('10')->setCount(1)->setPrice($order->delivery_price);

            $request->setDeclaredCost($paperProducts->count() * 290);

            if ($order->isPaymentOnline()) {
                $request->setCod(0); // сумма наложенного платежа
            } else {
                $request->setCod($order->total_price); // сумма наложенного платежа
            }
        }

        $request
            ->setExternalId('illion_' . $order->id)// номер заказа (order id)
            ->setShippingMethod($order->shipping_method)// способ доставки
            ->setComment($order->comment)
            ->setEmail($order->email)
            ->setPhone($order->phone)
            ->setCountryCode($order->country_code)
            ->setRegion($order->region)
            ->setSettlement($order->settlement)
            ->setAddressLine($order->address)
            ->setPostalCode($order->postal_code);

        if ($order->delivery_point) {
            $request->setDeliveryPoint($order->delivery_point);
        }

        $dops = [];
        foreach ($paperProducts as $paperProduct) {
            $product = $request->newProduct();

            if ($export) {
                $product->setEnglishName($paperProduct->article);
            }

            $product
                ->setShopArticle($paperProduct->article)
                ->setCount($paperProduct->quantity)
                ->setPrice(290);

            if ($paperProduct->article === '007') {
                $dops['S1'] = $paperProduct->quantity;
                $dops['S2'] = $paperProduct->quantity;
            }

            if ($paperProduct->article === '008') {
                $dops['SS1'] = $paperProduct->quantity;
                $dops['SS2'] = $paperProduct->quantity;
            }
        }

        foreach ($dops as $key => $value) {
            $product = $request->newProduct();

            if ($export) {
                $product->setEnglishName($key);
            }

            $product
                ->setShopArticle($key)
                ->setCount($value)
                ->setPrice(0);
        }

        $dimensions = $shiptorService->calculateDimensions($paperProducts);

        $request->setLength($dimensions['length'])
            ->setWidth($dimensions['width'])
            ->setHeight($dimensions['height'])
            ->setWeight($order->weight);

        $request->validate();

        $result = $request->send();

        if ($result instanceof ErrorResponse) {
            app('sentry')->captureMessage($result->getMessage() . ' | eo:' . $order->id);

            return;
        }

        Cache::set($this->redisKey, 1, 60 * 24);
    }
}
