<?php

namespace App\Jobs;

use App\Models\Order;
use App\Services\OrderService;

class YandexCheckPaymentJob extends Job
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function handle(OrderService $orderService): void
    {
        $orderService->checkPayment($this->order);
    }
}
