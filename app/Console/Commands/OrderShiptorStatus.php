<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Services\OrderService;
use App\Services\ShiptorService;
use Illuminate\Console\Command;

class OrderShiptorStatus extends Command
{
    protected $shiptorService;
    protected $orderService;

    /**
     * Count Chunk
     */
    const LIMIT = 500;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:shiptor_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update shiptor status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        ShiptorService $shiptorService,
        OrderService $orderService
    ) {
        $this->shiptorService = $shiptorService;
        $this->orderService = $orderService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::where(function ($query): void {
            $query->whereNotIn('shipping_status', [Order::SHIPPING_STATUS_DELIVERED]);
            $query->orWhereNull('shipping_status');
        })
            ->whereNotNull('shiptor_id')
            ->limit(self::LIMIT)
            ->inRandomOrder()
            ->get();

        foreach ($orders as $order) {
            $info = $this->shiptorService->getInfo($order);
            if ($info) {
                $this->orderService->setDeliveryStatus($order, $info);
            }
            sleep(1);
        }
    }
}
