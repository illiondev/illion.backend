<?php

namespace App\Console\Commands;

use App\Jobs\YandexCheckPaymentJob;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Console\Command;

class OrderStatus extends Command
{
    protected $orderService;

    /**
     * Count Chunk
     */
    const COUNT_CHUNK = 100;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update order\'s statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        OrderService $orderService
    )
    {
        $this->orderService = $orderService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Order::where([
            'status' => Order::STATUS_PROCESSING,
            'shipping_status' => Order::SHIPPING_STATUS_DELIVERED,
        ])->update(['status' => Order::STATUS_COMPLETED]);

        Order::where([
            'status' => Order::STATUS_WAITING_FOR_PAYMENT,
        ])->chunk(
            self::COUNT_CHUNK,
            function ($orders): void {
                foreach ($orders as $order) {
                    dispatch(new YandexCheckPaymentJob($order));
                }
            }
        );
    }
}
