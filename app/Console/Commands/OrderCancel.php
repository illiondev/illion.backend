<?php

namespace App\Console\Commands;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;

class OrderCancel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel old orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tokenExpire = (Carbon::now())->addMinutes(Order::LIFETIME_MINUTES);

        Order::where([
            'status' => Order::STATUS_NEW,
        ])
        ->where('created_at', '>', $tokenExpire)
        ->update(['status' => Order::STATUS_CANCELED]);
    }
}
