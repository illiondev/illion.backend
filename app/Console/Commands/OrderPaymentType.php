<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Services\YandexMoneyService;
use Illuminate\Console\Command;

class OrderPaymentType extends Command
{
    protected $yandexMoneyService;

    /**
     * Count Chunk
     */
    const LIMIT = 50;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:payment_type';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update payment type';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(YandexMoneyService $yandexMoneyService) {
        $this->yandexMoneyService = $yandexMoneyService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var Order[] $orders */
        $orders = Order::whereNull('payment_type')
            ->whereIn('status', [Order::STATUS_PROCESSING, Order::STATUS_COMPLETED])
            ->limit(self::LIMIT)
            ->get();

        foreach ($orders as $order) {
            $this->yandexMoneyService->setAuthFromOrder($order);

            $paymentInfo = $this->yandexMoneyService->getPaymentInfo($order->payment_id);

            if ($paymentInfo) {
                $order->payment_type = $paymentInfo->getPaymentMethod()->getType();
                $order->saveOrFail();
            }
        }
    }
}
