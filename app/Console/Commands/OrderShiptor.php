<?php

namespace App\Console\Commands;

use App\Jobs\ShiptorSendJob;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Console\Command;

class OrderShiptor extends Command
{
    protected $orderService;

    /**
     * Count Chunk
     */
    const COUNT_CHUNK = 100;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:shiptor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send to shiptor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        OrderService $orderService
    ) {
        $this->orderService = $orderService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Order::where([
            'status' => Order::STATUS_PROCESSING,
        ])
            ->whereNull('shiptor_id')
            ->chunk(
                self::COUNT_CHUNK,
                function ($orders): void {
                    foreach ($orders as $order) {
                        dispatch(new ShiptorSendJob($order));
                    }
                }
            );
    }
}
