<?php

namespace App\Console\Commands;

use App\Jobs\YandexCheckPaymentBillJob;
use App\Models\OrderBill;
use App\Services\OrderBillService;
use Illuminate\Console\Command;

class BillStatus extends Command
{
    protected $billService;

    /**
     * Count Chunk
     */
    const COUNT_CHUNK = 100;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bill:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update bill\'s statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        OrderBillService $billService
    )
    {
        $this->billService = $billService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        OrderBill::where([
            'status' => OrderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT,
        ])->chunk(
            self::COUNT_CHUNK,
            function ($bills): void {
                foreach ($bills as $bill) {
                    dispatch(new YandexCheckPaymentBillJob($bill));
                }
            }
        );
    }
}
