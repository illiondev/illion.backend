<?php

namespace App\Console;

use App\Console\Commands\BillStatus;
use App\Console\Commands\OrderCancel;
use App\Console\Commands\OrderPaymentType;
use App\Console\Commands\OrderShiptor;
use App\Console\Commands\OrderShiptorStatus;
use App\Console\Commands\OrderStatus;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        OrderCancel::class,
        OrderStatus::class,
        OrderShiptor::class,
        OrderPaymentType::class,
        OrderShiptorStatus::class,
        BillStatus::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('order:cancel')->withoutOverlapping()->hourly();
        $schedule->command('order:status')->withoutOverlapping()->everyTenMinutes()->environments(['production']);
        $schedule->command('order:shiptor')->withoutOverlapping()->everyTenMinutes()->environments(['production']);
        $schedule->command('order:payment_type')->withoutOverlapping()->everyTenMinutes()->environments(['production']);
        $schedule->command('order:shiptor_status')->withoutOverlapping()->everyThirtyMinutes()->environments(['production']);

        $schedule->command('bill:status')->withoutOverlapping()->everyTenMinutes()->environments(['production']);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
