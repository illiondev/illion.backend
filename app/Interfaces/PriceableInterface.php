<?php

namespace App\Interfaces;

use App\Models\Price;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;

/**
 * @property-read Price[]|Collection
 */
interface PriceableInterface
{
    /**
     * @return MorphMany
     */
    public function prices();
}
