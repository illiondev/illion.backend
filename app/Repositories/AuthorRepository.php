<?php

namespace App\Repositories;

use App\Models\Author;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class AuthorRepository
{
    protected $model;

    public function __construct(Author $model)
    {
        $this->model = $model;
    }

    protected function query(): Builder
    {
        return $this->model
            ->orderBy('name', 'asc');
    }

    public function all(): Collection
    {
        return $this
            ->query()
            ->get();
    }
}
