<?php

namespace App\Repositories;

use App\Models\Book;
use App\Models\Bundle;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redis;

class BookRepository
{
    const CACHE_KEY = 'books_cache_';
    const CACHE_TIME = 60;

    protected $model;

    public function __construct(Book $model)
    {
        $this->model = $model;
    }

    protected function query(): Builder
    {
        return $this->model
            ->whereHas('bundles', function ($query): void {
                $query->where(['bundles.enabled' => 1, 'bundles.hidden' => 0]);
            });
    }

    public function getNew(Carbon $carbon): Collection
    {
        return $this
            ->query()
            ->orderBy('priority', 'desc')
            ->where('updated_at', '>', $carbon)
            ->get();
    }

    public function findOrFail(int $id): Book
    {
        /** @var Book $book */
        $book = $this->query()->findOrFail($id);

        return $book;
    }

    public function findFromHash(string $hash): Book
    {
        $id = Redis::get($this->model::REDIS_BOOK_PREVIEW_KEY_ . $hash);

        if (!$id) {
            throw new ModelNotFoundException();
        }

        /** @var Book $book */
        $book = $this->query()->findOrFail($id);

        $book->setRelation('bundles', Bundle::where([
            'book_id' => $book->id,
            'hidden' => 0,
        ])->get());

        return $book;
    }
}
