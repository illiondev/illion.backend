<?php

namespace App\Repositories;

use App\Models\Vacancy;
use Illuminate\Support\Collection;

class VacancyRepository
{
    protected $model;

    public function __construct(Vacancy $model)
    {
        $this->model = $model;
    }

    public function findOrFail(int $id): Vacancy
    {
        return $this->model->where(['id' => $id, 'enabled' => true])->firstOrFail();
    }

    public function all(): Collection
    {
        return $this->model->where(['enabled' => true])->orderBy('priority', 'desc')->get();
    }
}
