<?php

namespace App\Repositories;

use App\Models\PreferenceOption;

class PreferenceOptionRepository
{
    protected $model;

    public function __construct(PreferenceOption $model)
    {
        $this->model = $model;
    }

    //TODO: cache
    public function find(int $preferenceId, int $id): ?PreferenceOption
    {
        return $this->model->where([
            'preference_id' => $preferenceId,
            'id' => $id,
        ])->first();
    }
}
