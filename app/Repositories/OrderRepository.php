<?php

namespace App\Repositories;

use App\Models\Bundle;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderProduct;
use App\Models\Promocode;
use App\Models\User;
use App\Services\PromocodeService;
use App\Services\ShiptorService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use App\Models\Redis\Cart as RedisCart;

class OrderRepository
{
    /** @var Order|Builder */
    protected $model;

    public function __construct(Order $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $userId
     * @param array $params
     * @param Collection|OrderItem[] $orderItems
     * @param Promocode $promocode
     * @return Order
     * @throws \Throwable
     */
    public function create(int $userId, array $params, Collection $orderItems, Promocode $promocode = null): Order
    {
        $shiptorService = app(ShiptorService::class);

        /** @var Order $order */
        $order = new $this->model();

        $order->user_id = $userId;
        $order->payment_method = $params['payment_method'] ?? null;

        $order->name = $params['name'] ?? null;
        $order->surname = $params['surname'] ?? null;
        $order->patronimic = $params['patronimic'] ?? '';
        $order->comment = $params['comment'] ?? '';
        $order->system_comment = $params['system_comment'] ?? '';
        $order->email = $params['email'] ?? null;
        $order->phone = $params['phone'] ?? null;
        $order->shipping_method = $params['shipping_method'] ?? null;
        $order->delivery_point = $params['delivery_point'] ?? null;
        $order->country_code = $params['country_code'] ?? null;

        $order->region = $params['region'] ?? null;
        $order->settlement = $params['settlement'] ?? null;

        $order->address = $params['address'] ?? null;
        $order->address2 = $params['address2'] ?? null;
        $order->postal_code = $params['postal_code'] ?? null;
        $order->kladr_id = $params['kladr_id'] ?? null;

        $order->status = Order::STATUS_NEW;
        $order->price = $order->getPrice($orderItems);

        $paperProducts = $order->getPaperProducts($orderItems);

        if ($paperProducts->count() > 0) {
            $dimensions = $shiptorService->calculateDimensions($orderItems);

            $order->length = $dimensions['length'];
            $order->width = $dimensions['width'];
            $order->height = $dimensions['height'];
            $order->weight = $order->getWeight($orderItems);
            $order->delivery_price = $shiptorService->calculateShipping($order, $orderItems);
        }

        $order->total_price = $order->price + $order->delivery_price;

        if ($promocode) {
            $promocodeService = app(PromocodeService::class);
            $promocodeService->applyPromocodeToOrder($promocode, $order);
        }

        $order->saveOrFail();

        foreach ($orderItems as $orderItem) {
            if ($orderItem->bundle_id == 15 && !$order->external_kassa) {
                $order->external_kassa = 1;
                $order->save();
            }

            $orderItem->order_id = $order->id;
            $orderItem->saveOrFail();
        }

        $this->generateOrderProducts($order, $orderItems);

        return $order;
    }

    public function createInstant(User $user, Bundle $bundle): Order
    {
        $order = new Order();
        $order->user_id = $user->id;
        $order->payment_method = Order::PAYMENT_METHOD_ONLINE;

        $order->email = $user->email;
        $order->phone = $user->phone;

        $orderItem = RedisCart::createOrderItem($bundle, 1);

        $order->price = $orderItem->price * $orderItem->quantity;
        $order->total_price = $orderItem->price * $orderItem->quantity;

        $order->status = $order::STATUS_NEW;
        $order->saveOrFail();

        $orderItem->order_id = $order->id;
        $orderItem->saveOrFail();

        $this->generateOrderProducts($order, collect([$orderItem]));

        return $order;
    }

    /**
     * @param Collection|OrderItem[] $orderItems
     * @throws \Throwable
     */
    protected function generateOrderProducts(Order $order, Collection $orderItems)
    {
        // todo optimization
        foreach ($orderItems as $orderItem) {
            foreach ($orderItem->bundle->products as $product) {

                $orderProduct = new OrderProduct();
                $orderProduct->order_id = $orderItem->order_id;
                $orderProduct->order_item_id = $orderItem->id;
                $orderProduct->bundle_id = $orderItem->bundle_id;
                $orderProduct->quantity = $orderItem->quantity;

                $orderProduct->book_id = $product->book_id;
                $orderProduct->product_id = $product->id;
                $orderProduct->prime_cost = $product->prime_cost;
                $orderProduct->article = $product->article;
                $orderProduct->book_title = $product->book->title; // todo performance
                $orderProduct->type = $product->type;
                $orderProduct->length = $product->length;
                $orderProduct->width = $product->width;
                $orderProduct->height = $product->height;
                $orderProduct->weight = $product->weight;
                $orderProduct->is_surprise = $product->pivot->is_surprise;
                $orderProduct->authors_percent = $product->pivot->authors_percent;
                $orderProduct->price = $product->pivot->price;
                $orderProduct->author_id = $product->book->author_id; // todo performance

                $orderProduct->saveOrFail();
            }
        }
    }

    /**
     * @param int $userId
     * @return Collection|Order[]
     */
    public function allByUserId(int $userId): Collection
    {
        $orders = $this->model
            ->where([
                'user_id' => $userId,
            ])
            ->get();

        return $orders;
    }

    public function findOrFail(int $userId, int $id, int $status = null): Order
    {
        $order = $this->model
            ->where([
                'id' => $id,
                'user_id' => $userId,
            ]);

        if ($status !== null) {
            $order = $order->where(['status' => $status]);
        }

        return $order->firstOrFail();
    }

    public function findOrFailNewNotExpired(int $userId, int $id): Order
    {
        $tokenExpire = (Carbon::now())->subMinutes(Order::LIFETIME_MINUTES);

        $order = $this->model
            ->where([
                'id' => $id,
                'user_id' => $userId,
                'status' => Order::STATUS_NEW,
            ])
            ->where('created_at', '>', $tokenExpire);


        return $order->firstOrFail();
    }

    public function findOrFailAnonymous(int $id, int $status = null): Order
    {
        $order = $this->model
            ->where([
                'id' => $id,
            ]);

        if ($status !== null) {
            $order = $order->where(['status' => $status]);
        }

        return $order->firstOrFail();
    }

    public function findOrFailNewNotExpiredAnonymous(int $id): Order
    {
        $tokenExpire = (Carbon::now())->subMinutes(Order::LIFETIME_MINUTES);

        $order = $this->model
            ->where([
                'id' => $id,
                'status' => Order::STATUS_NEW,
            ])
            ->where('created_at', '>', $tokenExpire);


        return $order->firstOrFail();
    }
}
