<?php

namespace App\Repositories;

use App\Models\Preference;
use Illuminate\Support\Collection;

class PreferenceRepository
{
    protected $model;

    public function __construct(Preference $model)
    {
        $this->model = $model;
    }

    public function all(): Collection
    {
        return $this->model->get();
    }
}
