<?php

namespace App\Repositories;

use App\Models\OrderBill;
use Illuminate\Database\Eloquent\Builder;

class OrderBillRepository
{
    protected $model;

    public function __construct(OrderBill $model)
    {
        $this->model = $model;
    }

    protected function findByHash(string $hash): Builder
    {
        return $this->model->where(['hash' => $hash]);
    }

    public function findOrFail(string $hash): OrderBill
    {
        /** @var OrderBill $bill */
        $bill = $this->findByHash($hash)->firstOrFail();

        return $bill;
    }

    public function findOrFailNew(string $hash): OrderBill
    {
        /** @var OrderBill $bill */
        $bill = $this->findByHash($hash)
            ->whereIn('status', [OrderBill::PAYMENT_STATUS_NEW])
            ->firstOrFail();

        return $bill;
    }

    public function findOrFailWaitingForPayment(string $hash): OrderBill
    {
        /** @var OrderBill $bill */
        $bill = $this
            ->findByHash($hash)
            ->whereIn('status', [OrderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT])
            ->firstOrFail();

        return $bill;
    }
}
