<?php

namespace App\Repositories;

use App\Models\Announcement;

class AnnouncementRepository
{
    protected $model;

    public function __construct(Announcement $model)
    {
        $this->model = $model;
    }

    public function firstOrNew(): Announcement
    {
        $announcement = $this->model->first();

        if (!$announcement) {
            $announcement = new $this->model();
            $announcement->enabled = false;
        }

        return $announcement;
    }
}
