<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Modules\Moderator\Models\Soon;

class SoonRepository
{
    protected $model;

    public function __construct(Soon $model)
    {
        $this->model = $model;
    }

    protected function queryAll(): Builder
    {
        return $this->model->where(['enabled' => true])->orderBy('id', 'desc');
    }

    public function all(): Collection
    {
        return $this->queryAll()->get();
    }
}
