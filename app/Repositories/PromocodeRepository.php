<?php

namespace App\Repositories;

use App\Models\Promocode;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class PromocodeRepository
{
    protected $model;

    public function __construct(Promocode $model)
    {
        $this->model = $model;
    }

    protected function query(string $code): Builder
    {
        return $this->model->where([
            'code' => $code,
        ])->where(function ($query): void {
            $query->where('expired_at', '>', Carbon::now());
            $query->orWhereNull('expired_at');
        })->where(function ($query): void {
            $query->whereColumn('limit', '>', 'used');
            $query->orWhere(['limit' => 0]);
        });
    }

    public function first(string $code): ?Promocode
    {
        return $this->query($code)->first();
    }
}
