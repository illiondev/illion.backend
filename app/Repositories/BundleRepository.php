<?php

namespace App\Repositories;

use App\Models\Bundle;
use Illuminate\Database\Eloquent\Builder;

class BundleRepository
{
    protected $model;

    public function __construct(Bundle $model)
    {
        $this->model = $model;
    }

    protected function query(): Builder
    {
        return $this->model
            ->where([
                'enabled' => true,
            ]);
    }

    public function findOrFail(int $id): Bundle
    {
        /** @var Bundle $bundle */
        $bundle = $this->query()->findOrFail($id);

        return $bundle;
    }

    public function findOrFailBySlug(string $slug): Bundle
    {
        /** @var Bundle $bundle */
        $bundle = $this
            ->query()
            ->where([
                'slug' => $slug,
            ])
            ->firstOrFail();

        return $bundle;
    }
}
