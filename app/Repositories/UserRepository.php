<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function findOrFail(int $userId): User
    {
        return $this->model->findOrFail($userId);
    }

    public function findByExternalId(int $externalId): ?User
    {
        return $this->model->where(['external_id' => $externalId])->first();
    }
}
