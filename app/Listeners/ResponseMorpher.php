<?php

namespace App\Listeners;

use Dingo\Api\Event\ResponseWasMorphed;

class ResponseMorpher
{
    const STATUS_SUCCESS = 'success'; // 200
    const STATUS_FAIL = 'fail'; // 400 client fails
    const STATUS_ERROR = 'error'; // 500 server error

    public function handle(ResponseWasMorphed $event): void
    {
        switch (true) {
            case $event->response->isSuccessful():
                $response['status'] = self::STATUS_SUCCESS;

                break;
            case $event->response->isClientError():
                $response['status'] = self::STATUS_FAIL;

                break;
            case $event->response->isServerError():
            default:
                $response['status'] = self::STATUS_ERROR;
                if (app()->environment('production')) {
                    $response['message'] = trans('exception.internal_error');
                }

                break;
        }

        $event->content = array_merge((array) $event->content, $response);
    }
}
