<?php

namespace App\Http\Requests\V1;

use App\Models\Order;
use Dingo\Api\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @OA\RequestBody(
 *     request="OrderSubmitRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/OrderSubmitRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"payment_method"},
 *     schema="OrderSubmitRequest",
 *     type="object",
 * )
 */
class OrderSubmitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="payment_method",
             *      type="integer",
             * )
             */
            'payment_method'  => ['required', 'integer', Rule::in([
                Order::PAYMENT_METHOD_ONLINE,
                Order::PAYMENT_METHOD_GOOGLE_PLAY,
            ])],
            /**
             * @OA\Property(
             *      property="name",
             *      type="string",
             * )
             */
            'name'  => 'required_with:shipping_method',
            /**
             * @OA\Property(
             *      property="surname",
             *      type="string",
             * )
             */
            'surname'  => 'required_with:shipping_method',
            /**
             * @OA\Property(
             *      property="patronimic",
             *      type="string",
             * )
             */
            'patronimic'  => '',
            /**
             * @OA\Property(
             *      property="comment",
             *      type="string",
             * )
             */
            'comment'  => 'max:255',
            /**
             * @OA\Property(
             *      property="system_comment",
             *      type="string",
             * )
             */
            'system_comment'  => 'max:255',
            /**
             * @OA\Property(
             *      property="email",
             *      type="string",
             * )
             */
            'email'  => 'email|required',
            /**
             * @OA\Property(
             *      property="phone",
             *      type="string",
             * )
             */
            'phone'  => '',
            /**
             * @OA\Property(
             *      property="shipping_method",
             *      type="integer",
             * )
             */
            'shipping_method'  => 'integer',
            /**
             * @OA\Property(
             *      property="country_code",
             *      type="string",
             * )
             */
            'country_code'  => 'required_with:shipping_method',
            /**
             * @OA\Property(
             *      property="region",
             *      type="string",
             * )
             */
            'region'  => '',
            /**
             * @OA\Property(
             *      property="settlement",
             *      type="string",
             * )
             */
            'settlement'  => '',
            /**
             * @OA\Property(
             *      property="address",
             *      type="string",
             * )
             */
            'address'  => 'max:255',
            /**
             * @OA\Property(
             *      property="address2",
             *      type="string",
             * )
             */
            'address2'  => 'max:50',
            /**
             * @OA\Property(
             *      property="postal_code",
             *      type="string",
             * )
             */
            'postal_code'  => '',
            /**
             * @OA\Property(
             *      property="kladr_id",
             *      type="string",
             * )
             */
            'kladr_id'  => '',
            /**
             * @OA\Property(
             *      property="delivery_point",
             *      type="integer",
             * )
             */
            'delivery_point'  => 'integer',
            /**
             * @OA\Property(
             *      property="hash",
             *          type="string",
             * )
             */
            'hash'    => 'required|min:30',
        ];
    }
}
