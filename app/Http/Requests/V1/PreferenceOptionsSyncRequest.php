<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="PreferenceOptionsSyncRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/PreferenceOptionsSyncRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"ids"},
 *     schema="PreferenceOptionsSyncRequest",
 *     type="object",
 * )
 */
class PreferenceOptionsSyncRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="ids",
             *      type="integer",
             *      description=""
             * )
             */
            'ids' => 'array|filled',
            'ids.*' => 'required|distinct|integer',
        ];
    }
}
