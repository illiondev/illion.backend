<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="AuthRegisterRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/AuthRegisterRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"name", "email", "password", "password_confirmation"},
 *     schema="AuthRegisterRequest",
 *     type="object",
 * )
 */
class AuthRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="name",
             *          type="string",
             * )
             */
            'name' => 'string|max:255',
            /**
             * @OA\Property(
             *      property="email",
             *          type="string",
             * )
             */
            'email' => 'required|string|email|max:255|unique:users',
            /**
             * @OA\Property(
             *      property="password",
             *          type="string",
             *          minLength=6,
             * )
             */
            /**
             * @OA\Property(
             *      property="password_confirmation",
             *          type="string",
             *          minLength=6,
             * )
             */
            'password' => 'required|string|min:6|confirmed',
        ];
    }
}
