<?php

namespace App\Http\Requests\V1;

use App\Models\Profile;
use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="MediaUploadRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/MediaUploadRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"media"},
 *     schema="MediaUploadRequest",
 *     type="object",
 * )
 */
class MediaUploadRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="media",
             *     type="string",
             *     format="binary",
             * )
             */
            'media' => 'required|file|dimensions:min_width='
                . Profile::MEDIA_MIN_WIDTH
                . ',min_height='
                . Profile::MEDIA_MIN_HEIGHT
                . '|mimes:jpg,jpeg,png|mimetypes:'
                . implode(',', Profile::MEDIA_ACCEPTABLE_MIME),
        ];
    }
}
