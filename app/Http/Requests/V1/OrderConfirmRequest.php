<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="OrderConfirmRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/OrderConfirmRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={},
 *     schema="OrderConfirmRequest",
 *     type="object",
 * )
 */
class OrderConfirmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="token",
             *      type="string",
             * )
             */
            'token'  => '',
        ];
    }
}
