<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="OrderCalculateShippingRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/OrderCalculateShippingRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"shipping_method", "country_code", "kladr_id"},
 *     schema="OrderCalculateShippingRequest",
 *     type="object",
 * )
 */
class OrderCalculateShippingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="shipping_method",
             *      type="integer",
             * )
             */
            'shipping_method'  => 'required|integer',
            /**
             * @OA\Property(
             *      property="country_code",
             *      type="string",
             * )
             */
            'country_code'  => 'required',
            /**
             * @OA\Property(
             *      property="kladr_id",
             *      type="string",
             * )
             */
            'kladr_id'  => '',
            /**
             * @OA\Property(
             *      property="hash",
             *      type="string",
             * )
             */
            'hash'    => 'required|min:30',
        ];
    }
}
