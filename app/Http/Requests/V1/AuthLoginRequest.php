<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="AuthLoginRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/AuthLoginRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"email", "password"},
 *     schema="AuthLoginRequest",
 *     type="object",
 * )
 */
class AuthLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="email",
             *          type="string",
             * )
             */
            'email'    => 'required|email',
            /**
             * @OA\Property(
             *      property="password",
             *          type="string",
             *          minLength=6,
             * )
             */
            'password' => 'required|min:6',
            /**
             * @OA\Property(
             *      property="remember_me",
             *         type="integer",
             *         enum={0, 1},
             * )
             */
            'remember_me' => 'boolean',
        ];
    }
}
