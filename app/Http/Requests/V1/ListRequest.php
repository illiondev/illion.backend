<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="ListRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/ListRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="ListRequest",
 *     type="object",
 * )
 */
class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="timestamp",
             *          type="integer",
             * )
             */
            'timestamp'    => 'integer',
        ];
    }
}
