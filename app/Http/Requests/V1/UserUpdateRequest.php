<?php

namespace App\Http\Requests\V1;

use App\Models\Profile;
use Auth;
use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="UserUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/UserUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="UserUpdateRequest",
 *     type="object",
 * )
 */
class UserUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="email",
             *         type="string",
             * )
             */
            'email' => 'email|unique:users,email,' . Auth::id(),
            /**
             * @OA\Property(
             *      property="subscription_email",
             *         type="integer",
             *         enum={0, 1},
             * )
             */
            'subscription_email' => 'boolean',
            // profile
            /**
             * @OA\Property(
             *      property="name",
             *         type="string",
             * )
             */
            'name' => 'string|max:190',
            /**
             * @OA\Property(
             *      property="gender",
             *      description="
             * 0 - female
             * 1 - male",
             *         type="integer",
             *         enum={0, 1},
             * )
             */
            'gender' => 'integer|in:' . implode(',', [
                    Profile::GENDER_FEMALE, Profile::GENDER_MALE
                ]),
            /**
             * @OA\Property(
             *      property="birthday",
             *      description="format: 1988-09-13",
             *         type="string",
             * )
             */
            'birthday' => 'date',
            /**
             * @OA\Property(
             *      property="city_id",
             *         type="integer",
             * )
             */
            'city_id' => 'integer',
        ];
    }
}
