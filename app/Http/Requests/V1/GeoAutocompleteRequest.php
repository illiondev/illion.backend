<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

class GeoAutocompleteRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'query' => 'required|string|min:3',
        ];
    }
}
