<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="PasswordForgotRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/PasswordForgotRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"email"},
 *     schema="PasswordForgotRequest",
 *     type="object",
 * )
 */
class PasswordForgotRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="email",
             *      type="string",
             * )
             */
            'email' => 'required|string',
        ];
    }
}
