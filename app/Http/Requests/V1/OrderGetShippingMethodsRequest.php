<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="OrderGetShippingMethodsRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/OrderGetShippingMethodsRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"country_code", "kladr_id"},
 *     schema="OrderGetShippingMethodsRequest",
 *     type="object",
 * )
 */
class OrderGetShippingMethodsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="country_code",
             *      type="string",
             * )
             */
            'country_code'  => 'required',
            /**
             * @OA\Property(
             *      property="kladr_id",
             *      type="string",
             * )
             */
            'kladr_id'  => '',
            /**
             * @OA\Property(
             *      property="hash",
             *      type="string",
             * )
             */
            'hash'    => 'required|min:30',
        ];
    }
}
