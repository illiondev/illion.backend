<?php

namespace App\Http\Requests\V1;

use App\Models\ExternalOrder;
use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="SyncStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/SyncStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"order"},
 *     schema="SyncStoreRequest",
 *     type="object",
 * )
 */
class SyncStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="order",
             *     type="object",
             *     required={"external_id", "status", "status", "sold_at", "items", "price", "delivery_price", "total_price", },
             *     description="Объект заказа",
             *     @OA\Property(
             *         property="external_id",
             *         description="ID заказа в с системе клиента",
             *         example="14",
             *         type="integer"
             *     ),
             *     @OA\Property(
             *         property="country_code",
             *         description="Код страны",
             *         example="RU",
             *         type="string"
             *     ),
             *     @OA\Property(
             *         property="region",
             *         description="Регион",
             *         example="Санкт-Петербург",
             *         type="string"
             *     ),
             *     @OA\Property(
             *         property="city",
             *         description="Город",
             *         example="Санкт-Петербург",
             *         type="string"
             *     ),
             *     @OA\Property(
             *         property="status",
             *         enum={3,4,5,7},
             *         description="
             * 3 - оплачен
             * 4 - выполнен, доставлен, завершен
             * 5 - отменен
             * 7 - возврат",
             *         example=4,
             *         type="integer"
             *     ),
             *     @OA\Property(
             *         property="sold_at",
             *         description="Когда была совершена продажа",
             *         example="2019-04-25 05:44:42",
             *         type="string"
             *     ),
             *     @OA\Property(
             *         property="price",
             *         description="Цена всех товаров",
             *         example="1000",
             *         type="number"
             *     ),
             *     @OA\Property(
             *         property="delivery_price",
             *         description="Цена доставки",
             *         example="100",
             *         type="number"
             *     ),
             *     @OA\Property(
             *         property="total_price",
             *         description="Общая стоимость",
             *         example="1100",
             *         type="number"
             *     ),
             *     @OA\Property(
             *         property="items",
             *         required={"external_id", "bundle_id", "quantity", "prime_cost", "price", "net_profit"},
             *         description="Товары в заказе",
             *         type="array",
             *         @OA\Items(
             *             @OA\Property(
             *                 property="external_id",
             *                 description="ID записи в с системе клиента",
             *                 example="1",
             *                 type="integer"
             *             ),
             *             @OA\Property(
             *                 property="bundle_id",
             *                 description="ID бандла",
             *                 example="22",
             *                 type="integer"
             *             ),
             *             @OA\Property(
             *                 property="quantity",
             *                 description="Количество товаров",
             *                 example="1",
             *                 type="integer"
             *             ),
             *             @OA\Property(
             *                 property="prime_cost",
             *                 description="Себестоимость одного товара в рублях",
             *                 example="99.14",
             *                 type="number"
             *             ),
             *             @OA\Property(
             *                 property="price",
             *                 description="Цена продажи за один товар в рублях",
             *                 example="299.99",
             *                 type="number"
             *             ),
             *             @OA\Property(
             *                 property="net_profit",
             *                 description="Цена продажи за один товар минус комиссия платежной системы, минус выплаты рефералам в рублях",
             *                 example="282.11",
             *                 type="number"
             *             ),
             *             @OA\Property(
             *                 property="referral_user_id",
             *                 description="Id юзера-реферала",
             *                 example="216",
             *                 type="integer"
             *             ),
             *             @OA\Property(
             *                 property="referral_amount",
             *                 description="Сумма выплаты рефералу",
             *                 example="282.11",
             *                 type="number"
             *             ),
             *         ),
             *     ),
             * )
             */
            /**
             */
            'order' => 'required',
            'order.external_id' => 'required|integer',
            'order.country_code' => 'string',
            'order.region' => 'string',
            'order.city' => 'string',
            'order.status' => 'required|integer|in:' . implode(',', ExternalOrder::getStatuses()),
            'order.price' => 'required|numeric',
            'order.delivery_price' => 'required|numeric',
            'order.total_price' => 'required|numeric',
            'order.sold_at' => 'required|date',
            'order.items' => 'array|filled',
            'order.items.*.external_id' => 'required|integer',
            'order.items.*.product_id' => 'integer',
            'order.items.*.bundle_id' => 'integer',
            'order.items.*.quantity' => 'required|integer',
            'order.items.*.prime_cost' => 'required|numeric',
            'order.items.*.price' => 'required|numeric',
            'order.items.*.net_profit' => 'required|numeric',
            'order.items.*.referral_user_id' => 'nullable|integer',
            'order.items.*.referral_amount' => 'nullable|numeric',
        ];
    }
}