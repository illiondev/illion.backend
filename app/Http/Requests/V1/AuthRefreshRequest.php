<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="AuthRefreshRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/AuthRefreshRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"refresh_token"},
 *     schema="AuthRefreshRequest",
 *     type="object",
 * )
 */
class AuthRefreshRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="refresh_token",
             *          type="string",
             * )
             */
            'refresh_token'    => 'required',
        ];
    }
}
