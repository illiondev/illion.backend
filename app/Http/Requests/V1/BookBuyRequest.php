<?php

namespace App\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookBuyRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookBuyRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"data", "signature"},
 *     schema="BookBuyRequest",
 *     type="object",
 * )
 */
class BookBuyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="data",
             *      type="string",
             * )
             */
            'data'    => 'required|string',
            /**
             * @OA\Property(
             *      property="password",
             *      type="string",
             *      minLength=6,
             * )
             */
            'signature' => 'required|string',
        ];
    }
}
