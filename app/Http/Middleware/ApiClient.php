<?php

namespace App\Http\Middleware;

use App\Models\ExternalClient;
use Closure;
use Illuminate\Support\Facades\Auth;

class ApiClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $client = ExternalClient::where(['token' => $request->header('Authorization')])->first();

        if (!$client) {
            return abort(401);
        }

        $request->attributes->add(['client' => $client]);

        return $next($request);
    }
}
