<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class StatusNotBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->isBlocked()) {
                return abort(403, 'blocked');
            }

            return $next($request);
        }

        return abort(401);
    }
}
