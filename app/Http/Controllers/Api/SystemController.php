<?php

namespace App\Http\Controllers\Api;

class SystemController extends ApiBaseController
{
    public function ping()
    {
        return $this->response->accepted(null, ['message' => 'pong!']);
    }
}
