<?php

namespace App\Http\Controllers\Api;

use App\Jobs\YandexCheckPaymentBillJob;
use App\Jobs\YandexCheckPaymentJob;
use App\Models\Order;
use App\Models\OrderBill;
use Dingo\Api\Http\Response;
use Exception;
use YandexCheckout\Model\Notification\NotificationSucceeded;
use YandexCheckout\Model\Notification\NotificationWaitingForCapture;
use YandexCheckout\Model\NotificationEventType;

class YandexKassaController extends ApiBaseController
{
    public function callback(): Response
    {
        $source = file_get_contents('php://input');
        $json = json_decode($source, true);

        switch ($json['event']) {
            case NotificationEventType::PAYMENT_SUCCEEDED:
                $notification = new NotificationSucceeded($json);
                break;
            case NotificationEventType::PAYMENT_WAITING_FOR_CAPTURE:
                $notification = new NotificationWaitingForCapture($json);
                break;
            default:
                throw new Exception('Unknown NotificationEventType ' . $json['event']);
        }

        $paymentInfo = $notification->getObject();
        $order = Order::where(['payment_id' => $paymentInfo->getId()])->first();

        if ($order) {
            dispatch(new YandexCheckPaymentJob($order));
        } else {
            $orderBill = OrderBill::where(['payment_id' => $paymentInfo->getId()])->first();

            if ($orderBill) {
                dispatch(new YandexCheckPaymentBillJob($orderBill));
            } else {
                app('sentry')->captureMessage('unknown payment_id ' . $paymentInfo->getId());
            }
        }

        return $this->response->accepted();
    }
}
