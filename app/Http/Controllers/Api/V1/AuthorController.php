<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Transformers\V1\AuthorTransformer;
use App\Repositories\AuthorRepository;
use Dingo\Api\Http\Response;

class AuthorController extends ApiController
{
    protected $authorRepository;

    public function __construct(
        AuthorRepository $authorRepository
    )
    {
        parent::__construct();

        $this->authorRepository = $authorRepository;
    }

    /**
     * @OA\Get(
     *     path="/authors",
     *     tags={"authors"},
     *     summary="List of authors",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Author")
     *         ),
     *     ),
     * )
     */
    public function index(): Response
    {
        $books = $this->authorRepository->all();

        return $this->response->collection($books, new AuthorTransformer());
    }
}
