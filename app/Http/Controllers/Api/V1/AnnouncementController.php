<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Transformers\V1\AnnouncementTransformer;
use App\Repositories\AnnouncementRepository;
use Dingo\Api\Http\Response;

class AnnouncementController extends ApiController
{
    protected $announcementRepository;

    public function __construct(AnnouncementRepository $announcementRepository)
    {
        parent::__construct();

        $this->announcementRepository = $announcementRepository;
    }

    /**
     * @OA\Get(
     *     path="/announcement",
     *     tags={"announcement"},
     *     summary="Show announcement",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Announcement"),
     *         )
     *     ),
     * )
     */
    public function show(): Response
    {
        $announcement = $this->announcementRepository->firstOrNew();

        return $this->response->item($announcement, new AnnouncementTransformer());
    }
}
