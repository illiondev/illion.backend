<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\UserUpdateRequest;
use App\Http\Transformers\V1\UserTransformer;
use App\Services\ProfileService;
use App\Services\UserService;
use Dingo\Api\Http\Response;

class UserController extends ApiController
{
    protected $userService;
    protected $profileService;
    protected $illionUserService;

    public function __construct(
        \Illion\Service\UserService $illionUserService,
        UserService $userService,
        ProfileService $profileService
    ) {
        parent::__construct();

        $this->illionUserService = $illionUserService;
        $this->userService = $userService;
        $this->profileService = $profileService;
    }

    /**
     * @OA\Get(
     *     path="/profile/me",
     *     tags={"profile"},
     *     summary="Current user info",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/User"),
     *         )
     *     ),
     * )
     */
    public function show(): Response
    {
        return $this->response->item($this->user, new UserTransformer());
    }

    /**
     * @OA\Put(
     *     path="/profile/me",
     *     tags={"profile"},
     *     summary="Update current user info",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/UserUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/User"),
     *         )
     *     ),
     * )
     */
    public function update(UserUpdateRequest $request): Response
    {
        $attributes = $request->validated();
        $token = $request->bearerToken();

        $illionUser = $this->illionUserService->update($token, $attributes);

        $this->userService->update($this->user, $attributes, $illionUser);
        $this->profileService->update($this->user->profile, $attributes, $illionUser);

        return $this->response->item($this->user, new UserTransformer());
    }
}
