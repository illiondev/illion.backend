<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\ListRequest;
use App\Http\Transformers\V1\BookshelfProductTransformer;
use App\Http\Transformers\V1\BookshelfTransformer;
use App\Models\Product;
use Carbon\Carbon;
use Dingo\Api\Http\Request;
use Dingo\Api\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Illuminate\Support\Facades\Redis;
use Validator;

class BookshelfController extends ApiController
{
    const REDIS_RAW_DOWNLOAD_KEY_ = 'book_raw_download_key_';
    const REDIS_RAW_DOWNLOAD_KEY_EXPIRE = 60 * 5; // seconds, 5 мин

    /**
     * @OA\Get(
     *     path="/profile/me/books",
     *     tags={"bookshelf"},
     *     summary="List of user's books",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/Bookshelf")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(ListRequest $listRequest): Response
    {
        $timestamp = $listRequest->input('timestamp', 0);

        $carbon = new Carbon();
        $carbon->setTimestamp($timestamp);

        $time = time();

        $books = $this->user->ownedBooks()
                ->where('user_has_books.updated_at', '>', $carbon)
                ->get();

        return $this->response->collection($books, new BookshelfTransformer())->setMeta(['timestamp' => $time]);
    }

    /**
     * @OA\Get(
     *     path="/profile/me/books/{id}",
     *     tags={"bookshelf"},
     *     summary="Show a book",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Bookshelf"),
     *         )
     *     ),
     * )
     */
    public function show(int $id): Response
    {
        $book = $this->user->ownedBooks()->findOrFail($id);

        return $this->response->item($book, new BookshelfTransformer());
    }

    /**
     * @OA\Get(
     *     path="profile/me/products/{productId}",
     *     tags={"bookshelf"},
     *     summary="Download a book",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="products's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/epub+zip"
     *         )
     *     ),
     * )
     */
    public function download(int $productId): BinaryFileResponse
    {
        $product = $this->user->ownedProducts()->findOrFail($productId);

        $publishedBook = $product->book->publishedEbook;

        $headers = ['Content-Type: application/epub+zip'];

        return response()->file(
            Storage::disk('books')->path($publishedBook->filename),
            $headers
        );
    }

    public function downloadKey(int $productId): Response
    {
        $product = $this->user->ownedProducts()->findOrFail($productId);

        $key = Str::random(32);

        Redis::set(self::REDIS_RAW_DOWNLOAD_KEY_ . $key, $product->id);
        Redis::expire(self::REDIS_RAW_DOWNLOAD_KEY_ . $key, self::REDIS_RAW_DOWNLOAD_KEY_EXPIRE);

        return $this->response->array(['key' => $key]);
    }


    public function downloadRaw(string $key, Request $request): StreamedResponse
    {
        $format = $request->input('format', 'epub');

        Validator::make(['format' => $format], [
            'format' => 'required|in:epub,pdf',
        ])->validate();

        $productId = Redis::get(self::REDIS_RAW_DOWNLOAD_KEY_ . $key);

        if (!$productId) {
            throw new ModelNotFoundException();
        }

        /** @var Product $product */
        $product = Product::findOrFail($productId);

        switch ($format) {
            case 'pdf':
                $headers = ['Content-Type: application/pdf'];
                return Storage::disk('local')->download($product->pdf, Str::slug($product->book->title) . '.pdf', $headers);
            default:
                $headers = ['Content-Type: application/epub+zip'];
                return Storage::disk('local')->download($product->epub, Str::slug($product->book->title) . '.epub', $headers);
        }
    }

    /**
     * @OA\Get(
     *     path="/books/{id}/products/{id}",
     *     tags={"bookshelf"},
     *     summary="Product",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="products's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/BookshelfProduct"),
     *         )
     *     ),
     * )
     */
    public function productShow(int $id, int $productId): Response
    {
        $product = $this->user->ownedProducts()->findOrFail($productId);

        return $this->response->item($product, new BookshelfProductTransformer());
    }

    /**
     * @OA\Get(
     *     path="/profile/me/products/{slug}/show",
     *     tags={"bookshelf"},
     *     summary="Product",
     *     description="",
     *     @OA\Parameter(
     *         name="slug",
     *         in="path",
     *         description="product's article",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="solodar"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/BookshelfProduct"),
     *         )
     *     ),
     * )
     */
    public function productShowByArticle(string $article): Response
    {
        $product = $this->user->ownedProducts()->where(['article' => $article])->firstOrFail();

        return $this->response->item($product, new BookshelfProductTransformer());
    }

    /**
     * @OA\Get(
     *     path="/profile/me/books/{slug}/download",
     *     tags={"bookshelf"},
     *     summary="Download a book",
     *     description="",
     *     @OA\Parameter(
     *         name="slug",
     *         in="path",
     *         description="product's article",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="solodar"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/epub+zip"
     *         )
     *     ),
     * )
     */
    public function downloadByArticle(string $article): BinaryFileResponse
    {
        $product = $this->user->ownedProducts()->where(['article' => $article])->firstOrFail();

        $publishedBook = $product->book->publishedEbook;

        $headers = ['Content-Type: application/epub+zip'];

        return response()->file(
            Storage::disk('books')->path($publishedBook->filename),
            $headers
        );
    }
}
