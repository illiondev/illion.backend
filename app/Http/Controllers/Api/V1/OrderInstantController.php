<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Transformers\V1\InstantOrderTransformer;
use App\Http\Transformers\V1\OrderTransformer;
use App\Jobs\Sendpulse\SendPulseSendFail;
use App\Models\Order;
use App\Repositories\BundleRepository;
use App\Repositories\OrderRepository;
use App\Services\OrderService;
use Dingo\Api\Http\Response;

class OrderInstantController extends ApiController
{
    protected $orderService;
    protected $orderRepository;
    protected $bundleRepository;

    public function __construct(
        OrderService $orderService,
        OrderRepository $orderRepository,
        BundleRepository $bundleRepository
    ) {
        parent::__construct();

        $this->orderService = $orderService;
        $this->orderRepository = $orderRepository;
        $this->bundleRepository = $bundleRepository;
    }

    /**
     * @OA\Get(
     *     path="/instant_orders/submit/{slug}",
     *     tags={"instant order"},
     *     summary="Create new order",
     *     description="",
     *     @OA\Parameter(
     *         name="slug",
     *         in="path",
     *         description="product's article",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="solodar"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Order"),
     *         )
     *     ),
     * )
     */
    public function submit(string $slug)
    {
        $bundle = $this->bundleRepository->findOrFailBySlug($slug);
        $order = $this->orderRepository->createInstant($this->user, $bundle);

        dispatch(new SendPulseSendFail($order));

        return $this->response()->item($order, new OrderTransformer());
    }

    /**
     * @OA\Get(
     *     path="/instant_orders/submit/{orderId}",
     *     tags={"instant order"},
     *     summary="Confirm order",
     *     description="",
     *     @OA\Parameter(
     *         name="orderId",
     *         in="path",
     *         description="order's id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function confirm(int $orderId): Response
    {
        $order = $this->orderRepository->findOrFailNewNotExpiredAnonymous($orderId);

        $result = $this->orderService->process($order, '', true);

        return $this->response->array($result);
    }

    /**
     * @OA\Get(
     *     path="/instant_orders/{order id}/payed",
     *     tags={"instant order"},
     *     summary="Check order status",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="order's id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/InstantOrder"),
     *         )
     *     ),
     * )
     */
    public function checkPayment(int $orderId): Response
    {
        $order = $this->orderRepository->findOrFailAnonymous($orderId);

        $order = $this->orderService->checkPayment($order);

        return $this->response()->item($order, new InstantOrderTransformer());
    }

    /**
     * @OA\Post(
     *     path="/instant_orders/{order id}/confirmation_url",
     *     tags={"order's bill"},
     *     summary="Pay order bill",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="order's id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Items(
     *                 @OA\Property(
     *                     property="preview_hash",
     *                     description="",
     *                     example="",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="preview_hash_expire",
     *                     description="",
     *                     example="",
     *                     type="string"
     *                 ),
     *             ),
     *         )
     *     ),
     * )
     */
    public function getConfirmationUrl(int $orderId): Response
    {
        $order = $this->orderRepository->findOrFailAnonymous($orderId, Order::STATUS_WAITING_FOR_PAYMENT);

        $result = $this->orderService->getConfirmation($order);

        return $this->response->array($result);
    }

}