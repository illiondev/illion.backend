<?php

namespace App\Http\Controllers\Api\V1;

use App\Jobs\Feedback\DealSent;
use App\Jobs\Feedback\QuestionSent;
use App\Jobs\Feedback\SupportSent;
use App\Jobs\Sendpulse\SendPulseSubscribe;
use Illuminate\Http\Request;
use Mail;

class FeedbackController extends ApiController
{
    /**
     * авторам
     * @see https://houseofillion.com/publication
     */
    public function deal(Request $request)
    {
        $request->validate([
            'name' => 'string',
            'email' => 'required|email',
            'text' => 'string',
        ]);

        $name = $request->input('name', '');
        $email = $request->input('email');
        $text = $request->input('text', '');

        Mail::to(getenv('DEALS_EMAIL'))
            ->queue(new DealSent($name, $email, $text));

        return $this->response->accepted();
    }

    /**
     * faq
     * @see https://houseofillion.com/faq
     */
    public function support(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email',
            'text' => 'required|string',
        ]);

        $name = $request->input('name');
        $email = $request->input('email');
        $text = $request->input('text');

        Mail::to(getenv('DEALS_EMAIL'))
            ->queue(new SupportSent($name, $email, $text));

        return $this->response->accepted();
    }

    /**
     * задать вопрос в поддержку
     * @see https://houseofillion.com/contacts
     */
    public function question(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'subject' => 'required|string',
            'text' => 'required|string',
        ]);

        $email = $request->input('email');
        $subject = $request->input('subject');
        $text = $request->input('text');

        Mail::to(getenv('DEALS_EMAIL'))
            ->queue(new QuestionSent($email, $subject, $text));

        return $this->response->accepted();
    }

    public function subscribe(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);

        $email = $request->input('email');

        dispatch(new SendPulseSubscribe($email));

        return $this->response->accepted();
    }
}
