<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\ListRequest;
use App\Http\Transformers\V1\BookTransformer;
use App\Repositories\BookRepository;
use Carbon\Carbon;
use Dingo\Api\Http\Response;

class BookController extends ApiController
{
    protected $bookRepository;

    public function __construct(
        BookRepository $bookRepository
    )
    {
        parent::__construct();

        $this->bookRepository = $bookRepository;
    }

    /**
     * @OA\Get(
     *     path="/books",
     *     tags={"books"},
     *     summary="List of books",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/Book")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(ListRequest $listRequest): Response
    {
        $timestamp = $listRequest->input('timestamp', 0);
        $carbon = new Carbon();
        $carbon->setTimestamp($timestamp);
        $time = time();

        $books = $this->bookRepository->getNew($carbon);

        return $this->response->collection($books, new BookTransformer())->setMeta(['timestamp' => $time]);
    }

    /**
     * @OA\Get(
     *     path="/books/{id}",
     *     tags={"books"},
     *     summary="Single book",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Book"),
     *         )
     *     ),
     * )
     */
    public function show(int $id): Response
    {
        $book = $this->bookRepository->findOrFail($id);

        return $this->response->item($book, new BookTransformer());
    }

    /**
     * @OA\Get(
     *     path="/books/{hash}/preview",
     *     tags={"books"},
     *     summary="Single book preview",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Book"),
     *         )
     *     ),
     * )
     */
    public function preview(string $hash): Response
    {
        $book = $this->bookRepository->findFromHash($hash);

        return $this->response->item($book, new BookTransformer());
    }
}
