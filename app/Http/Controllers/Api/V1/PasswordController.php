<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\PasswordForgotRequest;
use Dingo\Api\Http\Response;
use Illion\Service\UserService;

class PasswordController extends ApiController
{
    protected $illionUserService;

    public function __construct(
        UserService $illionUserService
    )
    {
        parent::__construct();

        $this->illionUserService = $illionUserService;
    }

    /**
     * @OA\Post(
     *     path="/auth/password/forgot",
     *     tags={"auth"},
     *     summary="Send email with code for restore password",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/PasswordForgotRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function forgot(PasswordForgotRequest $passwordForgotRequest): Response
    {
        $email = $passwordForgotRequest->input('email');

        $this->illionUserService->passwordForgot($email);

        return $this->response->accepted();
    }
}
