<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\GeoAutocompleteRequest;
use App\Http\Transformers\V1\CityTransformer;
use Dingo\Api\Http\Request;
use Dingo\Api\Http\Response;
use Illion\Service\LocationService;

class LocationController extends ApiController
{
    protected $locationService;

    public function __construct(LocationService $locationService)
    {
        parent::__construct();

        $this->locationService = $locationService;
    }

    /**
     * @OA\Get(
     *     path="/geo",
     *     tags={"geo"},
     *     summary="Detect user city by ip address",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/City"),
     *         )
     *     ),
     * )
     */
    public function geoInfo(Request $request): Response
    {
        $city = $this->locationService->detectCity($request->ip(), app()->getLocale());

        if (!$city) {
            return $this->response->accepted();
        }

        return $this->response->item($city, new CityTransformer());
    }

    /**
     * @OA\Get(
     *     path="/geo/autocomplete/city",
     *     tags={"geo"},
     *     summary="Autocomplete user's city",
     *     description="",
     *     @OA\Parameter(
     *         name="query",
     *         in="query",
     *         description="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="санкт"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/City"),
     *         )
     *     ),
     * )
     */
    public function cityAutocomplete(GeoAutocompleteRequest $request): Response
    {
        $cities = $this->locationService->autocompleteCity($request->input('query'), app()->getLocale());

        return $this->response->collection(collect($cities), new CityTransformer());
    }
}
