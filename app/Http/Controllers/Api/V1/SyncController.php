<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\SyncStoreRequest;
use App\Http\Transformers\V1\ExternalOrderTransformer;
use App\Services\SyncService;
use Dingo\Api\Http\Response;

class SyncController extends ApiController
{
    protected $syncService;

    public function __construct(
        SyncService $syncService
    )
    {
        parent::__construct();

        $this->syncService = $syncService;
    }

    /**
     * @OA\Post(
     *     path="/sync",
     *     tags={"sync"},
     *     summary="Store or update external order",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/SyncStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/ExternalOrder"),
     *         )
     *     ),
     *     security={
     *         {"ApiKeyAuth": {}}
     *     }
     * )
     */
    public function sync(SyncStoreRequest $request): Response
    {
        $order = $this->syncService->sync($request->get('client'), $request->input('order'));

        return $this->response->item($order, new ExternalOrderTransformer());
    }
}
