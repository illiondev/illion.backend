<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\OrderCalculateShippingRequest;
use App\Http\Requests\V1\OrderGetShippingMethodsRequest;
use App\Models\Order;
use App\Services\ShiptorService;
use Dingo\Api\Http\Response;
use App\Models\Redis\Cart as RedisCart;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ShiptorController extends ApiController
{
    protected $shiptorService;

    public function __construct(
        ShiptorService $shiptorService
    ) {
        parent::__construct();

        $this->shiptorService = $shiptorService;
    }

    public function getCountries()
    {
        $countries = $this->shiptorService::COUNTRIES;
        asort($countries, SORT_STRING );

        return $this->response->array(['data' => $countries]);
    }

    public function getExportShippingMethods(OrderGetShippingMethodsRequest $request): Response
    {
        $cart = new RedisCart($request->input('hash'));
        $orderItems = $cart->items();

        if (count($orderItems) === 0) {
            throw new BadRequestHttpException('Cart is empty');
        }

        $paperProducts = (new Order())->getPaperProducts($orderItems);

        $getShippingMethods = [];
        if ($paperProducts->count() > 0) {
            $order = new Order();
            $order->country_code = $request->input('country_code');

            $dimensions = $this->shiptorService->calculateDimensions($orderItems);

            $order->length = $dimensions['length'];
            $order->width = $dimensions['width'];
            $order->height = $dimensions['height'];
            $order->weight = (new Order())->getWeight($orderItems);

            $getShippingMethods = $this->shiptorService->getExportShippingMethods($order, $orderItems);
        }

        return $this->response->array(['data' => $getShippingMethods]);
    }

    public function calculateShipping(OrderCalculateShippingRequest $request): Response
    {
        $cart = new RedisCart($request->input('hash'));
        $orderItems = $cart->items();

        if (count($orderItems) === 0) {
            throw new BadRequestHttpException('Cart is empty');
        }

        $paperProducts = (new Order())->getPaperProducts($orderItems);

        $deliveryPrice = 0;
        if ($paperProducts->count() > 0) {
            $order = new Order();
            $order->shipping_method = $request->input('shipping_method');
            $order->country_code = $request->input('country_code');
            $order->kladr_id = $request->input('kladr_id');

            $dimensions = $this->shiptorService->calculateDimensions($orderItems);

            $order->length = $dimensions['length'];
            $order->width = $dimensions['width'];
            $order->height = $dimensions['height'];
            $order->weight = (new Order())->getWeight($orderItems);

            $deliveryPrice = $this->shiptorService->calculateShipping($order, $orderItems);
        }

        return $this->response->array(['delivery_price' => $deliveryPrice]);
    }
}
