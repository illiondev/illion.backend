<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\MediaUploadRequest;
use App\Http\Transformers\V1\MediaTransformer;
use App\Models\Profile;
use Dingo\Api\Http\Response;

class MediaController extends ApiController
{

    /**
     * @OA\Post(
     *     path="/profile/me/media",
     *     tags={"media"},
     *     summary="Upload media file",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/MediaUploadRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Media"),
     *         )
     *     ),
     * )
     */
    public function upload(MediaUploadRequest $request): Response
    {
        $file = $request->file('media');

        /** @var Profile $profile */
        $profile = $this->user->profile;

        $media = $profile
            ->addMedia($file)
            ->usingFileName(uniqid() . '.' . $file->getClientOriginalExtension())
            ->toMediaCollection(Profile::MEDIA_CATEGORY_AVATAR);

        return $this->response->item($media, new MediaTransformer())->setStatusCode(201);
    }

    /**
     * @OA\Delete(
     *     path="/profile/me/media",
     *     tags={"media"},
     *     summary="Delete media",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function delete(): Response
    {
        /** @var Profile $profile */
        $profile = $this->user->profile;

        $avatar = $profile->getFirstMedia(Profile::MEDIA_CATEGORY_AVATAR);

        $avatar->delete();

        return $this->response->accepted();
    }
}
