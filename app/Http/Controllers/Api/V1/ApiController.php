<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiBaseController;

/**
 * @OA\OpenApi(
 *     security={
 *         {"bearerAuth": {}}
 *     }
 * )
 */
/**
 * @OA\Info(
 *   title="House Of Illion api",
 *   version="1.0.0",
 * )
 */
/**
 * @OA\SecurityScheme(
 *     @OA\Flow(
 *         flow="password",
 *         tokenUrl="auth/login",
 *         refreshUrl="auth/refresh",
 *         scopes={}
 *     ),
 *     in="header",
 *     securityScheme="bearerAuth",
 *     name="bearerAuth",
 *     type="http",
 *     scheme="bearer",
 *     bearerFormat="JWT",
 * ),
 * @OA\SecurityScheme(
 *     in="header",
 *     securityScheme="ApiKeyAuth",
 *     name="Authorization ",
 *     type="apiKey",
 * ),
 */
class ApiController extends ApiBaseController
{
    public function __construct()
    {
        parent::__construct();
    }
}
