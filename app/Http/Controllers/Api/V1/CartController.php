<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\CartRequest;
use App\Http\Transformers\V1\CartTransformer;
use Dingo\Api\Http\Response;
use App\Models\Redis\Cart as RedisCart;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CartController extends ApiController
{
    public function index(CartRequest $request): Response
    {
        $cart = new RedisCart($request->input('hash'));

        return $this->response->item($cart, new CartTransformer());
    }

    public function set(int $id, int $quantity, CartRequest $request): Response
    {
        $cart = new RedisCart($request->input('hash'));
        $cart->set($id, $quantity);

        return $this->response->item($cart, new CartTransformer());
    }

    public function flush(CartRequest $request): Response
    {
        $cart = new RedisCart($request->input('hash'));
        $cart->flush();

        return $this->response->item($cart, new CartTransformer());
    }

    public function setPromocode(CartRequest $request): Response
    {
        $cart = new RedisCart($request->input('hash'));
        $request->validate([
            'promocode' => 'required|string',
        ]);
        $code = $request->input('promocode');

        if (!$cart->setPromocode($code)) {
            throw new NotFoundHttpException('Промокод не найден или срок его действия истек');
        }

        return $this->response->item($cart, new CartTransformer());
    }
}
