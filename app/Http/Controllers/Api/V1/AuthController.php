<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\AuthLoginRequest;
use App\Http\Requests\V1\AuthRefreshRequest;
use App\Http\Requests\V1\AuthRegisterFastRequest;
use App\Http\Requests\V1\AuthRegisterRequest;
use App\Http\Transformers\V1\TokenTransformer;
use App\Repositories\UserRepository;
use App\Services\ProfileService;
use App\Services\UserService;
use Auth;
use Dingo\Api\Http\Response;
use Illion\Service\AuthService;
use Illion\Service\TokenService;
use Illuminate\Http\Request;

class AuthController extends ApiController
{
    protected $illionAuthService;
    protected $illionUserService;
    protected $userService;
    protected $userRepository;
    protected $profileService;

    public function __construct(
        AuthService $illionAuthService,
        \Illion\Service\UserService $illionUserService,
        UserService $userService,
        UserRepository $userRepository,
        ProfileService $profileService
    )
    {
        parent::__construct();

        $this->illionAuthService = $illionAuthService;
        $this->illionUserService = $illionUserService;
        $this->userService = $userService;
        $this->profileService = $profileService;
        $this->userRepository = $userRepository;
    }

    /**
     * @OA\Post(
     *     path="/auth/login",
     *     tags={"auth"},
     *     summary="Get token",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/AuthLoginRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Token"),
     *         ),
     *     ),
     *     security={
     *     }
     * )
     */
    public function login(AuthLoginRequest $authLoginRequest): Response
    {
        $email = $authLoginRequest->input('email');
        $password = $authLoginRequest->input('password');
        $rememberMe = (bool) $authLoginRequest->input('remember_me');

        $token = $this->illionAuthService->attemptLogin($email, $password, $rememberMe);

        $tokenService = new TokenService($token->access_token);
        $user = $this->userRepository->findByExternalId($tokenService->getUserId());

        if (!$user) {
            $illionUser = $this->illionUserService->show($token->access_token);

            $user = $this->userService->create(
                $illionUser->name,
                $illionUser->email,
                $illionUser->id
            );

            $this->profileService->update($user->profile, [], $illionUser);
        }

        return $this->response->item($token, new TokenTransformer());
    }

    /**
     * @OA\Post(
     *     path="/auth/register",
     *     tags={"auth"},
     *     summary="Register new user",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/AuthRegisterRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Token"),
     *         ),
     *     ),
     * )
     */
    public function register(AuthRegisterRequest $authRegisterRequest): Response
    {
        $name = $authRegisterRequest->input('name');
        $email = $authRegisterRequest->input('email');
        $password = $authRegisterRequest->input('password');
        $passwordConfirmation = $authRegisterRequest->input('password_confirmation');

        $user = $this->illionUserService->register($name, $email, $password, $passwordConfirmation);

        if ($user) {
            $this->userService->create(
                $user->name,
                $user->email,
                $user->id
            );
        }

        $token = $this->illionAuthService->attemptLogin($email, $password, true);

        return $this->response->item($token, new TokenTransformer());
    }
    /**
     * @OA\Post(
     *     path="/auth/fast_register",
     *     tags={"auth"},
     *     summary="Register new user",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/AuthRegisterFastRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Token"),
     *         ),
     *     ),
     * )
     */
    public function fastRegister(AuthRegisterFastRequest $authRegisterRequest): Response
    {
        $name = $authRegisterRequest->input('name');
        $email = $authRegisterRequest->input('email');
        $password = str_random(8);

        $user = $this->illionUserService->register($name, $email, $password, $password);

        if ($user) {
            $this->userService->create(
                $user->name,
                $user->email,
                $user->id
            );
        }

        $token = $this->illionAuthService->attemptLogin($email, $password, true);

        return $this->response->item($token, new TokenTransformer());
    }

    /**
     * @OA\Post(
     *     path="/auth/refresh",
     *     tags={"auth"},
     *     summary="Refresh token",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/AuthRefreshRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Token"),
     *         ),
     *     ),
     * )
     */
    public function refresh(AuthRefreshRequest $authRefreshRequest): Response
    {
        $refreshToken = $authRefreshRequest->input('refresh_token');
        $token = $this->illionAuthService->attemptRefresh($refreshToken);

        return $this->response->item($token, new TokenTransformer());
    }

    /**
     * @OA\Post(
     *     path="/auth/logout",
     *     tags={"auth"},
     *     summary="Invalidate token",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function logout(Request $request): Response
    {
        $value = $request->bearerToken();

        $this->illionAuthService->logout($value);

        Auth::guard('api')->logout();

        return $this->response()->accepted();
    }
}
