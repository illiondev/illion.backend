<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\PreferenceOptionsSyncRequest;
use App\Repositories\PreferenceOptionRepository;
use Dingo\Api\Http\Response;

class PreferenceOptionController extends ApiController
{
    protected $preferenceOptionRepository;

    public function __construct(
        PreferenceOptionRepository $preferenceOptionRepository
    )
    {
        parent::__construct();

        $this->preferenceOptionRepository = $preferenceOptionRepository;
    }

    /**
     * @OA\Post(
     *     path="/profile/me/preferences/{preference id}/options/sync",
     *     tags={"preferences"},
     *     summary="Sync preference options to user's profile",
     *     description="",
     *     @OA\Parameter(
     *         name="preferenceId",
     *         in="path",
     *         description="preference id",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="1"
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/PreferenceOptionsSyncRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function sync(int $preferenceId, PreferenceOptionsSyncRequest $preferencesSyncRequest): Response
    {
        $ids = $preferencesSyncRequest->input('ids');

        $data = [];
        foreach ($ids as $id) {
            $preferenceOption = $this->preferenceOptionRepository->find($preferenceId, $id);

            if ($preferenceOption) {
                $data[$preferenceOption->id] = ['preference_id' => $preferenceOption->preference_id];
            }
        }

        $this->user->profile
            ->preferenceOptions()
            ->wherePivot('preference_id', '=', $preferenceId)
            ->sync($data);

        return $this->response->accepted();
    }
}
