<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Transformers\V1\SoonTransformer;
use App\Repositories\SoonRepository;
use Dingo\Api\Http\Response;

class SoonController extends ApiController
{
    protected $soonRepository;

    public function __construct(
        SoonRepository $soonRepository
    ) {
        parent::__construct();

        $this->soonRepository = $soonRepository;
    }

    /**
     * @OA\Get(
     *     path="/soon",
     *     tags={"soon"},
     *     summary="List of soon's",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/Soon")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(): Response
    {
        $soon = $this->soonRepository->all();

        return $this->response->collection($soon, new SoonTransformer());
    }
}
