<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Transformers\V1\PreferenceTransformer;
use App\Repositories\PreferenceOptionRepository;
use App\Repositories\PreferenceRepository;
use Dingo\Api\Http\Response;

class PreferenceController extends ApiController
{
    protected $preferenceRepository;
    protected $preferenceOptionRepository;

    public function __construct(
        PreferenceRepository $preferenceRepository,
        PreferenceOptionRepository $preferenceOptionRepository
    ) {
        parent::__construct();

        $this->preferenceRepository = $preferenceRepository;
        $this->preferenceOptionRepository = $preferenceOptionRepository;
    }

    /**
     * @OA\Get(
     *     path="/preferences",
     *     tags={"preferences"},
     *     summary="List of preferences",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/Preference")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(): Response
    {
        $preferences = $this->preferenceRepository->all();

        return $this->response->collection($preferences, new PreferenceTransformer());
    }

}
