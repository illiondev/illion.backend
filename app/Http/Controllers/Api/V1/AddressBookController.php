<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Transformers\V1\AddressBookTransformer;
use App\Models\Order;
use Dingo\Api\Http\Response;

class AddressBookController extends ApiController
{
    public function getDefault(): Response
    {
        $order = Order::where([
            'user_id' => $this->user->id,
        ])
            ->latest()
            ->first();

        return $this->response()->item($order ?? new Order(), new AddressBookTransformer());
    }
}
