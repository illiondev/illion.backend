<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Transformers\V1\VacancyTransformer;
use App\Repositories\VacancyRepository;
use Dingo\Api\Http\Response;

class VacancyController extends ApiController
{
    protected $vacancyRepository;

    public function __construct(VacancyRepository $vacancyRepository)
    {
        parent::__construct();

        $this->vacancyRepository = $vacancyRepository;
    }

    /**
     * @OA\Get(
     *     path="/vacancies/{vacancy_id}",
     *     tags={"vacancies"},
     *     summary="Show vacancy",
     *     description="",
     *     @OA\Parameter(
     *         name="vacancyId",
     *         in="path",
     *         description="vacancy's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Vacancy"),
     *         )
     *     ),
     * )
     */
    public function show(int $vacancyId): Response
    {
        $vacancy = $this->vacancyRepository->findOrFail($vacancyId);

        return $this->response->item($vacancy, new VacancyTransformer());
    }

    /**
     * @OA\Get(
     *     path="/vacancies",
     *     tags={"vacancies"},
     *     summary="Show vacancies",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Vacancy"),
     *         )
     *     ),
     * )
     */
    public function index(): Response
    {
        $vacancies = $this->vacancyRepository->all();

        return $this->response->collection($vacancies, new VacancyTransformer());
    }
}
