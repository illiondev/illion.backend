<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\V1\OrderConfirmRequest;
use App\Http\Requests\V1\OrderSubmitRequest;
use App\Http\Transformers\V1\OrderTransformer;
use App\Jobs\Sendpulse\SendPulseSendFail;
use App\Jobs\YandexCheckPaymentJob;
use App\Models\Order;
use App\Repositories\OrderRepository;
use App\Services\OrderService;
use Dingo\Api\Http\Response;
use App\Models\Redis\Cart as RedisCart;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class OrderController extends ApiController
{
    protected $shiptorService;
    protected $orderRepository;
    protected $orderService;
    protected $promocodeService;

    public function __construct(
        OrderRepository $orderRepository,
        OrderService $orderService
    ) {
        parent::__construct();

        $this->orderRepository = $orderRepository;
        $this->orderService = $orderService;
}

    public function index(): Response
    {
        $orders = $this->orderRepository->allByUserId($this->user->id);

        foreach ($orders as $order) {
            if ($order->status === Order::STATUS_WAITING_FOR_PAYMENT) {
                dispatch(new YandexCheckPaymentJob($order));
            }
        }

        return $this->response()->collection($orders, new OrderTransformer());
    }

    /**
     * @OA\Post(
     *     path="/orders/{id}/confirmation_url",
     *     tags={"order"},
     *     summary="Get confirmation url",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="order's id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Items(
     *                 @OA\Property(
     *                     property="preview_hash",
     *                     description="",
     *                     example="",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="preview_hash_expire",
     *                     description="",
     *                     example="",
     *                     type="string"
     *                 ),
     *             ),
     *         )
     *     ),
     * )
     */
    public function getConfirmationUrl(int $id): Response
    {
        $order = $this->orderRepository->findOrFail($this->user->id, $id, Order::STATUS_WAITING_FOR_PAYMENT);

        $result = $this->orderService->getConfirmation($order);

        return $this->response->array($result);
    }

    public function checkPayment(int $id): Response
    {
        $order = $this->orderRepository->findOrFail($this->user->id, $id);

        $order = $this->orderService->checkPayment($order);

        return $this->response()->item($order, new OrderTransformer());
    }

    public function submit(OrderSubmitRequest $request): Response
    {
        $cart = new RedisCart($request->input('hash'));
        $orderItems = $cart->items();
        $promocode = $cart->getPromocode();

        if (count($orderItems) === 0) {
            throw new BadRequestHttpException('Cart is empty');
        }

        $order = $this->orderRepository->create($this->user->id, $request->validated(), $orderItems, $promocode);

        dispatch(new SendPulseSendFail($order));

        $cart->flush();

        return $this->response()->item($order, new OrderTransformer());
    }

    public function confirm(int $id): Response
    {
        $order = $this->orderRepository->findOrFailNewNotExpired($this->user->id, $id);

        $result = $this->orderService->process($order);

        return $this->response->array($result);
    }

    public function confirmAndroid(int $id, OrderConfirmRequest $orderConfirmRequest): Response
    {
        $order = $this->orderRepository->findOrFailNewNotExpired($this->user->id, $id);

        $token = $orderConfirmRequest->input('token', '');

        $result = $this->orderService->process($order, $token);

        return $this->response->array($result);
    }
}