<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Transformers\V1\OrderBillTransformer;
use App\Jobs\YandexCheckPaymentBillJob;
use App\Models\OrderBill;
use App\Repositories\OrderBillRepository;
use App\Services\OrderBillService;
use Dingo\Api\Http\Response;

class OrderBillController extends ApiController
{
    protected $orderBillRepository;
    protected $orderBillService;

    public function __construct(
        OrderBillRepository $orderBillRepository,
        OrderBillService $orderBillService
    ) {
        parent::__construct();

        $this->orderBillRepository = $orderBillRepository;
        $this->orderBillService = $orderBillService;
    }

    /**
     * @OA\Get(
     *     path="/bills/{hash}",
     *     tags={"order's bill"},
     *     summary="Show order bill",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/OrderBill"),
     *         )
     *     ),
     * )
     */
    public function show(string $hash): Response
    {
        $orderBill = $this->orderBillRepository->findOrFail($hash);

        if ($orderBill->status === OrderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT) {
            dispatch(new YandexCheckPaymentBillJob($orderBill));
        }

        return $this->response()->item($orderBill, new OrderBillTransformer());
    }

    /**
     * @OA\Post(
     *     path="/bills/{hash}",
     *     tags={"order's bill"},
     *     summary="Pay order bill",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function confirm(string $hash): Response
    {
        $orderBill = $this->orderBillRepository->findOrFailNew($hash);

        $result = $this->orderBillService->process($orderBill);

        return $this->response->array($result);
    }

    /**
     * @OA\Post(
     *     path="/bills/{hash}/confirmation_url",
     *     tags={"order's bill"},
     *     summary="Get bill's confirmation url",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Items(
     *                 @OA\Property(
     *                     property="preview_hash",
     *                     description="",
     *                     example="",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="preview_hash_expire",
     *                     description="",
     *                     example="",
     *                     type="string"
     *                 ),
     *             ),
     *         )
     *     ),
     * )
     */
    public function getConfirmationUrl(string $hash): Response
    {
        $orderBill = $this->orderBillRepository->findOrFailWaitingForPayment($hash);

        $result = $this->orderBillService->getConfirmation($orderBill);

        return $this->response->array($result);
    }
}