<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Transformers\V1\BundleTransformer;
use App\Http\Transformers\V1\OrderBillTransformer;
use App\Repositories\BundleRepository;
use App\Services\OrderBillService;
use Dingo\Api\Http\Request;
use Dingo\Api\Http\Response;

class BundleController extends ApiController
{
    protected $bundleRepository;
    protected $orderBillService;

    public function __construct(
        BundleRepository $bundleRepository,
        OrderBillService $orderBillService
    )
    {
        parent::__construct();

        $this->bundleRepository = $bundleRepository;
        $this->orderBillService = $orderBillService;
    }

    /**
     * @OA\Get(
     *     path="/bundles/{id}",
     *     tags={"bundle"},
     *     summary="Show bundle",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Bundle"),
     *         )
     *     ),
     * )
     */
    public function show(int $id): Response
    {
        $bundle = $this->bundleRepository->findOrFail($id);

        $bundleTransformer = new BundleTransformer();
        $bundleTransformer->setDefaultIncludes([
            'prices',
        ]);

        return $this->response->item($bundle, $bundleTransformer);
    }

    /**
     * @OA\Post(
     *     path="/product/{bundleId}/bills/",
     *     tags={"order's bill"},
     *     summary="Generate order bill",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="bundle's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="int",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/OrderBill"),
     *         )
     *     ),
     * )
     */
    public function generate(int $bundleId, Request $request): Response
    {
        $request->validate([
            'email' => 'required|email',
        ]);

        $bundle = $this->bundleRepository->findOrFail($bundleId);

        $bill = $this->orderBillService->generate($bundle, $request->input('email'));

        return $this->response()->item($bill, new OrderBillTransformer());
    }
}
