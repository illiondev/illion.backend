<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Dingo\Api\Routing\Helpers;

/**
 * @property User $user
 */
abstract class ApiBaseController extends Controller
{
    use Helpers;

    const PAGE_SIZE = 15;

    public function __construct()
    {
    }
}
