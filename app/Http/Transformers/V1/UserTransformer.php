<?php

namespace App\Http\Transformers\V1;

use App\Models\Profile;
use App\Models\User;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="User",
 *     title="User",
 *     type="object",
 * )
 */
class UserTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'city',
        'preferenceOptions',
        'avatar',
    ];

    public function transform(User $user): array
    {
        $profile = $user->profile;

        return [
            /**
             * @OA\Property(
             *     property="status",
             *     type="integer",
             *     enum={1, 2, 3, 4},
             *     description="1 - new, 2 - registered, 3 - approved, 4 - blocked"),
             * )
             */
            'status'             => $user->status,
            /**
             * @OA\Property(
             *     property="email",
             *     type="string",
             * )
             */
            'email'              => $user->email,
            /**
             * @OA\Property(
             *     property="email_confirmed",
             *     type="boolean",
             * )
             */
            'email_confirmed'    => $user->isEmailConfirmed(),
            /**
             * @OA\Property(
             *     property="subscription_email",
             *     type="boolean",
             * )
             */
            'subscription_email'   => $user->subscription_email,
            /**
             * @OA\Property(
             *     property="agreements_accepted",
             *     type="boolean",
             * )
             */
            'agreements_accepted'   => $user->agreements_accepted,

            // profile

            /**
             * @OA\Property(
             *     property="name",
             *     description="User's full name",
             *     type="string",
             * )
             */
            'name'   => $profile->name,
            /**
             * @OA\Property(
             *      property="gender",
             *      type="integer",
             *      enum={0, 1, 2},
             *      description="0 - female, 1 - male, 2 - unknown"
             * )
             */
            'gender'   => ($profile->gender === null) ? Profile::GENDER_UNKNOWN : $profile->gender,
            /**
             * @OA\Property(
             *      property="birthday",
             *      type="string",
             *      description="format: 1988-09-13"
             * )
             */
            'birthday'   => $profile->birthday ? $profile->birthday->toDateString() : '',
        ];
    }

    /**
     * @OA\Property(
     *    property="city",
     *    description="Город",
     *    type="object",
     *    ref="#/components/schemas/City",
     * ),
     */
    public function includeCity(User $user): ?Item
    {
        if ($user->profile->city) {
            return $this->item($user->profile->city, new CityTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="preferenceOptions",
     *    description="Товары",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/PreferenceOption")
     * ),
     */
    public function includePreferenceOptions(User $user): ?Collection
    {
        return $this->collection($user->profile->preferenceOptions, new PreferenceOptionTransformer());
    }

    /**
     * @OA\Property(
     *    property="avatar",
     *    description="Аватар",
     *    type="object",
     *    ref="#/components/schemas/Media",
     * ),
     */
    public function includeAvatar(User $user): ?Item
    {
        $avatar = $user->profile->getFirstMedia(Profile::MEDIA_CATEGORY_AVATAR);

        if ($avatar) {
            return $this->item($avatar, new MediaTransformer());
        }

        return null;
    }
}
