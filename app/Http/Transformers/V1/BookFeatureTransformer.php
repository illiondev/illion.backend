<?php

namespace App\Http\Transformers\V1;

use App\Models\Feature;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="BookFeature",
 *     title="BookFeature",
 *     description="Особенности",
 *     type="object",
 * )
 */
class BookFeatureTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'image',
    ];

    public function transform(Feature $feature): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $feature->id,
            /**
             * @OA\Property(
             *     property="text",
             *     description="",
             *     type="string"
             * )
             */
            'text' => $feature->text,
        ];
    }

    /**
     * @OA\Property(
     *    property="image",
     *    description="Картинка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
    public function includeImage(Feature $feature): ?Item
    {
        $image = $feature->getFirstMedia($feature::MEDIA_CATEGORY_IMAGE);

        if ($image) {
            return $this->item($image, new MediaTransformer());
        }

        return null;
    }
}
