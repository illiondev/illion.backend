<?php

namespace App\Http\Transformers\V1;

use App\Models\Review;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="BookReview",
 *     title="BookReview",
 *     description="Отзывы",
 *     type="object",
 * )
 */
class BookReviewTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'image',
    ];

    public function transform(Review $review): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $review->id,
            /**
             * @OA\Property(
             *     property="name",
             *     description="",
             *     type="string"
             * )
             */
            'name' => $review->name,
            /**
             * @OA\Property(
             *     property="instagram",
             *     description="",
             *     type="string"
             * )
             */
            'instagram' => $review->instagram,
            /**
             * @OA\Property(
             *     property="text",
             *     description="",
             *     type="string"
             * )
             */
            'text' => $review->text,
        ];
    }

    /**
     * @OA\Property(
     *    property="image",
     *    description="Картинка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
    public function includeImage(Review $review): ?Item
    {
        $image = $review->getFirstMedia($review::MEDIA_CATEGORY_IMAGE);

        if ($image) {
            return $this->item($image, new MediaTransformer());
        }

        return null;
    }
}
