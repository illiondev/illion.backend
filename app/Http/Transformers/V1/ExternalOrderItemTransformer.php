<?php

namespace App\Http\Transformers\V1;

use App\Models\ExternalOrderItem;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="ExternalOrderItem",
 *     title="ExternalOrderItem",
 *     type="object",
 * )
 */
class ExternalOrderItemTransformer extends TransformerAbstract
{

    public function transform(ExternalOrderItem $orderItem): array
    {
        return [
            /**
             * @OA\Property(
             *     property="external_id",
             *     description="ID записи в системе клиента",
             *     example="1",
             *     type="integer"
             * )
             */
            'external_id' => $orderItem->external_id,
            /**
             * @OA\Property(
             *     property="external_order_id",
             *     description="ID заказа в системе клиента",
             *     example="1",
             *     type="integer"
             * )
             */
            'external_order_id' => $orderItem->external_order_id,
            /**
             * @OA\Property(
             *     property="bundle_id",
             *     description="ID бандла",
             *     example="22",
             *     type="integer"
             * )
             */
            'bundle_id' => $orderItem->bundle_id,
            /**
             * @OA\Property(
             *     property="quantity",
             *     description="Количество товаров",
             *     example="1",
             *     type="integer"
             * )
             */
            'quantity' => $orderItem->quantity,
            /**
             * @OA\Property(
             *     property="prime_cost",
             *     description="Себестоимость одного товара в рублях",
             *     example="99.14",
             *     type="number"
             * ),
             */
            'prime_cost' => $orderItem->prime_cost,
            /**
             * @OA\Property(
             *     property="price",
             *     description="Цена продажи за один товар в рублях",
             *     example="299.99",
             *     type="number"
             * ),
             */
            'price' => $orderItem->price,
            /**
             * @OA\Property(
             *     property="net_profit",
             *     description="Цена продажи за один товар минус комиссия платежной системы в рублях",
             *     example="282.11",
             *     type="number"
             * ),
             */
            'net_profit' => $orderItem->net_profit,
            /**
             * @OA\Property(
             *     property="referral_user_id",
             *     description="Id юзера-реферала",
             *     example="216",
             *     type="integer"
             * ),
             */
            'referral_user_id' => $orderItem->referral_user_id,
            /**
             * @OA\Property(
             *     property="referral_amount",
             *     description="Сумма выплаты рефералу",
             *     example="282.11",
             *     type="number"
             * ),
             */
            'referral_amount' => $orderItem->referral_amount,
        ];
    }
}
