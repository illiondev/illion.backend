<?php

namespace App\Http\Transformers\V1;

use App\Models\Book;
use Auth;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Book",
 *     title="Book",
 *     type="object",
 * )
 */
class BookTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'author',

        'cover',
        'hardcover',
        'coverSmall',
        'photos',
        'fragment',

        'advantages',
        'features',
        'reviews',
        'quotes',

        'bundles',
    ];

    public function transform(Book $book): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $book->id,
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $book->title,
            /**
             * @OA\Property(
             *     property="theme_id",
             *     description="",
             *     type="string"
             * )
             */
            'theme_id' => $book->theme_id,
            /**
             * @OA\Property(
             *     property="tagline",
             *     description="",
             *     type="string"
             * )
             */
            'tagline' => $book->tagline,
            /**
             * @OA\Property(
             *     property="preview",
             *     description="",
             *     type="string"
             * )
             */
            'preview' => $book->preview,
            /**
             * @OA\Property(
             *     property="owned",
             *     description="",
             *     type="bool"
             * )
             */
            'owned' => Auth::user() ? Auth::user()->ownedBooks()->where(['books.id' => $book->id])->exists() : false,
            /**
             * @OA\Property(
             *      property="publication_date",
             *      type="string",
             *      description="format: 1988-09-13"
             * )
             */
            'publication_date' => $book->publication_date ? $book->publication_date->toDateString() : '',
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'created_at' => $book->created_at ? $book->created_at->toDateTimeString() : '',
            /**
             * @OA\Property(
             *      property="updated_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'updated_at' => $book->updated_at ? $book->updated_at->toDateTimeString() : '',
        ];
    }

    /**
     * @OA\Property(
     *    property="cover",
     *    type="object",
     *    ref="#/components/schemas/Media",
     * ),
     */
    public function includeCover(Book $book): ?Item
    {
        $cover = $book->getFirstMedia($book::MEDIA_CATEGORY_COVER);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="hardcover",
     *    type="object",
     *    ref="#/components/schemas/Media",
     * ),
     */
    public function includeHardcover(Book $book): ?Item
    {
        $cover = $book->getFirstMedia($book::MEDIA_CATEGORY_HARDCOVER);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="coverSmall",
     *    type="object",
     *    ref="#/components/schemas/Media",
     * ),
     */
    public function includeCoverSmall(Book $book): ?Item
    {
        $cover = $book->getFirstMedia($book::MEDIA_CATEGORY_COVER_SMALL);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="photos",
     *    type="object",
     *    ref="#/components/schemas/Media",
     * ),
     */
    public function includePhotos(Book $book): ?Collection
    {
        $photos = $book->getMedia($book::MEDIA_CATEGORY_PHOTO);

        if ($photos) {
            return $this->collection($photos, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="fragment",
     *    type="object",
     *    ref="#/components/schemas/Media",
     * ),
     */
    public function includeFragment(Book $book): ?Item
    {
        $fragment = $book->getFirstMedia($book::MEDIA_CATEGORY_FRAGMENT);

        if ($fragment) {
            return $this->item($fragment, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="advantages",
     *    description="Преимущества",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/BookAdvantage")
     *    ),
     * ),
     */
    public function includeAdvantages(Book $book): ?Collection
    {
        return $this->collection($book->advantages, new BookAdvantageTransformer());
    }

    /**
     * @OA\Property(
     *    property="features",
     *    description="Преимущества",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/BookFeature")
     * ),
     */
    public function includeFeatures(Book $book): ?Collection
    {
        return $this->collection($book->features, new BookFeatureTransformer());
    }

    /**
     * @OA\Property(
     *    property="reviews",
     *    description="Отзывы",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/BookReview")
     * ),
     */
    public function includeReviews(Book $book): ?Collection
    {
        return $this->collection($book->reviews, new BookReviewTransformer());
    }

    /**
     * @OA\Property(
     *    property="quotes",
     *    description="Цитаты",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/BookQuote")
     * ),
     */
    public function includeQuotes(Book $book): ?Collection
    {
        return $this->collection($book->quotes, new BookQuoteTransformer());
    }

    /**
     * @OA\Property(
     *    property="author",
     *    description="Автор",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/Author")
     * ),
     */
    public function includeAuthor(Book $book): ?Item
    {
        return $this->item($book->author, new AuthorTransformer());
    }

    /**
     * @OA\Property(
     *    property="bundles",
     *    description="Бандл",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/Bundle")
     * ),
     */
    public function includeBundles(Book $book): ?Collection
    {
        return $this->collection($book->bundles, new BundleTransformer());
    }
}
