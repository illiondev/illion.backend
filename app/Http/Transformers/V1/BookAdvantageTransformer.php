<?php

namespace App\Http\Transformers\V1;

use App\Models\Advantage;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="BookAdvantage",
 *     title="BookAdvantage",
 *     description="Преимущество",
 *     type="object",
 * )
 */
class BookAdvantageTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'image',
    ];

    public function transform(Advantage $advantage): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $advantage->id,
            /**
             * @OA\Property(
             *     property="text",
             *     description="",
             *     type="string"
             * )
             */
            'text' => $advantage->text,
        ];
    }

    /**
     * @OA\Property(
     *    property="image",
     *    description="Картинка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
    public function includeImage(Advantage $advantage): ?Item
    {
        $image = $advantage->getFirstMedia($advantage::MEDIA_CATEGORY_IMAGE);

        if ($image) {
            return $this->item($image, new MediaTransformer());
        }

        return null;
    }
}
