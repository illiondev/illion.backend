<?php

namespace App\Http\Transformers\V1;

use App\Models\Order;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="AddressBook",
 *     title="AddressBook",
 *     type="object",
 * )
 */
class AddressBookTransformer extends TransformerAbstract
{
    public function transform(Order $order): array
    {
        return [
            /**
             * @OA\Property(
             *     property="name",
             *     description="",
             *     type="string"
             * )
             */
            'name' => $order->name,
            /**
             * @OA\Property(
             *     property="surname",
             *     description="",
             *     type="string"
             * )
             */
            'surname' => $order->surname,
            /**
             * @OA\Property(
             *     property="patronimic",
             *     description="",
             *     type="string"
             * )
             */
            'patronimic' => $order->patronimic,
            /**
             * @OA\Property(
             *     property="comment",
             *     description="",
             *     type="string"
             * )
             */
            'comment' => $order->comment,
            /**
             * @OA\Property(
             *     property="email",
             *     description="",
             *     type="string"
             * )
             */
            'email' => $order->email,
            /**
             * @OA\Property(
             *     property="phone",
             *     description="",
             *     type="string"
             * )
             */
            'phone' => $order->phone,
            /**
             * @OA\Property(
             *     property="shipping_method",
             *     description="",
             *     type="integer"
             * )
             */
            'shipping_method' => $order->shipping_method,
            /**
             * @OA\Property(
             *     property="delivery_point",
             *     description="",
             *     type="integer"
             * )
             */
            'delivery_point' => $order->delivery_point,
            /**
             * @OA\Property(
             *     property="country_code",
             *     description="",
             *     type="string"
             * )
             */
            'country_code' => $order->country_code,
            /**
             * @OA\Property(
             *     property="region",
             *     description="",
             *     type="string"
             * )
             */
            'region' => $order->region,
            /**
             * @OA\Property(
             *     property="settlement",
             *     description="",
             *     type="string"
             * )
             */
            'settlement' => $order->settlement,
            /**
             * @OA\Property(
             *     property="address",
             *     description="",
             *     type="string"
             * )
             */
            'address' => $order->address,
            /**
             * @OA\Property(
             *     property="address2",
             *     description="",
             *     type="string"
             * )
             */
            'address2' => $order->address2,
            /**
             * @OA\Property(
             *     property="postal_code",
             *     description="",
             *     type="string"
             * )
             */
            'postal_code' => $order->postal_code,
            /**
             * @OA\Property(
             *     property="kladr_id",
             *     description="",
             *     type="string"
             * )
             */
            'kladr_id' => $order->kladr_id,
            /**
             * @OA\Property(
             *     property="payment_method",
             *     description="",
             *     type="integer"
             * )
             */
            'payment_method' => $order->payment_method,
        ];
    }
}
