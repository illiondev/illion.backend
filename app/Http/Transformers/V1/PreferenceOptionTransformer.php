<?php

namespace App\Http\Transformers\V1;

use App\Models\PreferenceOption;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="PreferenceOption",
 *     title="PreferenceOption",
 *     type="object",
 * )
 */
class PreferenceOptionTransformer extends TransformerAbstract
{
    public function transform(PreferenceOption $preferenceOption): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     type="integer",
             * )
             */
            'id' => $preferenceOption->id,
            /**
             * @OA\Property(
             *     property="preferences_id",
             *     type="integer",
             * )
             */
            'preference_id' => $preferenceOption->preference_id,
            /**
             * @OA\Property(
             *     property="title",
             *     type="string",
             * )
             */
            'title' => $preferenceOption->title,
        ];
    }
}
