<?php

namespace App\Http\Transformers\V1;

use App\Models\Quote;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="BookQuote",
 *     title="BookQuote",
 *     description="Цитаты",
 *     type="object",
 * )
 */
class BookQuoteTransformer extends TransformerAbstract
{
    public function transform(Quote $quote): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $quote->id,
            /**
             * @OA\Property(
             *     property="text",
             *     description="",
             *     type="string"
             * )
             */
            'text' => $quote->text,
            /**
             * @OA\Property(
             *     property="name",
             *     description="",
             *     type="string"
             * )
             */
            'name' => $quote->name,
        ];
    }
}
