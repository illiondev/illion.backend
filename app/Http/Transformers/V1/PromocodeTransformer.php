<?php

namespace App\Http\Transformers\V1;

use App\Models\Promocode;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Promocode",
 *     title="Promocode",
 *     type="object",
 * )
 */
class PromocodeTransformer extends TransformerAbstract
{
    public function transform(Promocode $promocode): array
    {
        return [
            /**
             * @OA\Property(
             *     property="code",
             *     description="",
             *     type="string"
             * )
             */
            'code' => $promocode->code,
        ];
    }
}
