<?php

namespace App\Http\Transformers\V1;

use App\Models\Book;
use App\Models\Bundle;
use App\Models\OrderItem;
use App\Models\OrderProduct;
use App\Models\Product;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="OrderItem",
 *     title="OrderItem",
 *     type="object",
 * )
 */
class OrderItemTransformer extends TransformerAbstract
{
    /** @var OrderProduct|Product */
    protected $firstProduct;

    protected $defaultIncludes = [
        'cover',
        'products',
    ];

    public function transform(OrderItem $orderItem): array
    {
        // todo костыли для фронта
        if ($orderItem->exists) {
            $this->firstProduct = $orderItem->products()->first();
        } else {
            $this->firstProduct = $orderItem->bundle->products()->first();
        }

        $result = [
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $orderItem->title,
            /**
             * @OA\Property(
             *     property="book_id",
             *     description="",
             *     type="integer"
             * )
             */
            // todo костыли для фронта
            'book_id' => $this->firstProduct->book_id,
            /**
             * @OA\Property(
             *     property="bundle_id",
             *     description="",
             *     type="integer"
             * )
             */
            'bundle_id' => $orderItem->bundle_id,
            /**
             * @OA\Property(
             *     property="quantity",
             *     description="",
             *     type="integer"
             * )
             */
            'quantity' => $orderItem->quantity,
            /**
             * @OA\Property(
             *     property="original_price",
             *     description="",
             *     type="float"
             * )
             */
            'original_price' => $orderItem->original_price,
            /**
             * @OA\Property(
             *     property="discount",
             *     description="",
             *     type="float"
             * )
             */
            'discount' => $orderItem->discount,
            /**
             * @OA\Property(
             *     property="discount_flat",
             *     description="",
             *     type="float"
             * )
             */
            'discount_flat' => $orderItem->discount_flat,
            /**
             * @OA\Property(
             *     property="price",
             *     description="",
             *     type="float"
             * )
             */
            'price' => $orderItem->price,
        ];

        /**
         * @OA\Property(
         *     property="notification",
         *     description="",
         *     type="string"
         * )
         */
        if (!$orderItem->exists) {
            $result['notification'] = $orderItem->bundle->notification;
        }

        return $result;
    }

    /**
     * @OA\Property(
     *    property="cover",
     *    description="обложка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     * ),
     */
    public function includeCover(OrderItem $orderItem): ?Item
    {
        $cover = $orderItem->bundle->getFirstMedia(Bundle::MEDIA_CATEGORY_COVER);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        // todo костыли для фронта
        /** @var Book $book */
        $book = Book::with(['media'])->find($this->firstProduct->book_id);
        $cover = $book->getFirstMedia(Book::MEDIA_CATEGORY_COVER_SMALL);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="products",
     *    description="Продукты",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/OrderProduct")
     * ),
     */
    public function includeProducts(OrderItem $orderItem): ?Collection
    {
        if ($orderItem->exists) {
            return $this->collection($orderItem->products, new OrderProductTransformer());
        } else {
            return $this->collection($orderItem->bundle->products, new BookProductTransformer());
        }
    }
}
