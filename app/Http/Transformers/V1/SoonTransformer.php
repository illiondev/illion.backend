<?php

namespace App\Http\Transformers\V1;

use App\Models\Soon;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Soon",
 *     title="Soon",
 *     type="object",
 * )
 */
class SoonTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'author',
        'cover',
    ];

    public function transform(Soon $soon): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $soon->id,
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $soon->title,
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'created_at'   => $soon->created_at ? $soon->created_at->toDateTimeString() : '',
            /**
             * @OA\Property(
             *      property="updated_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'updated_at'   => $soon->updated_at ? $soon->updated_at->toDateTimeString() : '',
        ];
    }

    /**
     * @OA\Property(
     *    property="cover",
     *    description="обложка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     * ),
     */
    public function includeCover(Soon $soon): ?Item
    {
        $cover = $soon->getFirstMedia($soon::MEDIA_CATEGORY_COVER);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="author",
     *    description="Автор",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/Author")
     * ),
     */
    public function includeAuthor(Soon $soon): ?Item
    {
        return $this->item($soon->author, new AuthorTransformer());
    }
}
