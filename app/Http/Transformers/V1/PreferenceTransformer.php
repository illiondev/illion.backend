<?php

namespace App\Http\Transformers\V1;

use App\Models\Preference;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Preference",
 *     title="Preference",
 *     type="object",
 * )
 */
class PreferenceTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'preferenceOptions',
    ];

    public function transform(Preference $preference): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     type="integer",
             * )
             */
            'id' => $preference->id,
            /**
             * @OA\Property(
             *     property="title",
             *     type="string",
             * )
             */
            'title' => $preference->title,
        ];
    }

    /**
     * @OA\Property(
     *    property="preferenceOptions",
     *    description="Товары",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/PreferenceOption")
     * ),
     */
    public function includePreferenceOptions(Preference $preference): ?Collection
    {
        return $this->collection($preference->preferenceOptions, new PreferenceOptionTransformer());
    }
}
