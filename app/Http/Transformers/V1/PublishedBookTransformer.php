<?php

namespace App\Http\Transformers\V1;

use App\Models\PublishedBook;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="PublishedBook",
 *     title="PublishedBook",
 *     description="Публикация (ebook)",
 *     type="object",
 * )
 */
class PublishedBookTransformer extends TransformerAbstract
{
    public function transform(PublishedBook $publishedBook): array
    {
        return [
            /**
             * @OA\Property(
             *     property="size",
             *     description="",
             *     type="integer"
             * )
             */
            'size' => $publishedBook->size,
            /**
             * @OA\Property(
             *     property="secret_key",
             *     description="",
             *     type="string"
             * )
             */
            'secret_key' => $publishedBook->secret_key,
        ];
    }
}
