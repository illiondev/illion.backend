<?php

namespace App\Http\Transformers\V1;

use App\Models\Product;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="BookProduct",
 *     title="BookProduct",
 *     description="Продукт",
 *     type="object",
 * )
 */
class BookProductTransformer extends TransformerAbstract
{
    public function transform(Product $product): array
    {
        $result = [
            /**
             * @OA\Property(
             *     property="article",
             *     description="",
             *     type="string"
             * )
             */
            'article' => $product->article,
            /**
             * @OA\Property(
             *     property="type",
             *     description="",
             *     type="integer"
             * )
             */
            'type' => $product->type,
            /**
             * @OA\Property(
             *     property="book_id",
             *     description="",
             *     type="integer"
             * )
             */
            'book_id' => $product->book_id,
            /**
             * @OA\Property(
             *     property="weight",
             *     description="",
             *     type="number"
             * )
             */
            'weight' => $product->weight,
            /**
             * @OA\Property(
             *     property="length",
             *     description="",
             *     type="number"
             * )
             */
            'length' => $product->length,
            /**
             * @OA\Property(
             *     property="width",
             *     description="",
             *     type="number"
             * )
             */
            'width' => $product->width,
            /**
             * @OA\Property(
             *     property="height",
             *     description="",
             *     type="number"
             * )
             */
            'height' => $product->height,
        ];

        /**
         * @OA\Property(
         *     property="is_surprise",
         *     description="",
         *     type="boolean"
         * )
         */
        if (isset($product->pivot)) {
            $result['is_surprise'] = (bool) $product->pivot->is_surprise;
        }

        return $result;
    }
}
