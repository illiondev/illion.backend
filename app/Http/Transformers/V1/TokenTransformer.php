<?php

namespace App\Http\Transformers\V1;

use Illion\Service\Models\Token;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Token",
 *     title="Token",
 *     type="object",
 * )
 */
class TokenTransformer extends TransformerAbstract
{
    public function transform(Token $token): array
    {
        return [
            /**
             * @OA\Property(
             *     property="token_type",
             *     description="Bearer etc",
             *     type="string"
             * )
             */
            'token_type' => $token->token_type,
            /**
             * @OA\Property(
             *     property="expires_in",
             *     description="Expires in seconds",
             *     type="integer"
             * )
             */
            'expires_in' => $token->expires_in,
            /**
             * @OA\Property(
             *     property="access_token",
             *     description="Access token",
             *     type="string"
             * )
             */
            'access_token' => $token->access_token,
            /**
             * @OA\Property(
             *     property="refresh_token",
             *     description="Refresh Token",
             *     type="string"
             * )
             */
            'refresh_token' => $token->refresh_token ?: '',
        ];
    }

}
