<?php

namespace App\Http\Transformers\V1;

use Illion\Service\Models\City;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="City",
 *     title="City",
 *     type="object",
 * )
 */
class CityTransformer extends TransformerAbstract
{
    public function transform(City $city): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     type="integer",
             * )
             */
            'id' => $city->id,
            /**
             * @OA\Property(
             *     property="name",
             *     type="string"
             * )
             */
            'name' => $city->name,
            /**
             * @OA\Property(
             *     property="region_name",
             *     type="string"
             * )
             */
            'region_name' => $city->region_name,
            /**
             * @OA\Property(
             *     property="country_name",
             *     type="string"
             * )
             */
            'country_name' => $city->country_name,
        ];
    }
}
