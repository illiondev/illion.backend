<?php

namespace App\Http\Transformers\V1;

use App\Models\OrderBill;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="OrderBill",
 *     title="OrderBill",
 *     type="object",
 * )
 */
class OrderBillTransformer extends TransformerAbstract
{
    public function transform(OrderBill $orderBill): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     type="integer",
             * )
             */
            'id' => $orderBill->id,
            /**
             * @OA\Property(
             *     property="hash",
             *     type="string",
             * )
             */
            'hash' => $orderBill->hash,
            /**
             * @OA\Property(
             *     property="price",
             *     type="float",
             * )
             */
            'price' => $orderBill->price,
            /**
             * @OA\Property(
             *     property="description",
             *     type="string",
             * )
             */
            'description' => $orderBill->description,
            /**
             * @OA\Property(
             *     property="status",
             *     type="integer",
             * )
             */
            'status' => $orderBill->status,
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'created_at'   => $orderBill->created_at ? $orderBill->created_at->toDateTimeString() : '',
            /**
             * @OA\Property(
             *      property="updated_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'updated_at'   => $orderBill->updated_at ? $orderBill->updated_at->toDateTimeString() : '',
        ];
    }
}
