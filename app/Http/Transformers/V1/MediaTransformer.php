<?php

namespace App\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use Spatie\MediaLibrary\Models\Media;

/**
 * @OA\Schema(
 *     schema="Media",
 *     title="Media",
 *     type="object",
 * )
 */
class MediaTransformer extends TransformerAbstract
{
    protected $conversion;

    public function __construct(string $conversion = 'web')
    {
        $this->conversion = $conversion;
    }

    public function transform(Media $media): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer",
             * )
             */
            'id' => $media->id,
            /**
             * @OA\Property(
             *      property="category",
             *      type="string",
             *      enum={"avatar", "album"},
             *      description=""
             * )
             */
            'category' => $media->collection_name,
            /**
             * @OA\Property(
             *      property="name",
             *      type="string",
             *      description=""
             * )
             */
            'name' => $media->name,
            /**
             * @OA\Property(
             *      property="mime_type",
             *      type="string",
             *      description=""
             * )
             */
            'mime_type' => $media->mime_type,
            /**
             * @OA\Property(
             *      property="url",
             *      type="string",
             *      description=""
             * )
             */
            'url' => $media->hasGeneratedConversion($this->conversion)
                ? $media->getFullUrl($this->conversion)
                : $media->getFullUrl(),
        ];
    }
}
