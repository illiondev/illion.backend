<?php

namespace App\Http\Transformers\V1;

use App\Models\Announcement;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Announcement",
 *     title="Announcement",
 *     description="Объявление",
 *     type="object",
 * )
 */
class AnnouncementTransformer extends TransformerAbstract
{
    public function transform(Announcement $announcement): array
    {
        return [
            /**
             * @OA\Property(
             *     property="enabled",
             *     description="",
             *     type="boolean"
             * )
             */
            'enabled' => $announcement->enabled,
            /**
             * @OA\Property(
             *     property="content",
             *     description="",
             *     type="string"
             * )
             */
            'content' => $announcement->content,
            /**
             * @OA\Property(
             *     property="page_type",
             *     description="",
             *     type="integer"
             * )
             */
            'page_type' => $announcement->page_type,
        ];
    }
}
