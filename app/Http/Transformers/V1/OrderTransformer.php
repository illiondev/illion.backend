<?php

namespace App\Http\Transformers\V1;

use App\Models\Order;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Order",
 *     title="Order",
 *     type="object",
 * )
 */
class OrderTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'items',
    ];

    public function transform(Order $order): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $order->id,
            /**
             * @OA\Property(
             *     property="name",
             *     description="",
             *     type="string"
             * )
             */
            'name' => (string) $order->name,
            /**
             * @OA\Property(
             *     property="surname",
             *     description="",
             *     type="string"
             * )
             */
            'surname' => (string) $order->surname,
            /**
             * @OA\Property(
             *     property="patronimic",
             *     description="",
             *     type="string"
             * )
             */
            'patronimic' => (string) $order->patronimic,
            /**
             * @OA\Property(
             *     property="phone",
             *     description="",
             *     type="string"
             * )
             */
            'phone' => (string) $order->phone,
            /**
             * @OA\Property(
             *     property="address",
             *     description="",
             *     type="string"
             * )
             */
            'address' => (string) $order->address,
            /**
             * @OA\Property(
             *     property="address2",
             *     description="",
             *     type="string"
             * )
             */
            'address2' => (string) $order->address2,
            /**
             * @OA\Property(
             *     property="settlement",
             *     description="",
             *     type="string"
             * )
             */
            'settlement' => $order->settlement,
            /**
             * @OA\Property(
             *     property="region",
             *     description="",
             *     type="string"
             * )
             */
            'region' => $order->region,
            /**
             * @OA\Property(
             *     property="country",
             *     description="",
             *     type="string"
             * )
             */
            'country' => $order->getCountryName(),
            /**
             * @OA\Property(
             *     property="postal_code",
             *     description="",
             *     type="string"
             * )
             */
            'postal_code' => (string) $order->postal_code,
            /**
             * @OA\Property(
             *     property="status",
             *     description="",
             *     type="integer"
             * )
             */
            'status' => $order->status,
            /**
             * @OA\Property(
             *     property="price",
             *     description="",
             *     type="float"
             * )
             */
            'price' => $order->price,
            /**
             * @OA\Property(
             *     property="delivery_price",
             *     description="",
             *     type="float"
             * )
             */
            'delivery_price' => $order->delivery_price,
            /**
             * @OA\Property(
             *     property="total_price",
             *     description="",
             *     type="float"
             * )
             */
            'total_price' => $order->total_price,
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13"
             * )
             */
            'created_at'   => $order->created_at ? $order->created_at->toDateString() : '',
        ];
    }

    /**
     * @OA\Property(
     *    property="items",
     *    description="Товары",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/OrderItem")
     * ),
     */
    public function includeItems(Order $order): ?Collection
    {
        return $this->collection($order->items, new OrderItemTransformer());
    }
}
