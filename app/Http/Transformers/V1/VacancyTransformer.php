<?php

namespace App\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use App\Models\Vacancy;

/**
 * @OA\Schema(
 *     schema="Vacancy",
 *     title="Vacancy",
 *     type="object",
 * )
 */
class VacancyTransformer extends TransformerAbstract
{
    public function transform(Vacancy $vacancy): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $vacancy->id,
           /**
             * @OA\Property(
             *     property="priority",
             *     description="",
             *     type="integer"
             * )
             */
            'priority' => $vacancy->priority,
            /**
             * @OA\Property(
             *     property="enabled",
             *     description="",
             *     type="boolean"
             * )
             */
            'enabled' => $vacancy->enabled,
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $vacancy->title,
            /**
             * @OA\Property(
             *     property="description",
             *     description="",
             *     type="string"
             * )
             */
            'description' => $vacancy->description,
        ];
    }
}
