<?php

namespace App\Http\Transformers\V1;

use App\Models\ExternalOrder;
use App\Models\ExternalOrderItem;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="ExternalOrder",
 *     title="ExternalOrder",
 *     type="object",
 * )
 */
class ExternalOrderTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'items',
    ];

    public function transform(ExternalOrder $order): array
    {
        return [
            /**
             * @OA\Property(
             *     property="external_id",
             *     description="ID заказа в системе клиента",
             *     example="14",
             *     type="integer"
             * ),
             */
            'external_id' => $order->external_id,
            /**
             * @OA\Property(
             *     property="country_code",
             *     description="Код страны",
             *     type="string"
             * )
             */
            'country_code' => $order->country_code,
            /**
             * @OA\Property(
             *     property="region",
             *     description="Регион",
             *     type="string"
             * )
             */
            'region' => $order->region,
            /**
             * @OA\Property(
             *     property="city",
             *     description="Город",
             *     type="string"
             * )
             */
            'city' => $order->city,
            /**
             * @OA\Property(
             *     property="status",
             *         description="
             * 3 - оплачен
             * 4 - выполнен, доставлен, завершен
             * 5 - отменен
             * 7 - возврат",
             *     type="integer"
             * )
             */
            'status' => $order->status,
            /**
             * @OA\Property(
             *     property="price",
             *     description="Цена всех товаров",
             *     type="float"
             * )
             */
            'price' => $order->price,
            /**
             * @OA\Property(
             *     property="delivery_price",
             *     description="Цена доставки",
             *     type="float"
             * )
             */
            'delivery_price' => $order->delivery_price,
            /**
             * @OA\Property(
             *     property="total_price",
             *     description="Общая стоимость",
             *     type="float"
             * )
             */
            'total_price' => $order->total_price,
            /**
             * @OA\Property(
             *      property="sold_at",
             *      type="string",
             *      description="format: 1988-09-13"
             * )
             */
            'sold_at' => $order->sold_at ? $order->sold_at->toDateTimeString() : '',
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13"
             * )
             */
            'created_at' => $order->created_at ? $order->created_at->toDateTimeString() : '',
            /**
             * @OA\Property(
             *      property="updated_at",
             *      type="string",
             *      description="format: 1988-09-13"
             * )
             */
            'updated_at' => $order->updated_at ? $order->updated_at->toDateTimeString() : '',
        ];
    }

    /**
     * @OA\Property(
     *    property="items",
     *    description="Товары в заказе",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/ExternalOrderItem"),
     *    ),
     * ),
     */
    public function includeItems(ExternalOrder $order): ?Collection
    {
        return $this->collection(ExternalOrderItem::where([
            'external_order_id' => $order->external_id,
            'client_id' => $order->client_id,
        ])->get(), new ExternalOrderItemTransformer());
    }
}
