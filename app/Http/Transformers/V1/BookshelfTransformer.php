<?php

namespace App\Http\Transformers\V1;

use App\Models\Book;
use Auth;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Bookshelf",
 *     title="Bookshelf",
 *     description="Книга",
 *     type="object",
 * )
 */
class BookshelfTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'author',
        'cover',
        'hardcover',
        'coverSmall',
        'published',

        'products',
    ];

    public function transform(Book $book): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $book->id,
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $book->title,
            /**
             * @OA\Property(
             *     property="theme_id",
             *     description="",
             *     type="string"
             * )
             */
            'theme_id' => $book->theme_id,
            /**
             * @OA\Property(
             *     property="author_id",
             *     description="",
             *     type="integer"
             * )
             */
            'author_id' => $book->author_id,
            /**
             * @OA\Property(
             *     property="preview",
             *     description="",
             *     type="string"
             * )
             */
            'preview' => $book->preview,
            /**
             * @OA\Property(
             *      property="publication_date",
             *      type="string",
             *      description="format: 1988-09-13"
             * )
             */
            'publication_date'   => $book->publication_date ? $book->publication_date->toDateString() : '',
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'created_at'   => $book->created_at ? $book->created_at->toDateTimeString() : '',
            /**
             * @OA\Property(
             *      property="updated_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'updated_at'   => $book->updated_at ? $book->updated_at->toDateTimeString() : '',
        ];
    }

    /**
     * @OA\Property(
     *    property="hardcover",
     *    description="Обложка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
    public function includeCover(Book $book): ?Item
    {
        $cover = $book->getFirstMedia($book::MEDIA_CATEGORY_COVER);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="cover",
     *    description="СуперОбложка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
    public function includeHardcover(Book $book): ?Item
    {
        $cover = $book->getFirstMedia($book::MEDIA_CATEGORY_HARDCOVER);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="coverSmall",
     *    description="маленькая обложка?",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
    public function includeCoverSmall(Book $book): ?Item
    {
        $cover = $book->getFirstMedia($book::MEDIA_CATEGORY_COVER_SMALL);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="author",
     *    type="object",
     *    ref="#/components/schemas/Author",
     *    ),
     * ),
     */
    public function includeAuthor(Book $book): ?Item
    {
        return $this->item($book->author, new AuthorTransformer());
    }

    /**
     * @OA\Property(
     *    property="published",
     *    type="object",
     *    ref="#/components/schemas/PublishedBook",
     *    ),
     * ),
     */
    public function includePublished(Book $book): ?Item
    {
        $published = $book->publishedEbook;

        if ($published) {
            return $this->item($published, new PublishedBookTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="products",
     *    type="object",
     *    ref="#/components/schemas/BookshelfProduct",
     *    ),
     * ),
     */
    public function includeProducts(Book $book): ?Collection
    {
        return $this->collection(
            Auth::user()->ownedProducts()->where(['user_has_books.book_id' => $book->id])->get(),
            new BookshelfProductTransformer()
        );
    }
}
