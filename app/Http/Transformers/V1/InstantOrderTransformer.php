<?php

namespace App\Http\Transformers\V1;

use App\Models\Order;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="InstantOrder",
 *     title="InstantOrder",
 *     type="object",
 * )
 */
class InstantOrderTransformer extends TransformerAbstract
{
    public function transform(Order $order): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $order->id,
            /**
             * @OA\Property(
             *     property="status",
             *     description="",
             *     type="integer"
             * )
             */
            'status' => $order->status,
            /**
             * @OA\Property(
             *     property="price",
             *     description="",
             *     type="float"
             * )
             */
            'price' => $order->price,
            /**
             * @OA\Property(
             *     property="delivery_price",
             *     description="",
             *     type="float"
             * )
             */
            'delivery_price' => $order->delivery_price,
            /**
             * @OA\Property(
             *     property="total_price",
             *     description="",
             *     type="float"
             * )
             */
            'total_price' => $order->total_price,
        ];
    }
}
