<?php

namespace App\Http\Transformers\V1;

use App\Models\Product;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="BookshelfProduct",
 *     title="Bookshelf",
 *     description="Купленный продукт",
 *     type="object",
 * )
 */
class BookshelfProductTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'published',
    ];

    public function transform(Product $product): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $product->id,
            /**
             * @OA\Property(
             *     property="article",
             *     description="",
             *     type="string"
             * )
             */
            'article' => $product->article,
            /**
             * @OA\Property(
             *     property="type",
             *     description="",
             *     type="integer"
             * )
             */
            'type' => $product->type,
            /**
             * @OA\Property(
             *     property="book_id",
             *     description="",
             *     type="integer"
             * )
             */
            'book_id' => $product->book_id,
            /**
             * @OA\Property(
             *     property="epub",
             *     description="",
             *     type="string"
             * )
             */
            'epub' => $product->epub,
            /**
             * @OA\Property(
             *     property="pdf",
             *     description="",
             *     type="string"
             * )
             */
            'pdf' => $product->pdf,
            /**
             * @OA\Property(
             *     property="video",
             *     description="",
             *     type="string"
             * )
             */
            'video' => $product->video,
            /**
             * @OA\Property(
             *     property="description",
             *     description="",
             *     type="string"
             * )
             */
            'description' => $product->description,
        ];
    }

    /**
     * @OA\Property(
     *    property="published",
     *    type="object",
     *    ref="#/components/schemas/PublishedBook",
     *    ),
     * ),
     */
    public function includePublished(Product $product): ?Item
    {
        $published = $product->book->publishedEbook;

        if ($published) {
            return $this->item($published, new PublishedBookTransformer());
        }

        return null;
    }
}