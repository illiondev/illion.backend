<?php

namespace App\Http\Transformers\V1;

use App\Models\Order;
use App\Models\Promocode;
use App\Models\Redis\Cart;
use App\Services\PromocodeService;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Cart",
 *     title="Cart",
 *     type="object",
 * )
 */
class CartTransformer extends TransformerAbstract
{
    protected $items;

    /** @var Promocode|null $promocode */
    protected $promocode;

    protected $defaultIncludes = [
        'bundle',
        'promo',
    ];

    public function transform(Cart $cart): array
    {
        $this->items = $cart->items();
        $this->promocode = $cart->getPromocode();

        $order = new Order();
        $order->setRelation('items', $this->items);

        $order->price = $order->getPrice();
        $order->total_price = $order->price;

        if ($this->promocode && $this->promocode->isOverallType()) {
            $promocodeService = app(PromocodeService::class);
            $promocodeService->applyOverallPromocodeToOrder($this->promocode, $order);
        }

        return [
            'hash' => $cart->hash,
            'price' => $order->price,
            'discount' => $order->discount,
            'discount_flat' => $order->discount_flat,
            'total_price' => $order->total_price,
        ];
    }

    /**
     * @OA\Property(
     *    property="bundle",
     *    description="Товары",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/OrderItem")
     * ),
     */
    public function includeBundle(Cart $cart): ?Collection
    {
        if ($this->items) {
            return $this->collection($this->items, new OrderItemTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="promo",
     *    description="Промокоды",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/Promocode")
     * ),
     */
    public function includePromo(Cart $cart): ?Collection
    {
        return $this->collection(collect($this->promocode ? [$this->promocode] : []), new PromocodeTransformer());
    }
}
