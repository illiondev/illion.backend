<?php

namespace App\Services;

use App\Jobs\Sendpulse\SendPulseSendSuccess;
use App\Jobs\Sendpulse\SendPulseTrackingNumber;
use App\Jobs\ShiptorSendJob;
use App\Models\Order;
use App\Models\OrderProduct;
use Cache;

class OrderService
{
    protected $shiptorService;
    protected $yandexMoneyService;

    public function __construct(
        ShiptorService $shiptorService,
        YandexMoneyService $yandexMoneyService
    ) {
        $this->shiptorService = $shiptorService;
        $this->yandexMoneyService = $yandexMoneyService;
    }

    public function bought(Order $order, string $paymentType): void
    {
        /** @var OrderProduct[] $orderProducts */
        $orderProducts = $order->products;

        $data = [];
        foreach ($orderProducts as $orderProduct) {
            $data[$orderProduct->product_id] = ['book_id' => $orderProduct->book_id];
        }

        $order->user->ownedProducts()->syncWithoutDetaching($data);

        $order->status = $order::STATUS_PROCESSING;
        $order->payment_type = $paymentType;

        $fee = 0;
        if (!$order->external_kassa) {
            if (isset(Order::$paymentTypeFee[$order->payment_type])) {
                $fee = Order::$paymentTypeFee[$order->payment_type];
            } else {
                app('sentry')->captureMessage('order ' . $order->id . ' unknown payment type ' . $order->payment_type);
            }
        }

        $order->payment_fee = $fee;
        $order->saveOrFail();

        foreach ($order->items as $item) {
            $item->net_profit = $item->price * (1 - ($fee / 100));
            $item->saveOrFail();
        }

        foreach ($order->products as $product) {
            $product->net_profit = $product->price * (1 - ($fee / 100));
            $product->saveOrFail();
        }

        dispatch(new ShiptorSendJob($order));
        dispatch(new SendPulseSendSuccess($order));
    }

    public function send(Order $order): void
    {
        $paperProducts = $order->getPaperProducts();

        if ($paperProducts->count() > 0) {
            $order->shiptor_id = $this->shiptorService->send(
                $paperProducts,
                $order
            );
        } else {
            $order->status = $order::STATUS_COMPLETED;
        }

        $order->saveOrFail();
    }

    public function checkPayment(Order $order): Order
    {
        if ($order->status !== Order::STATUS_WAITING_FOR_PAYMENT) {
            return $order;
        }

        if (Cache::lock($order->getLockKey(), 60 * 10)->block(5)) {
            try {
                /** @var Order $order */
                $order = $order->fresh();

                if ($order->status === Order::STATUS_WAITING_FOR_PAYMENT) {
                    $this->yandexMoneyService->setAuthFromOrder($order);

                    $paymentInfo = $this->yandexMoneyService->getPaymentInfo($order->payment_id);

                    if ($paymentInfo) {
                        switch ($paymentInfo->getStatus()) {
                            case $this->yandexMoneyService::STATUS_PENDING:
                                break;
                            case $this->yandexMoneyService::STATUS_WAITING_FOR_CAPTURE:
                                if ($this->yandexMoneyService->capturePayment($paymentInfo)) {
                                    $this->bought($order, $paymentInfo->getPaymentMethod()->getType());
                                }
                                break;
                            case $this->yandexMoneyService::STATUS_SUCCEEDED:
                                $this->bought($order, $paymentInfo->getPaymentMethod()->getType());
                                break;
                            case $this->yandexMoneyService::STATUS_CANCELED:
                                $order->status = Order::STATUS_CANCELED;
                                $order->saveOrFail();
                                break;
                        }
                    }
                }
            } finally {
                Cache::lock($order->getLockKey())->release();
            }
        }

        return $order;
    }

    public function process(Order $order, string $token = '', bool $instant = false): array
    {
        $result = [];

        if ($order->status !== Order::STATUS_NEW) {
            return [];
        }

        if ($order->total_price == 0) {
            $this->bought($order, $order::PAYMENT_TYPE_FREE);
            $result = [
                'free' => true
            ];

            return $result;
        }

        if (Cache::lock($order->getLockKey(), 60 * 10)->block(5)) {
            try {
                /** @var Order $order */
                $order = $order->fresh();

                if ($order->status === Order::STATUS_NEW) {
                    if ($order->isPaymentOnline()) {
                        $this->yandexMoneyService->setAuthFromOrder($order);

                        if ($order->payment_method === Order::PAYMENT_METHOD_ONLINE) {
                            $payment = $this->yandexMoneyService->createPaymentFromOrder($order, $instant);
                        } elseif ($order->payment_method === Order::PAYMENT_METHOD_GOOGLE_PLAY) {
                            $payment = $this->yandexMoneyService->createPaymentFromToken($order, $token);
                        }

                        $result = ['confirmation_url' => $payment['confirmation_url']];
                        $order->status = $order::STATUS_WAITING_FOR_PAYMENT;
                        $order->payment_id = $payment['id'];
                    } else {
                        $order->status = $order::STATUS_PROCESSING;
                    }

                    $order->saveOrFail();
                }
            } finally {
                Cache::lock($order->getLockKey())->release();
            }
        }

        return $result;
    }

    public function getConfirmation(Order $order): array
    {
        $result = [];

        if ($order->status !== Order::STATUS_WAITING_FOR_PAYMENT) {
            return [];
        }

        if ($order->payment_method !== Order::PAYMENT_METHOD_ONLINE) {
            return [];
        }

        if (Cache::lock($order->getLockKey(), 60 * 10)->block(5)) {
            try {
                /** @var Order $order */
                $order = $order->fresh();

                if ($order->status === Order::STATUS_WAITING_FOR_PAYMENT) {
                    $this->yandexMoneyService->setAuthFromOrder($order);

                    $payment = $this->yandexMoneyService->getPaymentInfo($order->payment_id);

                    if ($payment) {
                        $result = $this->yandexMoneyService::getConfirmation($payment);
                        unset($result['id']);
                    } else {
                        // todo yandex failed
                    }
                }
            } finally {
                Cache::lock($order->getLockKey())->release();
            }
        }

        return $result;
    }

    public function setDeliveryStatus(Order $order, array $info): void
    {
        $status = $info['status'];

        if (!$status) {
            return;
        }

        if ($order->shipping_status === Order::SHIPPING_STATUS_DELIVERED) {
            return;
        }

        if (Cache::lock($order->getLockKey(), 60 * 10)->block(5)) {
            try {
                /** @var Order $order */
                $order = $order->fresh();

                if ($order->shipping_status !== Order::SHIPPING_STATUS_DELIVERED) {
                    if ($status === Order::SHIPPING_STATUS_SENT &&
                        $order->shipping_status !== Order::SHIPPING_STATUS_SENT &&
                        !$order->tracking_number) {

                        $order->tracking_number = $info['tracking_number'];
                        $order->sent_date = $info['sent_date'];

                        dispatch(new SendPulseTrackingNumber(
                            $order->email,
                            $order->tracking_number,
                            $order->sent_date->toDateString()
                        ));
                    }

                    $order->shipping_status = $status;
                    $order->save();
                }
            } finally {
                Cache::lock($order->getLockKey())->release();
            }
        }
    }
}
