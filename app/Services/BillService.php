<?php

namespace Modules\Moderator\Services;

use Illuminate\Support\Str;
use Modules\Moderator\Models\OrderBill;

class BillService
{
    public function createBill(array $attributes): OrderBill
    {
        $orderBill = new OrderBill($attributes);

        $orderBill->order_id = null;
        $orderBill->hash = Str::uuid();
        $orderBill->user_id = null;
        $orderBill->payment_id = null;
        $orderBill->status = $orderBill::PAYMENT_STATUS_NEW;

        $orderBill->saveOrFail();

        return $orderBill;
    }
}
