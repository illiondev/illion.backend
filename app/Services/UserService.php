<?php

namespace App\Services;

use App\Models\Profile;
use App\Models\User;
use App\Repositories\UserRepository;
use Illion\Service\Models\User as IllionUser;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function update(User $user, array $params, IllionUser $illionUser = null): void
    {
        $user->fill($params);

        if ($illionUser) {
            $user = $this->updateUserFromIllion($user, $illionUser);
        }

        $user->saveOrFail();
    }

    protected function updateUserFromIllion(User $user, IllionUser $illionUser): User
    {
        if ($user->external_id == $illionUser->id) {
            $user->email = $illionUser->email;
            $user->email_confirmed_at = $illionUser->email_confirmed_at  ?: null;
        }

        return $user;
    }

    public function create(string $name, string $email, int $externalId): User
    {
        $user = new User([
            'email' => $email,
        ]);

        $user->external_id = $externalId;

        $user->saveOrFail();

        $profile = new Profile();
        $profile->user_id = $user->id;
        $profile->name = $name;
        $profile->saveOrFail();

        return $user;
    }
}
