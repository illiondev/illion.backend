<?php

namespace App\Services;

use App\Models\Order;
use Sendpulse\RestApi\ApiClient;
use Sendpulse\RestApi\Storage\SessionStorage;

class SendpulseService
{
    const DELIVERY_ID_PVZ = 4;
    const DELIVERY_ID_RUSSIAN_POST = 6;
    const DELIVERY_ID_COURIER = 8;
    const DELIVERY_ID_COURIER_BELARUS = 10;
    const DELIVERY_ID_COURIER_KAZAKHSTAN = 12;
    const DELIVERY_ID_COURIER_EXPORT = 14;

    const ADDRESS_BOOK_SUBSCRIBERS = '2159395';
    const ADDRESS_BOOK_007 = '2159710'; // любарская
    const ADDRESS_BOOK_022 = '2159712'; // новосад
    const ADDRESS_BOOK_004 = '1711841'; // дунины сказки
    const ADDRESS_BOOK_005 = '2118984'; // Как познакомиться с достойным мужчиной и всё не испортить
    const ADDRESS_BOOK_008 = '2313025'; // солодар
    const ADDRESS_BOOK_008_E = '2332584'; // солодар электронная
    const ADDRESS_BOOK_031 = '2389558'; // эстер
    const ADDRESS_BOOK_035 = '2396024'; // дружинина
    const ADDRESS_BOOK_032 = '2402488'; // Всехсвятская

    const ADDRESS_BOOK_007_FAIL = '2159841'; // любарская fail
    const ADDRESS_BOOK_022_FAIL = '2159845'; // новосад fail
    const ADDRESS_BOOK_004_FAIL = '1711839'; // дунины сказки
    const ADDRESS_BOOK_005_FAIL = '2118975'; // Как познакомиться с достойным мужчиной и всё не испортить
    const ADDRESS_BOOK_008_FAIL = '2313027'; // солодар
    const ADDRESS_BOOK_031_FAIL = '2390301'; // эстер
    const ADDRESS_BOOK_035_FAIL = '2396038'; // дружинина
    const ADDRESS_BOOK_032_FAIL = '2402491'; // Всехсвятская

    const ADDRESS_BOOK_TRACK = '2409365'; // Всехсвятская

    /** @var ApiClient $sendPulseApiClient */
    protected $sendPulseApiClient;

    public function __construct()
    {
        $this->sendPulseApiClient = new ApiClient(
            getenv('SENDPULSE_API_USER_ID'),
            getenv('SENDPULSE_API_SECRET'),
            new SessionStorage()
        );
    }

    public function track(string $email, string $trackNumber, string $dateSent): void
    {
        $additionalParams = [
            'track_number' => $trackNumber,
            'date_sent' => $dateSent,
        ];

        $this->addRaw(self::ADDRESS_BOOK_TRACK, $email, $additionalParams);
    }

    public function subscribe(string $email): void
    {
        $this->sendPulseApiClient->addEmails(self::ADDRESS_BOOK_SUBSCRIBERS, [$email]);
    }

    public function addEmailToFail(Order $order, string $email): void
    {
        foreach ($order->items as $item) {
            foreach ($item->products as $product) {
                switch ($product->article) {
                    case '007':
                        $this->add(self::ADDRESS_BOOK_007_FAIL, $email);
                        break;
                    case '022':
                        $this->add(self::ADDRESS_BOOK_022_FAIL, $email);
                        break;
                    case '004':
                        $this->add(self::ADDRESS_BOOK_004_FAIL, $email);
                        break;
                    case '005':
                        $this->add(self::ADDRESS_BOOK_005_FAIL, $email);
                        break;
                    case '008':
                        $this->add(self::ADDRESS_BOOK_008_FAIL, $email);
                        break;
                    case '008-e':
                        $this->add(self::ADDRESS_BOOK_008_E, $email);
                        break;
                    case '031':
                        $this->add(self::ADDRESS_BOOK_031_FAIL, $email);
                        break;
                    case '035':
                        $this->add(self::ADDRESS_BOOK_035_FAIL, $email);
                        break;
                    case '032':
                        $this->add(self::ADDRESS_BOOK_032_FAIL, $email);
                        break;
                    default:
                        break;
                }
            }
        }

    }

    public function addEmailToSuccess(Order $order, string $email): void
    {
        $deliveryId = self::getDeliveryId($order);

        foreach ($order->items as $item) {
            foreach ($item->products as $product) {
                switch ($product->article) {
                    case '007':
                        $this->remove(self::ADDRESS_BOOK_007_FAIL, $email);
                        $this->add(self::ADDRESS_BOOK_007, $email,
                            $deliveryId, $order->payment_method);
                        break;
                    case '022':
                        $this->remove(self::ADDRESS_BOOK_022_FAIL, $email);
                        $this->add(self::ADDRESS_BOOK_022, $email,
                            $deliveryId, $order->payment_method);
                        break;
                    case '004':
                        $this->remove(self::ADDRESS_BOOK_004_FAIL, $email);
                        $this->add(self::ADDRESS_BOOK_004, $email,
                            $deliveryId, $order->payment_method);
                        break;
                    case '005':
                        $this->remove(self::ADDRESS_BOOK_005_FAIL, $email);
                        $this->add(self::ADDRESS_BOOK_005, $email,
                            $deliveryId, $order->payment_method);
                        break;
                    case '008':
                        $this->sendPulseApiClient->startEventAutomation360('purchase_93', [
                            'email' => $order->email,
                            'phone' => $order->phone,
                        ]);
                        $this->add(self::ADDRESS_BOOK_008, $email,
                            $deliveryId, $order->payment_method);
                        break;
                    case '008-e':
                        $this->sendPulseApiClient->startEventAutomation360('purchase_22', [
                            'email' => $order->email,
                            'phone' => $order->phone,
                        ]);
                        break;
                    case '031':
                        $this->remove(self::ADDRESS_BOOK_031_FAIL, $email);
                        $this->add(self::ADDRESS_BOOK_031, $email,
                            $deliveryId, $order->payment_method);
                        break;
                    case '035':
                        $this->sendPulseApiClient->startEventAutomation360('purchase_56', [
                            'email' => $order->email,
                            'phone' => $order->phone,
                        ]);
                        $this->add(self::ADDRESS_BOOK_035, $email,
                            $deliveryId, $order->payment_method);
                        break;
                    case '032':
                        $this->sendPulseApiClient->startEventAutomation360('purchase_68', [
                            'email' => $order->email,
                            'phone' => $order->phone,
                        ]);
                        $this->add(self::ADDRESS_BOOK_032, $email,
                            $deliveryId, $order->payment_method);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    protected function add(string $addressBook, string $email, int $deliveryId = null, int $paymentId = null): void
    {
        $additionalParams = ($deliveryId || $paymentId) ? [
            'delivery_id' => $deliveryId,
            'payment_id' => $paymentId,
        ] : [];

        $this->addRaw($addressBook, $email, $additionalParams);
    }

    protected function addRaw(string $addressBook, string $email, array $additionalParams): void
    {
        $this->sendPulseApiClient->addEmails($addressBook, [
            [
                'email' => $email,
                'variables' => $additionalParams,
            ],
        ]);
    }

    protected function remove(string $addressBook, string $email): void
    {
        $this->sendPulseApiClient->removeEmails($addressBook, [$email]);
    }

    protected static function shippingCategoryToDeliveryId(?string $shippingCategory): ?int
    {
        switch ($shippingCategory) {
            case 'to-door':
                return self::DELIVERY_ID_COURIER;
            case 'delivery-point':
                return self::DELIVERY_ID_PVZ;
            case 'post-office':
                return self::DELIVERY_ID_RUSSIAN_POST;
        }

        return 0;
    }

    protected static function getDeliveryId(Order $order)
    {
        if ($order->country_code == 'KZ') {
            return self::DELIVERY_ID_COURIER_KAZAKHSTAN;
        } elseif ($order->country_code == 'BY') {
            return self::DELIVERY_ID_COURIER_BELARUS;
        }

        return self::shippingCategoryToDeliveryId(
            ShiptorService::shipmentMethodToCategory($order->shipping_method)
        );
    }
}
