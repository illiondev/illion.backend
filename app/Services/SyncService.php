<?php

namespace App\Services;

use App\Models\Bundle;
use App\Models\ExternalClient;
use App\Models\ExternalOrder;
use App\Models\ExternalOrderItem;
use App\Models\Product;
use DB;

class SyncService
{
    public function sync(ExternalClient $client, array $order) : ExternalOrder
    {
        /** @var ExternalOrder $externalOrder */
        $externalOrder = ExternalOrder::firstOrNew([
            'client_id' => $client->id,
            'external_id' => $order['external_id']
        ]);

        $externalOrder->client_id = $client->id;
        $externalOrder->external_id = $order['external_id'];
        $externalOrder->country_code = $order['country_code'] ?: '';
        $externalOrder->region = $order['region'] ?: '';
        $externalOrder->city = $order['city'] ?: '';
        $externalOrder->status = $order['status'];
        $externalOrder->price = $order['price'];
        $externalOrder->delivery_price = $order['delivery_price'];
        $externalOrder->total_price = $order['total_price'];
        $externalOrder->sold_at = $order['sold_at'];

        $externalOrder->saveOrFail();

        foreach ($order['items'] as $item) {
            if (isset($item['product_id'])) {
                $bundleId = DB::table('bundles_has_products')->where('product_id', $item['product_id'])->value('bundle_id');
            } else {
                $bundleId = $item['bundle_id'];
            }
            $bundle = Bundle::findOrFail($bundleId);

            /** @var ExternalOrderItem $externalItem */
            $externalItem = ExternalOrderItem::firstOrNew([
                'client_id' => $client->id,
                'external_order_id' => $externalOrder->external_id,
                'external_id' => $item['external_id']
            ]);

            $externalItem->external_id = $item['external_id'];
            $externalItem->external_order_id = $externalOrder->external_id;
            $externalItem->bundle_id = $bundle->id;
            $externalItem->quantity = $item['quantity'];
            $externalItem->prime_cost = $item['prime_cost'];
            $externalItem->price = $item['price'];
            $externalItem->net_profit = $item['net_profit'];
            $externalItem->referral_user_id = $item['referral_user_id'] ?? null;
            $externalItem->referral_amount = $item['referral_amount'] ?? 0;

            $externalItem->saveOrFail();
        }

        $externalOrder = $externalOrder->fresh();

        return $externalOrder;
    }
}
