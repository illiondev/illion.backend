<?php

namespace App\Services;

use App\Models\BillProduct;
use App\Models\Bundle;
use App\Models\Order;
use App\Models\OrderBill;
use App\Models\Price;
use Cache;
use Illuminate\Support\Str;

class OrderBillService
{
    protected $yandexMoneyService;

    public function __construct(
        YandexMoneyService $yandexMoneyService
    )
    {
        $this->yandexMoneyService = $yandexMoneyService;
    }


    public function complete(OrderBill $orderBill, string $paymentType): void
    {
        $orderBill->status = OrderBill::PAYMENT_STATUS_PAYED;
        $orderBill->payment_type = $paymentType;

        $fee = 0;
        if (!$orderBill->external_kassa) {
            if (isset(Order::$paymentTypeFee[$orderBill->payment_type])) {
                $fee = Order::$paymentTypeFee[$orderBill->payment_type];
            } else {
                app('sentry')->captureMessage('bill ' . $orderBill->id . ' unknown payment type ' . $orderBill->payment_type);
            }
        }

        $orderBill->payment_fee = $fee;
        $orderBill->net_profit = $orderBill->price * (1 - ($fee / 100));

        foreach ($orderBill->products as $product) {
            $product->net_profit = $product->price * (1 - ($fee / 100));
            $product->saveOrFail();
        }

        $orderBill->saveOrFail();
    }

    public function process(OrderBill $orderBill): array
    {
        $result = [];

        if ($orderBill->status !== OrderBill::PAYMENT_STATUS_NEW) {
            return [];
        }

        if (Cache::lock($orderBill->getLockKey(), 60 * 10)->block(5)) {
            try {
                /** @var OrderBill $orderBill */
                $orderBill = $orderBill->fresh();

                if ($orderBill->status === OrderBill::PAYMENT_STATUS_NEW) {
                    $this->yandexMoneyService->setAuthFromBill($orderBill);

                    $payment = $this->yandexMoneyService->createPaymentFromBill($orderBill);

                    $result = ['confirmation_url' => $payment['confirmation_url']];
                    $orderBill->status = $orderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT;
                    $orderBill->payment_id = $payment['id'];

                    $orderBill->saveOrFail();
                }
            } finally {
                Cache::lock($orderBill->getLockKey())->release();
            }
        }

        return $result;
    }

    public function getConfirmation(OrderBill $orderBill): array
    {
        Cache::lock($orderBill->getLockKey())->release();
        $result = [];

        if ($orderBill->status !== OrderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT) {
            return [];
        }

        if (Cache::lock($orderBill->getLockKey(), 60 * 10)->block(5)) {
            try {
                /** @var OrderBill $orderBill */
                $orderBill = $orderBill->fresh();

                if ($orderBill->status === $orderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT) {
                    $this->yandexMoneyService->setAuthFromBill($orderBill);

                    $payment = $this->yandexMoneyService->getPaymentInfo($orderBill->payment_id);

                    if ($payment) {
                        $result = $this->yandexMoneyService::getConfirmation($payment);
                        unset($result['id']);
                    }
                }
            } finally {
                Cache::lock($orderBill->getLockKey())->release();
            }
        }

        return $result;
    }

    public function checkPayment(OrderBill $orderBill): OrderBill
    {
        if ($orderBill->status !== OrderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT) {
            return $orderBill;
        }

        if (Cache::lock($orderBill->getLockKey(), 60 * 10)->block(5)) {
            try {
                /** @var OrderBill $orderBill */
                $orderBill = $orderBill->fresh();

                if ($orderBill->status === OrderBill::PAYMENT_STATUS_WAITING_FOR_PAYMENT) {
                    $this->yandexMoneyService->setAuthFromBill($orderBill);

                    $paymentInfo = $this->yandexMoneyService->getPaymentInfo($orderBill->payment_id);

                    if ($paymentInfo) {
                        switch ($paymentInfo->getStatus()) {
                            case $this->yandexMoneyService::STATUS_PENDING:
                                break;
                            case $this->yandexMoneyService::STATUS_WAITING_FOR_CAPTURE:
                                if ($this->yandexMoneyService->capturePayment($paymentInfo)) {
                                    $this->complete($orderBill, $paymentInfo->getPaymentMethod()->getType());
                                }
                                break;
                            case $this->yandexMoneyService::STATUS_SUCCEEDED:
                                $this->complete($orderBill, $paymentInfo->getPaymentMethod()->getType());
                                break;
                            case $this->yandexMoneyService::STATUS_CANCELED:
                                $orderBill->status = OrderBill::PAYMENT_STATUS_CANCELED;
                                $orderBill->saveOrFail();
                                break;
                        }
                    }
                }
            } finally {
                Cache::lock($orderBill->getLockKey())->release();
            }
        }

        return $orderBill;
    }

    public function generate(Bundle $bundle, string $email) : OrderBill
    {
        $bill = new OrderBill();

        if ($bill->bundle_id == 15) {
            $bill->external_kassa = 1;
        }

        $bill->price = $bundle->priceByCurrency(Price::CURRENCY_RUB);
        $bill->description = $bundle->title;

        $bill->order_id = null;
        $bill->bundle_id = $bundle->id;
        $bill->hash = Str::uuid();
        $bill->email = $email;
        $bill->user_id = null;
        $bill->payment_id = null;
        $bill->status = $bill::PAYMENT_STATUS_NEW;

        $bill->saveOrFail();

        foreach ($bundle->products as $product) {
            $billProduct = new BillProduct();

            $billProduct->bill_id = $bill->id;
            $billProduct->book_id = $product->book_id;
            $billProduct->product_id = $product->id;
            $billProduct->prime_cost = $product->prime_cost;
            $billProduct->author_id = $product->author_id;
            $billProduct->quantity = 1;
            $billProduct->authors_percent = $product->pivot->authors_percent;
            $billProduct->article = $product->article;
            $billProduct->book_title = $product->book->title;
            $billProduct->type = $product->type;
            $billProduct->bundle_id = $bundle->id;
            $billProduct->length = $product->length;
            $billProduct->width = $product->width;
            $billProduct->height = $product->height;
            $billProduct->weight = $product->weight;
            $billProduct->is_surprise = $product->pivot->is_surprise;
            $billProduct->price = $product->pivot->price;

            $billProduct->save();
        }

        return $bill;
    }
}
