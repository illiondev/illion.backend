<?php

namespace App\Services;

use App\Models\Bundle;
use App\Models\Order;
use App\Models\OrderItem;
use Cache;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use ShiptorRussiaApiClient\Client\Core\Response\ErrorResponse;
use ShiptorRussiaApiClient\Client\Shiptor;
use Throwable;

class ShiptorService
{
    const COUNTRIES = [
        'AU' => 'Австралия',
        'AT' => 'Австрия',
        'AZ' => 'Азербайджан',
        'AL' => 'Албания',
        'DZ' => 'Алжир',
        'AI' => 'Ангилья',
        'AO' => 'Ангола',
        'AG' => 'Антигуа и Барбуда',
        'AR' => 'Аргентина',
        'AM' => 'Армения',
        'AW' => 'Аруба',
        'AF' => 'Афганистан',
        'BS' => 'Багамские о-ва',
        'BD' => 'Бангладеш',
        'BB' => 'Барбадос',
        'BH' => 'Бахрейн',
        'BZ' => 'Белиз',
        'BE' => 'Бельгия',
        'BJ' => 'Бенин',
        'BM' => 'Бермудские о-ва',
        'BG' => 'Болгария',
        'BO' => 'Боливия',
        'BA' => 'Босния и Герцеговина',
        'BW' => 'Ботсвана',
        'BR' => 'Бразилия',
        'BN' => 'Бруней-Даруссалам',
        'BF' => 'Буркина-Фасо',
        'BI' => 'Бурунди',
        'BT' => 'Бутан',
        'VU' => 'Вануату',
        'GB' => 'Великобритания',
        'HU' => 'Венгрия',
        'VE' => 'Венесуэла',
        'VN' => 'Вьетнам',
        'GA' => 'Габон',
        'HT' => 'Гаити',
        'GY' => 'Гайана',
        'GM' => 'Гамбия',
        'GH' => 'Гана',
        'GT' => 'Гватемала',
        'GN' => 'Гвинея',
        'DE' => 'Германия',
        'GI' => 'Гибралтар',
        'HN' => 'Гондурас',
        'HK' => 'Гонконг',
        'GD' => 'Гренада',
        'GR' => 'Греция',
        'GE' => 'Грузия',
        'DK' => 'Дания',
        'CD' => 'Демократическая Республика Конго',
        'DJ' => 'Джибути',
        'DM' => 'Доминика',
        'DO' => 'Доминиканская Республика',
        'EG' => 'Египет',
        'ZM' => 'Замбия',
        'ZW' => 'Зимбабве',
        'IL' => 'Израиль',
        'IN' => 'Индия',
        'ID' => 'Индонезия',
        'JO' => 'Иордания',
        'IQ' => 'Ирак',
        'IR' => 'Иран',
        'IE' => 'Ирландия',
        'IS' => 'Исландия',
        'ES' => 'Испания',
        'IT' => 'Италия',
        'YE' => 'Йемен',
        'KY' => 'Каймановы о-ва',
        'KH' => 'Камбоджа',
        'CM' => 'Камерун',
        'CA' => 'Канада',
        'KE' => 'Кения',
        'CY' => 'Кипр',
        'CN' => 'Китай',
        'CO' => 'Колумбия',
        'CG' => 'Конго-Браззавиль',
        'KR' => 'Корея',
        'CR' => 'Коста-рика',
        'CI' => 'Кот-дивуар',
        'CU' => 'Куба',
        'KW' => 'Кувейт',
        'KG' => 'Кыргызстан',
        'CW' => 'Кюрасао',
        'LA' => 'Лаос',
        'LV' => 'Латвия',
        'LS' => 'Лесото',
        'LR' => 'Либерия',
        'LB' => 'Ливан',
        'LT' => 'Литва',
        'LU' => 'Люксембург',
        'MU' => 'Маврикий',
        'MR' => 'Мавритания',
        'MG' => 'Мадагаскар',
        'MO' => 'Макао',
        'MK' => 'Македония',
        'MW' => 'Малави',
        'MY' => 'Малайзия',
        'ML' => 'Мали',
        'MV' => 'Мальдивы',
        'MT' => 'Мальта',
        'MA' => 'Марокко',
        'MX' => 'Мексика',
        'MZ' => 'Мозамбик',
        'MD' => 'Молдова',
        'MN' => 'Монголия',
        'MM' => 'Мьянма',
        'NA' => 'Намибия',
        'NP' => 'Непал',
        'NE' => 'Нигер',
        'NG' => 'Нигерия',
        'NL' => 'Нидерланды',
        'NI' => 'Никарагуа',
        'NZ' => 'Новая Зеландия',
        'NC' => 'Новая Каледония',
        'NO' => 'Норвегия',
        'AE' => 'ОАЭ',
        'OM' => 'Оман',
        'PK' => 'Пакистан',
        'PA' => 'Панама',
        'PG' => 'Папуа - Новая Гвинея',
        'PY' => 'Парагвай',
        'PE' => 'Перу',
        'PL' => 'Польша',
        'PT' => 'Португалия',
        'RW' => 'Руанда',
        'RO' => 'Румыния',
        'SV' => 'Сальвадор',
        'ST' => 'Сан-томе и Принсипи',
        'SA' => 'Саудовская Аравия',
        'SC' => 'Сейшельские о-ва',
        'SN' => 'Сенегал',
        'VC' => 'Сент-винсент и Гренадины',
        'KN' => 'Сент-китс и Невис',
        'LC' => 'Сент-Люсия',
        'RS' => 'Сербия',
        'SG' => 'Сингапур',
        'SY' => 'Сирия',
        'SK' => 'Словакия',
        'SI' => 'Словения',
        'SB' => 'Соломоновы о-ва',
        'SD' => 'Судан',
        'SR' => 'Суринам',
        'US' => 'США',
        'TJ' => 'Таджикистан',
        'TH' => 'Таиланд',
        'TZ' => 'Танзания',
        'TC' => 'Теркс и Кайкос',
        'TG' => 'Того',
        'TT' => 'Тринидад и Тобаго',
        'TN' => 'Тунис',
        'TM' => 'Туркменистан',
        'TR' => 'Турция',
        'UG' => 'Уганда',
        'UZ' => 'Узбекистан',
        'UA' => 'Украина',
        'UY' => 'Уругвай',
        'FJ' => 'Фиджи',
        'PH' => 'Филиппины',
        'FI' => 'Финляндия (включая аландские о-ва)',
        'FR' => 'Франция',
        'HR' => 'Хорватия',
        'CF' => 'Центральная Африканская Республика',
        'TD' => 'Чад',
        'ME' => 'Черногория',
        'CZ' => 'Чехия',
        'CL' => 'Чили',
        'CH' => 'Швейцария',
        'SE' => 'Швеция',
        'LK' => 'Шри-ланка',
        'EC' => 'Эквадор',
        'GQ' => 'Экваториальная Гвинея',
        'ER' => 'Эритрея',
        'EE' => 'Эстония',
        'ET' => 'Эфиопия',
        'ZA' => 'Южная Африка',
        'JM' => 'Ямайка',
        'JP' => 'Япония',
        'PF' => 'Французская Полинезия',

        'AB' => 'Абхазия',
        'AD' => 'Андорра',
        'AQ' => 'Антарктида',
        'AS' => 'Американское Самоа',
        'AX' => 'Аландские острова',
        'BL' => 'Сен-Бартелеми',
        'BQ' => 'Бонэйр, Синт-Эстатиус и Саба',
        'BV' => 'Остров Буве',
        'CC' => 'Кокосовые острова',
        'CK' => 'Острова Кука',
        'CX' => 'Остров Рождества',
//        'DN' => '',
        'FK' => 'Фолклендские острова',
        'FM' => 'Микронезия',
        'FO' => 'Фареры',
        'GF' => 'Гвиана',
        'GL' => 'Гренландия',
        'GP' => 'Гваделупа',
        'GS' => 'Южная Георгия и Южные Сандвичевы Острова',
        'GU' => 'Гуам',
        'GW' => 'Гвинея-Бисау',
        'HM' => 'Херд и Макдональд',
        'IM' => 'Остров Мэн',
        'IO' => 'Британская территория в Индийском океане',
        'JE' => 'Джерси',
        'KI' => 'Кирибати',
        'KM' => 'Коморы',
        'KP' => 'Корейская Народно-Демократическая Республика',
        'LI' => 'Лихтенштейн',
//        'LN' => '',
        'LY' => 'Ливия',
        'MC' => 'Монако',
        'MF' => 'Сен-Мартен',
        'MH' => 'Маршалловы Острова',
        'MP' => 'Северные Марианские Острова',
        'MQ' => 'Мартиника',
        'MS' => 'Монтсеррат',
        'NF' => 'Остров Норфолк',
        'NR' => 'Науру',
        'NU' => 'Ниуэ',
 //       'OS' => '',
        'PM' => 'Сен-Пьер и Микелон',
        'PN' => 'Острова Питкэрн',
        'PR' => 'Пуэрто-Рико',
        'PS' => 'Государство Палестина',
        'PW' => 'Палау',
        'QA' => 'Катар',
        'RE' => 'Реюньон',
        'SH' => 'Острова Святой Елены, Вознесения и Тристан-да-Кунья',
        'SJ' => 'Шпицберген и Ян-Майен',
        'SL' => 'Сьерра-Леоне',
        'SM' => 'Сан-Марино',
        'SO' => 'Сомали',
        'SS' => 'Южный Судан',
        'SX' => 'Синт-Мартен',
        'SZ' => 'Эсватини',
        'TF' => 'Французские Южные и Антарктические Территории',
        'TK' => 'Токелау',
        'TL' => 'Восточный Тимор',
        'TO' => 'Тонга',
        'TV' => 'Тувалу',
        'TW' => 'Китайская Республика',
        'VA' => 'Ватикан',
        'VG' => 'Виргинские Острова (Великобритания)',
        'VI' => 'Виргинские Острова (США)',
        'WF' => 'Уоллис и Футуна',
        'WS' => 'Самоа',
        'YT' => 'Майотта',
    ];

    const ORDER_STATUS_REDIS_KEY = 'shiptor_order_status_redis_key_2_';
    const ORDER_CALCULATE_SHIPPING_EXPORT = 'shiptor_order_calculate_shipping_export_';
    const ORDER_CALCULATE_SHIPPING = 'shiptor_order_calculate_shipping_';
    const CACHE_TIME = 60; // hour

    const SHIPPING_METHOD_RUSSIAN_POST = 20;

    protected $shiptor;

    public function __construct()
    {
        $this->shiptor = new Shiptor(['API_KEY' => getenv('SHIPTOR_API_KEY')]);
    }

    /**
     * @see https://shiptor.ru/doc/#api-Shipping-getShippingMethods
     */
    public static function shipmentMethodToCategory(?int $shipmentMethod): ?string
    {
        switch ($shipmentMethod) {
            case 12:
            case 13:
            case 16:
            case 17:
            case 19:
            case 24:
            case 33:
            case 61:
            case 68:
            case 69:
            case 77:
            case 81:
            case 82:
            case 150:
            case 151:
            case 152:
                return 'to-door';
            case 11:
            case 14:
            case 18:
            case 25:
            case 35:
            case 53:
            case 67:
            case 70:
            case 80:
            case 83:
            case 187:
            case 191:
            case 193:
            case 194:
            case 221:
                return 'delivery-point';
            case 20:
                return 'post-office';
            case null:
                return '';
        }

        app('sentry')->captureMessage('unknown shipping method category  ' . $shipmentMethod);

        return null;
    }

    public function getInfo(Order $order): ?array
    {
        if ($order->shipping_status === Order::SHIPPING_STATUS_DELIVERED) {
            return [
                'status' => $order->shipping_status,
                'tracking_number' => $order->tracking_number,
                'sent_date' => $order->sent_date,
            ];
        }

        return Cache::remember(self::ORDER_STATUS_REDIS_KEY . $order->id, self::CACHE_TIME, function () use ($order) {
            $response = $this->shiptor
                ->ShippingEndpoint()
                ->getPackage()
                ->setId($order->shiptor_id)
                ->send();

            if ($response instanceof ErrorResponse) {
                app('sentry')->captureMessage($response->getMessage());
                return null;
            }

            $result = $response->getResult();
            $sentDate = null;

            if ($result->get('tracking_number')) {
                $histories = $result->get('history');

                foreach ($histories as $history) {
                    if (Str::lower($history['event'])==='отправлена') {
                        $sentDate= new Carbon($history['date']);
                    }
                }

                return [
                    'status' => $result->get('status'),
                    'tracking_number' => $result->get('tracking_number'),
                    'sent_date' => $sentDate,
                ];
            }
        });
    }

    public function calculateShipping(
        Order $order,
        Collection $orderItems
    ): float {
        if (in_array($order->country_code, ['RU', 'KZ', 'BY'])) {
            $methods = $this->getLocalShippingMethods($order, $orderItems);
        } elseif (isset(self::COUNTRIES[$order->country_code])) {
            $methods = $this->getExportShippingMethods($order, $orderItems);
        }

        foreach ($methods as $method) {
            if ($method['id'] == $order->shipping_method) {
                $deliveryCost = $method['delivery_cost'];
                break;
            }
        }

        if (!isset($deliveryCost)) {
            throw new Exception("cannot calculate shipping cost");
        }

        return $deliveryCost;
    }

    /**
     * @param  Order  $order
     * @param  Collection|Bundle[]|OrderItem[]  $orderItems
     * @return array
     */
    public function getLocalShippingMethods(Order $order, Collection $orderItems): array
    {
        $paperBooksTotalPrice = 0;
        $totalGoods = 0;
        foreach ($orderItems as $orderItem) {
            if ($orderItem->getPaperProducts()->count() > 0) {
                $totalGoods += $orderItem->quantity;
                $paperBooksTotalPrice += ($orderItem->price * $orderItem->quantity);
            }
        }

        // почта россии
        if ($order->shipping_method == self::SHIPPING_METHOD_RUSSIAN_POST) {
            $extraGoods = $totalGoods > 2 ? $totalGoods - 2 : 0;
            $additionalPrice = 116 + ($extraGoods * 10);
        // пвз
        } elseif (self::shipmentMethodToCategory($order->shipping_method) === 'delivery-point') {
            $extraGoods = $totalGoods > 3 ? $totalGoods - 3 : 0;
            $additionalPrice = 60 + ($extraGoods * 10);
        // курьер
        } else {
            $extraGoods = $totalGoods > 2 ? $totalGoods - 2 : 0;
            $additionalPrice = 86 + ($extraGoods * 10);
        }

        $orderHash = md5($order->length . '_' . $order->width . '_' . $order->height . '_' . $order->weight . '_' . $order->kladr_id . '_' . $paperBooksTotalPrice);

        $methods = Cache::remember(self::ORDER_CALCULATE_SHIPPING . $orderHash, self::CACHE_TIME,
            function () use ($order, $paperBooksTotalPrice, $additionalPrice) {
                $response = $this->shiptor
                    ->ShippingEndpoint()
                    ->calculateShipping()
                    ->setLength($order->length)
                    ->setWidth($order->width)
                    ->setHeight($order->height)
                    ->setWeight($order->weight)
                    ->setKladrId($order->kladr_id)
                    ->setCod(0)
                    ->setDeclaredCost($paperBooksTotalPrice)
                    ->setCountryCode($order->country_code)
                    ->send();

                if ($response) {
                    if ($response instanceof ErrorResponse) {
                        app('sentry')->captureMessage($response->getMessage());
                    } else {
                        return $this->transformMethods($response->getResult()->get('methods'), $additionalPrice);
                    }
                }

                throw new Exception("cannot get shipping methods");
            });

        return $methods;
    }

    /**
     * @param  Order  $order
     * @param  Collection|Bundle[]|OrderItem[]  $orderItems
     * @return array
     */
    public function getExportShippingMethods(Order $order, Collection $orderItems): array
    {
        $paperBooksTotalPrice = 0;
        $totalGoods = 0;
        foreach ($orderItems as $orderItem) {
            if ($orderItem->getPaperProducts()->count() > 0) {
                $totalGoods += $orderItem->quantity;
                $paperBooksTotalPrice += ($orderItem->price * $orderItem->quantity);
            }
        }

        // почта россии
        if ($order->shipping_method == self::SHIPPING_METHOD_RUSSIAN_POST) {
            $extraGoods = $totalGoods > 2 ? $totalGoods - 2 : 0;
            $additionalPrice = 116 + ($extraGoods * 10);
            // пвз
        } elseif (self::shipmentMethodToCategory($order->shipping_method) === 'delivery-point') {
            $extraGoods = $totalGoods > 3 ? $totalGoods - 3 : 0;
            $additionalPrice = 60 + ($extraGoods * 10);
            // курьер
        } else {
            $extraGoods = $totalGoods > 2 ? $totalGoods - 2 : 0;
            $additionalPrice = 86 + ($extraGoods * 10);
        }

        $orderHash = md5($order->length . '_' . $order->width . '_' . $order->height . '_' . $order->weight . '_' . $order->country_code);

        $methods = Cache::remember(self::ORDER_CALCULATE_SHIPPING_EXPORT . $orderHash, self::CACHE_TIME,
            function () use ($order, $paperBooksTotalPrice, $additionalPrice) {
                $response = $this->shiptor
                    ->ShippingEndpoint()
                    ->calculateShippingInternational()
                    ->setLength($order->length)
                    ->setWidth($order->width)
                    ->setHeight($order->height)
                    ->setWeight($order->weight)
                    ->setDeclaredCost($paperBooksTotalPrice)
                    ->fromRu()
                    ->setCountryTo($order->country_code)
                    ->send();

                if ($response) {
                    if ($response instanceof ErrorResponse) {
                        app('sentry')->captureMessage($response->getMessage());
                    } else {
                        return $this->transformMethods($response->getResult()->get('methods'), $additionalPrice);
                    }
                }

                throw new Exception("cannot get shipping methods");
            });

        return $methods;
    }

    protected function transformMethods(array $methods, float $additionalPrice): array
    {
        $result = [];

        foreach ($methods as $method) {
            $result[] = [
                'id' => (int) $method['method']['id'],
                'name' => $method['method']['name'],
                'delivery_cost' => ((float) $method['cost']['total']['sum']) + $additionalPrice,
                'days' => $method['days'],
            ];
        }

        return $result;
    }

    /**
     * @param  Collection|Bundle[]|OrderItem[] $orderItems
     * @return array
     */
    public function calculateDimensions(Collection $orderItems): array
    {
        // algo: https://shiptor.ru/help/integration/api/api-cases#article_8
        $sumV = 0;
        foreach ($orderItems as $orderItem) {
            $products = $orderItem->getPaperProducts();

            foreach ($products as $paperProduct) {
                $sumV += (($paperProduct->length * $paperProduct->width * $paperProduct->height) * $orderItem->quantity);
            }
        }

        $sumVcbrt = round(pow($sumV, 1 / 3));

        $calcVar = 2;
        $max = 0;
        foreach ($orderItems as $orderItem) {
            $products = $orderItem->getPaperProducts();

            foreach ($products as $paperProduct) {
                $max = max($max, $paperProduct->length, $paperProduct->width, $paperProduct->height);
                if ($paperProduct->length > $sumVcbrt || $paperProduct->width > $sumVcbrt || $paperProduct->height > $sumVcbrt) {
                    $calcVar = 1;
                }
            }
        }

        if ($calcVar == 1) {
            $totalLength = $max;
            $totalWidth = round(pow($sumV / $max, 1 / 2));
            $totalHeight = round(pow($sumV / $max, 1 / 2));
        } else {
            $totalLength = $sumVcbrt;
            $totalWidth = $sumVcbrt;
            $totalHeight = $sumVcbrt;
        }

        return [
            'length' => $totalLength,
            'width' => $totalWidth,
            'height' => $totalHeight,
        ];
    }

    public function send(
        Collection $paperProducts,
        Order $order
    ): ?int {
        if (!app()->environment('production')) {
            return false;
        }

        $request = $this->shiptor->ShippingEndpoint();

        $export = !in_array($order->country_code, ['RU', 'KZ', 'BY']);

        if ($export) {
            $request = $request->addPackageExport();
            $request->setAddressLine2($order->address2);

            if (app()->environment('production')) {
                $request
                    ->setReciever($order->name . ' ' . $order->surname . ' ' . $order->patronimic);
            } else {
                $request->setReciever('Тест Тестов Тестович');
            }
        } else {
            $request = $request->addPackage()
                ->setKladrId($order->kladr_id)
                ->setAdditionalService('additional-pack')
                ->setCashlessPayment($order->payment_method === Order::PAYMENT_METHOD_CARD);

            if (app()->environment('production')) {
                $request
                    ->setName($order->name)
                    ->setSurname($order->surname)
                    ->setPatronimic($order->patronimic);
            } else {
                $request
                    ->setName('Тест')
                    ->setSurname('Тестов')
                    ->setPatronimic('Тестович');
            }

            $request->newService()->setShopArticle('10')->setCount(1)->setPrice($order->delivery_price);

            $request->setDeclaredCost($paperProducts->count() * 290);

            if ($order->isPaymentOnline()) {
                $request->setCod(0); // сумма наложенного платежа
            } else {
                $request->setCod($order->total_price); // сумма наложенного платежа
            }
        }

        if ($order->name === 'Тест' || !app()->environment('production')) {
            $request->noGather();
        }

        $request
            ->setExternalId('illion_' . $order->id)// номер заказа (order id)
            ->setShippingMethod($order->shipping_method)// способ доставки
            ->setComment($order->comment)
            ->setEmail($order->email)
            ->setPhone($order->phone)
            ->setCountryCode($order->country_code)
            ->setRegion($order->region)
            ->setSettlement($order->settlement)
            ->setAddressLine($order->address)
            ->setPostalCode($order->postal_code);

        if ($order->delivery_point) {
            $request->setDeliveryPoint($order->delivery_point);
        }

        $dops = [];
        foreach ($order->items as $orderItem) {
            $paperProducts = $orderItem->getPaperProducts();

            foreach ($paperProducts as $paperProduct) {
                $product = $request->newProduct();

                if ($export) {
                    $product->setEnglishName($paperProduct->article);
                }

                $product
                    ->setShopArticle($paperProduct->article)
                    ->setCount($orderItem->quantity)
                    ->setPrice(290);

                if ($paperProduct->article === '007') {
                    $dops['S1'] = $orderItem->quantity;
                    $dops['S2'] = $orderItem->quantity;
                }

                if ($paperProduct->article === '008') {
                    $dops['SS1'] = $orderItem->quantity;
                    $dops['SS2'] = $orderItem->quantity;
                }
            }
        }

        foreach ($dops as $key => $value) {
            $product = $request->newProduct();

            if ($export) {
                $product->setEnglishName($key);
            }

            $product
                ->setShopArticle($key)
                ->setCount($value)
                ->setPrice(0);
        }

        $dimensions = $this->calculateDimensions($order->items);

        $request->setLength($dimensions['length'])
            ->setWidth($dimensions['width'])
            ->setHeight($dimensions['height'])
            ->setWeight($order->weight);

//        $request->validate();

        try {
            $result = $request->send();

            if ($result instanceof ErrorResponse) {
                $order->shiptor_error = $result->getMessage();
                $order->save();
//                app('sentry')->captureMessage($result->getMessage() . ' | order:' . $order->id);

                return null;
            }

            return $result->getResult()->get('id');
        } catch (Throwable $exception) {
            app('sentry')->captureException($exception);

            return null;
        }
    }
}
