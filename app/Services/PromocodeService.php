<?php

namespace App\Services;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Promocode;
use Exception;

class PromocodeService
{
    public function applyPromocodeToOrderItem(Promocode $promocode, OrderItem $orderItem): OrderItem
    {
        if ($promocode->bundle_id === $orderItem->bundle_id) {
            $orderItem->promocode = $promocode->code;

            if ($promocode->discount_flat) {
                $orderItem->discount_flat = $promocode->discount_flat;
            } elseif ($promocode->discount) {
                $orderItem->discount = $promocode->discount;
            }
        }

        return $orderItem;
    }

    public function applyPromocodeToOrder(Promocode $promocode, Order $order): Order
    {
        $order->promocode = $promocode->code;

        $promocode->use();

        if ($promocode->isDeliveryType()) {
            return $this->applyDeliveryPromocodeToOrder($promocode, $order);
        }

        if ($promocode->isOverallType()) {
            return $this->applyOverallPromocodeToOrder($promocode, $order);
        }

        return $order;
    }

    protected function applyDeliveryPromocodeToOrder(Promocode $promocode, Order $order): Order
    {
        if ($promocode->discount_flat) {
            $order->delivery_discount_flat = $promocode->discount_flat;
        } elseif ($promocode->discount) {
            $order->delivery_discount = $promocode->discount;
        }

        if ($order->delivery_discount_flat) {
            $order->delivery_price = $order->delivery_price - $order->delivery_discount_flat;
        } elseif ($order->delivery_discount) {
            $order->delivery_price = $order->delivery_price * (1 - ($order->delivery_discount / 100));
        }

        if ($order->delivery_price < 0) {
            $order->delivery_price = 0;
        }

        $order->total_price = $order->price + $order->delivery_price;

        return $order;
    }

    public function applyOverallPromocodeToOrder(Promocode $promocode, Order $order): Order
    {
        if ($promocode->discount_flat) {
            $order->discount_flat = $promocode->discount_flat;
        } elseif ($promocode->discount) {
            $order->discount = $promocode->discount;
        }

        if ($order->discount_flat) {
            $order->total_price = $order->total_price - $order->discount_flat;
        } elseif ($order->discount) {
            $order->total_price = $order->total_price * (1 - ($order->discount / 100));
        }

        if ($order->total_price < 0) {
            $order->total_price = 0;
        }

        return $order;
    }
}
