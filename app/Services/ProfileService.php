<?php

namespace App\Services;

use App\Models\Profile;
use Illion\Service\Models\User as IllionUser;

class ProfileService
{
    public function update(Profile $profile, array $params, IllionUser $illionUser = null): void
    {
        $profile->fill($params);

        if ($illionUser) {
            $profile = $this->updateProfileFromIllion($profile, $illionUser);
        }

        $profile->saveOrFail();
    }

    protected function updateProfileFromIllion(Profile $profile, IllionUser $illionUser): Profile
    {
        if ($profile->user->external_id == $illionUser->id) {
            $profile->name = $illionUser->name;
            $profile->gender = $illionUser->gender;
            $profile->birthday = $illionUser->birthday ?: null;
            $profile->country_id = $illionUser->country_id ?: 0;
            $profile->country_names = $illionUser->country_names ?: null;
            $profile->city_id = $illionUser->city_id ?: 0;
            $profile->city_names = $illionUser->city_names ?: null;
        }

        return $profile;
    }
}
