<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionsServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
    }

    public function register(): void
    {
        app(\Dingo\Api\Exception\Handler::class)->register(function (ModelNotFoundException $exception): void {
            throw new NotFoundHttpException();
        });
    }
}
