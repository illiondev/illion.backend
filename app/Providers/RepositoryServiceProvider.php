<?php

namespace App\Providers;

use App\Repositories\PreferenceOptionRepository;
use App\Repositories\PreferenceRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(PreferenceOptionRepository::class);
        $this->app->singleton(PreferenceRepository::class);
        $this->app->singleton(UserRepository::class);
    }
}
