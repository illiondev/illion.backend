<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (!app()->environment('production')) {
    Route::get('apidoc/main', function () {
        $openapi = \OpenApi\scan('../app');
        header('Content-Type: application/json');
        echo $openapi->toJson();
    });

    Route::get('apidoc/author', function () {
        $openapi = \OpenApi\scan('../Modules/Author');
        header('Content-Type: application/json');
        echo $openapi->toJson();
    });

    Route::get('apidoc/moderator', function () {
        $openapi = \OpenApi\scan('../Modules/Moderator');
        header('Content-Type: application/json');
        echo $openapi->toJson();
    });
}
