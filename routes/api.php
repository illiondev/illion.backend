<?php

use Dingo\Api\Routing\Router as ApiRouter;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app(ApiRouter::class);

$api->version(['v1'], [
    'namespace' => 'App\Http\Controllers\Api',
    'prefix'    => 'api',
], function (ApiRouter $api) {
    /**
     * yandex kassa callback
     */
    $api->get('payment', 'YandexKassaController@callback');

    $api->get('system/ping', 'SystemController@ping');

    if (!app()->environment('production')) {
        $api->any('system/test', 'SystemController@test');
    }
});

$api->version('v1', [
    'namespace' => 'App\Http\Controllers\Api\V1',
    'prefix'    => 'api/v1',
], function (ApiRouter $api) {
    /**
     * Announcement
     */
    $api->get('announcement', 'AnnouncementController@show');

    /**
     * Vacancies
     */
    $api->resource('vacancies', 'VacancyController', ['only' => ['index', 'show']]);

    /**
     * Auth
     */
    $api->post('auth/login', 'AuthController@login');
    $api->post('auth/refresh', 'AuthController@refresh');
    $api->post('auth/register', 'AuthController@register');
    $api->post('auth/fast_register', 'AuthController@fastRegister');

    /**
     * Soon
     */
    $api->resource('soon', 'SoonController', ['only' => ['index']]);

    /**
     * Books
     */
    $api->resource('books', 'BookController', ['only' => ['index', 'show']]);
    $api->get('books/{hash}/preview', 'BookController@preview');

    /**
     * Authors
     */
    $api->resource('authors', 'AuthorController', ['only' => ['index']]);

    /**
     * Bundles
     */
    $api->get('bundles/{id}', 'BundleController@show');
    $api->post('bundles/{id}/bills', 'BundleController@generate');

    /**
     * Products
     */
    // TODO: tmp method, remove later
    $api->get('products/{key}/raw', 'BookshelfController@downloadRaw');


    /**
     * Password
     */
    $api->post('auth/password/forgot', 'PasswordController@forgot');

    /**
     * Feedback
     */
    $api->post('feedback/deal', 'FeedbackController@deal');
    $api->post('feedback/support', 'FeedbackController@support');
    $api->post('feedback/question', 'FeedbackController@question');
    $api->post('feedback/subscribe', 'FeedbackController@subscribe');

    /**
     * Cart
     */
    $api->get('cart', 'CartController@index')->name('cart');
    $api->post('cart/set/{bundleId}/{quantity}', 'CartController@set')->name('cart/set/bundle');
    $api->post('cart/flush', 'CartController@flush')->name('cart/flush');
    $api->post('cart/promocode', 'CartController@setPromocode')->name('cart/set/promocode');

    $api->post('cart/hash', 'CartController@createHash')->name('cart/create/hash');

    /**
     * Order
     */
    $api->post('orders/calculate_shipping', 'ShiptorController@calculateShipping')
        ->name('orders/calculate_shipping');
    $api->post('orders/export/shipping_methods', 'ShiptorController@getExportShippingMethods')
        ->name('orders/export/shipping_methods');
    $api->get('shiptor/countries', 'ShiptorController@getCountries')->name('shiptor/countries');

    /**
     * Bills
     */
    $api->get('bills/{hash}', 'OrderBillController@show');
    $api->post('bills/{hash}', 'OrderBillController@confirm');
    $api->post('bills/{hash}/confirmation_url', 'OrderBillController@getConfirmationUrl');

    /**
     * Instant order
     */
    $api->post('instant_orders/{orderId}/confirm', 'OrderInstantController@confirm');
    $api->post('instant_orders/{orderId}/payed', 'OrderInstantController@checkPayment');
    $api->post('instant_orders/{orderId}/confirmation_url', 'OrderInstantController@getConfirmationUrl');

    $api->group(['middleware' => [
        'api.auth',
        'status.not_blocked',
    ]], function (ApiRouter $api) {

        /**
         * Location
         */
        $api->get('geo', 'LocationController@geoInfo');
        $api->get('geo/autocomplete/city', 'LocationController@cityAutocomplete');

        /**
         * Auth
         */
        $api->post('auth/logout', 'AuthController@logout');

        /**
         * Profile (self)
         */
        $api->get('profile/me', 'UserController@show');
        $api->put('profile/me', 'UserController@update');

        /**
         * Preferences
         */
        $api->resource('preferences', 'PreferenceController', ['only' => ['index']]);
        $api->post('profile/me/preferences/{preferenceId}/options/sync', 'PreferenceOptionController@sync');

        /**
         * Media
         */
        $api->post('profile/me/media', 'MediaController@upload');
        $api->delete('profile/me/media', 'MediaController@delete');

        /**
         * Books
         */
        $api->get('profile/me/books', 'BookshelfController@index');
        $api->get('profile/me/books/{id}', 'BookshelfController@show');

        /**
         * Products
         */
        $api->get('profile/me/products/{product}', 'BookshelfController@downloadByArticle')
            ->where(['product' => '[0-9A-Za-z\-]+']);
        $api->get('profile/me/products/{product}/show', 'BookshelfController@productShowByArticle')
            ->where(['product' => '[0-9A-Za-z\-]+']);
        $api->get('profile/me/products/{productId}', 'BookshelfController@download')
            ->where(['productId' => '[0-9]+']);
        $api->get('profile/me/products/{productId}/show', 'BookshelfController@productShow')
            ->where(['productId' => '[0-9]+']);

        // TODO: tmp method, remove later
        $api->get('profile/me/products/{productId}/key', 'BookshelfController@downloadKey')
            ->where(['productId' => '[0-9]+']);

        /**
         * Instant order
         */
        $api->post('instant_orders/submit/{product}', 'OrderInstantController@submit');

        /**
         * Order
         */
        $api->post('orders/submit', 'OrderController@submit')->name('orders/submit');
        $api->get('profile/me/orders', 'OrderController@index')->name('profile/orders');
        $api->post('orders/{id}/confirm', 'OrderController@confirm')->name('orders/confirm');
        $api->post('orders/{orderId}/payed', 'OrderController@checkPayment')->name('orders/payed');
        $api->post('orders/{orderId}/confirmation_url', 'OrderController@getConfirmationUrl')
            ->name('orders/confirmation_url');

        $api->post('orders/{id}/confirm/android', 'OrderController@confirmAndroid')
            ->name('orders/confirm/android');
        /**
         * Address book
         */
        $api->get('address/default', 'AddressBookController@getDefault');
    });

    $api->group(['middleware' => [
        'api.client',
    ]], function (ApiRouter $api) {
        $api->post('sync', 'SyncController@sync');
    });
});
