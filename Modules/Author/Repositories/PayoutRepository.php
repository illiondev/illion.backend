<?php

namespace Modules\Author\Repositories;

use Illuminate\Support\Collection;
use Modules\Moderator\Models\Payout;

class PayoutRepository
{
    protected $model;

    public function __construct(Payout $model)
    {
        $this->model = $model;
    }

    public function allByUserAuthorId(int $userAuthorId): Collection
    {
        return $this->model
            ->where(['author_id' => $userAuthorId])
            ->orderBy('created_at', 'desc')
            ->get();
    }
}
