<?php

namespace Modules\Author\Repositories;

use App\Models\Author;
use Illuminate\Database\Eloquent\Collection;

class AuthorRepository
{
    protected $model;

    public function __construct(Author $model)
    {
        $this->model = $model;
    }

    public function findOrFailByUserId(int $userId): Author
    {
        return $this->model
            ->whereNotNull('user_id')
            ->where(['user_id' => $userId])
            ->firstOrFail();
    }

    public function findOrFail(int $authorId): Author
    {
        return $this->model
            ->findOrFail($authorId);
    }

    public function all(): Collection
    {
        return $this->model
            ->get();
    }
}
