<?php

namespace Modules\Author\Repositories;

use Illuminate\Support\Collection;
use Modules\Author\Models\Bundle;

class BundleRepository
{
    protected $model;

    public function __construct(Bundle $model)
    {
        $this->model = $model;
    }

    public function allByAuthorId(int $authorId): Collection
    {
        return $this
            ->model
            ->whereHas('products', function ($query) use ($authorId): void {
                $query->where(['book_products.author_id' => $authorId]);
            })
            ->get();
    }
}
