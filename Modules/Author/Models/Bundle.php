<?php

namespace Modules\Author\Models;

use App\Models\BillProduct;
use App\Models\Bundle as BaseBundle;
use App\Models\ExternalSaleProduct;
use App\Models\Order;
use App\Models\OrderBill;
use App\Models\OrderProduct;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;

class Bundle extends BaseBundle
{
    public function orderProducts(int $authorId): HasMany
    {
        return $this
            ->hasMany(OrderProduct::class)
            ->whereHas('order', function ($query): void {
                /** @var Builder $query */
                $query->whereIn('orders.status', [Order::STATUS_PROCESSING, Order::STATUS_COMPLETED]);
            })
            ->where(['author_id' => $authorId]);
    }

    public function externalSalesProducts(int $authorId): HasMany
    {
        return $this
            ->hasMany(ExternalSaleProduct::class)
            ->whereHas('externalSale', function ($query): void {
                /** @var Builder $query */
                $query->where(['is_paid' => true]);
            })
            ->where(['author_id' => $authorId]);
    }

    public function billsProducts(int $authorId): HasMany
    {
        return $this->hasMany(BillProduct::class)
            ->whereHas('bill', function ($query): void {
                /** @var Builder $query */
                $query->where(['status' => OrderBill::PAYMENT_STATUS_PAYED]);
            })
            ->where(['author_id' => $authorId]);
    }
}
