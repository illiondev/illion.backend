<?php

use Dingo\Api\Routing\Router as ApiRouter;
use Modules\Moderator\Helpers\Permissions;

$api = app(ApiRouter::class);

/**
 * Author's API
 */
$api->version('v1', [
    'prefix' => 'api/author/v1'
], function (ApiRouter $api) {
    /**
     * Auth
     */
    $api->group(['namespace' => 'App\Http\Controllers\Api\V1'], function (ApiRouter $api) {
        /**
         * Auth
         */
        $api->post('auth/login', 'AuthController@login');
        $api->post('auth/refresh', 'AuthController@refresh');
        $api->post('auth/register', 'AuthController@register');

        /**
         * Password
         */
        $api->post('auth/password/forgot', 'PasswordController@forgot');

        /**
         * Auth
         */
        $api->group(['middleware' => ['api.auth']], function (ApiRouter $api) {
            $api->post('auth/logout', 'AuthController@logout');
        });
    });

    $api->group([
        'namespace' => 'Modules\Author\Http\Controllers\Api\V1',
        'middleware' => [
            'api.auth',
            'status.not_blocked',
            'roles.author'
        ],
    ], function (ApiRouter $api) {
        /**
         * Profile (self)
         */
        $api->get('profile/me', 'UserController@me');

        /**
         * Statistics
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_AUTHORS_STATISTICS]], function (ApiRouter $api) {
            $api->resource('statistics', 'StatisticsController', ['only' => ['index']]);
        });

        $api->group(['middleware' => ['roles.moderator']], function (ApiRouter $api) {
            /**
             * Authors
             */
            $api->resource('authors', 'AuthorController', ['only' => ['index']]);

            $api->group(['middleware' => ['permission:' . Permissions::READ_AUTHORS_PAYOUTS]], function (ApiRouter $api) {
                $api->get('payouts/{userId}', 'PayoutController@indexByUser');
            });

            /**
             * Statistics
             */
            $api->group(['middleware' => ['permission:' . Permissions::READ_AUTHORS_STATISTICS]], function (ApiRouter $api) {
                $api->get('statistics/{userId}', 'StatisticsController@indexByAuthor');
            });
        });

        /**
         * Payouts
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_AUTHORS_PAYOUTS]], function (ApiRouter $api) {
            $api->resource('payouts', 'PayoutController', ['only' => ['index']]);
        });
    });
});
