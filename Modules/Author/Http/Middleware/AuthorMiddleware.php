<?php

namespace Modules\Author\Http\Middleware;

use Auth;
use Closure;

class AuthorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() &&
            (Auth::user()->isAuthor() ||
            Auth::user()->isModerator())
        ) {
            return $next($request);
        }

        abort(403);
    }
}
