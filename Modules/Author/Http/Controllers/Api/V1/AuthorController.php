<?php

namespace Modules\Author\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Author\Http\Transformers\V1\AuthorTransformer;
use Modules\Author\Repositories\AuthorRepository;

class AuthorController extends ApiController
{
    protected $authorRepository;

    public function __construct(AuthorRepository $authorRepository)
    {
        parent::__construct();

        $this->authorRepository = $authorRepository;
    }

    /**
     * @OA\Get(
     *     path="/authors",
     *     tags={"authors"},
     *     summary="List of authors",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/Author")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(): Response
    {
        $authors = $this->authorRepository->all();

        return $this->response->collection($authors, new AuthorTransformer());
    }
}
