<?php

namespace Modules\Author\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Author\Http\Transformers\V1\BundleTransformer;
use Modules\Author\Repositories\AuthorRepository;
use Modules\Author\Repositories\BundleRepository;

class StatisticsController extends ApiController
{
    protected $authorRepository;
    protected $bundleRepository;

    public function __construct(
        AuthorRepository $authorRepository,
        BundleRepository $bundleRepository
    )
    {
        parent::__construct();

        $this->authorRepository = $authorRepository;
        $this->bundleRepository = $bundleRepository;
    }

    /**
     * @OA\Get(
     *     path="/statistics",
     *     tags={"statistics"},
     *     summary="Products sales statistics",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/Bundle")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(): Response
    {
        $author = $this->authorRepository->findOrFailByUserId($this->user->id);
        $bundles = $this->bundleRepository->allByAuthorId($author->id);

        $bundleTransformer = app(BundleTransformer::class);
        $bundleTransformer->authorId = $author->id;

        return $this->response->collection($bundles, $bundleTransformer);
    }

    /**
     * @OA\Get(
     *     path="/statistics/{author id}",
     *     tags={"statistics"},
     *     summary="Products sales statistics",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/Bundle")
     *             ),
     *         )
     *     ),
     * )
     */
    public function indexByAuthor(int $authorId): Response
    {
        $author = $this->authorRepository->findOrFail($authorId);
        $bundles = $this->bundleRepository->allByAuthorId($author->id);

        $bundleTransformer = app(BundleTransformer::class);
        $bundleTransformer->authorId = $author->id;

        return $this->response->collection($bundles, $bundleTransformer);
    }
}