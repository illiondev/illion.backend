<?php

namespace Modules\Author\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Author\Repositories\PayoutRepository;
use Modules\Moderator\Http\Transformers\V1\PayoutTransformer;

class PayoutController extends ApiController
{
    protected $payoutRepository;

    public function __construct(PayoutRepository $payoutRepository)
    {
        parent::__construct();

        $this->payoutRepository = $payoutRepository;
    }

    /**
     * @OA\Get(
     *     path="/payouts",
     *     tags={"payouts"},
     *     summary="List of payouts",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/author_Payout")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(): Response
    {
        $payouts = $this->payoutRepository->allByUserAuthorId($this->user->id);

        return $this->response->collection($payouts, new PayoutTransformer());
    }

    /**
     * @OA\Get(
     *     path="/payouts/{author user id}",
     *     tags={"payouts"},
     *     summary="List of payouts",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/author_Payout")
     *             ),
     *         )
     *     ),
     * )
     */
    public function indexByUser(int $userId): Response
    {
        $payouts = $this->payoutRepository->allByUserAuthorId($userId);

        return $this->response->collection($payouts, new PayoutTransformer());
    }
}
