<?php

namespace Modules\Author\Http\Controllers\Api\V1;

use Modules\Author\Http\Transformers\V1\UserTransformer;

class UserController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/profile/me",
     *     tags={"users"},
     *     summary="Show current user",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/User"),
     *         )
     *     ),
     * )
     */
    public function me()
    {
        return $this->response->item($this->user, new UserTransformer());
    }
}
