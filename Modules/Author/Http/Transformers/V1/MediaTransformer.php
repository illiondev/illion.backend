<?php

namespace Modules\Author\Http\Transformers\V1;

use App\Http\Transformers\V1\MediaTransformer as BaseMediaTransformer;

/**
 * @OA\Schema(
 *     schema="Media",
 *     title="Media",
 *     type="object",
 * )
 */
class MediaTransformer extends BaseMediaTransformer
{
    /**
     * @OA\Property(
     *     property="id",
     *     description="",
     *     type="integer",
     * )
     */
    /**
     * @OA\Property(
     *      property="category",
     *      type="string",
     *      enum={"avatar", "album"},
     *      description=""
     * )
     */
    /**
     * @OA\Property(
     *      property="name",
     *      type="string",
     *      description=""
     * )
     */
    /**
     * @OA\Property(
     *      property="mime_type",
     *      type="string",
     *      description=""
     * )
     */
    /**
     * @OA\Property(
     *      property="url",
     *      type="string",
     *      description=""
     * )
     */
}
