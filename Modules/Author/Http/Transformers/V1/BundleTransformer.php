<?php

namespace Modules\Author\Http\Transformers\V1;

use App\Models\Book;
use App\Models\Product;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use Modules\Author\Models\Bundle;
use Modules\Author\Services\StatisticService;

/**
 * @OA\Schema(
 *     schema="Bundle",
 *     title="Bundle",
 *     description="Бандл",
 *     type="object",
 * )
 */
class BundleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'cover',
    ];

    public $authorId;

    /**
     * @var StatisticService
     */
    protected $statisticService;

    public function __construct(StatisticService $statisticService)
    {
        $this->statisticService = $statisticService;
    }

    public function transform(Bundle $bundle): array
    {
        return [
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'id' => $bundle->id,
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $bundle->title,
            /**
             * @OA\Property(
             *     property="type",
             *     description="",
             *     type="integer"
             * )
             */
            'type' => $bundle->getType(),
            /**
             * @OA\Property(
             *     property="prime_cost",
             *     description="",
             *     type="float"
             * )
             */
            'prime_cost' => round($this->statisticService->totalPrimeCost($bundle, $this->authorId), 2),
            /**
             * @OA\Property(
             *     property="total_sold",
             *     description="",
             *     type="integer"
             * )
             */
            'total_sold' => $this->statisticService->totalSoldCount($bundle, $this->authorId),
            /**
             * @OA\Property(
             *     property="profit",
             *     description="",
             *     type="float"
             * )
             */
            'profit' => round($this->statisticService->profit($bundle, $this->authorId), 2),
        ];
    }

    /**
     * @OA\Property(
     *    property="cover",
     *    description="обложка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     * ),
     */
    public function includeCover(Bundle $bundle): ?Item
    {
        $cover = $bundle->getFirstMedia(Bundle::MEDIA_CATEGORY_COVER);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        /** @var Product $firstProduct */
        $firstProduct = $bundle->products()->where(['author_id' => $this->authorId])->first();
        $cover = $firstProduct->book->getFirstMedia(Book::MEDIA_CATEGORY_COVER_SMALL);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }
}
