<?php

namespace Modules\Author\Http\Transformers\V1;

use App\Models\Author;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Author",
 *     title="Author",
 *     type="object",
 * )
 */
class AuthorTransformer extends TransformerAbstract
{
    public function transform(Author $author): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $author->id,
            /**
             * @OA\Property(
             *     property="name",
             *     description="",
             *     type="string"
             * )
             */
            'name' => $author->name,
            /**
             * @OA\Property(
             *     property="surname",
             *     description="",
             *     type="string"
             * )
             */
            'surname' => $author->surname,
        ];
    }
}
