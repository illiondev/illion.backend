<?php

namespace Modules\Author\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\Payout;

/**
 * @OA\Schema(
 *     schema="author_Payout",
 *     title="Payout",
 *     type="object",
 * )
 */
class PayoutTransformer extends TransformerAbstract
{
    public function transform(Payout $payout): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $payout->id,
            /**
             * @OA\Property(
             *     property="moderator",
             *     description="moderator user entity",
             *     type="integer"
             * )
             */
            'moderator_email' => $payout->moderator->email,
            /**
             * @OA\Property(
             *     property="amount",
             *     description="",
             *     type="numeric"
             * )
             */
            'amount' => $payout->amount,
            /**
             * @OA\Property(
             *     property="payout_date",
             *     type="string",
             *     example="2018-11-20",
             * )
             */
            'payout_date' => $payout->payout_date->toDateTimeString(),
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 11:11:11"
             * )
             */
            'created_at' => $payout->created_at ? $payout->created_at->toDateTimeString() : '',
        ];
    }
}
