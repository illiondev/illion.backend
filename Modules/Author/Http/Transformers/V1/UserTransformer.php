<?php

namespace Modules\Author\Http\Transformers\V1;

use Modules\Moderator\Http\Transformers\V1\ModeratorTransformer;

/**
 * @OA\Schema(
 *     schema="User",
 *     title="User",
 *     type="object",
 * )
 */
class UserTransformer extends ModeratorTransformer
{
}
