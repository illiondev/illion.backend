<?php

namespace Modules\Author\Services;

use Modules\Author\Models\Bundle;

class StatisticService
{
    /**
     * Сколько продано
     * todo кешировать
     * todo добавить анонимные продажи
     */
    public function totalSoldCount(Bundle $bundle, int $authorId): int
    {
        return (int)
            $bundle->orderProducts($authorId)
                ->sum('quantity') +
            $bundle->externalSalesProducts($authorId)
                ->sum('quantity') +
            $bundle->billsProducts($authorId)
                ->sum('quantity');
    }

    /**
     * Себестоимость всех продуктов конкретного автора в бандле
     * todo кешировать
     * todo добавить анонимные продажи
     */
    public function totalPrimeCost(Bundle $bundle, int $authorId): float
    {
        return (float)
            $bundle->orderProducts($authorId)
                ->selectRaw('SUM(prime_cost * quantity) as total')
                ->value('total') +
            $bundle->externalSalesProducts($authorId)
                ->selectRaw('SUM(prime_cost * quantity) as total')
                ->value('total') +
            $bundle->billsProducts($authorId)
                ->selectRaw('SUM(prime_cost * quantity) as total')
                ->value('total');
    }

    /**
     * Заработок автора
     * todo кешировать
     * todo добавить анонимные продажи
     */
    public function profit(Bundle $bundle, int $authorId): float
    {
        return (float)
            $bundle->orderProducts($authorId)
                ->selectRaw('SUM(((net_profit - prime_cost) * quantity) * (authors_percent / 100)) as total')
                ->value('total') +
            $bundle->externalSalesProducts($authorId)
                ->selectRaw('SUM(((net_profit - prime_cost) * quantity) * (authors_percent / 100)) as total')
                ->value('total') +
            $bundle->billsProducts($authorId)
                ->selectRaw('SUM(((net_profit - prime_cost) * quantity) * (authors_percent / 100)) as total')
                ->value('total');
    }
}
