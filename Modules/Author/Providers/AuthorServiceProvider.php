<?php

namespace Modules\Author\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Author\Http\Middleware\AuthorMiddleware;
use Modules\Moderator\Http\Middleware\ModeratorMiddleware;

class AuthorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerMiddleware();
    }

    /**
     * Register middleware.
     *
     * @return void
     */
    public function registerMiddleware()
    {
        $this->app['router']->aliasMiddleware('roles.author', AuthorMiddleware::class);
        $this->app['router']->aliasMiddleware('roles.moderator', ModeratorMiddleware::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
