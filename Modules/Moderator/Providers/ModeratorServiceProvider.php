<?php

namespace Modules\Moderator\Providers;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Moderator\Http\Middleware\ModeratorMiddleware;

class ModeratorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot(): void
    {
        $this->registerMiddleware();
        $this->registerFactories();
    }

    public function registerMiddleware(): void
    {
        $this->app['router']->aliasMiddleware('roles.moderator', ModeratorMiddleware::class);
    }

    public function registerFactories(): void
    {
        if (!app()->environment('production')) {
            if (class_exists('Faker\Factory')) {
                app(Factory::class)->load(__DIR__ . '/../Database/factories');
            }
        }
    }

    public function register(): void
    {
        //
    }

    public function provides(): array
    {
        return [];
    }
}
