<?php

namespace Modules\Moderator\Models;

use Illuminate\Database\Eloquent\Model;

class StatisticsProfit extends Model
{
    protected $fillable = [
        'author_id',
        'authors_profit',
        'house_profit',
    ];

    protected $casts = [
        'author_id' => 'integer',
        'authors_profit' => 'float',
        'house_profit' => 'float',
    ];
}
