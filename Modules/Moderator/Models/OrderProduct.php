<?php

namespace Modules\Moderator\Models;

use App\Models\OrderProduct as BaseOrderProduct;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\QueryBuilder;

class OrderProduct extends BaseOrderProduct
{
    public function scopeDateFrom(QueryBuilder $query, $date): Builder
    {
        return $query->where('orders.created_at', '>=', Carbon::parse($date, 'Europe/Moscow')->startOfDay()->tz('UTC'));
    }

    public function scopeDateTo(QueryBuilder $query, $date): Builder
    {
        return $query->where('orders.created_at', '<=', Carbon::parse($date, 'Europe/Moscow')->endOfDay()->tz('UTC'));
    }
}
