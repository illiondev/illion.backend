<?php

namespace Modules\Moderator\Models;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{
    protected $fillable = [
        'bundle_id',
        'title',
        'type',
        'ordered',
        'paid',
        'notPaid',
        'sum',
        'delivery_sum',
        'returned',
        'external_sales_sum',
        'bills_sales',
        'bills_sales_sum',

        'created_at',
    ];

    protected $casts = [
        'bundle_id' => 'integer',
        'title' => 'string',
        'type' => 'integer',
        'ordered' => 'integer',
        'paid' => 'integer',
        'notPaid' => 'integer',
        'sum' => 'float',
        'delivery_sum' => 'float',
        'returned' => 'integer',
        'external_sales_sum' => 'float',
        'bills_sales' => 'integer',
        'bills_sales_sum' => 'float',

        'created_at' => 'datetime',
    ];
}
