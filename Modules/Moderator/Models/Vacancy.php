<?php

namespace Modules\Moderator\Models;

use Modules\Moderator\Traits\Loggable;
use App\Models\Vacancy as BaseVacancy;

class Vacancy extends BaseVacancy
{
    use Loggable;

    protected $fillable = [
        'priority',
        'description',
        'title',
        'enabled',
    ];

    public function getMorphClass()
    {
        return BaseVacancy::class;
    }
}
