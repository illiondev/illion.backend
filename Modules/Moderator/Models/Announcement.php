<?php

namespace Modules\Moderator\Models;

use App\Models\Announcement as BaseAnnouncement;
use Modules\Moderator\Traits\Loggable;

class Announcement extends BaseAnnouncement
{
    use Loggable;

    protected $fillable = [
        'enabled',
        'content',
        'page_type',
    ];

    public function getMorphClass()
    {
        return BaseAnnouncement::class;
    }
}
