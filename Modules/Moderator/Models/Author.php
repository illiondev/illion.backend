<?php

namespace Modules\Moderator\Models;

use App\Models\Author as BaseAuthor;
use Modules\Moderator\Traits\Loggable;

class Author extends BaseAuthor
{
    use Loggable;

    protected $fillable = [
        'name',
        'surname',
        'about',
        'instagram',
    ];

    public function getMorphClass()
    {
        return BaseAuthor::class;
    }
}
