<?php

namespace Modules\Moderator\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\QueryBuilder\QueryBuilder;
use App\Models\ExternalSaleProduct as BaseExternalSaleProduct;

/**
 * @property-read ExternalSale|null $externalSale
 */
class ExternalSaleProduct extends BaseExternalSaleProduct
{
    public function getMorphClass()
    {
        return BaseExternalSaleProduct::class;
    }

    public function scopeDateFrom(QueryBuilder $query, $date): Builder
    {
        return $query->where('external_sales.sold_at', '>=', Carbon::parse($date, 'Europe/Moscow')->startOfDay()->tz('UTC'));
    }

    public function scopeDateTo(QueryBuilder $query, $date): Builder
    {
        return $query->where('external_sales.sold_at', '<=', Carbon::parse($date, 'Europe/Moscow')->endOfDay()->tz('UTC'));
    }

    public function externalSale() : BelongsTo
    {
        return $this->belongsTo(ExternalSale::class);
    }
}
