<?php

namespace Modules\Moderator\Models;

use App\Models\OrderBill as BaseOrderBill;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Moderator\Traits\Loggable;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * @property-read Bundle $bundle
 */
class OrderBill extends BaseOrderBill
{
    use Loggable;

    protected $fillable = [
        'price',
        'description',
    ];

    public function getMorphClass()
    {
        return BaseOrderBill::class;
    }

    public function scopeDateFrom(QueryBuilder $query, $date): Builder
    {
        return $query->where('created_at', '>=', Carbon::parse($date, 'Europe/Moscow')->startOfDay()->tz('UTC'));
    }

    public function scopeDateTo(QueryBuilder $query, $date): Builder
    {
        return $query->where('created_at', '<=', Carbon::parse($date, 'Europe/Moscow')->endOfDay()->tz('UTC'));
    }

    public function bundle() : BelongsTo
    {
        return $this->belongsTo(Bundle::class);
    }
}
