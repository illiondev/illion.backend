<?php

namespace Modules\Moderator\Models;

use App\Models\Order as BaseOrder;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Moderator\Traits\Loggable;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property-read OrderBill[]|Collection $bills
 * @property-read OrderItem[]|Collection $items
 * @property-read OrderProduct[]|Collection $products
 */
class Order extends BaseOrder
{
    public function getMorphClass()
    {
        return BaseOrder::class;
    }

    use Loggable;

    protected $fillable = [
        'name',
        'surname',
        'patronimic',
        'comment',
        'phone',
        'address',
        'address2',
        'postal_code',
    ];

    public function bills(): HasMany
    {
        return $this->hasMany(OrderBill::class)->orderBy('id', 'desc');
    }

    public function scopePriceFrom(QueryBuilder $query, $price): Builder
    {
        return $query->where('total_price', '>=', $price);
    }

    public function scopePriceTo(QueryBuilder $query, $price): Builder
    {
        return $query->where('total_price', '<=', $price);
    }

    public function scopeDateFrom(QueryBuilder $query, $date): Builder
    {
        return $query->where('orders.created_at', '>=', Carbon::parse($date, 'Europe/Moscow')->startOfDay()->tz('UTC'));
    }

    public function scopeDateTo(QueryBuilder $query, $date): Builder
    {
        return $query->where('orders.created_at', '<=', Carbon::parse($date, 'Europe/Moscow')->endOfDay()->tz('UTC'));
    }

    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    public function products(): HasMany
    {
        return $this->hasMany(OrderProduct::class);
    }
}
