<?php

namespace Modules\Moderator\Models;

use App\Models\ExternalSaleProduct;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Moderator\Traits\Loggable;
use App\Models\ExternalSale as BaseExternalSale;
use Carbon\Carbon;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property-read Bundle $bundle
 */
class ExternalSale extends BaseExternalSale
{
    use Loggable;

    protected $fillable = [
        'is_paid',
        'quantity',
        'price',
        'prime_cost',
        'author_fee',
        'commission',
        'commission_flat',
        'comment',
        'sold_at',
        'bundle_id',
    ];

    public function getMorphClass()
    {
        return BaseExternalSale::class;
    }

    public function scopeDateFrom(QueryBuilder $query, $date): Builder
    {
        return $query->where('sold_at', '>=', Carbon::parse($date, 'Europe/Moscow')->startOfDay()->tz('UTC'));
    }

    public function scopeDateTo(QueryBuilder $query, $date): Builder
    {
        return $query->where('sold_at', '<=', Carbon::parse($date, 'Europe/Moscow')->endOfDay()->tz('UTC'));
    }

    public function bundle(): BelongsTo
    {
        return $this->belongsTo(Bundle::class);
    }

    public function save(array $options = [])
    {
        if ($this->commission_flat) {
            $this->net_profit = $this->price - $this->commission_flat;
        } else if ($this->commission) {
            $this->net_profit = $this->price * (1 - ($this->commission / 100));
        } else {
            $this->net_profit = $this->price;
        }

        if (!$this->exists) {
            $bundle = $this->bundle;

            foreach ($bundle->products as $product) {
                $externalSaleProduct = new ExternalSaleProduct();

                $externalSaleProduct->external_sale_id = $this->id;
                $externalSaleProduct->book_id = $product->book_id;
                $externalSaleProduct->product_id = $product->id;
                $externalSaleProduct->prime_cost = $product->prime_cost;
                $externalSaleProduct->author_id = $product->author_id;
                $externalSaleProduct->quantity = 1;
                $externalSaleProduct->authors_percent = $this->author_fee;
                $externalSaleProduct->article = $product->article;
                $externalSaleProduct->book_title = $product->book->title;
                $externalSaleProduct->type = $product->type;
                $externalSaleProduct->bundle_id = $bundle->id;
                $externalSaleProduct->length = $product->length;
                $externalSaleProduct->width = $product->width;
                $externalSaleProduct->height = $product->height;
                $externalSaleProduct->weight = $product->weight;
                $externalSaleProduct->is_surprise = $product->pivot->is_surprise;
                $externalSaleProduct->price = $product->pivot->price;

                if ($this->commission_flat) {
                    $externalSaleProduct->net_profit = $externalSaleProduct->price - $this->commission_flat;
                } else if ($this->commission) {
                    $externalSaleProduct->net_profit = $externalSaleProduct->price * (1 - ($this->commission / 100));
                } else {
                    $externalSaleProduct->net_profit = $externalSaleProduct->price;
                }

                $externalSaleProduct->save();
            }
        }

        return parent::save($options);
    }
}
