<?php

namespace Modules\Moderator\Models;

use App\Models\Feature as BaseFeature;
use Modules\Moderator\Traits\Loggable;

class Feature extends BaseFeature
{
    use Loggable;

    protected $fillable = [
        'text',
    ];

    public function getMorphClass()
    {
        return BaseFeature::class;
    }
}
