<?php

namespace Modules\Moderator\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Moderator\Traits\Loggable;
use App\Models\Soon as BaseSoon;

class Soon extends BaseSoon
{
    use Loggable;

    protected $fillable = [
        'enabled',
        'title',
        'author_id',
    ];

    public function getMorphClass()
    {
        return BaseSoon::class;
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(Author::class);
    }
}
