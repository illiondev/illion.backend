<?php

namespace Modules\Moderator\Models;

use App\Models\ExternalOrderItem as BaseExternalOrderItem;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * @property-read Bundle $bundle
 */
class ExternalOrderItem extends BaseExternalOrderItem
{
    public function scopeDateFrom(QueryBuilder $query, $date): Builder
    {
        return $query->where('external_orders.sold_at', '>=', Carbon::parse($date, 'Europe/Moscow')->startOfDay()->tz('UTC'));
    }

    public function scopeDateTo(QueryBuilder $query, $date): Builder
    {
        return $query->where('external_orders.sold_at', '<=', Carbon::parse($date, 'Europe/Moscow')->endOfDay()->tz('UTC'));
    }

    public function bundle(): BelongsTo
    {
        return $this->belongsTo(Bundle::class);
    }
}
