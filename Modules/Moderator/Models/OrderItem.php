<?php

namespace Modules\Moderator\Models;

use App\Models\OrderItem as BaseOrderItem;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * @property-read Bundle $bundle
 * @property-read OrderProduct[] $products
 */
class OrderItem extends BaseOrderItem
{
    public function scopeDateFrom(QueryBuilder $query, $date): Builder
    {
        return $query->where('orders.created_at', '>=', Carbon::parse($date, 'Europe/Moscow')->startOfDay()->tz('UTC'));
    }

    public function scopeDateTo(QueryBuilder $query, $date): Builder
    {
        return $query->where('orders.created_at', '<=', Carbon::parse($date, 'Europe/Moscow')->endOfDay()->tz('UTC'));
    }

    public function bundle(): BelongsTo
    {
        return $this->belongsTo(Bundle::class);
    }

    public function products(): HasMany
    {
        return $this->hasMany(OrderProduct::class);
    }
}
