<?php

namespace Modules\Moderator\Models;

use App\Models\BookChapter as BaseBookChapter;
use Illion\Epuber\Interfaces\ChapterInterface;
use Modules\Moderator\Traits\Loggable;

/**
 * @deprecated
 */
class BookChapter extends BaseBookChapter implements ChapterInterface
{
    use Loggable;

    protected $fillable = [
        'title',
        'content',
        'completed',
    ];

    public function getMorphClass()
    {
        return BaseBookChapter::class;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
