<?php

namespace Modules\Moderator\Models;

use App\Models\Bundle as BaseBundle;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\Moderator\Traits\Loggable;

class Bundle extends BaseBundle
{
    use Loggable;

    protected $fillable = [
        'slug',

        'enabled',
        'hidden',

        'title',
        'description',

        'prime_cost',
        'pre_order',
        'notification',
    ];

    public function getMorphClass()
    {
        return BaseBundle::class;
    }

    public function products(): BelongsToMany
    {
        return $this
            ->belongsToMany(Product::class, 'bundles_has_products')
            ->withPivot(['is_surprise', 'price', 'authors_percent']);
    }

    public function prices(): MorphMany
    {
        return $this->morphMany(Price::class, 'model')->where(['type' => Price::TYPE_WEB]);
    }

    public function setSlugAttribute($value)
    {
        if (!$value) {
            $value = uniqid();
        }

        $this->attributes['slug'] = str_slug($value);
    }

    public function save(array $options = [])
    {
        if (!$this->title && $this->products()->count() > 0) {
            $this->title = $this->products()->first()->book->title;
        }

        return parent::save($options);
    }

    public function books(): BelongsToMany
    {
        return $this->belongsToMany(Book::class, 'books_has_bundles');
    }
}
