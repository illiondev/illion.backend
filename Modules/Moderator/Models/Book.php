<?php

namespace Modules\Moderator\Models;

use App\Models\Book as BaseBook;
use Illion\Epuber\Interfaces\BookInterface;
use Illion\Epuber\Interfaces\ChapterInterface;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Modules\Moderator\Traits\Loggable;
use Illuminate\Support\Facades\Redis;

class Book extends BaseBook implements BookInterface
{
    use Loggable;

    protected $fillable = [
        'priority',

        'title',

        'theme_id',
        'author_id',

        'title',
        'tagline',
        'preview',

        'publication_date',
    ];

    public function getMorphClass()
    {
        return BaseBook::class;
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(Author::class);
    }

    public function features(): HasMany
    {
        return $this->hasMany(Feature::class);
    }

    public function advantages(): HasMany
    {
        return $this->hasMany(Advantage::class);
    }

    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    public function bundles(): BelongsToMany
    {
        return $this
            ->belongsToMany(Bundle::class, 'books_has_bundles');
    }

    public function quotes(): HasMany
    {
        return $this->hasMany(Quote::class);
    }

    /**
     * @return ChapterInterface[]
     */
    public function getChapters(): Collection
    {
        return $this->bookChapters;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAuthorName(): string
    {
        return $this->author->name . ' ' . $this->author->surname;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getLanguage(): string
    {
        return 'ru';
    }

    public function getCoverImageUrl(): ?string
    {
        $cover = $this->getFirstMedia(self::MEDIA_CATEGORY_COVER);

        if ($cover) {
            return $cover->getFullUrl();
        }

        return null;
    }

    public function bookChapters(): HasMany
    {
        return $this->hasMany(BookChapter::class)->orderBy('number');
    }

    public function getPreview(): string
    {
        return $this->preview;
    }

    public function generatePreviewLink(): string
    {
        $key = Str::random(32);

        Redis::set(self::REDIS_BOOK_PREVIEW_KEY_ . $key, $this->id);
        Redis::expire(self::REDIS_BOOK_PREVIEW_KEY_ . $key, self::REDIS_BOOK_PREVIEW_KEY_EXPIRE);

        return $key;
    }
}
