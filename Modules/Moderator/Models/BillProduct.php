<?php

namespace Modules\Moderator\Models;

use App\Models\OrderBill as BaseOrderBill;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\BillProduct as BaseBillProduct;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * @property-read OrderBill $bill
 */
class BillProduct extends BaseBillProduct
{
    public function getMorphClass()
    {
        return BaseOrderBill::class;
    }

    public function bill() : BelongsTo
    {
        return $this->belongsTo(OrderBill::class);
    }

    public function scopeDateFrom(QueryBuilder $query, $date): Builder
    {
        return $query->where('bills.created_at', '>=', Carbon::parse($date, 'Europe/Moscow')->startOfDay()->tz('UTC'));
    }

    public function scopeDateTo(QueryBuilder $query, $date): Builder
    {
        return $query->where('bills.created_at', '<=', Carbon::parse($date, 'Europe/Moscow')->endOfDay()->tz('UTC'));
    }

}
