<?php

namespace Modules\Moderator\Models;

use App\Models\ExternalOrder as BaseExternalOrder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\QueryBuilder;

class ExternalOrder extends BaseExternalOrder
{
    public function getMorphClass()
    {
        return BaseExternalOrder::class;
    }

    public function scopeDateFrom(QueryBuilder $query, $date): Builder
    {
        return $query->where('external_orders.sold_at', '>=', Carbon::parse($date, 'Europe/Moscow')->startOfDay()->tz('UTC'));
    }

    public function scopeDateTo(QueryBuilder $query, $date): Builder
    {
        return $query->where('external_orders.sold_at', '<=', Carbon::parse($date, 'Europe/Moscow')->endOfDay()->tz('UTC'));
    }
}
