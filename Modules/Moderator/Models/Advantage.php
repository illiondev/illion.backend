<?php

namespace Modules\Moderator\Models;

use App\Models\Advantage as BaseAdvantage;
use Modules\Moderator\Traits\Loggable;

class Advantage extends BaseAdvantage
{
    use Loggable;

    protected $fillable = [
        'text',
    ];

    public function getMorphClass()
    {
        return BaseAdvantage::class;
    }
}
