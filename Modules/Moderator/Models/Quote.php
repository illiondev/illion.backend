<?php

namespace App\Models;

namespace Modules\Moderator\Models;

use App\Models\Quote as BaseQuote;
use Modules\Moderator\Traits\Loggable;

class Quote extends BaseQuote
{
    use Loggable;

    protected $fillable = [
        'name',
        'text',
    ];

    public function getMorphClass()
    {
        return BaseQuote::class;
    }
}
