<?php

namespace Modules\Moderator\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Moderator\Traits\Loggable;
use App\Models\Payout as BasePayout;

class Payout extends BasePayout
{
    use Loggable;

    protected $fillable = [
        'amount',
        'author_id',
        'payout_date',
    ];

    public function getMorphClass()
    {
        return BasePayout::class;
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(Author::class);
    }
}
