<?php

namespace Modules\Moderator\Models;

use App\Models\Price as BasePrice;
use Modules\Moderator\Traits\Loggable;

class Price extends BasePrice
{
    use Loggable;

    protected $fillable = [
        'type',
        'currency',
        'amount',
    ];

    public function getMorphClass()
    {
        return BasePrice::class;
    }
}
