<?php

namespace Modules\Moderator\Models;

use App\Models\Product as BaseProduct;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Moderator\Traits\Loggable;

class Product extends BaseProduct
{
    use Loggable;

    protected $fillable = [
        'article',
        'type',

        'prime_cost',
    ];

    public function getMorphClass()
    {
        return BaseProduct::class;
    }

    public function book() : BelongsTo
    {
        return $this->belongsTo(Book::class);
    }
}
