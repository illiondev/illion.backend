<?php

namespace App\Models;

namespace Modules\Moderator\Models;

use App\Models\Review as BaseReview;
use Modules\Moderator\Traits\Loggable;

class Review extends BaseReview
{
    use Loggable;

    protected $fillable = [
        'name',
        'instagram',
        'text',
    ];

    public function getMorphClass()
    {
        return BaseReview::class;
    }
}
