<?php

namespace Modules\Moderator\Services;

use Modules\Moderator\Jobs\BookGenerateJob;
use Modules\Moderator\Models\Product;
use Modules\Moderator\Repositories\BookChapterRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductService
{
    protected $bookChapterRepository;

    public function __construct(
        BookChapterRepository $bookChapterRepository
    )
    {
        $this->bookChapterRepository = $bookChapterRepository;
    }

    public function publish(Product $product, int $type): void
    {
        if ($product->type === $product::PRODUCT_TYPE_E_BOOK) {
            if ($product->epub) {
                // skip all checks
            } else {
                $uncompletedChaptersCount = $this->bookChapterRepository->getUncompletedChaptersCount($product->book_id);

                if ($uncompletedChaptersCount !== 0) {
                    throw new BadRequestHttpException('Book contains unfinished chapters');
                }

                $chaptersCount = $this->bookChapterRepository->getChaptersCount($product->book_id);

                if ($chaptersCount === 0) {
                    throw new BadRequestHttpException('Book do not have any chapters');
                }
            }

            dispatch(new BookGenerateJob($product, $type));
        }
    }
}
