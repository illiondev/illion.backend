<?php

namespace Modules\Moderator\Services;

use App\Models\User;
use App\Models\Vendor\Activity;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Modules\Moderator\Models\Order;
use Modules\Moderator\Models\OrderBill;
use Modules\Moderator\Traits\Loggable;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use App\Services\OrderService as BaseOrderService;

class OrderService extends BaseOrderService
{
    public function update(Order $order, array $params): Order
    {
        if ($order->shiptor_id !== null) {
            throw new BadRequestHttpException('Can not edit shipped order');
        }

        if (Cache::lock($order->getLockKey(), 60 * 10)->block(5)) {
            try {
                /** @var Order $order */
                $order = $order->fresh();
                if ($order->shiptor_id !== null) {
                    throw new BadRequestHttpException('Can not edit shipped order');
                }
                $order->fill($params);
                $order->saveOrFail();
            } finally {
                Cache::lock($order->getLockKey())->release();
            }
        }

        return $order;
    }

    public function pay(Order $order, User $user): Order
    {
        if (!in_array($order->status, [Order::STATUS_CANCELED, Order::STATUS_WAITING_FOR_PAYMENT])) {
            throw new BadRequestHttpException(sprintf('Bad order status[%d] for set paid', $order->status));
        }

        if (Cache::lock($order->getLockKey(), 60 * 10)->block(5)) {
            try {
                /** @var Order $order */
                $order = $order->fresh();
                if (!in_array($order->status, [Order::STATUS_CANCELED, Order::STATUS_WAITING_FOR_PAYMENT])) {
                    throw new BadRequestHttpException(sprintf('Bad order status[%d] for set paid', $order->status));
                }

                $this->bought($order, $order::PAYMENT_TYPE_PAYPAL);

                activity()
                    ->inLog(Loggable::getLogName())
                    ->performedOn($order)
                    ->causedBy($user)
                    ->withProperties([
                        'attributes' => ['status' => Order::STATUS_PROCESSING],
                        'old' => ['status' => $order->status],
                    ])
                    ->log(Activity::DESCRIPTION_PAID);
            } finally {
                Cache::lock($order->getLockKey())->release();
            }
        }

        return $order;
    }

    public function createBill(Order $order, array $attributes, User $user): OrderBill
    {
        $orderBill = new OrderBill($attributes);

        $orderBill->order_id = $order->id;
        $orderBill->hash = Str::uuid();
        $orderBill->user_id = $user->id;
        $orderBill->payment_id = null;
        $orderBill->status = $orderBill::PAYMENT_STATUS_NEW;

        $orderBill->saveOrFail();

        return $orderBill;
    }
}
