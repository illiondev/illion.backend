<?php

namespace Modules\Moderator\Services;

use App\Models\Order;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_Permission;
use Google_Service_Sheets;
use Google_Service_Sheets_Spreadsheet;
use Google_Service_Sheets_ValueRange;
use Illuminate\Support\Collection;

class GoogleSpreadsheet
{
    public function generate(Collection $orders): string
    {
        /** @var Collection|Order[] $orders */
        $orders = $orders->unique('email');

        /*
         * We need to get a Google_Client object first to handle auth and api calls, etc.
         */
        $client = new Google_Client();
        $client->setApplicationName('Google Sheets API PHP Quickstart');
        $client->setScopes([Google_Service_Sheets::SPREADSHEETS, Google_Service_Drive::DRIVE_FILE]);
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        $jsonAuth = \Storage::disk('local')->get('56892-ed3e2787839d.json');
        $client->setAuthConfig(json_decode($jsonAuth, true));

        /*
         * With the Google_Client we can get a Google_Service_Sheets service object to interact with sheets
         */
        $service = new Google_Service_Sheets($client);

        $spreadsheet = new Google_Service_Sheets_Spreadsheet([
            'properties' => [
                'title' => 'Выгрузка ' . date('Y-m-d H:i:s'),
            ],
        ]);
        $spreadsheet = $service->spreadsheets->create($spreadsheet, [
            'fields' => 'spreadsheetId',
        ]);

        $columnNames = [
            'id',
            'имя',
            'фамилия',
            'отчество',
            'телефон',
            'адрес 1',
            'адрес 2',
            'город',
            'регион',
            'страна',

            'email',
            'заказан',
            'сумма заказа',
        ];

        $values = [];
        $values[] = $columnNames;
        foreach ($orders as $order) {
            $values[] = [
                (string) $order->id,
                (string) $order->name,
                (string) $order->surname,
                (string) $order->patronimic,
                (string) $order->phone,
                (string) $order->address,
                (string) $order->address2,
                (string) $order->settlement,
                (string) $order->region,
                (string) $order->getCountryName(),
                (string) $order->email,
                (string) $order->created_at->toDateString(),
                (string) $order->total_price,
            ];
        }

        $range = 'A1:' . 'M' . (count($values) + 1);
        $data = new Google_Service_Sheets_ValueRange([
            'range' => $range,
            'majorDimension' => 'ROWS',
            'values' => $values,
        ]);

        $service->spreadsheets_values->update($spreadsheet->getSpreadsheetId(), $range, $data,
            ['valueInputOption' => 'USER_ENTERED']);

        $driveService = new Google_Service_Drive($client);

        $userPermission = new Google_Service_Drive_Permission([
            'type' => 'anyone',
            'role' => 'writer',
        ]);
        $driveService->permissions->create($spreadsheet->getSpreadsheetId(), $userPermission);

        return 'https://docs.google.com/spreadsheets/d/' . $spreadsheet->getSpreadsheetId();
    }

}
