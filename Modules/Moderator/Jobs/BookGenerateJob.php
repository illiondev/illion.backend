<?php

namespace Modules\Moderator\Jobs;

use App\Jobs\Job;
use App\Models\Book;
use App\Models\PublishedBook;
use Illion\Epuber\Generator;
use Illion\Epuber\Interfaces\BookInterface;
use Modules\Moderator\Models\Product;
use Storage;

class BookGenerateJob extends Job
{
    /** @var BookInterface|Book $book */
    protected $book;
    protected $product;
    protected $type;
    protected $generator;

    public function __construct(Product $product, int $type)
    {
        $this->book = $product->book;
        $this->product = $product;
        $this->type = $type;
    }

    public function handle(Generator $generator): void
    {
        if ($this->type === PublishedBook::TYPE_BOOK) {
            $this->generateEpub($generator);
        } else if ($this->type === PublishedBook::TYPE_PREVIEW) {
            $this->generateEpubPreview($generator);
        }
    }

    protected function generateEpub(Generator $generator)
    {
        if ($this->product->epub) {
            $previousPublished = $this->book->publishedEbook;

            $published = $generator->encryptEpub(
                Storage::disk('local')->path($this->product->epub),
                $this->book->getId() . '_' . date('Ymd') . '_' . uniqid(),
                $previousPublished ? $previousPublished->secret_key : null
            );
        } else {
            $published = $generator->generate($this->book);
        }

        $publishedBook = new PublishedBook();

        $publishedBook->book_id = $this->book->getId();
        $publishedBook->filename = $published->file_name;
        $publishedBook->size = $published->size;
        $publishedBook->secret_key = $published->secret_key;
        $publishedBook->type = PublishedBook::TYPE_BOOK;

        $publishedBook->saveOrFail();
    }

    protected function generateEpubPreview(Generator $generator)
    {
        if ($this->product->epub_preview) {
            $published = $generator->encryptEpub(
                Storage::disk('local')->path($this->product->epub_preview),
                $this->book->getId() . '_preview_' . date('Ymd') . '_' . uniqid()
            );

            $publishedBook = new PublishedBook();

            $publishedBook->book_id = $this->book->getId();
            $publishedBook->filename = $published->file_name;
            $publishedBook->size = $published->size;
            $publishedBook->secret_key = $published->secret_key;
            $publishedBook->type = PublishedBook::TYPE_PREVIEW;

            $publishedBook->saveOrFail();
        }
    }
}
