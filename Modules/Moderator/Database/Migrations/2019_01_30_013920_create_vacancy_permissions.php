<?php

use Modules\Moderator\Helpers\Permissions;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Migrations\Migration;

class CreateVacancyPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => Permissions::READ_VACANCIES]);
        Permission::create(['name' => Permissions::EDIT_VACANCIES]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
