<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Moderator\Helpers\Permissions;
use Spatie\Permission\Models\Permission;

class CreateSoonPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => Permissions::READ_SOON]);
        Permission::create(['name' => Permissions::EDIT_SOON]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
