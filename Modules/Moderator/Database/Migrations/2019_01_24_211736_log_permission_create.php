<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Moderator\Helpers\Permissions;
use Spatie\Permission\Models\Permission;

class LogPermissionCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => Permissions::READ_LOGS]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
