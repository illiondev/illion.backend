<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Moderator\Helpers\Permissions;
use Spatie\Permission\Models\Permission;

class CreatePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create permissions
        Permission::create(['name' => Permissions::READ_BOOKS]);
        Permission::create(['name' => Permissions::EDIT_BOOKS]);
        Permission::create(['name' => Permissions::READ_AUTHORS]);
        Permission::create(['name' => Permissions::EDIT_AUTHORS]);
        Permission::create(['name' => Permissions::READ_USERS]);
        Permission::create(['name' => Permissions::EDIT_USERS]);
        Permission::create(['name' => Permissions::READ_CONTENT]);
        Permission::create(['name' => Permissions::EDIT_CONTENT]);
        Permission::create(['name' => Permissions::READ_STATISTICS]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
