<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Modules\Moderator\Helpers\Permissions;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateReadAuthorsStatisticPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => Permissions::READ_AUTHORS_STATISTICS]);
        Permission::create(['name' => Permissions::READ_AUTHORS_PAYOUTS]);

        $role = Role::findByName(User::ROLE_AUTHOR);
        $role->givePermissionTo(Permissions::READ_AUTHORS_STATISTICS);
        $role->givePermissionTo(Permissions::READ_AUTHORS_PAYOUTS);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
