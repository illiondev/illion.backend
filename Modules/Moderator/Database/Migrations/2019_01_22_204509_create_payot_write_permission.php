<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Moderator\Helpers\Permissions;
use Spatie\Permission\Models\Permission;

class CreatePayotWritePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => Permissions::READ_PAYOUTS]);
        Permission::create(['name' => Permissions::EDIT_PAYOUTS]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
