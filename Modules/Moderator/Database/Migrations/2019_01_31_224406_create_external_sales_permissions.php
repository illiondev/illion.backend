<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Moderator\Helpers\Permissions;
use Spatie\Permission\Models\Permission;

class CreateExternalSalesPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => Permissions::READ_EXTERNAL_SALES]);
        Permission::create(['name' => Permissions::EDIT_EXTERNAL_SALES]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
