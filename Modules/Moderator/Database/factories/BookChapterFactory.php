<?php

use Faker\Generator as Faker;
use Modules\Moderator\Models\BookChapter;

$factory->define(BookChapter::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'content' => $faker->text,
        'number' => $faker->numberBetween(1, 9),
        'book_id' => function () {
            return factory(\App\Models\Book::class)->create()->id;
        },
        'completed' => false,
    ];
});

$factory->state(BookChapter::class, 'completed', [
    'completed' => true,
]);
