<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Moderator\Models\Vacancy::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'priority' => $faker->randomNumber(1, true),
        'description' => $faker->text,
        'enabled' => true,
    ];
});
