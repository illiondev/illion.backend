<?php

use Faker\Generator as Faker;
use Modules\Moderator\Models\Order;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
        'price' => $faker->numberBetween(10, 100),
        'total_price' => $faker->numberBetween(10, 100),
        'payment_method' => $faker->randomElement([
            Order::PAYMENT_METHOD_CASH,
            Order::PAYMENT_METHOD_CARD,
            Order::PAYMENT_METHOD_ONLINE,
            Order::PAYMENT_METHOD_GOOGLE_PLAY,
        ]),
        'status' => Order::STATUS_NEW,
    ];
});
