<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Moderator\Models\Soon::class, function (Faker $faker) {
    return [
        'enabled' => $faker->boolean,
        'title' => $faker->title,
        'author_id' => function () {
            return factory(\Modules\Moderator\Models\Author::class)->create()->id;
        },
    ];
});
