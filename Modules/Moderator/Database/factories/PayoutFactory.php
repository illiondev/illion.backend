<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Moderator\Models\Payout::class, function (Faker $faker) {
    return [
        'amount' => $faker->numberBetween(10, 100),
        'author_id' => function () {
            return factory(\Modules\Moderator\Models\Author::class)->create()->user_id;
        },
        'moderator_id' => function () {
            // TODO: add role "Moderator" to user, for now simple user is enough
            return factory(App\Models\User::class)->create()->id;
        },
    ];
});
