<?php

use Faker\Generator as Faker;

$factory->define(Modules\Moderator\Models\Announcement::class, function (Faker $faker) {
    return [
        'enabled' => $faker->boolean,
        'content' => $faker->text,
        'page_type' => $faker->randomElement([
            Modules\Moderator\Models\Announcement::PAGE_TYPE_MAIN,
            Modules\Moderator\Models\Announcement::PAGE_TYPE_ALL
        ]),
    ];
});
