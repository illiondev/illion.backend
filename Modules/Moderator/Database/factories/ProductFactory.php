<?php

use Faker\Generator as Faker;
use Modules\Moderator\Models\Product;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'article' => $faker->text,
        'title' => '',
        'description' => '',
        'enabled' => $faker->boolean,
        'type' => $faker->randomElement([
            Product::PRODUCT_TYPE_PAPER_BOOK,
            Product::PRODUCT_TYPE_E_BOOK,
        ]),
        'book_id' => function () {
            return factory(\App\Models\Book::class)->create()->id;
        },
        'prime_cost' => $faker->numberBetween(10, 100),
        'meta' => [
            'length' => $faker->numberBetween(20, 500),
            'width' => $faker->numberBetween(20, 500),
            'height' => $faker->numberBetween(5, 50),
            'weight' => $faker->numberBetween(100, 400),
        ],
    ];
});

$factory->state(Product::class, 'enabled', [
    'enabled' => true,
]);

$factory->state(Product::class, 'paperBook', [
    'type' => Product::PRODUCT_TYPE_PAPER_BOOK,
]);

$factory->state(Product::class, 'eBook', [
    'type' => Product::PRODUCT_TYPE_E_BOOK,
]);
