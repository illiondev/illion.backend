<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Moderator\Models\Author::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'about' => $faker->text,
        'instagram' => $faker->url,
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
    ];
});
