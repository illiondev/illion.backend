<?php

use Dingo\Api\Routing\Router as ApiRouter;
use Modules\Moderator\Helpers\Permissions;

$api = app(ApiRouter::class);

/**
 * Moderator's API
 */
$api->version('v1', [
    'prefix' => 'api/moderator/v1',
], function (ApiRouter $api) {
    /**
     * Auth
     */
    $api->group(['namespace' => 'App\Http\Controllers\Api\V1'], function (ApiRouter $api) {
        /**
         * Auth
         */
        $api->post('auth/login', 'AuthController@login');
        $api->post('auth/refresh', 'AuthController@refresh');
        $api->post('auth/register', 'AuthController@register');

        /**
         * Password
         */
        $api->post('auth/password/forgot', 'PasswordController@forgot');

        /**
         * Auth
         */
        $api->group(['middleware' => ['api.auth']], function (ApiRouter $api) {
            $api->post('auth/logout', 'AuthController@logout');
        });
    });

    $api->group([
        'namespace' => 'Modules\Moderator\Http\Controllers\Api\V1',
        'middleware' => [
            'api.auth',
            'status.not_blocked',
            'roles.moderator',
        ],
    ], function (ApiRouter $api) {

        /**
         * Profile (self)
         */
        $api->get('profile/me', 'UserController@me');

        /**
         * Lists
         */
        $api->get('books/bundles', 'BundleController@index');

        /**
         * Books
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_BOOKS]], function (ApiRouter $api) {
            $api->resource('books', 'BookController', ['only' => ['index', 'show']]);
            $api->resource('books.chapters', 'BookChapterController', ['only' => ['index', 'show']]);
            $api->resource('books.products', 'BookProductController', ['only' => ['index', 'show']]);
            $api->resource('books.features', 'BookFeatureController', ['only' => ['index', 'show']]);
            $api->resource('books.advantages', 'BookAdvantageController', ['only' => ['index', 'show']]);
            $api->resource('books.reviews', 'BookReviewController', ['only' => ['index', 'show']]);
            $api->resource('books.quotes', 'BookQuoteController', ['only' => ['index', 'show']]);

            $api->group(['middleware' => ['permission:' . Permissions::EDIT_BOOKS]], function (ApiRouter $api) {
                $api->post('books/{id}/preview', 'BookController@preview');
                $api->resource('books', 'BookController', ['only' => ['store', 'update']]);
                $api->post('books/{id}/image', 'BookController@imageUpload');
                $api->delete('books/{id}/image/{imageId}', 'BookController@imageDelete');

                $api->resource('books.products', 'BookProductController', ['only' => ['store', 'update', 'destroy']]);

                $api->resource('books.features', 'BookFeatureController', ['only' => ['store', 'update', 'destroy']]);
                $api->post('books/{bookId}/features/{id}/image', 'BookFeatureController@imageUpload');
                $api->resource('books.advantages', 'BookAdvantageController', ['only' => ['store', 'update', 'destroy']]);
                $api->post('books/{bookId}/advantages/{id}/image', 'BookAdvantageController@imageUpload');
                $api->resource('books.reviews', 'BookReviewController', ['only' => ['store', 'update', 'destroy']]);
                $api->post('books/{bookId}/reviews/{id}/image', 'BookReviewController@imageUpload');
                $api->resource('books.quotes', 'BookQuoteController', ['only' => ['store', 'update', 'destroy']]);

                /**
                 * books epub files
                 */
                $api->post('books/{id}/products/{productId}/epub_preview', 'BookProductController@attachPreview');
                $api->delete('books/{id}/products/{productId}/epub_preview', 'BookProductController@detachPreview');

                $api->post('books/{id}/products/{productId}/{format}', 'BookProductController@attachFile');
                $api->delete('books/{id}/products/{productId}/{format}', 'BookProductController@detachFile');

                /**
                 * Book's chapter
                 */
                $api->resource('books.chapters', 'BookChapterController', ['only' => ['store', 'update', 'destroy']]);
                $api->post('books/{bookId}/chapters/{id}/image', 'BookChapterController@imageUpload');
                $api->get('books/{bookId}/chapters/{id}/preview', 'BookChapterController@preview');

                /**
                 * Bundles
                 */
                $api->resource('books.bundles', 'BundleController', ['only' => ['store', 'update', 'destroy']]);

                $api->post('books/{bookId}/bundles/{id}/images', 'BundleController@imageUpload');
                $api->delete('books/{bookId}/bundles/{id}/images/{imageId}', 'BundleController@imageDelete');

                $api->post('books/{bookId}/bundles/{id}/attach', 'BookController@attachBundle');
                $api->delete('books/{bookId}/bundles/{id}/detach', 'BookController@detachBundle');

                $api->post('books/{bookId}/bundles/{id}/products/{productId}', 'BundleController@attachProduct');
                $api->delete('books/{bookId}/bundles/{id}/products/{productId}', 'BundleController@detachProduct');
            });
        });

        /**
         * Autocomplete
         */
        $api->get('books/products/autocomplete', 'BookProductController@autocomplete');
        $api->get('books/bundles/autocomplete', 'BundleController@autocomplete');

        /**
         * Authors
         */
        $api->get('authors/autocomplete', 'AuthorController@autocomplete');
        $api->group(['middleware' => ['permission:' . Permissions::READ_AUTHORS]], function (ApiRouter $api) {
            $api->resource('authors', 'AuthorController', ['only' => ['index', 'show']]);

            $api->group(['middleware' => ['permission:' . Permissions::EDIT_AUTHORS]], function (ApiRouter $api) {
                $api->resource('authors', 'AuthorController', ['only' => ['store', 'update']]);
                $api->post('authors/{id}/image', 'AuthorController@imageUpload');
                $api->delete('authors/{id}/image', 'AuthorController@imageDelete');

                $api->post('authors/{id}/user', 'AuthorController@connect');
                $api->delete('authors/{id}/user', 'AuthorController@disconnect');
            });
        });

        /**
         * Statistics
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_STATISTICS]], function (ApiRouter $api) {
            $api->get('statistics', 'StatisticsController@index');
            $api->get('statistics/summary', 'StatisticsController@summary');
            $api->get('statistics/profit', 'StatisticsController@profit');
        });

        /**
         * Logs
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_LOGS]], function (ApiRouter $api) {
            $api->resource('logs', 'LogsController', ['only' => ['index']]);
            $api->get('logs/subject-classes', 'LogsController@subjectClasses');
        });

        /**
         * Orders
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_ORDERS]], function (ApiRouter $api) {
            $api->get('orders/spreadsheet', 'OrderController@spreadsheet');
            $api->resource('orders', 'OrderController', ['only' => ['index', 'show']]);

            $api->group(['middleware' => ['permission:' . Permissions::EDIT_ORDERS]], function (ApiRouter $api) {
                $api->resource('orders', 'OrderController', ['only' => ['update']]);
                $api->post('orders/{id}/pay', 'OrderController@pay');

                $api->resource('orders.bill', 'OrderBillController', ['only' => ['store']]);
            });
        });

        /**
         * Payouts
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_PAYOUTS]], function (ApiRouter $api) {
            $api->resource('payouts', 'PayoutController', ['only' => ['index', 'show']]);
            $api->group(['middleware' => ['permission:' . Permissions::EDIT_PAYOUTS]], function (ApiRouter $api) {

                $api->resource('payouts', 'PayoutController', ['only' => ['store', 'update']]);
            });
        });

        /**
         * Vacancies
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_VACANCIES]], function (ApiRouter $api) {
            $api->resource('vacancies', 'VacancyController', ['only' => ['index', 'show']]);
            $api->group(['middleware' => ['permission:' . Permissions::EDIT_VACANCIES]], function (ApiRouter $api) {

                $api->resource('vacancies', 'VacancyController', ['only' => ['store', 'update', 'destroy']]);
            });
        });

        /**
         * Content
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_CONTENT]], function (ApiRouter $api) {
            /**
             * Announcement
             */
            $api->get('announcement', 'AnnouncementController@show');

            $api->group(['middleware' => ['permission:' . Permissions::EDIT_CONTENT]], function (ApiRouter $api) {
                $api->post('announcement', 'AnnouncementController@store');
                $api->put('announcement', 'AnnouncementController@update');
            });
        });

        /**
         * ExternalSales
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_EXTERNAL_SALES]], function (ApiRouter $api) {
            $api->resource('external-sales', 'ExternalSaleController', ['only' => ['index', 'show']]);
            $api->group(['middleware' => ['permission:' . Permissions::EDIT_EXTERNAL_SALES]], function (ApiRouter $api) {

                $api->resource('external-sales', 'ExternalSaleController', ['only' => ['store', 'update']]);
            });
        });
        
        /**
         * Users
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_USERS]], function (ApiRouter $api) {
            $api->resource('users', 'UserController', ['only' => ['index', 'show']]);

            $api->group(['middleware' => ['permission:' . Permissions::EDIT_USERS]], function (ApiRouter $api) {
                $api->post('users/{id}/comment', 'UserController@comment');

                $api->post('users/moderator', 'UserController@addModeratorRole');
                $api->delete('users/{id}/moderator', 'UserController@removeModeratorRole');
                $api->resource('users', 'UserController', ['only' => ['update']]);
            });
        });

        /**
         * Soon
         */
        $api->group(['middleware' => ['permission:' . Permissions::READ_SOON]], function (ApiRouter $api) {
            $api->resource('soon', 'SoonController', ['only' => ['index', 'show']]);

            $api->group(['middleware' => ['permission:' . Permissions::EDIT_SOON]], function (ApiRouter $api) {
                $api->resource('soon', 'SoonController', ['only' => ['store', 'update', 'destroy']]);
                $api->post('soon/{id}/image', 'SoonController@imageUpload');
                $api->delete('soon/{id}/image/{imageId}', 'SoonController@imageDelete');
            });
        });
    });
});
