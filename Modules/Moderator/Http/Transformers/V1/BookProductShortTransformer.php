<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Item;
use Modules\Moderator\Models\Product;

/**
 * @OA\Schema(
 *     schema="moderator_BookProductShort",
 *     title="BookProductShort",
 *     description="Продукт",
 *     type="object",
 * )
 */
class BookProductShortTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'author',
    ];

    public function transform(Product $product): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $product->id,
            /**
             * @OA\Property(
             *     property="product type",
             *     description="product_type=1 paper book, 2 - ebook",
             *     type="integer"
             * )
             */
            'type' => $product->type,
            /**
             * @OA\Property(
             *     property="prime_cost",
             *     description="",
             *     type="float"
             * )
             */
            'prime_cost' => $product->prime_cost,
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $product->book->title,
        ];
    }

    /**
     * @OA\Property(
     *    property="author",
     *    type="object",
     *    ref="#/components/schemas/moderator_Author",
     *    ),
     * ),
     */
    public function includeAuthor(Product $product): ?Item
    {
        return $this->item($product->book->author, new AuthorTransformer());
    }
}
