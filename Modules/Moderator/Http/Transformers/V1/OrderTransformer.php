<?php

namespace Modules\Moderator\Http\Transformers\V1;

use App\Services\ShiptorService;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\Order;

/**
 * @OA\Schema(
 *     schema="moderator_Order",
 *     title="Order",
 *     type="object",
 * )
 */
class OrderTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'items',
        'bills',
    ];

    public function transform(Order $order): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $order->id,

            /**
             * @OA\Property(
             *     property="name",
             *     description="",
             *     type="string"
             * )
             */
            'name' => (string) $order->name,
            /**
             * @OA\Property(
             *     property="surname",
             *     description="",
             *     type="string"
             * )
             */
            'surname' => (string) $order->surname,
            /**
             * @OA\Property(
             *     property="patronimic",
             *     description="",
             *     type="string"
             * )
             */
            'patronimic' => (string) $order->patronimic,
            /**
             * @OA\Property(
             *     property="phone",
             *     description="",
             *     type="string"
             * )
             */
            'phone' => (string) $order->phone,
            /**
             * @OA\Property(
             *     property="address",
             *     description="",
             *     type="string"
             * )
             */
            'address' => (string) $order->address,
            /**
             * @OA\Property(
             *     property="address2",
             *     description="",
             *     type="string"
             * )
             */
            'address2' => (string) $order->address2,
            /**
             * @OA\Property(
             *     property="settlement",
             *     description="",
             *     type="string"
             * )
             */
            'settlement' => $order->settlement,
            /**
             * @OA\Property(
             *     property="region",
             *     description="",
             *     type="string"
             * )
             */
            'region' => $order->region,
            /**
             * @OA\Property(
             *     property="country",
             *     description="",
             *     type="string"
             * )
             */
            'country' => $order->getCountryName(),
            /**
             * @OA\Property(
             *     property="postal_code",
             *     description="",
             *     type="string"
             * )
             */
            'postal_code' => (string) $order->postal_code,

            /**
             * @OA\Property(
             *     property="status",
             *     description="",
             *     type="integer"
             * )
             */
            'status' => $order->status,
            /**
             * @OA\Property(
             *     property="shipping_status",
             *     description="",
             *     type="string"
             * )
             */
            'shipping_status' => $order->shipping_status,
            /**
             * @OA\Property(
             *     property="tracking_number",
             *     description="",
             *     type="string"
             * )
             */
            'tracking_number' => $order->tracking_number,
            /**
             * @OA\Property(
             *     property="shiptor_error",
             *     description="",
             *     type="string"
             * )
             */
            'shiptor_error' => $order->shiptor_error,
            /**
             * @OA\Property(
             *     property="price",
             *     description="",
             *     type="float"
             * )
             */
            'price' => $order->price,
            /**
             * @OA\Property(
             *     property="delivery_price",
             *     description="",
             *     type="float"
             * )
             */
            'delivery_price' => $order->delivery_price,
            /**
             * @OA\Property(
             *     property="total_price",
             *     description="",
             *     type="float"
             * )
             */
            'total_price' => $order->total_price,
            /**
             * @OA\Property(
             *     property="shiptor_id",
             *     description="",
             *     type="integer"
             * )
             */
            'shiptor_id' => $order->shiptor_id,
            /**
             * @OA\Property(
             *     property="email",
             *     description="",
             *     type="string"
             * )
             */
            'email' => $order->email,
            /**
             * @OA\Property(
             *     property="comment",
             *     description="",
             *     type="string"
             * )
             */
            'comment' => (string) $order->comment,
            /**
             * @OA\Property(
             *     property="system_comment",
             *     description="",
             *     type="string"
             * )
             */
            'system_comment' => (string) $order->system_comment,
            /**
             * @OA\Property(
             *     property="payment_id",
             *     description="",
             *     type="integer"
             * )
             */
            'payment_id' => $order->payment_id,
            /**
             * @OA\Property(
             *     property="shipping_method",
             *     description="",
             *     type="integer"
             * )
             */
            'shipping_method' => $order->shipping_method,
            /**
             * @OA\Property(
             *     property="shipping_category",
             *     description="",
             *     enum={"to-door", "delivery-point", "post-office"}
             * )
             */
            'shipping_category' => $order->shipping_method ? ShiptorService::shipmentMethodToCategory($order->shipping_method) : '',
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 11:11:11"
             * )
             */
            'created_at' => $order->created_at ? $order->created_at->toDateTimeString() : '',
        ];
    }

    /**
     * @OA\Property(
     *    property="bills",
     *    description="Счета",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/moderator_OrderBill")
     *    ),
     * ),
     */
    public function includeBills(Order $order): ?Collection
    {
        return $this->collection($order->bills, new OrderBillTransformer());
    }

    /**
     * @OA\Property(
     *    property="items",
     *    description="Товары заказа",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/moderator_OrderItem")
     *    ),
     * ),
     */
    public function includeItems(Order $order): ?Collection
    {
        return $this->collection($order->items, new OrderItemTransformer());
    }
}
