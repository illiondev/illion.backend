<?php

namespace Modules\Moderator\Http\Transformers\V1;

use App\Models\Comment;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Comment",
 *     title="Comment",
 *     description="Комментарий",
 *     type="object",
 * )
 */
class CommentTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'user',
    ];

    public function transform(Comment $comment): array
    {
        return [
            /**
             * @OA\Property(
             *     property="text",
             *     description="",
             *     type="string"
             * )
             */
            'text' => $comment->text,
            /**
             * @OA\Property(
             *      property="updated_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'updated_at'   => $comment->updated_at ? $comment->updated_at->toDateTimeString() : '',
        ];
    }

    /**
     * @OA\Property(
     *    property="user",
     *    description="Юзер",
     *    type="object",
     *    ref="#/components/schemas/Moderator",
     *    ),
     * ),
     */
    public function includeUser(Comment $comment): ?Item
    {
        if ($comment->user) {

            $moderatorTransformer = new ModeratorTransformer();
            $moderatorTransformer->setDefaultIncludes([]);

            return $this->item($comment->user, $moderatorTransformer);
        }

        return null;
    }
}
