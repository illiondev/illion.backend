<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\Author;

/**
 * @OA\Schema(
 *     schema="moderator_Author",
 *     title="Author",
 *     description="Автор",
 *     type="object",
 * )
 */
class AuthorTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'photo',
        'user',
    ];

    public function transform(Author $author): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $author->id,
            /**
             * @OA\Property(
             *     property="name",
             *     description="",
             *     type="string"
             * )
             */
            'name' => $author->name,
            /**
             * @OA\Property(
             *     property="surname",
             *     description="",
             *     type="string"
             * )
             */
            'surname' => $author->surname,
            /**
             * @OA\Property(
             *     property="about",
             *     description="",
             *     type="string"
             * )
             */
            'about' => $author->about,
            /**
             * @OA\Property(
             *     property="instagram",
             *     description="",
             *     type="string"
             * )
             */
            'instagram' => $author->instagram,
        ];
    }

    /**
     * @OA\Property(
     *    property="user",
     *    description="Юзер",
     *    type="object",
     *    ref="#/components/schemas/Moderator",
     *    ),
     * ),
     */
    public function includeUser(Author $author): ?Item
    {
        if ($author->user) {
            return $this->item($author->user, new ModeratorTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="photo",
     *    description="Фото",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
    public function includePhoto(Author $author): ?Item
    {
        $photo = $author->getFirstMedia($author::MEDIA_CATEGORY_PHOTO);

        if ($photo) {
            return $this->item($photo, new MediaTransformer());
        }

        return null;
    }
}
