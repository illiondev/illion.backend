<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\OrderItem;

/**
 * @OA\Schema(
 *     schema="moderator_OrderItem",
 *     title="OrderItem",
 *     description="Товар в заказе",
 *     type="object",
 * )
 */
class OrderItemTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'products',
    ];

    public function transform(OrderItem $orderItem): array
    {
        return [
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $orderItem->title,
            /**
             * @OA\Property(
             *     property="bundle_id",
             *     description="",
             *     type="integer"
             * )
             */
            'bundle_id' => $orderItem->bundle_id,
            /**
             * @OA\Property(
             *     property="quantity",
             *     description="",
             *     type="integer"
             * )
             */
            'quantity' => $orderItem->quantity,
            /**
             * @OA\Property(
             *     property="original_price",
             *     description="",
             *     type="float"
             * )
             */
            'original_price' => $orderItem->original_price,
            /**
             * @OA\Property(
             *     property="discount",
             *     description="",
             *     type="float"
             * )
             */
            'discount' => $orderItem->discount,
            /**
             * @OA\Property(
             *     property="discount_flat",
             *     description="",
             *     type="float"
             * )
             */
            'discount_flat' => $orderItem->discount_flat,
            /**
             * @OA\Property(
             *     property="price",
             *     description="",
             *     type="float"
             * )
             */
            'price' => $orderItem->price,
        ];
    }

    public function includeProducts(OrderItem $orderItem): ?Collection
    {
        return $this->collection($orderItem->products, new OrderProductTransformer());
    }
}
