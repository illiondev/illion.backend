<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\OrderProduct;

/**
 * @OA\Schema(
 *     schema="OrderProduct",
 *     title="OrderProduct",
 *     type="object",
 * )
 */
class OrderProductTransformer extends TransformerAbstract
{
    public function transform(OrderProduct $product): array
    {
        return [
            /**
             * @OA\Property(
             *     property="article",
             *     description="",
             *     type="string"
             * )
             */
            'article' => $product->article,
            /**
             * @OA\Property(
             *     property="type",
             *     description="",
             *     type="integer"
             * )
             */
            'type' => $product->type,
            /**
             * @OA\Property(
             *     property="book_id",
             *     description="",
             *     type="integer"
             * )
             */
            'book_id' => $product->book_id,
            /**
             * @OA\Property(
             *     property="weight",
             *     description="",
             *     type="number"
             * )
             */
            'weight' => $product->weight,
            /**
             * @OA\Property(
             *     property="length",
             *     description="",
             *     type="number"
             * )
             */
            'length' => $product->length,
            /**
             * @OA\Property(
             *     property="width",
             *     description="",
             *     type="number"
             * )
             */
            'width' => $product->width,
            /**
             * @OA\Property(
             *     property="height",
             *     description="",
             *     type="number"
             * )
             */
            'height' => $product->height,
            /**
             * @OA\Property(
             *     property="is_surprise",
             *     description="",
             *     type="boolean"
             * )
             */
            'is_surprise' => $product->is_surprise,
        ];
    }
}
