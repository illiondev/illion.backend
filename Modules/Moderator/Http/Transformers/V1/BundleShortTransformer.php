<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\Bundle;

/**
 * @OA\Schema(
 *     schema="moderator_BundleShort",
 *     title="BundleShort",
 *     description="Бандл",
 *     type="object",
 * )
 */
class BundleShortTransformer extends TransformerAbstract
{
    public function transform(Bundle $bundle): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $bundle->id,
            /**
             * @OA\Property(
             *     property="product type",
             *     description="product_type=1 paper book, 2 - ebook",
             *     type="integer"
             * )
             */
            'type' => $bundle->getType(),
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $bundle->title,
        ];
    }
}
