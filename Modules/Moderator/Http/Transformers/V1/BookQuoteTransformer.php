<?php

namespace Modules\Moderator\Http\Transformers\V1;

use App\Http\Transformers\V1\BookQuoteTransformer as BaseBookQuoteTransformer;

/**
 * @OA\Schema(
 *     schema="moderator_BookQuote",
 *     title="BookQuote",
 *     description="Цитаты",
 *     type="object",
 * )
 */
class BookQuoteTransformer extends BaseBookQuoteTransformer
{
    /**
     * @OA\Property(
     *     property="id",
     *     description="",
     *     type="integer"
     * )
     */
    /**
     * @OA\Property(
     *     property="text",
     *     description="",
     *     type="string"
     * )
     */
    /**
     * @OA\Property(
     *     property="name",
     *     description="",
     *     type="string"
     * )
     */
}
