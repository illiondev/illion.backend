<?php

namespace Modules\Moderator\Http\Transformers\V1;

use App\Http\Transformers\V1\AnnouncementTransformer as BaseAnnouncementTransformer;

/**
 * @OA\Schema(
 *     schema="moderator_Announcement",
 *     title="Announcement",
 *     description="Объявление",
 *     type="object",
 * )
 */
class AnnouncementTransformer extends BaseAnnouncementTransformer
{
    /**
     * @OA\Property(
     *     property="enabled",
     *     description="",
     *     type="boolean"
     * )
     */
    /**
     * @OA\Property(
     *     property="content",
     *     description="",
     *     type="string"
     * )
     */
    /**
     * @OA\Property(
     *     property="page_type",
     *     description="",
     *     type="integer"
     * )
     */
}
