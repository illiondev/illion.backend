<?php

namespace Modules\Moderator\Http\Transformers\V1;

use App\Models\User;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="moderator_Moderator",
 *     title="Moderator",
 *     type="object",
 * )
 */
class ModeratorTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'permissions',
        'roles',
        'comment',
    ];

    public function transform(User $user): array
    {
        $profile = $user->profile;

        return [
            /**
             * @OA\Property(
             *     property="id",
             *     type="integer",
             * )
             */
            'id' => $user->id,
            /**
             * @OA\Property(
             *     property="email",
             *     type="string",
             * )
             */
            'email' => $user->email,

            // profile

            /**
             * @OA\Property(
             *     property="name",
             *     description="User's full name",
             *     type="string",
             * )
             */
            'name'   => $profile->name,
        ];
    }

    /**
     * @OA\Property(
     *    property="permissions",
     *    description="Права пользователя",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/Permission")
     *    ),
     * ),
     */
    public function includePermissions(User $user): ?Collection
    {
        return $this->collection($user->getAllPermissions(), new PermissionTransformer());
    }

    /**
     * @OA\Property(
     *    property="roles",
     *    description="Роли пользователя",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/Role")
     *    ),
     * ),
     */
    public function includeRoles(User $user): ?Collection
    {
        return $this->collection($user->roles, new RolesTransformer());
    }

    /**
     * @OA\Property(
     *    property="comment",
     *    description="Комментарий",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/Comment")
     *    ),
     * ),
     */
    public function includeComment(User $user): ?Item
    {
        if ($user->comment) {
            return $this->item($user->comment, new CommentTransformer());
        }

        return null;
    }
}
