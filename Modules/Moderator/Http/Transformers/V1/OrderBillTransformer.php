<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\OrderBill;

/**
 * @OA\Schema(
 *     schema="moderator_OrderBill",
 *     title="OrderBill",
 *     description="Счет",
 *     type="object",
 * )
 */
class OrderBillTransformer extends TransformerAbstract
{
    public function transform(OrderBill $orderBill): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     type="integer",
             * )
             */
            'id' => $orderBill->id,
            /**
             * @OA\Property(
             *     property="hash",
             *     type="string",
             * )
             */
            'hash' => $orderBill->hash,
            /**
             * @OA\Property(
             *     property="order_id",
             *     type="integer",
             * )
             */
            'order_id' => $orderBill->order_id,
            /**
             * @OA\Property(
             *     property="user_id",
             *     type="integer",
             * )
             */
            'user_id' => $orderBill->user_id,
            /**
             * @OA\Property(
             *     property="moderator_email",
             *     description="moderator user email",
             *     type="string"
             * )
             */
            'moderator_email' => $orderBill->user->email,
            /**
             * @OA\Property(
             *     property="price",
             *     type="float",
             * )
             */
            'price' => $orderBill->price,
            /**
             * @OA\Property(
             *     property="description",
             *     type="string",
             * )
             */
            'description' => $orderBill->description,
            /**
             * @OA\Property(
             *     property="payment_id",
             *     type="integer",
             * )
             */
            'payment_id' => $orderBill->payment_id,
            /**
             * @OA\Property(
             *     property="status",
             *     type="integer",
             * )
             */
            'status' => $orderBill->status,
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'created_at'   => $orderBill->created_at ? $orderBill->created_at->toDateTimeString() : '',
            /**
             * @OA\Property(
             *      property="updated_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'updated_at'   => $orderBill->updated_at ? $orderBill->updated_at->toDateTimeString() : '',
        ];
    }
}
