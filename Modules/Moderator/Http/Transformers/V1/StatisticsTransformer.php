<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\Statistics;

/**
 * @OA\Schema(
 *     schema="moderator_Statistics",
 *     title="Statistics",
 *     type="object",
 * )
 */
class StatisticsTransformer extends TransformerAbstract
{
    public function transform(Statistics $object): array
    {
        return [
            /**
             * @OA\Property(
             *     property="bundle_id",
             *     description="bundle id",
             *     type="integer"
             * )
             */
            'bundle_id' => $object->bundle_id,
            /**
             * @OA\Property(
             *     property="title",
             *     description="product title",
             *     type="integer"
             * )
             */
            'title' => $object->title,
            /**
             * @OA\Property(
             *     property="type",
             *     description="",
             *     type="integer"
             * )
             */
            'type' => $object->type,
            /**
             * @OA\Property(
             *     property="ordered",
             *     description="",
             *     type="integer"
             * )
             */
            'ordered' => $object->ordered,
            /**
             * @OA\Property(
             *     property="paid",
             *     description="",
             *     type="integer"
             * )
             */
            'paid' => $object->paid,
            /**
             * @OA\Property(
             *     property="not paid",
             *     description="",
             *     type="integer"
             * )
             */
            'not_paid' => $object->notPaid,
            /**
             * @OA\Property(
             *     property="returned",
             *     description="",
             *     type="integer"
             * )
             */
            'returned' => $object->returned,
            /**
             * @OA\Property(
             *     property="sum",
             *     description="",
             *     type="float"
             * )
             */
            'sum' => $object->sum,
            /**
             * @OA\Property(
             *     property="delivery_sum",
             *     description="",
             *     type="float"
             * )
             */
            'delivery_sum' => round($object->delivery_sum, 2),
            /**
             * @OA\Property(
             *     property="external_sales_sum",
             *     description="",
             *     type="float"
             * )
             */
            'external_sales_sum' => $object->external_sales_sum ?: 0,
            /**
             * @OA\Property(
             *     property="bills_sales",
             *     description="",
             *     type="float"
             * )
             */
            'bills_sales' => $object->bills_sales ?: 0,
            /**
             * @OA\Property(
             *     property="bills_sales_sum",
             *     description="",
             *     type="float"
             * )
             */
            'bills_sales_sum' => $object->bills_sales_sum ?: 0,
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'created_at' => $object->created_at ? $object->created_at->toDateTimeString() : '',
        ];
    }
}
