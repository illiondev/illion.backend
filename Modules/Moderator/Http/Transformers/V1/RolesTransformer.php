<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use Spatie\Permission\Contracts\Role;

/**
 * @OA\Schema(
 *     schema="Role",
 *     title="Role",
 *     description="Роль пользователя",
 *     type="object",
 * )
 */
class RolesTransformer extends TransformerAbstract
{
    public function transform(Role $role): array
    {
        return [
            /**
             * @OA\Property(
             *     property="name",
             *     type="string",
             * )
             */
            'name' => $role->name,
        ];
    }

}
