<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use Spatie\Permission\Contracts\Permission;

/**
 * @OA\Schema(
 *     schema="Permission",
 *     title="Permission",
 *     description="Права пользователя",
 *     type="object",
 * )
 */
class PermissionTransformer extends TransformerAbstract
{
    public function transform(Permission $permission): array
    {
        return [
            /**
             * @OA\Property(
             *     property="name",
             *     type="string",
             * )
             */
            'name' => $permission->name,
        ];
    }

}
