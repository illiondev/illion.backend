<?php

namespace Modules\Moderator\Http\Transformers\V1;

use App\Http\Transformers\V1\BookReviewTransformer as BaseBookReviewTransformer;

/**
 * @OA\Schema(
 *     schema="moderator_BookReview",
 *     title="BookReview",
 *     description="Отзывы",
 *     type="object",
 * )
 */
class BookReviewTransformer extends BaseBookReviewTransformer
{
    /**
     * @OA\Property(
     *     property="id",
     *     description="",
     *     type="integer"
     * )
     */
    /**
     * @OA\Property(
     *     property="name",
     *     description="",
     *     type="string"
     * )
     */
    /**
     * @OA\Property(
     *     property="instagram",
     *     description="",
     *     type="string"
     * )
     */
    /**
     * @OA\Property(
     *     property="text",
     *     description="",
     *     type="string"
     * )
     */
    /**
     * @OA\Property(
     *    property="image",
     *    description="Картинка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
}
