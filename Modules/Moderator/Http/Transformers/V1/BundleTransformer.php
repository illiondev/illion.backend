<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\Bundle;

/**
 * @OA\Schema(
 *     schema="moderator_Bundle",
 *     title="Bundle",
 *     description="Бандл",
 *     type="object",
 * )
 */
class BundleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'cover',
        'prices',
        'products',
    ];

    public function transform(Bundle $bundle): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $bundle->id,
            /**
             * @OA\Property(
             *     property="slug",
             *     description="",
             *     type="string"
             * )
             */
            'slug' => $bundle->slug ?? '',
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $bundle->title,
            /**
             * @OA\Property(
             *     property="description",
             *     description="",
             *     type="string"
             * )
             */
            'description' => $bundle->description ?? '',
            /**
             * @OA\Property(
             *     property="enabled",
             *     description="",
             *     type="boolean"
             * )
             */
            'enabled' => $bundle->enabled,
            /**
             * @OA\Property(
             *      property="hidden",
             *      type="boolean",
             * )
             */
            'hidden' => $bundle->hidden,
            /**
             * @OA\Property(
             *     property="pre_order",
             *     description="",
             *     type="boolean"
             * )
             */
            'pre_order' => $bundle->pre_order,
            /**
             * @OA\Property(
             *     property="notification",
             *     description="",
             *     type="string"
             * )
             */
            'notification' => $bundle->notification,
        ];
    }

    /**
     * @OA\Property(
     *    property="products",
     *    description="Продукты",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/moderator_BookProduct")
     * ),
     */
    public function includeProducts(Bundle $bundle): ?Collection
    {
        return $this->collection($bundle->products, new BookProductTransformer());
    }

    /**
     * @OA\Property(
     *    property="prices",
     *    description="Цены",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/moderator_Price")
     * ),
     */
    public function includePrices(Bundle $bundle): ?Collection
    {
        return $this->collection($bundle->prices, new PriceTransformer());
    }

    /**
     * @OA\Property(
     *    property="cover",
     *    description="обложка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     * ),
     */
    public function includeCover(Bundle $bundle): ?Item
    {
        $cover = $bundle->getFirstMedia(Bundle::MEDIA_CATEGORY_COVER);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }
}
