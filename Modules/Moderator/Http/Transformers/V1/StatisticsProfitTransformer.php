<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\StatisticsProfit;
use Modules\Moderator\Repositories\AuthorRepository;

/**
 * @OA\Schema(
 *     schema="StatisticsProfit",
 *     title="StatisticsProfit",
 *     description="",
 *     type="object",
 * )
 */
class StatisticsProfitTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'author',
    ];

    public function transform(StatisticsProfit $statisticsProfit): array
    {
        return [
            /**
             * @OA\Property(
             *     property="author_id",
             *     description="",
             *     type="integer"
             * )
             */
            'author_id' => $statisticsProfit->author_id,
            /**
             * @OA\Property(
             *     property="authors_profit",
             *     description="",
             *     type="float"
             * )
             */
            'authors_profit' => round($statisticsProfit->authors_profit, 2),
            /**
             * @OA\Property(
             *     property="house_profit",
             *     description="",
             *     type="float"
             * )
             */
            'house_profit' => round($statisticsProfit->house_profit, 2),
        ];
    }

    /**
     * @OA\Property(
     *    property="author",
     *    description="автор",
     *    type="object",
     *    ref="#/components/schemas/moderator_Author",
     *    ),
     * ),
     */
    public function includeAuthor(StatisticsProfit $statisticsProfit): ?Item
    {
        $authorRepository = app(AuthorRepository::class);

        return $this->item($authorRepository->findOrFail($statisticsProfit->author_id), new AuthorTransformer());
    }
}
