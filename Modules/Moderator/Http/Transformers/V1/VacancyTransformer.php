<?php

namespace Modules\Moderator\Http\Transformers\V1;

use App\Http\Transformers\V1\VacancyTransformer as BaseVacancyTransformer;

/**
 * @OA\Schema(
 *     schema="moderator_Vacancy",
 *     title="Vacancy",
 *     type="object",
 * )
 */
class VacancyTransformer extends BaseVacancyTransformer
{
    /**
     * @OA\Property(
     *     property="id",
     *     description="",
     *     type="integer"
     * )
     */
    /**
     * @OA\Property(
     *     property="priority",
     *     description="",
     *     type="integer"
     * )
     */
    /**
     * @OA\Property(
     *     property="enabled",
     *     description="",
     *     type="boolean"
     * )
     */
    /**
     * @OA\Property(
     *     property="title",
     *     description="",
     *     type="string"
     * )
     */
    /**
     * @OA\Property(
     *     property="description",
     *     description="",
     *     type="string"
     * )
     */
}