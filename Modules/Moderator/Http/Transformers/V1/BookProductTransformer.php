<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\Product;

/**
 * @OA\Schema(
 *     schema="moderator_BookProduct",
 *     title="BookProduct",
 *     description="Продукт",
 *     type="object",
 * )
 */
class BookProductTransformer extends TransformerAbstract
{
    public function transform(Product $product): array
    {
        $result = [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $product->id,
            /**
             * @OA\Property(
             *     property="article",
             *     description="",
             *     type="string"
             * )
             */
            'article' => $product->article,
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $product->book->title,
            /**
             * @OA\Property(
             *     property="type",
             *     description="",
             *     type="integer"
             * )
             */
            'type' => $product->type,
            /**
             * @OA\Property(
             *     property="book_id",
             *     description="",
             *     type="integer"
             * )
             */
            'book_id' => $product->book_id,
            /**
             * @OA\Property(
             *     property="prime_cost",
             *     description="",
             *     type="float"
             * )
             */
            'prime_cost' => $product->prime_cost,
            /**
             * @OA\Property(
             *     property="epub",
             *     description="",
             *     type="string"
             * )
             */
            'epub' => $product->epub,
            /**
             * @OA\Property(
             *     property="pdf",
             *     description="",
             *     type="string"
             * )
             */
            'pdf' => $product->pdf,
            /**
             * @OA\Property(
             *     property="preview",
             *     description="",
             *     type="string"
             * )
             */
            'epub_preview' => $product->epub_preview,
            /**
             * @OA\Property(
             *     property="video",
             *     description="",
             *     type="string"
             * )
             */
            'video' => $product->video,
            /**
             * @OA\Property(
             *     property="weight",
             *     description="",
             *     type="number"
             * )
             */
            'weight' => $product->weight,
            /**
             * @OA\Property(
             *     property="length",
             *     description="",
             *     type="number"
             * )
             */
            'length' => $product->length,
            /**
             * @OA\Property(
             *     property="width",
             *     description="",
             *     type="number"
             * )
             */
            'width' => $product->width,
            /**
             * @OA\Property(
             *     property="height",
             *     description="",
             *     type="number"
             * )
             */
            'height' => $product->height,
            /**
             * @OA\Property(
             *     property="description",
             *     description="",
             *     type="string"
             * )
             */
            'description' => $product->description,
        ];

        /**
         * @OA\Property(
         *     property="is_surprise",
         *     description="",
         *     type="boolean"
         * )
         */
        /**
         * @OA\Property(
         *     property="price",
         *     description="",
         *     type="number"
         * )
         */
        /**
         * @OA\Property(
         *     property="authors_percent",
         *     description="",
         *     type="number"
         * )
         */
        if (isset($product->pivot)) {
            $result['is_surprise'] = (bool) $product->pivot->is_surprise;
            $result['price'] = (float) $product->pivot->price;
            $result['authors_percent'] = (float) $product->pivot->authors_percent;
        }

        return $result;
    }
}
