<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Item;
use Modules\Moderator\Models\ExternalSale;

/**
 * @OA\Schema(
 *     schema="moderator_ExternalSale",
 *     title="ExternalSale",
 *     type="object",
 * )
 */
class ExternalSaleTransformer extends TransformerAbstract
{
    public function transform(ExternalSale $externalSale): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $externalSale->id,
            /**
             * @OA\Property(
             *     property="paid",
             *     description="",
             *     type="boolean"
             * )
             */
            'is_paid' => $externalSale->is_paid,
            /**
             * @OA\Property(
             *     property="email",
             *     description="moderator user email",
             *     type="string"
             * )
             */
            'moderator_email' => $externalSale->moderator->email,
            /**
             * @OA\Property(
             *     property="price",
             *     description="",
             *     type="float"
             * )
             */
            'price' => $externalSale->price,
            /**
             * @OA\Property(
             *     property="prime_cost",
             *     description="",
             *     type="float"
             * )
             */
            'prime_cost' => $externalSale->prime_cost,
            /**
             * @OA\Property(
             *     property="author_fee",
             *     description="",
             *     type="float"
             * )
             */
            'author_fee' => $externalSale->author_fee,
            /**
             * @OA\Property(
             *     property="bundle_id",
             *     description="",
             *     type="float"
             * )
             */
            'bundle_id' => $externalSale->bundle_id,
            /**
             * @OA\Property(
             *     property="commission",
             *     description="",
             *     type="float"
             * )
             */
            'commission' => $externalSale->commission,
            /**
             * @OA\Property(
             *     property="commission_flat",
             *     description="",
             *     type="float"
             * )
             */
            'commission_flat' => $externalSale->commission_flat,
            /**
             * @OA\Property(
             *     property="comment",
             *     description="",
             *     type="string"
             * )
             */
            'comment' => $externalSale->comment,
            /**
             * @OA\Property(
             *     property="quantity",
             *     type="integer"
             * )
             */
            'quantity' => $externalSale->quantity,
            /**
             * @OA\Property(
             *     property="externalSale_at",
             *     type="string",
             *     example="2018-11-20",
             * )
             */
            'sold_at' => $externalSale->sold_at ? $externalSale->sold_at->toDateString() : '',
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13"
             * )
             */
            'created_at' => $externalSale->created_at ? $externalSale->created_at->toDateString() : '',
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $externalSale->bundle->title,
            /**
             * @OA\Property(
             *     property="type",
             *     description="",
             *     type="string"
             * )
             */
            'type' => $externalSale->bundle->getType(),
        ];
    }
}
