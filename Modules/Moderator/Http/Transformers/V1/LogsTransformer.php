<?php

namespace Modules\Moderator\Http\Transformers\V1;

use App\Models\Vendor\Activity;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="moderator_Logs",
 *     title="Logs",
 *     type="object",
 * )
 */
class LogsTransformer extends TransformerAbstract
{

    public function transform(Activity $activity): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $activity->id,
            /**
             * @OA\Property(
             *     property="email",
             *     description="",
             *     type="string"
             * )
             */
            'email' => $activity->causer->email,
            /**
             * @OA\Property(
             *     property="properties",
             *     description="",
             *     type="object"
             * )
             */
            'properties' => $activity->properties,
            /**
             * @OA\Property(
             *     property="description",
             *     description="",
             *     type="string"
             * )
             */
            'description' => $activity->description,
            /**
             * @OA\Property(
             *     property="subject_id",
             *     description="subject_id of action object",
             *     type="integer"
             * )
             */
            'subject_id' => $activity->subject_id,
            /**
             * @OA\Property(
             *     property="subject_label",
             *     description="",
             *     type="string"
             * )
             */
            'subject_label' => $activity->subject_label,
            /**
             * @OA\Property(
             *     property="subject_name",
             *     description="",
             *     type="string"
             * )
             */
            'subject_name' => $activity->subject_name,
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'created_at' => $activity->created_at ? $activity->created_at->toDateTimeString() : '',
        ];
    }

}
