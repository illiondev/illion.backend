<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Item;
use Modules\Moderator\Models\Payout;

/**
 * @OA\Schema(
 *     schema="moderator_Payout",
 *     title="Payout",
 *     type="object",
 * )
 */
class PayoutTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'author'
    ];

    public function transform(Payout $payout): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $payout->id,
            /**
             * @OA\Property(
             *     property="moderator",
             *     description="moderator user entity",
             *     type="integer"
             * )
             */
            'moderator_email' => $payout->moderator->email,
            /**
             * @OA\Property(
             *     property="amount",
             *     description="",
             *     type="numeric"
             * )
             */
            'amount' => $payout->amount,
            /**
             * @OA\Property(
             *     property="payout_date",
             *     type="string",
             *     example="2018-11-20",
             * )
             */
            'payout_date' => $payout->payout_date->toDateString(),
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'created_at' => $payout->created_at->toDateString(),
        ];
    }

    /**
     * @OA\Property(
     *    property="author",
     *    description="автор",
     *    type="object",
     *    ref="#/components/schemas/moderator_Author",
     *    ),
     * ),
     */
    public function includeAuthor(Payout $payout): ?Item
    {
        return $this->item($payout->author, new AuthorTransformer());
    }
}
