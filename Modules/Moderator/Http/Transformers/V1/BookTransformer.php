<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\Book;

/**
 * @OA\Schema(
 *     schema="moderator_Book",
 *     title="Book",
 *     description="Книга",
 *     type="object",
 * )
 */
class BookTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'bookChapters',
        'author',

        'cover',
        'hardcover',
        'coverSmall',
        'photos',
        'fragment',

        'products',
        'bundles',
        'advantages',
        'features',
        'reviews',
        'quotes',
    ];

    public function transform(Book $book): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $book->id,
            /**
             * @OA\Property(
             *     property="priority",
             *     description="",
             *     type="integer"
             * )
             */
            'priority' => $book->priority,
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $book->title,
            /**
             * @OA\Property(
             *     property="theme_id",
             *     description="",
             *     type="string"
             * )
             */
            'theme_id' => $book->theme_id,
            /**
             * @OA\Property(
             *     property="author_id",
             *     description="",
             *     type="integer"
             * )
             */
            'author_id' => $book->author_id,
            /**
             * @OA\Property(
             *     property="preview",
             *     description="",
             *     type="string"
             * )
             */
            'preview' => $book->preview,
            /**
             * @OA\Property(
             *     property="tagline",
             *     description="",
             *     type="string"
             * )
             */
            'tagline' => $book->tagline,
            /**
             * @OA\Property(
             *      property="publication_date",
             *      type="string",
             *      description="format: 1988-09-13"
             * )
             */
            'publication_date'   => $book->publication_date ? $book->publication_date->toDateString() : '',
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'created_at'   => $book->created_at ? $book->created_at->toDateTimeString() : '',
            /**
             * @OA\Property(
             *      property="updated_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'updated_at'   => $book->updated_at ? $book->updated_at->toDateTimeString() : '',
        ];
    }

    /**
     * @OA\Property(
     *    property="cover",
     *    description="Обложка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
    public function includeCover(Book $book): ?Item
    {
        $cover = $book->getFirstMedia($book::MEDIA_CATEGORY_COVER);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="hardcover",
     *    description="СуперОбложка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
    public function includeHardcover(Book $book): ?Item
    {
        $cover = $book->getFirstMedia($book::MEDIA_CATEGORY_HARDCOVER);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="coverSmall",
     *    description="Маленькая обложка?",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
    public function includeCoverSmall(Book $book): ?Item
    {
        $cover = $book->getFirstMedia($book::MEDIA_CATEGORY_COVER_SMALL);

        if ($cover) {
            return $this->item($cover, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="photos",
     *    description="Фотографии",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/Media")
     *    ),
     * ),
     */
    public function includePhotos(Book $book): ?Collection
    {
        $photos = $book->getMedia($book::MEDIA_CATEGORY_PHOTO);

        if ($photos) {
            return $this->collection($photos, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="fragment",
     *    description="Фрагмент",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
    public function includeFragment(Book $book): ?Item
    {
        $fragment = $book->getFirstMedia($book::MEDIA_CATEGORY_FRAGMENT);

        if ($fragment) {
            return $this->item($fragment, new MediaTransformer());
        }

        return null;
    }

    /**
     * @OA\Property(
     *    property="bookChapters",
     *    description="Главы",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/moderator_BookChapter")
     *    ),
     * ),
     */
    public function includeBookChapters(Book $book): ?Collection
    {
        return $this->collection($book->bookChapters, new BookChapterTransformer());
    }

    /**
     * @OA\Property(
     *    property="products",
     *    description="Продукты",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/moderator_BookProduct")
     *    ),
     * ),
     */
    public function includeProducts(Book $book): ?Collection
    {
        return $this->collection($book->products, new BookProductTransformer());
    }

    /**
     * @OA\Property(
     *    property="bundles",
     *    description="Бандлы",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/moderator_Bundle")
     *    ),
     * ),
     */
    public function includeBundles(Book $book): ?Collection
    {
        return $this->collection($book->bundles, new BundleTransformer());
    }

    /**
     * @OA\Property(
     *    property="advantages",
     *    description="Преимущества",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/moderator_BookAdvantage")
     *    ),
     * ),
     */
    public function includeAdvantages(Book $book): ?Collection
    {
        return $this->collection($book->advantages, new BookAdvantageTransformer());
    }

    /**
     * @OA\Property(
     *    property="features",
     *    description="Особенности",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/moderator_BookFeature")
     *    ),
     * ),
     */
    public function includeFeatures(Book $book): ?Collection
    {
        return $this->collection($book->features, new BookFeatureTransformer());
    }

    /**
     * @OA\Property(
     *    property="reviews",
     *    description="Обзоры",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/moderator_BookReview")
     *    ),
     * ),
     */
    public function includeReviews(Book $book): ?Collection
    {
        return $this->collection($book->reviews, new BookReviewTransformer());
    }

    /**
     * @OA\Property(
     *    property="quotes",
     *    description="Цитаты",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/moderator_BookQuote")
     *    ),
     * ),
     */
    public function includeQuotes(Book $book): ?Collection
    {
        return $this->collection($book->quotes, new BookQuoteTransformer());
    }

    /**
     * @OA\Property(
     *    property="author",
     *    type="object",
     *    ref="#/components/schemas/moderator_Author",
     *    ),
     * ),
     */
    public function includeAuthor(Book $book): ?Item
    {
        return $this->item($book->author, new AuthorTransformer());
    }
}
