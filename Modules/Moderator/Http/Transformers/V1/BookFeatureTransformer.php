<?php

namespace Modules\Moderator\Http\Transformers\V1;

use App\Http\Transformers\V1\BookFeatureTransformer as BaseBookFeatureTransformer;

/**
 * @OA\Schema(
 *     schema="moderator_BookFeature",
 *     title="BookFeature",
 *     description="Особенности",
 *     type="object",
 * )
 */
class BookFeatureTransformer extends BaseBookFeatureTransformer
{
    /**
     * @OA\Property(
     *     property="id",
     *     description="",
     *     type="integer"
     * )
     */
    /**
     * @OA\Property(
     *     property="text",
     *     description="",
     *     type="string"
     * )
     */
    /**
     * @OA\Property(
     *    property="image",
     *    description="Картинка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
}
