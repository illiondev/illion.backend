<?php

namespace Modules\Moderator\Http\Transformers\V1;

use App\Http\Transformers\V1\BookAdvantageTransformer as BaseBookAdvantageTransformer;

/**
 * @OA\Schema(
 *     schema="moderator_BookAdvantage",
 *     title="BookAdvantage",
 *     description="Преимущество",
 *     type="object",
 * )
 */
class BookAdvantageTransformer extends BaseBookAdvantageTransformer
{
    /**
     * @OA\Property(
     *     property="id",
     *     description="",
     *     type="integer"
     * )
     */
    /**
     * @OA\Property(
     *     property="text",
     *     description="",
     *     type="string"
     * )
     */
    /**
     * @OA\Property(
     *    property="image",
     *    description="Картинка",
     *    type="object",
     *    ref="#/components/schemas/Media",
     *    ),
     * ),
     */
}
