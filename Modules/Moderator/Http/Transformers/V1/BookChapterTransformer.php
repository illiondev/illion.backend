<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\BookChapter;

/**
 * @OA\Schema(
 *     schema="moderator_BookChapter",
 *     title="BookChapter",
 *     description="Глава",
 *     type="object",
 * )
 */
class BookChapterTransformer extends TransformerAbstract
{
    public function transform(BookChapter $bookChapter): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $bookChapter->id,
            /**
             * @OA\Property(
             *     property="number",
             *     description="",
             *     type="integer"
             * )
             */
            'number' => $bookChapter->number,
            /**
             * @OA\Property(
             *     property="book_id",
             *     description="",
             *     type="integer"
             * )
             */
            'book_id' => $bookChapter->book_id,
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => $bookChapter->title,
            /**
             * @OA\Property(
             *     property="content",
             *     description="",
             *     type="string"
             * )
             */
            'content' => $bookChapter->content,
            /**
             * @OA\Property(
             *      property="completed",
             *      type="boolean",
             *      description=""
             * )
             */
            'completed'   => $bookChapter->completed,
            /**
             * @OA\Property(
             *      property="created_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'created_at'   => $bookChapter->created_at ? $bookChapter->created_at->toDateTimeString() : '',
            /**
             * @OA\Property(
             *      property="updated_at",
             *      type="string",
             *      description="format: 1988-09-13 14:59:59"
             * )
             */
            'updated_at'   => $bookChapter->updated_at ? $bookChapter->updated_at->toDateTimeString() : '',
        ];
    }
}
