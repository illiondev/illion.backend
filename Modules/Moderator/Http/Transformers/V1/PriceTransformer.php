<?php

namespace Modules\Moderator\Http\Transformers\V1;

use League\Fractal\TransformerAbstract;
use Modules\Moderator\Models\Price;

/**
 * @OA\Schema(
 *     schema="moderator_Price",
 *     title="Price",
 *     description="Цена",
 *     type="object",
 * )
 */
class PriceTransformer extends TransformerAbstract
{
    public function transform(Price $price): array
    {
        return [
            /**
             * @OA\Property(
             *     property="id",
             *     description="",
             *     type="integer"
             * )
             */
            'id' => $price->id,
            /**
             * @OA\Property(
             *     property="type",
             *     description="",
             *     type="integer"
             * )
             */
            'type' => $price->type,
            /**
             * @OA\Property(
             *     property="currency",
             *     description="",
             *     type="string"
             * )
             */
            'currency' => $price->currency,
            /**
             * @OA\Property(
             *     property="amount",
             *     description="",
             *     type="number"
             * )
             */
            'amount' => $price->amount,
        ];
    }
}
