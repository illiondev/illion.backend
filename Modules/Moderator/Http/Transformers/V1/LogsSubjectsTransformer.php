<?php

namespace Modules\Moderator\Http\Transformers\V1;

use App\Models\Vendor\Activity;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="moderator_LogsSubjects",
 *     title="Logs",
 *     type="object",
 * )
 */
class LogsSubjectsTransformer extends TransformerAbstract
{

    public function transform(Activity $activity): array
    {
        return [
            /**
             * @OA\Property(
             *     property="subject_label",
             *     description="",
             *     type="string"
             * )
             */
            'subject_label' => $activity->subject_label,
            /**
             * @OA\Property(
             *     property="subject_name",
             *     description="",
             *     type="string"
             * )
             */
            'subject_name' => $activity->subject_name,
        ];
    }

}
