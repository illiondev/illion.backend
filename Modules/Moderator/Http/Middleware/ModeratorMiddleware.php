<?php

namespace Modules\Moderator\Http\Middleware;

use Auth;
use Closure;

class ModeratorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (Auth::user()->isModerator())) {
            return $next($request);
        }

        abort(403);
    }
}
