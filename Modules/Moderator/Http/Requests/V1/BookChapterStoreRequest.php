<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookChapterStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookChapterStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="BookChapterStoreRequest",
 *     type="object",
 * )
 */
class BookChapterStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="title",
             *      type="string",
             * )
             */
            'title' => 'string',
            /**
             * @OA\Property(
             *      property="content",
             *      type="string",
             * )
             */
            'content' => 'string',
            /**
             * @OA\Property(
             *      property="completed",
             *      type="integer",
             *      enum={0, 1},
             * )
             */
            'completed' => 'integer',
        ];
    }
}

