<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="AuthorUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/AuthorUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="AuthorUpdateRequest",
 *     type="object",
 * )
 */
class AuthorUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="name",
             *     type="string",
             * )
             */
            'name' => 'string',
            /**
             * @OA\Property(
             *      property="surname",
             *      type="string",
             * )
             */
            'surname' => 'string',
            /**
             * @OA\Property(
             *      property="about",
             *      type="string",
             * )
             */
            'about' => 'string',
            /**
             * @OA\Property(
             *      property="instagram",
             *      type="string",
             * )
             */
            'instagram' => 'string',
        ];
    }
}

