<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="LogsGetRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/LogsGetRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={},
 *     schema="LogsGetRequest",
 *     type="object",
 * )
 */
class LogsGetRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="user_id",
             *     type="integer",
             * )
             */
            'filter.user_id' => 'integer',
            /**
             * @OA\Property(
             *     property="email",
             *     description="part of email for partial search",
             *     type="string",
             * )
             */
            'filter.email' => 'string|email',
            /**
             * @OA\Property(
             *     property="description",
             *     description="part of description for partial search",
             *     type="string",
             * )
             */
            'filter.description' => 'string',
            /**
             * @OA\Property(
             *     property="id",
             *     description="search by id",
             *     type="integer",
             * )
             */
            'filter.subject_id' => 'integer',
            /**
             * @OA\Property(
             *     property="subject_label",
             *     description="search by subject_label short class name",
             *     type="string",
             * )
             */
            'filter.subject_label' => 'string',
        ];
    }
}

