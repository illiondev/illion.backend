<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BundleChangeProductRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BundleChangeProductRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="BundleChangeProductRequest",
 *     type="object",
 * )
 */
class BundleChangeProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="is_surprise",
             *     description="is surprise?",
             *     type="boolean",
             *     example="1",
             * ),
             */
            'is_surprise' => 'boolean',
            /**
             * @OA\Property(
             *     property="price",
             *     description="cost of the product",
             *     type="number",
             *     example="299.99",
             * ),
             */
            'price' => 'numeric|max:999999',
            /**
             * @OA\Property(
             *     property="authors_percent",
             *     description="author's percent",
             *     type="number",
             *     example="50.5",
             * ),
             */
            'authors_percent' => 'max:100',
        ];
    }
}

