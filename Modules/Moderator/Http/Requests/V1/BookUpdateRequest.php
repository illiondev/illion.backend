<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="BookUpdateRequest",
 *     type="object",
 * )
 */
class BookUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="priority",
             *     type="integer",
             * )
             */
            'priority' => 'integer',
            /**
             * @OA\Property(
             *     property="title",
             *     type="string",
             * )
             */
            'title' => 'string',
            /**
             * @OA\Property(
             *     property="theme_id",
             *     type="string",
             * )
             */
            'theme_id' => 'string',
            /**
             * @OA\Property(
             *      property="author_id",
             *      type="integer",
             * )
             */
            'author_id' => 'integer|exists:authors,id',
            /**
             * @OA\Property(
             *      property="preview",
             *      type="string",
             * )
             */
            'preview' => 'required',
            /**
             * @OA\Property(
             *      property="tagline",
             *      type="string",
             * )
             */
            'tagline' => '',
            /**
             * @OA\Property(
             *      property="publication_date",
             *      description="format: 1988-09-13",
             *      type="string",
             * )
             */
            'publication_date' => 'date',
        ];
    }
}

