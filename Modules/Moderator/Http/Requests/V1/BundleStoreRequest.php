<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;
use Modules\Moderator\Models\Price;

/**
 * @OA\RequestBody(
 *     request="BundleStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BundleStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"hidden", "enabled", "pre_order"},
 *     schema="BundleStoreRequest",
 *     type="object",
 * )
 */
class BundleStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="slug",
             *     description="",
             *     type="string"
             * )
             */
            'slug' =>  'string|unique:bundles',
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' =>  'string',
            /**
             * @OA\Property(
             *      property="description",
             *      type="string",
             * )
             */
            'description' => 'string',
            /**
             * @OA\Property(
             *      property="enabled",
             *      type="boolean",
             * )
             */
            'enabled' => 'required|boolean',
            /**
             * @OA\Property(
             *      property="hidden",
             *      type="boolean",
             * )
             */
            'hidden' => 'required|boolean',
            /**
             * @OA\Property(
             *      property="pre_order",
             *      type="boolean",
             * )
             */
            'pre_order' => 'required|boolean',
            /**
             * @OA\Property(
             *      property="notification",
             *      type="string",
             * )
             */
            'notification' => 'string',
            /**
             * @OA\Property(
             *     property="prices",
             *     required={"type", "currency", "amount"},
             *     description="Цены",
             *     type="array",
             *     @OA\Items(
             *          @OA\Property(
             *               property="type",
             *               description="
             *               1 - web",
             *               type="integer",
             *               enum={1},
             *          ),
             *          @OA\Property(
             *               property="currency",
             *               description="currency code",
             *               type="string",
             *               enum={"rub"},
             *          ),
             *          @OA\Property(
             *               property="amount",
             *               description="price",
             *               type="number",
             *          )
             *     ),
             * ),
             */
            'prices' => 'array|filled',
            'prices.*.type' => 'required|integer|in:' . implode(',', Price::getTypes()),
            'prices.*.currency' => 'required|string|in:' . implode(',', Price::getCurrencies()),
            'prices.*.amount' => 'required|numeric',
        ];
    }
}

