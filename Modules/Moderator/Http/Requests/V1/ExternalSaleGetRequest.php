<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="ExternalSaleGetRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/ExternalSaleGetRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={},
 *     schema="ExternalSaleGetRequest",
 *     type="object",
 * )
 */
class ExternalSaleGetRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="bundle_id",
             *      description="bundle_id filter",
             *      type="string",
             * )
             */
            'filter.bundle_id' => 'integer',
        ];
    }
}

