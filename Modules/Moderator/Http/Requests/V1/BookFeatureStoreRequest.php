<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookFeatureStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookFeatureStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"text"},
 *     schema="BookFeatureStoreRequest",
 *     type="object",
 * )
 */
class BookFeatureStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="text",
             *      type="string",
             * )
             */
            'text' => 'required|string',
        ];
    }
}

