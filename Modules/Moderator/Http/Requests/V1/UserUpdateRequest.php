<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="Moderator_UserUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/Moderator_UserUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="Moderator_UserUpdateRequest",
 *     type="object",
 * )
 */
class UserUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="permissions",
             *      type="string",
             *      description=""
             * )
             * todo: валидация in
             */
            'permissions' => 'array',
            'permissions.*' => 'distinct|string',
        ];
    }
}
