<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookProductStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookProductStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"type", "prime_cost"},
 *     schema="BookProductStoreRequest",
 *     type="object",
 * )
 */
class BookProductStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="article",
             *      type="string",
             * )
             */
            'article' => 'string',
            /**
             * @OA\Property(
             *      property="type",
             *      type="integer",
             * )
             */
            'type' => 'required|integer',
            /**
             * @OA\Property(
             *      property="prime_cost",
             *      type="number",
             * )
             */
            'prime_cost' => 'required|numeric',
            /**
             * @OA\Property(
             *      property="weight",
             *      type="number",
             * )
             */
            'weight' => 'numeric',
            /**
             * @OA\Property(
             *      property="length",
             *      type="number",
             * )
             */
            'length' => 'numeric',
            /**
             * @OA\Property(
             *      property="width",
             *      type="number",
             * )
             */
            'width' => 'numeric',
            /**
             * @OA\Property(
             *      property="height",
             *      type="number",
             * )
             */
            'height' => 'numeric',
            /**
             * @OA\Property(
             *      property="video",
             *      type="string",
             * )
             */
            'video' => 'string',
            /**
             * @OA\Property(
             *      property="description",
             *      type="string",
             * )
             */
            'description' => 'string',
        ];
    }
}

