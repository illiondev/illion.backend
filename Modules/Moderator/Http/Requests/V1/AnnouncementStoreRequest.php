<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="AnnouncementStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/AnnouncementStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"enabled", "content", "page_type"},
 *     schema="AnnouncementStoreRequest",
 *     type="object",
 * )
 */
class AnnouncementStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="enabled",
             *     type="boolean",
             * )
             */
            'enabled' => 'required|boolean',
            /**
             * @OA\Property(
             *      property="content",
             *      type="string",
             * )
             */
            'content' => 'required|string',
            /**
             * @OA\Property(
             *      property="page_type",
             *      type="string",
             * )
             */
            'page_type' => 'required|integer',
        ];
    }
}

