<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"title", "author_id", "preview", "publication_date"},
 *     schema="BookStoreRequest",
 *     type="object",
 * )
 */
class BookStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="priority",
             *     type="integer",
             * )
             */
            'priority' => 'required|integer',
            /**
             * @OA\Property(
             *     property="title",
             *     type="string",
             * )
             */
            'title' => 'required',
            /**
             * @OA\Property(
             *     property="theme_id",
             *     type="string",
             * )
             */
            'theme_id' => 'required|string',
            /**
             * @OA\Property(
             *      property="author_id",
             *      type="integer",
             * )
             */
            'author_id' => 'required|integer|exists:authors,id',
            /**
             * @OA\Property(
             *      property="preview",
             *      type="string",
             * )
             */
            'preview' => 'required',
            /**
             * @OA\Property(
             *      property="tagline",
             *      type="string",
             * )
             */
            'tagline' => '',
            /**
             * @OA\Property(
             *      property="publication_date",
             *      description="format: 1988-09-13",
             *      type="string",
             * )
             */
            'publication_date' => 'required|date',
        ];
    }
}

