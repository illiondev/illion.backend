<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="MediaUploadRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/MediaUploadRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"image", "category"},
 *     schema="MediaUploadRequest",
 *     type="object",
 * )
 */
class MediaUploadRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="image",
             *     type="string",
             *     format="binary",
             * )
             */
            'image' => 'required|file'
//                . '|dimensions:min_width=200,min_height=200'
                . '|mimes:jpg,jpeg,png,gif,pdf,epub,zip'
                . '|mimetypes:image/jpeg,image/png,image/gif,application/pdf,application/epub+zip,application/zip',
            /**
             * @OA\Property(
             *     property="category",
             *     type="string",
             * )
             */
            'category' => 'string|required',
        ];
    }
}
