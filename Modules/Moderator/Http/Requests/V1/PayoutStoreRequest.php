<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="PayoutStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/PayoutStoreRequest"),
 *     )
 * )
 */

/**
 * @OA\Schema(
 *     required={"author_id", "amount", "payout_date"},
 *     schema="PayoutStoreRequest",
 *     type="object",
 * )
 */
class PayoutStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="author_id",
             *      type="integer",
             * )
             */
            'author_id' => 'required|integer|exists:authors,id',
            /**
             * @OA\Property(
             *      property="amount",
             *      type="number",
             * )
             */
            'amount' => 'required|numeric|max:999999',
            /**
             * @OA\Property(
             *      property="payout_date",
             *      type="string",
             *      example="2018-11-20",
             * )
             */
            'payout_date' => 'required|date',
        ];
    }
}

