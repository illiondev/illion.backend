<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="AuthorStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/AuthorStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"name", "surname", "about"},
 *     schema="AuthorStoreRequest",
 *     type="object",
 * )
 */
class AuthorStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="name",
             *     type="string",
             * )
             */
            'name' => 'required',
            /**
             * @OA\Property(
             *      property="surname",
             *      type="string",
             * )
             */
            'surname' => 'required',
            /**
             * @OA\Property(
             *      property="about",
             *      type="string",
             * )
             */
            'about' => 'string',
            /**
             * @OA\Property(
             *      property="instagram",
             *      type="string",
             * )
             */
            'instagram' => 'string',
        ];
    }
}

