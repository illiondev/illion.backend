<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="OrderUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/OrderUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={},
 *     schema="OrderUpdateRequest",
 *     type="object",
 * )
 */
class OrderUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="name",
             *      type="string",
             * )
             */
            'name'  => 'string',
            /**
             * @OA\Property(
             *      property="surname",
             *      type="string",
             * )
             */
            'surname'  => 'string',
            /**
             * @OA\Property(
             *      property="patronimic",
             *      type="string",
             * )
             */
            'patronimic'  => 'string',
            /**
             * @OA\Property(
             *      property="comment",
             *      type="string",
             * )
             */
            'comment'  => 'string|max:255',
            /**
             * @OA\Property(
             *      property="system_comment",
             *      type="string",
             * )
             */
            'system_comment'  => 'string|max:255',
            /**
             * @OA\Property(
             *      property="phone",
             *      type="string",
             * )
             */
            'phone'  => 'string',
            /**
             * @OA\Property(
             *      property="address",
             *      type="integer",
             * )
             */
            'address'  => 'string|max:255',
            /**
             * @OA\Property(
             *      property="address2",
             *      type="string",
             * )
             */
            'address2'  => 'string|max:50',
            /**
             * @OA\Property(
             *      property="postal_code",
             *      type="string",
             * )
             */
            'postal_code'  => 'string',
        ];
    }
}

