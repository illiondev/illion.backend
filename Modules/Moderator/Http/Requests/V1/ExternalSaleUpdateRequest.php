<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="ExternalSaleUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/ExternalSaleUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="ExternalSaleUpdateRequest",
 *     type="object",
 * )
 */
class ExternalSaleUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="paid",
             *     type="boolean",
             * )
             */
            'is_paid' => 'boolean',
            /**
             * @OA\Property(
             *      property="bundle_id",
             *      type="integer",
             * )
             */
            'bundle_id' => 'integer|exists:bundles,id',
            /**
             * @OA\Property(
             *      property="quantity",
             *      type="integer",
             * )
             */
            'quantity' => 'integer',
            /**
             * @OA\Property(
             *     property="price",
             *     type="number",
             * )
             */
            'price' => 'numeric|max:999999',
            /**
             * @OA\Property(
             *      property="prime_cost",
             *      type="number",
             * )
             */
            'prime_cost' => 'numeric|max:999999',
            /**
             * @OA\Property(
             *      property="author_fee",
             *      type="number",
             * )
             */
            'author_fee' => 'numeric|max:100',
            /**
             * @OA\Property(
             *      property="commission",
             *      type="integer",
             * )
             */
            'commission' => 'numeric|max:100|min:0',
            /**
             * @OA\Property(
             *      property="commission_flat",
             *      type="number",
             * )
             */
            'commission_flat' => 'numeric',
            /**
             * @OA\Property(
             *      property="comment",
             *      type="string",
             * )
             */
            'comment' => 'string',
            /**
             * @OA\Property(
             *      property="sold_at",
             *      type="string",
             *      example="2018-11-20 06:58:49",
             * )
             */
            'sold_at' => 'date',

        ];
    }
}
