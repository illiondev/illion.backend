<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="OrderBillCreateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/OrderBillCreateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={},
 *     schema="OrderBillCreateRequest",
 *     type="object",
 * )
 */
class OrderBillCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="price",
             *      type="number",
             * )
             */
            'price'  => 'numeric|max:999999',
            /**
             * @OA\Property(
             *      property="description",
             *      type="string",
             * )
             */
            'description'  => 'string',
        ];
    }
}

