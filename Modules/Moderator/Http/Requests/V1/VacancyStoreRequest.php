<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="VacancyStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/VacancyStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"enabled", "priority", "title"},
 *     schema="VacancyStoreRequest",
 *     type="object",
 * )
 */
class VacancyStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="priority",
             *      type="integer",
             * )
             */
            'priority' => 'required|integer',
            /**
             * @OA\Property(
             *     property="enabled",
             *     type="boolean",
             * )
             */
            'enabled' => 'required|boolean',
            /**
             * @OA\Property(
             *      property="title",
             *      type="string",
             * )
             */
            'title' => 'required|string|max:255',
            /**
             * @OA\Property(
             *      property="description",
             *      type="string",
             * )
             */
            'description' => 'string',
        ];
    }
}

