<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="PayoutUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/PayoutUpdateRequest"),
 *     )
 * )
 */

/**
 * @OA\Schema(
 *     schema="PayoutUpdateRequest",
 *     type="object",
 * )
 */
class PayoutUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="author_id",
             *      type="integer",
             * )
             */
            'author_id' => 'integer|exists:authors,id',
            /**
             * @OA\Property(
             *      property="amount",
             *      type="number",
             * )
             */
            'amount' => 'numeric|max:999999',
            /**
             * @OA\Property(
             *      property="payout_date",
             *      type="string",
             *      example="2018-11-20",
             * )
             */
            'payout_date' => 'date',
        ];
    }
}

