<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookQuoteStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookQuoteStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"name", "text"},
 *     schema="BookQuoteStoreRequest",
 *     type="object",
 * )
 */
class BookQuoteStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="name",
             *      type="string",
             * )
             */
            'name' => 'required|string',
            /**
             * @OA\Property(
             *      property="text",
             *      type="string",
             * )
             */
            'text' => 'required|string',
        ];
    }
}

