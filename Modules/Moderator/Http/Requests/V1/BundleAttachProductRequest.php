<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BundleAttachProductRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BundleAttachProductRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"is_surprise", "price", "authors_percent"},
 *     schema="BundleAttachProductRequest",
 *     type="object",
 * )
 */
class BundleAttachProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="is_surprise",
             *     description="is surprise?",
             *     type="boolean",
             *     example="1",
             * ),
             */
            'is_surprise' => 'required|boolean',
            /**
             * @OA\Property(
             *     property="price",
             *     description="cost of the product",
             *     type="number",
             *     example="299.99",
             * ),
             */
            'price' => 'required|numeric|max:999999',
            /**
             * @OA\Property(
             *     property="authors_percent",
             *     description="author's percent",
             *     type="number",
             *     example="50.5",
             * ),
             */
            'authors_percent' => 'numeric|max:100',
        ];
    }
}

