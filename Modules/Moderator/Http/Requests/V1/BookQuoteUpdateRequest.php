<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookQuoteUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookQuoteUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="BookQuoteUpdateRequest",
 *     type="object",
 * )
 */
class BookQuoteUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="name",
             *      type="string",
             * )
             */
            'name' => 'string',
            /**
             * @OA\Property(
             *      property="text",
             *      type="string",
             * )
             */
            'text' => 'string',
        ];
    }
}

