<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="StatisticsGetRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/StatisticsGetRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={},
 *     schema="StatisticsGetRequest",
 *     type="object",
 * )
 */
class StatisticsGetRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="date_from",
             *     type="string",
             * )
             */
            'filter.date_from' => 'date|required_with:filter.date_to|before_or_equal:filter.date_to',
            /**
             * @OA\Property(
             *      property="date_to",
             *      type="string",
             * )
             */
            'filter.date_to' => 'date|required_with:filter.date_from|before:tomorrow',
        ];
    }
}

