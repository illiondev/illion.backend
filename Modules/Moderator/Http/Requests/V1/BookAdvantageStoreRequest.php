<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookAdvantageStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookAdvantageStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"text"},
 *     schema="BookAdvantageStoreRequest",
 *     type="object",
 * )
 */
class BookAdvantageStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="text",
             *      type="string",
             * )
             */
            'text' => 'required|string',
        ];
    }
}

