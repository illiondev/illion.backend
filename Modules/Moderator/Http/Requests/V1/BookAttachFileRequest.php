<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookAttachFileRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookAttachFileRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="BookAttachFileRequest",
 *     type="object",
 * )
 */
class BookAttachFileRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="file",
             *      description="file",
             *      type="string",
             *      format="binary",
             * )
             */
            'epub' => 'file|mimes:epub,zip,pdf',
        ];
    }
}

