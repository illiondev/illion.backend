<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookProductUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookProductUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="BookProductUpdateRequest",
 *     type="object",
 * )
 */
class BookProductUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="article",
             *      type="string",
             * )
             */
            'article' => 'string',
            /**
             * @OA\Property(
             *      property="enabled",
             *      type="boolean",
             * )
             */
            'enabled' => 'boolean',
            /**
             * @OA\Property(
             *      property="hidden",
             *      type="boolean",
             * )
             */
            'hidden' => 'boolean',
            /**
             * @OA\Property(
             *      property="prime_cost",
             *      type="number",
             * )
             */
            'prime_cost' => 'numeric',
            /**
             * @OA\Property(
             *      property="weight",
             *      type="number",
             * )
             */
            'weight' => 'numeric',
            /**
             * @OA\Property(
             *      property="length",
             *      type="number",
             * )
             */
            'length' => 'numeric',
            /**
             * @OA\Property(
             *      property="width",
             *      type="number",
             * )
             */
            'width' => 'numeric',
            /**
             * @OA\Property(
             *      property="height",
             *      type="number",
             * )
             */
            'height' => 'numeric',
            /**
             * @OA\Property(
             *      property="video",
             *      type="string",
             * )
             */
            'video' => 'string',
            /**
             * @OA\Property(
             *      property="description",
             *      type="string",
             * )
             */
            'description' => 'string',
        ];
    }
}

