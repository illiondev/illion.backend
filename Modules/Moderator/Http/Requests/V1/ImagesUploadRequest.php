<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="ImagesUploadRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/ImagesUploadRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"image"},
 *     schema="ImagesUploadRequest",
 *     type="object",
 * )
 */
class ImagesUploadRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="image",
             *     type="string",
             *     format="binary",
             * )
             */
            'image' => 'required|file'
//                . '|dimensions:min_width=200,min_height=200'
                . '|mimes:jpg,jpeg,png,gif'
                . '|mimetypes:image/jpeg,image/png,image/gif',
        ];
    }
}
