<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookReviewUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookReviewUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="BookReviewUpdateRequest",
 *     type="object",
 * )
 */
class BookReviewUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="name",
             *      type="string",
             * )
             */
            'name' => 'string',
            /**
             * @OA\Property(
             *      property="instagram",
             *      type="string",
             * )
             */
            'instagram' => 'string',
            /**
             * @OA\Property(
             *      property="text",
             *      type="string",
             * )
             */
            'text' => 'string',
        ];
    }
}

