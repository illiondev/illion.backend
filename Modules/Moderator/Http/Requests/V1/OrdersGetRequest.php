<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="OrdersGetRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/OrdersGetRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={},
 *     schema="OrdersGetRequest",
 *     type="object",
 * )
 */
class OrdersGetRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="sort",
             *     type="string",
             * )
             */
            'sort' => 'string',
            /**
             * @OA\Property(
             *      property="id",
             *      description="id filter",
             *      type="integer",
             * )
             */
            'filter.id' => 'integer',
            /**
             * @OA\Property(
             *      property="fio",
             *      description="fio like filter",
             *      type="string",
             * )
             */
            'filter.fio' => 'string',
            /**
             * @OA\Property(
             *      property="email",
             *      description="email filter",
             *      type="string",
             * )
             */
            'filter.email' => 'string',
            /**
             * @OA\Property(
             *      property="status",
             *      description="email filter",
             *      type="integer",
             * )
             */
            'filtrer.status' => 'integer',
            /**
             * @OA\Property(
             *      property="price_from",
             *      description="price_from filter",
             *      type="number",
             * )
             */
            'filter.price_from' => 'numeric',
            /**
             * @OA\Property(
             *      property="price_to",
             *      description="price_to filter",
             *      type="number",
             * )
             */
            'filter.price_to' => 'numeric',
            /**
             * @OA\Property(
             *      property="date_from",
             *      description="price_from filter",
             *      type="string",
             *      example="2018-11-20 06:58:49",
             * )
             */
            'filter.date_from' => 'date|before:tomorrow',
            /**
             * @OA\Property(
             *      property="date_to",
             *      description="price_to filter",
             *      type="string",
             *      example="2018-11-20 06:58:49",
             * )
             */
            'filter.date_to' => 'date|before:tomorrow',
            /**
             * @OA\Property(
             *      property="shiptor",
             *      description="shiptor filter",
             *      type="integer",
             * )
             */
            'filter.shiptor' => 'integer',
            /**
             * @OA\Property(
             *      property="bundle_id",
             *      description="bundle id",
             *      type="integer",
             * )
             */
            'filter.bundle_id' => 'integer',
            /**
             * @OA\Property(
             *      property="only_with_failed_orders",
             *      description="",
             *      type="boolean",
             * )
             */
            'filter.only_with_failed_orders' => 'boolean',
        ];
    }
}

