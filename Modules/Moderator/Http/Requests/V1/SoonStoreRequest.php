<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="SoonStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/SoonStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"enabled", "title", "author_id"},
 *     schema="SoonStoreRequest",
 *     type="object",
 * )
 */
class SoonStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="enabled",
             *      type="boolean",
             * )
             */
            'enabled' => 'required|boolean',
            /**
             * @OA\Property(
             *     property="title",
             *     type="string",
             * )
             */
            'title' => 'required|string',
            /**
             * @OA\Property(
             *      property="author_id",
             *      type="integer",
             * )
             */
            'author_id' => 'required|integer|exists:authors,id',
        ];
    }
}

