<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="SoonUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/SoonUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="SoonUpdateRequest",
 *     type="object",
 * )
 */
class SoonUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="enabled",
             *      type="boolean",
             * )
             */
            'enabled' => 'boolean',
            /**
             * @OA\Property(
             *     property="title",
             *     type="string",
             * )
             */
            'title' => 'string',
            /**
             * @OA\Property(
             *      property="author_id",
             *      type="integer",
             * )
             */
            'author_id' => 'integer|exists:authors,id',
        ];
    }
}

