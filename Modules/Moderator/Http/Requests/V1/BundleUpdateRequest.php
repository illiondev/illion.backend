<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Moderator\Models\Price;

/**
 * @OA\RequestBody(
 *     request="BundleUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BundleUpdateRequest"),
 *     )
 * )
 */

/**
 * @OA\Schema(
 *     schema="BundleUpdateRequest",
 *     type="object",
 * )
 */
class BundleUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="slug",
             *     description="",
             *     type="string"
             * )
             */
            'slug' => [
                'string',
                Rule::unique('bundles')->ignore($this->route('bundle')),
            ],
            /**
             * @OA\Property(
             *     property="title",
             *     description="",
             *     type="string"
             * )
             */
            'title' => 'string',
            /**
             * @OA\Property(
             *      property="description",
             *      type="string",
             * )
             */
            'description' => 'string',
            /**
             * @OA\Property(
             *      property="enabled",
             *      type="boolean",
             * )
             */
            'enabled' => 'boolean',
            /**
             * @OA\Property(
             *      property="hidden",
             *      type="boolean",
             * )
             */
            'hidden' => 'boolean',
            /**
             * @OA\Property(
             *      property="pre_order",
             *      type="boolean",
             * )
             */
            'pre_order' => 'boolean',
            /**
             * @OA\Property(
             *      property="notification",
             *      type="string",
             * )
             */
            'notification' => 'string',
            /**
             * @OA\Property(
             *     property="prices",
             *     required={"type", "currency", "amount"},
             *     description="Цены",
             *     type="array",
             *     @OA\Items(
             *          @OA\Property(
             *               property="type",
             *               description="
             *               1 - web",
             *               type="integer",
             *               enum={1},
             *          ),
             *          @OA\Property(
             *               property="currency",
             *               description="currency code",
             *               type="string",
             *               enum={"rub"},
             *          ),
             *          @OA\Property(
             *               property="amount",
             *               description="price",
             *               type="number",
             *          )
             *     ),
             * ),
             */
            'prices' => 'filled',
            'prices.*.type' => 'required|integer|in:' . implode(',', Price::getTypes()),
            'prices.*.currency' => 'required|string|in:' . implode(',', Price::getCurrencies()),
            'prices.*.amount' => 'required|numeric',
        ];
    }
}

