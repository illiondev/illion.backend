<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="BookReviewStoreRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/BookReviewStoreRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     required={"name", "instagram", "text"},
 *     schema="BookReviewStoreRequest",
 *     type="object",
 * )
 */
class BookReviewStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="name",
             *      type="string",
             * )
             */
            'name' => 'required|string',
            /**
             * @OA\Property(
             *      property="instagram",
             *      type="string",
             * )
             */
            'instagram' => 'required|string',
            /**
             * @OA\Property(
             *      property="text",
             *      type="string",
             * )
             */
            'text' => 'required|string',
        ];
    }
}

