<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="VacancyUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/VacancyUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="VacancyUpdateRequest",
 *     type="object",
 * )
 */
class VacancyUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *      property="priority",
             *      type="integer",
             * )
             */
            'priority' => 'integer',
            /**
             * @OA\Property(
             *     property="enabled",
             *     type="boolean",
             * )
             */
            'enabled' => 'boolean',
            /**
             * @OA\Property(
             *      property="title",
             *      type="string",
             * )
             */
            'title' => 'string|max:255',
            /**
             * @OA\Property(
             *      property="description",
             *      type="string",
             * )
             */
            'description' => 'string',
        ];
    }
}

