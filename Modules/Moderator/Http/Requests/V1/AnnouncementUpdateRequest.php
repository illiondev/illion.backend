<?php

namespace Modules\Moderator\Http\Requests\V1;

use Dingo\Api\Http\FormRequest;

/**
 * @OA\RequestBody(
 *     request="AnnouncementUpdateRequest",
 *     required=true,
 *     description="",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(ref="#/components/schemas/AnnouncementUpdateRequest"),
 *     )
 * )
 */
/**
 * @OA\Schema(
 *     schema="AnnouncementUpdateRequest",
 *     type="object",
 * )
 */
class AnnouncementUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            /**
             * @OA\Property(
             *     property="enabled",
             *     type="boolean",
             * )
             */
            'enabled' => 'boolean',
            /**
             * @OA\Property(
             *      property="content",
             *      type="string",
             * )
             */
            'content' => 'string',
            /**
             * @OA\Property(
             *      property="page_type",
             *      type="string",
             * )
             */
            'page_type' => 'integer',
        ];
    }
}

