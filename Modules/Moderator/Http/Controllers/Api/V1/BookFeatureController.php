<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\BookFeatureStoreRequest;
use Modules\Moderator\Http\Requests\V1\ImagesUploadRequest;
use Modules\Moderator\Http\Transformers\V1\BookFeatureTransformer;
use Modules\Moderator\Http\Transformers\V1\MediaTransformer;
use Modules\Moderator\Repositories\BookRepository;

class BookFeatureController extends ApiController
{
    protected $bookRepository;

    public function __construct(
        BookRepository $bookRepository
    )
    {
        parent::__construct();

        $this->bookRepository = $bookRepository;
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/features",
     *     tags={"book's features"},
     *     summary="All features of book",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_BookFeature")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(int $bookId): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);

        return $this->response->collection($book->features, new BookFeatureTransformer());
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/features/{feature id}",
     *     tags={"book's features"},
     *     summary="Book's feature info",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="feature ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookFeature"),
     *         )
     *     ),
     * )
     */
    public function show(int $bookId, int $id): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $feature = $book->features()->where(['id' => $id])->firstOrFail();

        return $this->response->item($feature, new BookFeatureTransformer());
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/features",
     *     tags={"book's features"},
     *     summary="Create new feature",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookFeatureStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookFeature"),
     *         )
     *     ),
     * )
     */
    public function store(int $bookId, BookFeatureStoreRequest $bookFeatureStoreRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $feature = $book->features()->create($bookFeatureStoreRequest->validated());

        return $this->response->item($feature, new BookFeatureTransformer());
    }

    /**
     * @OA\Put(
     *     path="/books/{book id}/features/{feature id}",
     *     tags={"book's features"},
     *     summary="Update feature",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="feature ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookFeatureStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookFeature"),
     *         )
     *     ),
     * )
     */
    public function update(int $bookId, int $id, BookFeatureStoreRequest $bookFeatureStoreRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $feature = $book->features()->where(['id' => $id])->firstOrFail();

        $feature->fill($bookFeatureStoreRequest->validated())->saveOrFail();

        return $this->response->item($feature, new BookFeatureTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/books/{book id}/features/{feature id}",
     *     tags={"book's features"},
     *     summary="Delete feature",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="feature ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function destroy(int $bookId, int $id): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $feature = $book->features()->where(['id' => $id])->firstOrFail();

        $feature->delete();

        return $this->response->accepted();
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/features/{feature id}/image",
     *     tags={"book's features", "images"},
     *     summary="Add image to feature",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="feature ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/ImagesUploadRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Media"),
     *         )
     *     ),
     * )
     */
    public function imageUpload(int $bookId, int $id, ImagesUploadRequest $request): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $feature = $book->features()->where(['id' => $id])->firstOrFail();

        $file = $request->file('image');

        $media = $feature
            ->addMedia($file)
            ->usingFileName(uniqid() . '.' . $file->getClientOriginalExtension())
            ->toMediaCollection($feature::MEDIA_CATEGORY_IMAGE);

        return $this->response->item($media, new MediaTransformer())->setStatusCode(Response::HTTP_CREATED);
    }
}
