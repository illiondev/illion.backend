<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Modules\Moderator\Http\Controllers\Api\ApiBaseController;

/**
 * @OA\Info(
 *   title="House Of Illion moderator api",
 *   version="1.0.0",
 * )
 */
/**
 * @OA\OpenApi(
 *     security={
 *         {"bearerAuth": {}}
 *     }
 * )
 */
/**
 * @OA\Server(
 *      url="http://dev.api.illion.io/api/moderator/v1",
 *      description="",
 * )
 */
/**
 * @OA\SecurityScheme(
 *     @OA\Flow(
 *         flow="clientCredentials",
 *         tokenUrl="oauth/token",
 *         scopes={}
 *     ),
 *     in="header",
 *  securityScheme="bearerAuth",
 *  type="http",
 *  scheme="bearer",
 *  bearerFormat="JWT",
 * )
 */
abstract class ApiController extends ApiBaseController
{
    public function __construct()
    {
        parent::__construct();
    }
}
