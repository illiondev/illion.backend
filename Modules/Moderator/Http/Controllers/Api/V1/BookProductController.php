<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\BookAttachFileRequest;
use Modules\Moderator\Http\Requests\V1\BookProductStoreRequest;
use Modules\Moderator\Http\Requests\V1\BookProductUpdateRequest;
use Modules\Moderator\Http\Transformers\V1\BookProductTransformer;
use Modules\Moderator\Models\Product;
use Modules\Moderator\Models\PublishedBook;
use Modules\Moderator\Repositories\BookProductRepository;
use Modules\Moderator\Repositories\BookRepository;
use Modules\Moderator\Services\ProductService;
use Modules\Moderator\Http\Transformers\V1\BookProductShortTransformer;
use Dingo\Api\Http\Request;
use Validator;

class BookProductController extends ApiController
{
    protected $bookRepository;
    protected $productService;

    public function __construct(
        BookRepository $bookRepository,
        ProductService $productService
    )
    {
        parent::__construct();

        $this->bookRepository = $bookRepository;
        $this->productService = $productService;
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/products",
     *     tags={"producs"},
     *     summary="All products of book",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_BookProduct")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(int $bookId): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);

        return $this->response->collection($book->products, new BookProductTransformer());
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/products/{product id}",
     *     tags={"producs"},
     *     summary="Book's product info",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookProduct"),
     *         )
     *     ),
     * )
     */
    public function show(int $bookId, int $id): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $product = $book->products()->where(['id' => $id])->firstOrFail();

        return $this->response->item($product, new BookProductTransformer());
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/products",
     *     tags={"producs"},
     *     summary="Create new product",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookProductStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookProduct"),
     *         )
     *     ),
     * )
     */
    public function store(int $bookId, BookProductStoreRequest $bookProductStoreRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $product = new Product($bookProductStoreRequest->validated());

        $product->meta = [
            'weight' => $bookProductStoreRequest->input('weight'),
            'length' => $bookProductStoreRequest->input('length'),
            'width' => $bookProductStoreRequest->input('width'),
            'height' => $bookProductStoreRequest->input('height'),
            'epub' => '',
            'pdf' => '',
            'epub_preview' => '',
            'video' => $bookProductStoreRequest->input('video'),
            'description' => $bookProductStoreRequest->input('description'),
        ];

        if (!$product->article) {
            $product->article = uniqid();
        }

        $book->products()->save($product);

        return $this->response->item($product, new BookProductTransformer());
    }

    /**
     * @OA\Put(
     *     path="/books/{book id}/products/{product id}",
     *     tags={"producs"},
     *     summary="Update product",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookProductUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookProduct"),
     *         )
     *     ),
     * )
     */
    public function update(int $bookId, int $id, BookProductUpdateRequest $bookProductUpdateRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $product = $book->products()->where(['id' => $id])->firstOrFail();

        $product->fill($bookProductUpdateRequest->validated());

        $meta = $product->meta;
        $meta['weight'] = $bookProductUpdateRequest->has('weight') ? $bookProductUpdateRequest->input('weight') : $product->weight;
        $meta['length'] = $bookProductUpdateRequest->has('length') ? $bookProductUpdateRequest->input('length') : $product->length;
        $meta['width'] = $bookProductUpdateRequest->has('width') ? $bookProductUpdateRequest->input('width') : $product->width;
        $meta['height'] = $bookProductUpdateRequest->has('height') ? $bookProductUpdateRequest->input('height') : $product->height;
        $meta['video'] = $bookProductUpdateRequest->has('video') ? $bookProductUpdateRequest->input('video') : $product->video;
        $meta['description'] = $bookProductUpdateRequest->has('description') ? $bookProductUpdateRequest->input('description') : $product->description;

        $product->meta = $meta;

        $product->saveOrFail();

        return $this->response->item($product, new BookProductTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/books/{book id}/products/{product id}",
     *     tags={"producs"},
     *     summary="Delete product",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function destroy(int $bookId, int $id): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $product = $book->products()->where(['id' => $id])->firstOrFail();

        $product->delete();

        return $this->response->accepted();
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/products/{product id}/{format}",
     *     tags={"producs"},
     *     summary="Attach file to a product",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="format",
     *         in="path",
     *         description="file format",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="epub"
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookAttachFileRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookProduct"),
     *         )
     *     ),
     * )
     */
    public function attachFile(int $bookId, int $id, string $format, BookAttachFileRequest $bookAttachFileRequest): Response
    {
        Validator::make(['format' => $format], [
            'format' => 'required|in:epub,pdf',
        ])->validate();

        $book = $this->bookRepository->findOrFail($bookId);
        /** @var Product $product */
        $product = $book->products()->where(['id' => $id])->firstOrFail();
        $file = $bookAttachFileRequest->file('epub');

        if ($file) {
            $meta = $product->meta;
            $meta[$format] = $file->storeAs('bookfiles/' . $product->id, uniqid() . '.' . $format);
            $product->meta = $meta;

            $product->saveOrFail();

            if ($format === 'epub') {
                $this->productService->publish($product, PublishedBook::TYPE_BOOK);
            }
        }

        return $this->response->item($product, new BookProductTransformer());
    }


    /**
     * @OA\Delete(
     *     path="/books/{book id}/products/{product id}/{format}",
     *     tags={"producs"},
     *     summary="Detach file from a product",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="format",
     *         in="path",
     *         description="file format",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="epub"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookProduct"),
     *         )
     *     ),
     * )
     */
    public function detachFile(int $bookId, int $id, string $format): Response
    {
        Validator::make(['format' => $format], [
            'format' => 'required|in:epub,pdf',
        ])->validate();

        $book = $this->bookRepository->findOrFail($bookId);
        $product = $book->products()->where(['id' => $id])->firstOrFail();

        $meta = $product->meta;
        $meta[$format] = '';
        $product->meta = $meta;

        $product->saveOrFail();

        return $this->response->item($product, new BookProductTransformer());
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/products/{product id}/epub_preview",
     *     tags={"producs"},
     *     summary="Attach file to a product",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookAttachFileRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookProduct"),
     *         )
     *     ),
     * )
     */
    public function attachPreview(int $bookId, int $id, BookAttachFileRequest $bookAttachFileRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        /** @var Product $product */
        $product = $book->products()->where(['id' => $id])->firstOrFail();
        $epub = $bookAttachFileRequest->file('epub_preview');

        if ($epub) {
            $meta = $product->meta;
            $meta['epub_preview'] = $epub->storeAs('bookfiles_preview/' . $product->id, uniqid() . '.epub');
            $product->meta = $meta;

            $product->saveOrFail();

            $this->productService->publish($product, PublishedBook::TYPE_PREVIEW);
        }

        return $this->response->item($product, new BookProductTransformer());
    }


    /**
     * @OA\Delete(
     *     path="/books/{book id}/products/{product id}/epub_preview",
     *     tags={"producs"},
     *     summary="Detach file from a product",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookProduct"),
     *         )
     *     ),
     * )
     */
    public function detachPreview(int $bookId, int $id): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $product = $book->products()->where(['id' => $id])->firstOrFail();

        $meta = $product->meta;
        $meta['epub_preview'] = '';
        $product->meta = $meta;

        $product->saveOrFail();

        return $this->response->item($product, new BookProductTransformer());
    }

    /**
     * @OA\Get(
     *     path="/books/products/autocomplete",
     *     tags={"producs"},
     *     summary="books having enabled products autocomplete",
     *     description="",
     *     @OA\Parameter(
     *         name="query",
     *         in="query",
     *         description="query for autocomplete",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="элеонора",
     *             minimum=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_BookProduct")
     *             ),
     *         )
     *     ),
     * )
     */
    public function autocomplete(Request $request, BookProductRepository $bookProductRepository): Response
    {
        $request->validate([
            'query' => 'required|string|min:1',
        ]);

        $products = $bookProductRepository->autocomplete($request->input('query'), self::AUTOCOMPLETE_LIMIT);

        return $this->response->collection($products, new BookProductShortTransformer());
    }
}
