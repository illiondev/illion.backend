<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Request;
use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\BundleAttachProductRequest;
use Modules\Moderator\Http\Requests\V1\BundleChangeProductRequest;
use Modules\Moderator\Http\Requests\V1\BundleStoreRequest;
use Modules\Moderator\Http\Requests\V1\BundleUpdateRequest;
use Modules\Moderator\Http\Requests\V1\MediaUploadRequest;
use Modules\Moderator\Http\Transformers\V1\BundleShortTransformer;
use Modules\Moderator\Http\Transformers\V1\BundleTransformer;
use Modules\Moderator\Http\Transformers\V1\MediaTransformer;
use Modules\Moderator\Repositories\BookRepository;
use Modules\Moderator\Repositories\BundleRepository;
use Modules\Moderator\Repositories\PriceRepository;
use Modules\Moderator\Services\ProductService;

class BundleController extends ApiController
{
    protected $bundleRepository;
    protected $productService;
    protected $priceRepository;
    protected $bookRepository;

    public function __construct(
        BundleRepository $bundleRepository,
        BookRepository $bookRepository,
        PriceRepository $priceRepository,
        ProductService $productService
    ) {
        parent::__construct();

        $this->bundleRepository = $bundleRepository;
        $this->productService = $productService;
        $this->priceRepository = $priceRepository;
        $this->bookRepository = $bookRepository;
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/bundles",
     *     tags={"bundle"},
     *     summary="Create new bundle",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BundleStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Bundle"),
     *         )
     *     ),
     * )
     */
    public function store(int $bookId, BundleStoreRequest $bundleStoreRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);

        $bundle = $this->bundleRepository->new($bundleStoreRequest->validated());

        $book->bundles()->save($bundle);

        if ($bundleStoreRequest->input('prices')) {
            foreach ($bundleStoreRequest->input('prices') as $price) {
                $this->priceRepository->update($bundle, $price['type'], $price['currency'], $price['amount']);
            }

            $bundle->load('prices');
        }

        return $this->response->item($bundle, new BundleTransformer());
    }

    /**
     * @OA\Put(
     *     path="/books/{book id}/bundles/{id}",
     *     tags={"bundle"},
     *     summary="Update bundle",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="bundle ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BundleUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Bundle"),
     *         )
     *     ),
     * )
     */
    public function update(int $bookId, int $id, BundleUpdateRequest $bundleUpdateRequest): Response
    {
        $bundle = $this->bundleRepository->findOrFail($id);

        $bundle = $this->bundleRepository->update($bundle, $bundleUpdateRequest->validated());

        if ($bundleUpdateRequest->input('prices')) {
            foreach ($bundleUpdateRequest->input('prices') as $price) {
                $this->priceRepository->update($bundle, $price['type'], $price['currency'], $price['amount']);
            }

            $bundle->load('prices');
        }

        return $this->response->item($bundle, new BundleTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/books/{book id}/bundles/{id}",
     *     tags={"bundle"},
     *     summary="Delete bundle",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="bundle ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function destroy(int $bookId, int $id): Response
    {
        $this->bundleRepository->delete($id);

        return $this->response->accepted();
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/bundles/{id}/products/{productId}",
     *     tags={"bundle"},
     *     summary="Add product to a bundle",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="bundle ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="productId",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BundleAttachProductRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Bundle"),
     *         )
     *     ),
     * )
     */
    public function attachProduct(int $bookId, int $id, int $productId, BundleAttachProductRequest $request): Response
    {
        $bundle = $this->bundleRepository->findOrFail($id);

        $data[$productId] = [
            'is_surprise' => (int) $request->input('is_surprise'),
            'price' => (float) $request->input('price'),
            'authors_percent' => (float) $request->input('authors_percent'),
        ];

        $bundle->products()->syncWithoutDetaching($data);

        $bundle->load('products');

        return $this->response->item($bundle, new BundleTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/books/{book id}/bundles/{id}/products/{productId}",
     *     tags={"bundle"},
     *     summary="Detach product from a bundle",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="bundle ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="productId",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Bundle"),
     *         )
     *     ),
     * )
     */
    public function detachProduct(int $bookId, int $id, int $productId): Response
    {
        $bundle = $this->bundleRepository->findOrFail($id);

        $bundle->products()->detach($productId);

        $bundle->load('products');

        return $this->response->item($bundle, new BundleTransformer());
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/bundles/{id}/images",
     *     tags={"bundle", "images"},
     *     summary="Upload image to bundle",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="bundle's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/MediaUploadRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Media"),
     *         )
     *     ),
     * )
     */
    public function imageUpload(int $bookId, int $id, MediaUploadRequest $request): Response
    {
        $bundle = $this->bundleRepository->findOrFail($id);

        $file = $request->file('image');

        $media = $bundle
            ->addMedia($file)
            ->usingFileName(uniqid() . '.' . $file->getClientOriginalExtension())
            ->toMediaCollection($request->input('category'));

        return $this->response->item($media, new MediaTransformer())->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @OA\Delete(
     *     path="/books/{book id}/bundles/{id}/images/{imageId}",
     *     tags={"bundle", "images"},
     *     summary="Remove image from bundle",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="bundle's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="imageId",
     *         in="path",
     *         description="image ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success"
     *     ),
     * )
     */
    public function imageDelete(int $bookId, int $id, int $imageId): Response
    {
        $bundle = $this->bundleRepository->findOrFail($id);

        $image = $bundle->media()->where(['id' => $imageId])->firstOrFail();

        $image->delete();

        return $this->response->accepted();
    }

    /**
     * @OA\Get(
     *     path="/books/bundles",
     *     tags={"bundle"},
     *     summary="bundles",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Bundle")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(BundleRepository $bundleRepository): Response
    {
        $bundles = $bundleRepository->all();

        return $this->response->collection($bundles, new BundleShortTransformer());

    }

    /**
     * @OA\Get(
     *     path="/books/bundles/autocomplete",
     *     tags={"bundle"},
     *     summary="bundles autocomplete",
     *     description="",
     *     @OA\Parameter(
     *         name="query",
     *         in="query",
     *         description="query for autocomplete",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="элеонора",
     *             minimum=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Bundle")
     *             ),
     *         )
     *     ),
     * )
     */
    public function autocomplete(Request $request, BundleRepository $bundleRepository): Response
    {
        $request->validate([
            'query' => 'required|string|min:1',
        ]);

        $bundles = $bundleRepository->autocomplete($request->input('query'), self::AUTOCOMPLETE_LIMIT);

        return $this->response->collection($bundles, new BundleShortTransformer());

    }
}