<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\MediaUploadRequest;
use Modules\Moderator\Http\Requests\V1\SoonStoreRequest;
use Modules\Moderator\Http\Requests\V1\SoonUpdateRequest;
use Modules\Moderator\Http\Transformers\V1\MediaTransformer;
use Modules\Moderator\Http\Transformers\V1\SoonTransformer;
use Modules\Moderator\Repositories\SoonRepository;

class SoonController extends ApiController
{
    protected $soonRepository;

    public function __construct(
        SoonRepository $soonRepository
    )
    {
        parent::__construct();

        $this->soonRepository = $soonRepository;
    }

    /**
     * @OA\Get(
     *     path="/soon",
     *     tags={"soon"},
     *     summary="All coming soon items",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Soon")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(): Response
    {
        $soon = $this->soonRepository->all();

        return $this->response->collection($soon, new SoonTransformer());
    }

    /**
     * @OA\Get(
     *     path="/soon/{soon id}",
     *     tags={"soon"},
     *     summary="Get soon info",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="soon's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Soon"),
     *         )
     *     ),
     * )
     */
    public function show(int $id): Response
    {
        $soon = $this->soonRepository->findOrFail($id);

        return $this->response->item($soon, new SoonTransformer());
    }

    /**
     * @OA\Post(
     *     path="/soon",
     *     tags={"soon"},
     *     summary="Create new soon",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/SoonStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Soon"),
     *         )
     *     ),
     * )
     */
    public function store(SoonStoreRequest $soonStoreRequest): Response
    {
        $soon = $this->soonRepository->create($soonStoreRequest->validated());

        return $this->response->item($soon, new SoonTransformer());
    }

    /**
     * @OA\Put(
     *     path="/soon/{soon id}",
     *     tags={"soon"},
     *     summary="Update current user's soon info",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="soon's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/SoonUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Soon"),
     *         )
     *     ),
     * )
     */
    public function update(int $id, SoonUpdateRequest $soonUpdateRequest): Response
    {
        $soon = $this->soonRepository->findOrFail($id);
        $soon = $this->soonRepository->update($soon, $soonUpdateRequest->validated());

        return $this->response->item($soon, new SoonTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/soon/{soonId}",
     *     tags={"soon"},
     *     summary="Delete soon",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="soon's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function destroy(int $id): Response
    {
        $soon = $this->soonRepository->findOrFail($id);
        $soon->delete();

        return $this->response->accepted();
    }

    /**
     * @OA\Post(
     *     path="/soon/{soon id}/image",
     *     tags={"soon"},
     *     summary="Upload image to soon",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="soon's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/MediaUploadRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Media"),
     *         )
     *     ),
     * )
     */
    public function imageUpload(int $id, MediaUploadRequest $request): Response
    {
        $soon = $this->soonRepository->findOrFail($id);

        $file = $request->file('image');

        $media = $soon
            ->addMedia($file)
            ->usingFileName(uniqid() . '.' . $file->getClientOriginalExtension())
            ->toMediaCollection($request->input('category'));

        return $this->response->item($media, new MediaTransformer())->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @OA\Delete(
     *     path="/soon/{soon id}/image/{image id}",
     *     tags={"soon"},
     *     summary="Remove image from soon",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="soon's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="imageId",
     *         in="path",
     *         description="image's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success"
     *     ),
     * )
     */
    public function imageDelete(int $id, int $imageId): Response
    {
        $soon = $this->soonRepository->findOrFail($id);

        $image = $soon->media()->where(['id' => $imageId])->firstOrFail();

        $image->delete();

        return $this->response->accepted();
    }
}
