<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\BookAdvantageStoreRequest;
use Modules\Moderator\Http\Requests\V1\ImagesUploadRequest;
use Modules\Moderator\Http\Transformers\V1\BookAdvantageTransformer;
use Modules\Moderator\Http\Transformers\V1\MediaTransformer;
use Modules\Moderator\Repositories\BookRepository;

class BookAdvantageController extends ApiController
{
    protected $bookRepository;

    public function __construct(
        BookRepository $bookRepository
    )
    {
        parent::__construct();

        $this->bookRepository = $bookRepository;
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/advantages",
     *     tags={"book's advantages"},
     *     summary="All advantages of book",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_BookAdvantage")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(int $bookId): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);

        return $this->response->collection($book->advantages, new BookAdvantageTransformer());
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/advantages/{advantage id}",
     *     tags={"book's advantages"},
     *     summary="Book's advantage info",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="advantage ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookAdvantage"),
     *         )
     *     ),
     * )
     */
    public function show(int $bookId, int $id): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $advantage = $book->advantages()->where(['id' => $id])->firstOrFail();

        return $this->response->item($advantage, new BookAdvantageTransformer());
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/advantages",
     *     tags={"book's advantages"},
     *     summary="Create new advantage",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookAdvantageStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookAdvantage"),
     *         )
     *     ),
     * )
     */
    public function store(int $bookId, BookAdvantageStoreRequest $bookAdvantageStoreRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $advantage = $book->advantages()->create($bookAdvantageStoreRequest->validated());

        return $this->response->item($advantage, new BookAdvantageTransformer());
    }

    /**
     * @OA\Put(
     *     path="/books/{book id}/advantages/{advantage id}",
     *     tags={"book's advantages"},
     *     summary="Update advantage",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="advantage ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookAdvantageStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookAdvantage"),
     *         )
     *     ),
     * )
     */
    public function update(int $bookId, int $id, BookAdvantageStoreRequest $bookAdvantageStoreRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $advantage = $book->advantages()->where(['id' => $id])->firstOrFail();

        $advantage->fill($bookAdvantageStoreRequest->validated())->saveOrFail();

        return $this->response->item($advantage, new BookAdvantageTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/books/{book id}/advantages/{advantage id}",
     *     tags={"book's advantages"},
     *     summary="Delete advantage",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="advantage ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function destroy(int $bookId, int $id): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $advantage = $book->advantages()->where(['id' => $id])->firstOrFail();

        $advantage->delete();

        return $this->response->accepted();
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/advantages/{advantage id}/image",
     *     tags={"book's advantages", "images"},
     *     summary="Add image to advantage",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="advantage ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/ImagesUploadRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Media"),
     *         )
     *     ),
     * )
     */
    public function imageUpload(int $bookId, int $id, ImagesUploadRequest $request): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $advantage = $book->advantages()->where(['id' => $id])->firstOrFail();

        $file = $request->file('image');

        $media = $advantage
            ->addMedia($file)
            ->usingFileName(uniqid() . '.' . $file->getClientOriginalExtension())
            ->toMediaCollection($advantage::MEDIA_CATEGORY_IMAGE);

        return $this->response->item($media, new MediaTransformer())->setStatusCode(Response::HTTP_CREATED);
    }
}
