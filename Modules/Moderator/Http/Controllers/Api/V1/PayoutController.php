<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\PayoutStoreRequest;
use Modules\Moderator\Http\Requests\V1\PayoutUpdateRequest;
use Modules\Moderator\Http\Transformers\V1\PayoutTransformer;
use Modules\Moderator\Repositories\AuthorRepository;
use Modules\Moderator\Repositories\PayoutRepository;

class PayoutController extends ApiController
{
    protected $payoutRepository;
    protected $authorRepository;

    public function __construct(PayoutRepository $payoutRepository, AuthorRepository $authorRepository)
    {
        parent::__construct();

        $this->payoutRepository = $payoutRepository;
        $this->authorRepository = $authorRepository;
    }

    /**
     * @OA\Get(
     *     path="/payouts",
     *     tags={"payouts"},
     *     summary="List of payouts",
     *     description="",
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="page number",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Payout")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(): Response
    {
        $payouts = $this->payoutRepository->allPaginate(self::PAGE_SIZE);

        return $this->response->paginator($payouts, new PayoutTransformer());
    }

    /**
     * @OA\Get(
     *     path="/payouts/{payout id}",
     *     tags={"payouts"},
     *     summary="Show payout by id",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="payout's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Payout"),
     *         )
     *     ),
     * )
     */
    public function show(int $id): Response
    {
        $payout = $this->payoutRepository->findOrFail($id);

        return $this->response->item($payout, new PayoutTransformer());
    }

    /**
     * @OA\Post(
     *     path="/payouts",
     *     tags={"payouts"},
     *     summary="Create new payout",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/PayoutStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Payout"),
     *         )
     *     ),
     * )
     */
    public function store(PayoutStoreRequest $payoutStoreRequest): Response
    {
        $author = $this->authorRepository->findOrFail($payoutStoreRequest->input('author_id'));

        $payout = $this->payoutRepository->create($author->id,  $this->user->id, $payoutStoreRequest->input('amount'), $payoutStoreRequest->input('payout_date'));

        return $this->response->item($payout, new PayoutTransformer());
    }

    /**
     * @OA\Put(
     *     path="/payouts/{payout id}",
     *     tags={"payouts"},
     *     summary="Update payout's info",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="payout's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/PayoutUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Payout"),
     *         )
     *     ),
     * )
     */
    public function update(int $id, PayoutUpdateRequest $payoutUpdateRequest): Response
    {
        $payout = $this->payoutRepository->findOrFail($id);

        $author = $this->authorRepository->findOrFail($payoutUpdateRequest->input('author_id'));

        $payout = $this->payoutRepository->update($payout, $author->id, $this->user->id, $payoutUpdateRequest->input('amount'), $payoutUpdateRequest->input('payout_date'));

        return $this->response->item($payout, new PayoutTransformer());
    }
}
