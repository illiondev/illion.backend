<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\BookQuoteStoreRequest;
use Modules\Moderator\Http\Requests\V1\BookQuoteUpdateRequest;
use Modules\Moderator\Http\Transformers\V1\BookQuoteTransformer;
use Modules\Moderator\Repositories\BookRepository;

class BookQuoteController extends ApiController
{
    protected $bookRepository;

    public function __construct(
        BookRepository $bookRepository
    )
    {
        parent::__construct();

        $this->bookRepository = $bookRepository;
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/quotes",
     *     tags={"book's quotes"},
     *     summary="All quotes of book",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_BookQuote")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(int $bookId): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);

        return $this->response->collection($book->quotes, new BookQuoteTransformer());
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/quotes/{quote id}",
     *     tags={"book's quotes"},
     *     summary="Book's quote info",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="quote ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookQuote"),
     *         )
     *     ),
     * )
     */
    public function show(int $bookId, int $id): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $quote = $book->quotes()->where(['id' => $id])->firstOrFail();

        return $this->response->item($quote, new BookQuoteTransformer());
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/quotes",
     *     tags={"book's quotes"},
     *     summary="Create new quote",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookQuoteStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookQuote"),
     *         )
     *     ),
     * )
     */
    public function store(int $bookId, BookQuoteStoreRequest $bookQuoteStoreRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $quote = $book->quotes()->create($bookQuoteStoreRequest->validated());

        return $this->response->item($quote, new BookQuoteTransformer());
    }

    /**
     * @OA\Put(
     *     path="/books/{book id}/quotes/{quote id}",
     *     tags={"book's quotes"},
     *     summary="Update quote",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="quote ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookQuoteUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookQuote"),
     *         )
     *     ),
     * )
     */
    public function update(int $bookId, int $id, BookQuoteUpdateRequest $bookQuoteUpdateRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $quote = $book->quotes()->where(['id' => $id])->firstOrFail();

        $quote->fill($bookQuoteUpdateRequest->validated())->saveOrFail();

        return $this->response->item($quote, new BookQuoteTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/books/{book id}/quotes/{quote id}",
     *     tags={"book's quotes"},
     *     summary="Delete quote",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="quote ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function destroy(int $bookId, int $id): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $quote = $book->quotes()->where(['id' => $id])->firstOrFail();

        $quote->delete();

        return $this->response->accepted();
    }
}
