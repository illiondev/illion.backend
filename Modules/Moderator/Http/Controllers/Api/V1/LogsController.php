<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\LogsGetRequest;
use Modules\Moderator\Http\Transformers\V1\LogsSubjectsTransformer;
use Modules\Moderator\Http\Transformers\V1\LogsTransformer;
use Modules\Moderator\Repositories\LogsRepository;

class LogsController extends ApiController
{

    protected $logsRepository;

    public function __construct(LogsRepository $logsRepository)
    {
        parent::__construct();

        $this->logsRepository = $logsRepository;
    }

    /**
     * @OA\Get(
     *     path="/logs",
     *     tags={"logs"},
     *     summary="users activity logs",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/LogsGetRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Logs")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(LogsGetRequest $logsGetRequest): Response
    {
        $logs = $this->logsRepository->usersLogs(self::PAGE_SIZE);

        return $this->response->paginator($logs, new LogsTransformer());
    }

    /**
     * @OA\Get(
     *     path="/logs/subject-classes",
     *     tags={"logs"},
     *     summary="list of subject_types for filter",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_LogsSubjects")
     *             ),
     *         )
     *     ),
     * )
     */
    public function subjectClasses(): Response
    {
        $subjects = $this->logsRepository->subjectClasses();

        return $this->response->collection($subjects, new LogsSubjectsTransformer());
    }
}