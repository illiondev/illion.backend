<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use App\Models\User;
use Dingo\Api\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Moderator\Http\Requests\V1\AuthorStoreRequest;
use Modules\Moderator\Http\Requests\V1\AuthorUpdateRequest;
use Modules\Moderator\Http\Requests\V1\ImagesUploadRequest;
use Modules\Moderator\Http\Transformers\V1\AuthorTransformer;
use Modules\Moderator\Http\Transformers\V1\MediaTransformer;
use Modules\Moderator\Repositories\AuthorRepository;
use Dingo\Api\Http\Request;

class AuthorController extends ApiController
{

    protected $authorRepository;

    public function __construct(AuthorRepository $authorRepository)
    {
        parent::__construct();

        $this->authorRepository = $authorRepository;
    }

    /**
     * @OA\Get(
     *     path="/authors/autocomplete",
     *     tags={"authors"},
     *     summary="Authors autocomplete",
     *     description="",
     *     @OA\Parameter(
     *         name="query",
     *         in="query",
     *         description="query for autocomplete",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="элеонора",
     *             minimum=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Author")
     *             ),
     *         )
     *     ),
     * )
     */
    public function autocomplete(Request $request): Response
    {
        $request->validate([
            'query' => 'required|string|min:1',
        ]);

        $authors = $this->authorRepository->autocomplete($request->input('query'), self::AUTOCOMPLETE_LIMIT);

        return $this->response->collection($authors, new AuthorTransformer());
    }

    /**
     * @OA\Get(
     *     path="/authors",
     *     tags={"authors"},
     *     summary="List of authors",
     *     description="",
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="page number",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Author")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(): Response
    {
        $authors = $this->authorRepository->allPaginate(self::PAGE_SIZE);

        return $this->response->paginator($authors, new AuthorTransformer());
    }

    /**
     * @OA\Get(
     *     path="/authors/{author id}",
     *     tags={"authors"},
     *     summary="Show author by id",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="author's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Author"),
     *         )
     *     ),
     * )
     */
    public function show(int $id): Response
    {
        $author = $this->authorRepository->findOrFail($id);

        return $this->response->item($author, new AuthorTransformer());
    }

    /**
     * @OA\Post(
     *     path="/authors",
     *     tags={"authors"},
     *     summary="Create new author",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/AuthorStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Author"),
     *         )
     *     ),
     * )
     */
    public function store(AuthorStoreRequest $authorStoreRequest): Response
    {
        $author = $this->authorRepository->create($authorStoreRequest->validated());

        return $this->response->item($author, new AuthorTransformer());
    }

    /**
     * @OA\Put(
     *     path="/authors/{author id}",
     *     tags={"authors"},
     *     summary="Update author's info",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="author's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/AuthorUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Author"),
     *         )
     *     ),
     * )
     */
    public function update(int $id, AuthorUpdateRequest $authorUpdateRequest): Response
    {
        $author = $this->authorRepository->findOrFail($id);

        $author = $this->authorRepository->update($author, $authorUpdateRequest->validated());

        return $this->response->item($author, new AuthorTransformer());
    }

    /**
     * @OA\Post(
     *     path="/authors/{author id}/image",
     *     tags={"authors", "images"},
     *     summary="Add author's photo",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="author's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/ImagesUploadRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Media"),
     *         )
     *     ),
     * )
     */
    public function imageUpload(int $id, ImagesUploadRequest $request): Response
    {
        $author = $this->authorRepository->findOrFail($id);

        $file = $request->file('image');

        $media = $author
            ->addMedia($file)
            ->usingFileName(uniqid() . '.' . $file->getClientOriginalExtension())
            ->toMediaCollection($author::MEDIA_CATEGORY_PHOTO);

        return $this->response->item($media, new MediaTransformer())->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @OA\Delete(
     *     path="/authors/{author id}/image",
     *     tags={"authors", "images"},
     *     summary="Remove author's photo",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="author's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success"
     *     ),
     * )
     */
    public function imageDelete(int $id): Response
    {
        $author = $this->authorRepository->findOrFail($id);

        $image = $author->getFirstMedia($author::MEDIA_CATEGORY_PHOTO);

        if (!$image) {
            throw new ModelNotFoundException();
        }

        $image->delete();

        return $this->response->accepted();
    }

    /**
     * @OA\Post(
     *     path="/authors/{author id}/connect",
     *     tags={"authors"},
     *     summary="Connect user to author",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="author's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="email",
     *         in="path",
     *         description="user's email",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="test@test.ru",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Author"),
     *         )
     *     ),
     * )
     */
    public function connect(int $id, Request $request): Response
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
        ]);

        $author = $this->authorRepository->findOrFail($id);

        if ($author->user_id !== null) {
            $oldUser = $author->user;
            $oldUser->removeRole(User::ROLE_AUTHOR);
            $author->user_id = null;
        }

        $user = User::where(['email' => $request->input('email')])->firstOrFail();

        $user->assignRole(User::ROLE_AUTHOR);

        $author->user_id = $user->id;
        $author->saveOrFail();

        $author->load('user');

        return $this->response->item($author, new AuthorTransformer());
    }

    /**
     * @OA\Post(
     *     path="/authors/{author id}/disconnect",
     *     tags={"authors"},
     *     summary="Connect user to author",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="author's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Author"),
     *         )
     *     ),
     * )
     */
    public function disconnect(int $id): Response
    {
        $author = $this->authorRepository->findOrFail($id);

        if ($author->user_id !== null) {
            $oldUser = $author->user;
            $oldUser->removeRole(User::ROLE_AUTHOR);
            $author->user_id = null;
            $author->saveOrFail();
        }

        $author->load('user');

        return $this->response->item($author, new AuthorTransformer());
    }
}
