<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\OrderBillCreateRequest;
use Modules\Moderator\Http\Transformers\V1\OrderBillTransformer;
use Modules\Moderator\Repositories\OrderRepository;
use Modules\Moderator\Services\OrderService;

class OrderBillController extends ApiController
{
    protected $orderRepository;
    protected $orderService;

    public function __construct(OrderRepository $orderRepository, OrderService $orderService)
    {
        parent::__construct();

        $this->orderRepository = $orderRepository;
        $this->orderService = $orderService;
    }

    /**
     * @OA\Post(
     *     path="/orders/{id}/bill",
     *     tags={"order's bill"},
     *     summary="Create new bill",
     *     description="",
     *      @OA\Parameter(
     *         name="orderId",
     *         in="path",
     *         description="orders's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Order"),
     *         )
     *     ),
     * )
     */
    public function store(int $orderId, OrderBillCreateRequest $request): Response
    {
        $order = $this->orderRepository->findOrFail($orderId);
        $bill = $this->orderService->createBill($order, $request->validated(), $this->user);

        return $this->response->item($bill, new OrderBillTransformer());
    }
}
