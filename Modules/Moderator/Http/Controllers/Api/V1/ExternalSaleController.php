<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\ExternalSaleGetRequest;
use Modules\Moderator\Http\Requests\V1\ExternalSaleStoreRequest;
use Modules\Moderator\Http\Requests\V1\ExternalSaleUpdateRequest;
use Modules\Moderator\Http\Transformers\V1\ExternalSaleTransformer;
use Modules\Moderator\Repositories\ExternalSaleRepository;

class ExternalSaleController extends ApiController
{
    protected $externalSaleRepository;

    public function __construct(ExternalSaleRepository $externalSaleRepository)
    {
        parent::__construct();

        $this->externalSaleRepository = $externalSaleRepository;
    }

    /**
     * @OA\Get(
     *     path="/external-sales/{externalSale_id}",
     *     tags={"externalSale"},
     *     summary="Show externalSale",
     *     description="",
     *     @OA\Parameter(
     *         name="externalSaleId",
     *         in="path",
     *         description="externalSale's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookProduct"),
     *         )
     *     ),
     * )
     */
    public function show(int $externalSaleId): Response
    {
        $externalSale = $this->externalSaleRepository->findOrFail($externalSaleId);

        return $this->response->item($externalSale, new ExternalSaleTransformer());
    }

    /**
     * @OA\Get(
     *     path="/external-sales",
     *     tags={"externalSale"},
     *     summary="Show external sales",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_ExternalSale"),
     *         )
     *     ),
     * )
     */
    public function index(ExternalSaleGetRequest $externalSaleGetRequest): Response
    {
        $externalSales = $this->externalSaleRepository->allPaginate(self::PAGE_SIZE);

        return $this->response->paginator($externalSales, new ExternalSaleTransformer());
    }

    /**
     * @OA\Post(
     *     path="/external-sales",
     *     tags={"externalSale"},
     *     summary="Create externalSale",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/ExternalSaleStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_ExternalSale"),
     *         )
     *     ),
     * )
     */
    public function store(ExternalSaleStoreRequest $externalSaleStoreRequest): Response
    {
        $params = $externalSaleStoreRequest->validated();

        if ($externalSaleStoreRequest->input('commission_flat')) {
            $params['commission'] = 0;
        } else if ($externalSaleStoreRequest->input('commission')) {
            $params['commission_flat'] = 0;
        }

        $externalSale = $this->externalSaleRepository->create($this->user->id, $params);

        return $this->response->item($externalSale, new ExternalSaleTransformer());
    }

    /**
     * @OA\Put(
     *     path="/external-sales/{externalSale_id}",
     *     tags={"externalSale"},
     *     summary="Update externalSale",
     *     description="",
     *     @OA\Parameter(
     *         name="externalSaleId",
     *         in="path",
     *         description="externalSale's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/ExternalSaleUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_ExternalSale"),
     *         )
     *     ),
     * )
     */
    public function update(int $externalSaleId, ExternalSaleUpdateRequest $externalSaleUpdateRequest): Response
    {
        $externalSale = $this->externalSaleRepository->findOrFail($externalSaleId);

        $params = $externalSaleUpdateRequest->validated();
        if ($externalSaleUpdateRequest->input('commission_flat')) {
            $params['commission'] = 0;
        } else if ($externalSaleUpdateRequest->input('commission')) {
            $params['commission_flat'] = 0;
        }

        $externalSale = $this->externalSaleRepository->update($externalSale, $this->user->id, $params);

        return $this->response->item($externalSale, new ExternalSaleTransformer());
    }
}
