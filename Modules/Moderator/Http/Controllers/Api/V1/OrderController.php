<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\OrdersGetRequest;
use Modules\Moderator\Http\Requests\V1\OrderUpdateRequest;
use Modules\Moderator\Http\Transformers\V1\OrderTransformer;
use Modules\Moderator\Repositories\OrderRepository;
use Modules\Moderator\Services\GoogleSpreadsheet;
use Modules\Moderator\Services\OrderService;

class OrderController extends ApiController
{
    const PAGE_SIZE = 50;

    protected $orderRepository;
    protected $orderService;

    public function __construct(OrderRepository $orderRepository, OrderService $orderService)
    {
        parent::__construct();

        $this->orderRepository = $orderRepository;
        $this->orderService = $orderService;
    }
    
    /**
     * @OA\Get(
     *     path="/orders",
     *     tags={"orders"},
     *     summary="List of orders",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/OrdersGetRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Order")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(OrdersGetRequest $ordersGetRequest): Response
    {
        $orders = $this->orderRepository->allPaginate($ordersGetRequest->validated(), self::PAGE_SIZE);

        return $this->response->paginator($orders, new OrderTransformer());
    }

    /**
     * @OA\Get(
     *     path="/orders/spreadsheet",
     *     tags={"orders"},
     *     summary="List of orders as spreadsheet",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/OrdersGetRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Order")
     *             ),
     *         )
     *     ),
     * )
     */
    public function spreadsheet(OrdersGetRequest $ordersGetRequest): Response
    {
        $orders = $this->orderRepository->all($ordersGetRequest->validated(), 500);
        $googleSpreadsheet = app(GoogleSpreadsheet::class);
        $googleSpreadsheet->generate($orders);

        return $this->response->array(['data' => $googleSpreadsheet->generate($orders)]);
    }

    /**
     * @OA\Get(
     *     path="/orders/{order id}",
     *     tags={"orders"},
     *     summary="Get order info",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="order's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Order"),
     *         )
     *     ),
     * )
     */
    public function show(int $id): Response
    {
        $order = $this->orderRepository->findOrFail($id);

        return $this->response->item($order, new OrderTransformer());
    }

    /**
     * @OA\Put(
     *     path="/orders",
     *     tags={"order"},
     *     summary="Update order",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/OrderUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Order"),
     *         )
     *     ),
     * )
     */
    public function update(int $orderId, OrderUpdateRequest $orderUpdateRequest): Response
    {
        $order = $this->orderRepository->findOrFail($orderId);
        $order = $this->orderService->update($order, $orderUpdateRequest->validated());

        return $this->response->item($order, new OrderTransformer());
    }

    /**
     * @OA\Put(
     *     path="/orders/{id}/pay",
     *     tags={"order"},
     *     summary="Set order paid",
     *     description="",
     *      @OA\Parameter(
     *         name="orderId",
     *         in="path",
     *         description="orders's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Order"),
     *         )
     *     ),
     * )
     */
    public function pay(int $orderId): Response
    {
        $order = $this->orderRepository->findOrFail($orderId);
        $order = $this->orderService->pay($order, $this->user);

        return $this->response->item($order, new OrderTransformer());
    }
}
