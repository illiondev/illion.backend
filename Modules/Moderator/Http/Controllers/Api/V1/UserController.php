<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use App\Models\User;
use Dingo\Api\Http\Request;
use Modules\Moderator\Http\Requests\V1\UserUpdateRequest;
use Modules\Moderator\Http\Transformers\V1\ModeratorTransformer;
use Modules\Moderator\Repositories\CommentRepository;
use Modules\Moderator\Repositories\UserRepository;

class UserController extends ApiController
{
    protected $userRepository;
    protected $commentRepository;

    public function __construct(
        UserRepository $userRepository,
        CommentRepository $commentRepository
    )
    {
        parent::__construct();

        $this->userRepository = $userRepository;
        $this->commentRepository = $commentRepository;
    }

    /**
     * @OA\Get(
     *     path="/users",
     *     tags={"users"},
     *     summary="users list",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Moderator")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index()
    {
        $users = $this->userRepository->getModeratorsPaginate(self::PAGE_SIZE);

        return $this->response->paginator($users, new ModeratorTransformer());
    }

    /**
     * @OA\Get(
     *     path="/users/{userId}",
     *     tags={"users"},
     *     summary="Show user by id",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="users's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/Moderator_UserUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Moderator"),
     *         )
     *     ),
     * )
     */
    public function show(int $id)
    {
        $user = $this->userRepository->getModerator($id);

        return $this->response->item($user, new ModeratorTransformer());
    }


    /**
     * @OA\Get(
     *     path="/profile/me",
     *     tags={"users"},
     *     summary="Show current user",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Moderator"),
     *         )
     *     ),
     * )
     */
    public function me()
    {
        return $this->response->item($this->user, new ModeratorTransformer());
    }

    /**
     * @OA\Put(
     *     path="/users/{userId}",
     *     tags={"users"},
     *     summary="Update user by id",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="users's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Moderator"),
     *         )
     *     ),
     * )
     */
    public function update(int $id, UserUpdateRequest $userUpdateRequest)
    {
        $user = $this->userRepository->getModerator($id);

        $permissions = $userUpdateRequest->input('permissions');

        $user->syncPermissions($permissions);

        return $this->response->item($user, new ModeratorTransformer());
    }

    /**
     * @OA\Post(
     *     path="/users/moderator",
     *     tags={"users"},
     *     summary="Add moderator role",
     *     description="",
     *     @OA\Parameter(
     *         name="email",
     *         in="path",
     *         description="user's email",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="test@test.ru",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Moderator"),
     *         )
     *     ),
     * )
     */
    public function addModeratorRole(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
        ]);

        $user = User::where(['email' => $request->input('email')])->firstOrFail();

        $user->assignRole(User::ROLE_MODERATOR);

        return $this->response->item($user, new ModeratorTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/users/{userId}/moderator",
     *     tags={"users"},
     *     summary="Remove moderator role",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="users's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Moderator"),
     *         )
     *     ),
     * )
     */
    public function removeModeratorRole(int $id)
    {
        $user = $this->userRepository->getModerator($id);

        $user->removeRole(User::ROLE_MODERATOR);

        return $this->response->item($user, new ModeratorTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/users/{userId}/comment",
     *     tags={"users"},
     *     summary="Remove moderator role",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="users's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Moderator"),
     *         )
     *     ),
     * )
     */
    public function comment(int $id, Request $request)
    {
        $comment = $request->input('comment');

        $user = $this->userRepository->getModerator($id);

        $this->commentRepository->update($user, $comment, $this->user->id);

        $user->load('comment');

        return $this->response->item($user, new ModeratorTransformer());
    }
}
