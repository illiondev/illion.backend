<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Illion\Epuber\Generator;
use Modules\Moderator\Http\Requests\V1\BookChapterStoreRequest;
use Modules\Moderator\Http\Requests\V1\BookChapterUpdateRequest;
use Modules\Moderator\Http\Requests\V1\ImagesUploadRequest;
use Modules\Moderator\Http\Transformers\V1\BookChapterTransformer;
use Modules\Moderator\Http\Transformers\V1\MediaTransformer;
use Modules\Moderator\Repositories\BookChapterRepository;
use Modules\Moderator\Repositories\BookRepository;
use Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class BookChapterController extends ApiController
{
    protected $bookRepository;
    protected $bookChapterRepository;

    public function __construct(
        BookRepository $bookRepository,
        BookChapterRepository $bookChapterRepository
    )
    {
        parent::__construct();

        $this->bookRepository = $bookRepository;
        $this->bookChapterRepository = $bookChapterRepository;
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/chapters",
     *     tags={"book's chapter"},
     *     summary="All chapters of book",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_BookChapter")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(int $bookId): Response
    {
        $bookChapters = $this->bookChapterRepository->allByBookId($bookId);

        return $this->response->collection($bookChapters, new BookChapterTransformer());
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/chapters/{chapter id}",
     *     tags={"book's chapter"},
     *     summary="Book's chapter info",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="chapter's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookChapter"),
     *         )
     *     ),
     * )
     */
    public function show(int $bookId, int $id): Response
    {
        $bookChapter = $this->bookChapterRepository->findOrFailByBookId($bookId, $id);

        return $this->response->item($bookChapter, new BookChapterTransformer());
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/chapters",
     *     tags={"book's chapter"},
     *     summary="Create new chapter",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookChapterStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookChapter"),
     *         )
     *     ),
     * )
     */
    public function store(int $bookId, BookChapterStoreRequest $bookChapterStoreRequest): Response
    {
        $bookChapter = $this->bookChapterRepository->create($bookId, $bookChapterStoreRequest->validated());

        return $this->response->item($bookChapter, new BookChapterTransformer());
    }

    /**
     * @OA\Put(
     *     path="/books/{book id}/chapters/{chapter id}",
     *     tags={"book's chapter"},
     *     summary="Update chapter",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="chapter's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookChapterUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookChapter"),
     *         )
     *     ),
     * )
     */
    public function update(int $bookId, int $id, BookChapterUpdateRequest $bookChapterUpdateRequest): Response
    {
        $bookChapter = $this->bookChapterRepository->findOrFailByBookId($bookId, $id);

        $bookChapter = $this->bookChapterRepository->update($bookChapter, $bookChapterUpdateRequest->validated());

        return $this->response->item($bookChapter, new BookChapterTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/books/{book id}/chapters/{chapter id}",
     *     tags={"book's chapter"},
     *     summary="Delete chapter",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="chapter's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookChapter"),
     *         )
     *     ),
     * )
     */
    public function destroy(int $bookId, int $id): Response
    {
        $bookChapter = $this->bookChapterRepository->findOrFailByBookId($bookId, $id);

        $bookChapter->delete();

        return $this->response->accepted();
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/chapters/{chapter id}/image",
     *     tags={"book's chapter", "images"},
     *     summary="Add image to chapter",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="chapter's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/ImagesUploadRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Media"),
     *         )
     *     ),
     * )
     */
    public function imageUpload(int $bookId, int $id, ImagesUploadRequest $request): Response
    {
        $bookChapter = $this->bookChapterRepository->findOrFailByBookId($bookId, $id);

        $file = $request->file('image');

        $media = $bookChapter
            ->addMedia($file)
            ->usingFileName(uniqid() . '.' . $file->getClientOriginalExtension())
            ->toMediaCollection($bookChapter::MEDIA_CATEGORY_IMAGE);

        return $this->response->item($media, new MediaTransformer())->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/chapters/{chapter id}/preview",
     *     tags={"book's chapter"},
     *     summary="Add image to chapter",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="chapter's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/epub+zip"
     *         )
     *     ),
     * )
     */
    public function preview(int $bookId, int $id, Generator $generator): BinaryFileResponse
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $bookChapter = $this->bookChapterRepository->findOrFailByBookId($book->id, $id);

        $book->bookChapters = collect([$bookChapter]);

        $folder = 'tmp_' . $book->id . '_' . $bookChapter->id;
        $published = $generator->generate($book, false, $folder);

        $headers = ['Content-Type: application/epub+zip'];

        return response()->file(
            Storage::disk('books')->path($published->file_name),
            $headers
        );
    }
}
