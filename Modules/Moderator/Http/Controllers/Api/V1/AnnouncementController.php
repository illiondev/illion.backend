<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\AnnouncementStoreRequest;
use Modules\Moderator\Http\Requests\V1\AnnouncementUpdateRequest;
use Modules\Moderator\Http\Transformers\V1\AnnouncementTransformer;
use Modules\Moderator\Repositories\AnnouncementRepository;

class AnnouncementController extends ApiController
{
    protected $announcementRepository;

    public function __construct(AnnouncementRepository $announcementRepository)
    {
        parent::__construct();

        $this->announcementRepository = $announcementRepository;
    }

    /**
     * @OA\Get(
     *     path="/announcement",
     *     tags={"announcement"},
     *     summary="Show announcement",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Announcement"),
     *         )
     *     ),
     * )
     */
    public function show(): Response
    {
        $announcement = $this->announcementRepository->firstOrNew();

        return $this->response->item($announcement, new AnnouncementTransformer());
    }

    /**
     * @OA\Post(
     *     path="/announcement",
     *     tags={"announcement"},
     *     summary="Create announcement",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/AnnouncementStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Announcement"),
     *         )
     *     ),
     * )
     */
    public function store(AnnouncementStoreRequest $announcementStoreRequest): Response
    {
        $announcement = $this->announcementRepository->create($announcementStoreRequest->validated());

        return $this->response->item($announcement, new AnnouncementTransformer());
    }

    /**
     * @OA\Put(
     *     path="/announcement",
     *     tags={"announcement"},
     *     summary="Update announcement",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/AnnouncementUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Announcement"),
     *         )
     *     ),
     * )
     */
    public function update(AnnouncementUpdateRequest $announcementUpdateRequest): Response
    {
        $announcement = $this->announcementRepository->firstOrFail();

        $announcement = $this->announcementRepository->update($announcement, $announcementUpdateRequest->validated());

        return $this->response->item($announcement, new AnnouncementTransformer());
    }
}
