<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\StatisticsGetRequest;
use Modules\Moderator\Http\Transformers\V1\StatisticsProfitTransformer;
use Modules\Moderator\Http\Transformers\V1\StatisticsTransformer;
use Modules\Moderator\Repositories\StatisticsRepository;

class StatisticsController extends ApiController
{

    protected $statisticsRepository;

    public function __construct(StatisticsRepository $statisticsRepository)
    {
        parent::__construct();

        $this->statisticsRepository = $statisticsRepository;
    }

    /**
     * @OA\Get(
     *     path="/statistics",
     *     tags={"statistics"},
     *     summary="Bundles sales statistics",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/StatisticsGetRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Statistics")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(StatisticsGetRequest $statisticsGetRequest): Response
    {
        $products = $this->statisticsRepository->salesStatistics();

        return $this->response->collection($products, new StatisticsTransformer());
    }

    /**
     * @OA\Get(
     *     path="/statistics/summary",
     *     tags={"statistics"},
     *     summary="Bundles statistics summary",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/StatisticsGetRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Statistics")
     *             ),
     *         )
     *     ),
     * )
     */
    public function summary(StatisticsGetRequest $statisticsGetRequest): Response
    {
        return $this->response->array(['data' => $this->statisticsRepository->summary()]);
    }

    /**
     * @OA\Get(
     *     path="/statistics/profit",
     *     tags={"statistics"},
     *     summary="Bundles statistics profit",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/StatisticsGetRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Statistics")
     *             ),
     *         )
     *     ),
     * )
     */
    public function profit(StatisticsGetRequest $statisticsGetRequest): Response
    {
        $authors = $this->statisticsRepository->profit();

        return $this->response->collection($authors, new StatisticsProfitTransformer());
    }
}