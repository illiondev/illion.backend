<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\VacancyStoreRequest;
use Modules\Moderator\Http\Requests\V1\VacancyUpdateRequest;
use Modules\Moderator\Http\Transformers\V1\VacancyTransformer;
use Modules\Moderator\Repositories\VacancyRepository;

class VacancyController extends ApiController
{
    protected $vacancyRepository;

    public function __construct(VacancyRepository $vacancyRepository)
    {
        parent::__construct();

        $this->vacancyRepository = $vacancyRepository;
    }

    /**
     * @OA\Get(
     *     path="/vacancies/{vacancy_id}",
     *     tags={"vacancies"},
     *     summary="Show vacancy",
     *     description="",
     *     @OA\Parameter(
     *         name="vacancyId",
     *         in="path",
     *         description="vacancy's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Vacancy"),
     *         )
     *     ),
     * )
     */
    public function show(int $vacancyId): Response
    {
        $vacancy = $this->vacancyRepository->findOrFail($vacancyId);

        return $this->response->item($vacancy, new VacancyTransformer());
    }

    /**
     * @OA\Get(
     *     path="/vacancies",
     *     tags={"vacancies"},
     *     summary="Show vacancies",
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Vacancy"),
     *         )
     *     ),
     * )
     */
    public function index(): Response
    {
        $vacancies = $this->vacancyRepository->all();

        return $this->response->collection($vacancies, new VacancyTransformer());
    }

    /**
     * @OA\Post(
     *     path="/vacancies",
     *     tags={"vacancies"},
     *     summary="Create vacancy",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/VacancyStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Vacancy"),
     *         )
     *     ),
     * )
     */
    public function store(VacancyStoreRequest $vacancyStoreRequest): Response
    {
        $vacancy = $this->vacancyRepository->create($vacancyStoreRequest->validated());

        return $this->response->item($vacancy, new VacancyTransformer());
    }

    /**
     * @OA\Put(
     *     path="/vacancies/{vacancy_id}",
     *     tags={"vacancies"},
     *     summary="Update vacancy",
     *     description="",
     *     @OA\Parameter(
     *         name="vacancyId",
     *         in="path",
     *         description="vacancy's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/VacancyUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Vacancy"),
     *         )
     *     ),
     * )
     */
    public function update(int $vacancyId, VacancyUpdateRequest $vacancyUpdateRequest): Response
    {
        $vacancy = $this->vacancyRepository->findOrFail($vacancyId);

        $vacancy = $this->vacancyRepository->update($vacancy, $vacancyUpdateRequest->validated());

        return $this->response->item($vacancy, new VacancyTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/vacancies/{vacancy id}",
     *     tags={"vacancies"},
     *     summary="Delete vacancy",
     *     description="",
     *     @OA\Parameter(
     *         name="vacancyId",
     *         in="path",
     *         description="vacancy's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Vacancy"),
     *         )
     *     ),
     * )
     */
    public function destroy(int $vacancyId): Response
    {
        $vacancy = $this->vacancyRepository->findOrFail($vacancyId);

        $vacancy->delete();

        return $this->response->accepted();
    }
}
