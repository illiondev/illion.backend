<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\BookStoreRequest;
use Modules\Moderator\Http\Requests\V1\BookUpdateRequest;
use Modules\Moderator\Http\Requests\V1\MediaUploadRequest;
use Modules\Moderator\Http\Transformers\V1\BookTransformer;
use Modules\Moderator\Http\Transformers\V1\MediaTransformer;
use Modules\Moderator\Models\Bundle;
use Modules\Moderator\Repositories\BookRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BookController extends ApiController
{
    protected $bookRepository;

    public function __construct(
        BookRepository $bookRepository
    )
    {
        parent::__construct();

        $this->bookRepository = $bookRepository;
    }

    /**
     * @OA\Get(
     *     path="/books",
     *     tags={"books"},
     *     summary="All books of current user",
     *     description="",
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="page number",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_Book")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(): Response
    {
        $books = $this->bookRepository->allPaginate(self::PAGE_SIZE);

        return $this->response->paginator($books, new BookTransformer());
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}",
     *     tags={"books"},
     *     summary="Get current user's book info",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Book"),
     *         )
     *     ),
     * )
     */
    public function show(int $id): Response
    {
        $book = $this->bookRepository->findOrFail($id);

        return $this->response->item($book, new BookTransformer());
    }

    /**
     * @OA\Post(
     *     path="/books",
     *     tags={"books"},
     *     summary="Create new book",
     *     description="",
     *     @OA\RequestBody(ref="#/components/requestBodies/BookStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Book"),
     *         )
     *     ),
     * )
     */
    public function store(BookStoreRequest $bookStoreRequest): Response
    {
        $book = $this->bookRepository->create($this->user->id, $bookStoreRequest->validated());

        return $this->response->item($book, new BookTransformer());
    }

    /**
     * @OA\Put(
     *     path="/books/{book id}",
     *     tags={"books"},
     *     summary="Update current user's book info",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Book"),
     *         )
     *     ),
     * )
     */
    public function update(int $id, BookUpdateRequest $bookUpdateRequest): Response
    {
        $book = $this->bookRepository->findOrFail($id);
        $book = $this->bookRepository->update($book, $bookUpdateRequest->validated());

        return $this->response->item($book, new BookTransformer());
    }

    /**
     * @OA\Put(
     *     path="/books/{book id}/preview",
     *     tags={"books"},
     *     summary="Generate link for book preview",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Items(
     *                 @OA\Property(
     *                     property="confirmation_url",
     *                     description="",
     *                     example="",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     description="",
     *                     example="",
     *                     type="string"
     *                 ),
     *             ),
     *         )
     *     ),
     * )
     */
    public function preview(int $id): Response
    {
        $book = $this->bookRepository->findOrFail($id);

        return $this->response->array([
            'preview_hash' => $book->generatePreviewLink(),
            'preview_hash_expire' => $book::REDIS_BOOK_PREVIEW_KEY_EXPIRE,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/image",
     *     tags={"books", "images"},
     *     summary="Upload image to book",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/MediaUploadRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Media"),
     *         )
     *     ),
     * )
     */
    public function imageUpload(int $id, MediaUploadRequest $request): Response
    {
        $book = $this->bookRepository->findOrFail($id);

        $file = $request->file('image');

        $media = $book
            ->addMedia($file)
            ->usingFileName(uniqid() . '.' . $file->getClientOriginalExtension())
            ->toMediaCollection($request->input('category'));

        return $this->response->item($media, new MediaTransformer())->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @OA\Delete(
     *     path="/books/{book id}/image/{image id}",
     *     tags={"books", "images"},
     *     summary="Remove image from book",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="imageId",
     *         in="path",
     *         description="image's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success"
     *     ),
     * )
     */
    public function imageDelete(int $id, int $imageId): Response
    {
        $book = $this->bookRepository->findOrFail($id);

        $image = $book->media()->where(['id' => $imageId])->firstOrFail();

        $image->delete();

        return $this->response->accepted();
    }

    /**
     * @OA\Post(
     *     path="/books/{id}/bundles/{bundleId}/attach",
     *     tags={"books", "bundle"},
     *     summary="Add bundle to a book",
     *     description="",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="bundleId",
     *         in="path",
     *         description="bundle ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Book"),
     *         )
     *     ),
     * )
     */
    public function attachBundle(int $id, int $bundleId): Response
    {
        $book = $this->bookRepository->findOrFail($id);

        $book->bundles()->syncWithoutDetaching([$bundleId]);

        $book->load('bundles');

        return $this->response->item($book, new BookTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/books/{id}/bundles/{bundleId}/detach",
     *     tags={"books", "bundle"},
     *     summary="Detach bundle from a book",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="bundle ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_Bundle"),
     *         )
     *     ),
     * )
     */
    public function detachBundle(int $id, int $bundleId): Response
    {
        $book = $this->bookRepository->findOrFail($id);
        /** @var Bundle $bundle */
        $bundle = $book->bundles()->findOrFail($bundleId);

        if ($bundle->books()->count() > 1) {
            $book->bundles()->detach($bundleId);
            $book->load('bundles');
        } else {
            throw new BadRequestHttpException('Это единственная книга к которой прикреплен комплект');
        }

        return $this->response->item($book, new BookTransformer());
    }
}
