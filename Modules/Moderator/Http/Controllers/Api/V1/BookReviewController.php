<?php

namespace Modules\Moderator\Http\Controllers\Api\V1;

use Dingo\Api\Http\Response;
use Modules\Moderator\Http\Requests\V1\BookReviewStoreRequest;
use Modules\Moderator\Http\Requests\V1\BookReviewUpdateRequest;
use Modules\Moderator\Http\Requests\V1\ImagesUploadRequest;
use Modules\Moderator\Http\Transformers\V1\BookReviewTransformer;
use Modules\Moderator\Http\Transformers\V1\MediaTransformer;
use Modules\Moderator\Repositories\BookRepository;

class BookReviewController extends ApiController
{
    protected $bookRepository;

    public function __construct(
        BookRepository $bookRepository
    )
    {
        parent::__construct();

        $this->bookRepository = $bookRepository;
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/reviews",
     *     tags={"book's reviews"},
     *     summary="All reviews of book",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/moderator_BookReview")
     *             ),
     *         )
     *     ),
     * )
     */
    public function index(int $bookId): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);

        return $this->response->collection($book->reviews, new BookReviewTransformer());
    }

    /**
     * @OA\Get(
     *     path="/books/{book id}/reviews/{review id}",
     *     tags={"book's reviews"},
     *     summary="Book's review info",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="review ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookReview"),
     *         )
     *     ),
     * )
     */
    public function show(int $bookId, int $id): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $review = $book->reviews()->where(['id' => $id])->firstOrFail();

        return $this->response->item($review, new BookReviewTransformer());
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/reviews",
     *     tags={"book's reviews"},
     *     summary="Create new review",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookReviewStoreRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookReview"),
     *         )
     *     ),
     * )
     */
    public function store(int $bookId, BookReviewStoreRequest $bookReviewStoreRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $review = $book->reviews()->create($bookReviewStoreRequest->validated());

        return $this->response->item($review, new BookReviewTransformer());
    }

    /**
     * @OA\Put(
     *     path="/books/{book id}/reviews/{review id}",
     *     tags={"book's reviews"},
     *     summary="Update review",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="review ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/BookReviewUpdateRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/moderator_BookReview"),
     *         )
     *     ),
     * )
     */
    public function update(int $bookId, int $id, BookReviewUpdateRequest $bookReviewUpdateRequest): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $review = $book->reviews()->where(['id' => $id])->firstOrFail();

        $review->fill($bookReviewUpdateRequest->validated())->saveOrFail();

        return $this->response->item($review, new BookReviewTransformer());
    }

    /**
     * @OA\Delete(
     *     path="/books/{book id}/reviews/{review id}",
     *     tags={"book's reviews"},
     *     summary="Delete review",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="review ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *     ),
     * )
     */
    public function destroy(int $bookId, int $id): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $review = $book->reviews()->where(['id' => $id])->firstOrFail();

        $review->delete();

        return $this->response->accepted();
    }

    /**
     * @OA\Post(
     *     path="/books/{book id}/reviews/{review id}/image",
     *     tags={"book's reviews", "images"},
     *     summary="Add image to review",
     *     description="",
     *     @OA\Parameter(
     *         name="bookId",
     *         in="path",
     *         description="book's ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="review ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64",
     *             example=1
     *         )
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/ImagesUploadRequest"),
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Media"),
     *         )
     *     ),
     * )
     */
    public function imageUpload(int $bookId, int $id, ImagesUploadRequest $request): Response
    {
        $book = $this->bookRepository->findOrFail($bookId);
        $review = $book->reviews()->where(['id' => $id])->firstOrFail();

        $file = $request->file('image');

        $media = $review
            ->addMedia($file)
            ->usingFileName(uniqid() . '.' . $file->getClientOriginalExtension())
            ->toMediaCollection($review::MEDIA_CATEGORY_IMAGE);

        return $this->response->item($media, new MediaTransformer())->setStatusCode(Response::HTTP_CREATED);
    }
}
