<?php

namespace Modules\Moderator\Http\Controllers\Api;

use App\Models\User;
use Dingo\Api\Routing\Helpers;

/**
 * @property User $user
 */
abstract class ApiBaseController extends \Illuminate\Routing\Controller
{
    use Helpers;

    const PAGE_SIZE = 15;
    const AUTOCOMPLETE_LIMIT = 15;

    public function __construct()
    {
    }
}
