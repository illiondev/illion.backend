<?php

namespace Modules\Moderator\Helpers;


use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FioQueryFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property): Builder
    {
        return $query->where(function ($query) use ($value): void {
            $query->where('name', 'like', '%' . $value . '%');
            $query->orWhere('surname', 'like', '%' . $value . '%');
            $query->orWhere('patronimic', 'like', '%' . $value . '%');
        });
    }
}