<?php

namespace Modules\Moderator\Helpers;


use Illuminate\Database\Eloquent\Builder;
use Modules\Moderator\Models\Order;
use Spatie\QueryBuilder\Filters\Filter;

class ShiptorQueryFilter implements Filter
{
    const SHIPTOR_FILTER_ACCEPTED = 1; // Приняты (это те, где есть трек-номер и нет ошибок)
    const SHIPTOR_FILTER_ERROR = 2; // Ошибка (с ошибкой)

    public function __invoke(Builder $query, $value, string $property): Builder
    {
        if (intval($value) === self::SHIPTOR_FILTER_ACCEPTED) {
            return
                $query->whereNotNull('shiptor_id');
        } elseif (intval($value) === self::SHIPTOR_FILTER_ERROR) {
            return
                $query->whereNull('shiptor_id')
                    ->whereNotNull('shiptor_error')
                    ->whereIn('status', [Order::STATUS_PROCESSING]); // на всякий случай
        }

        return $query;
    }
}