<?php

namespace Modules\Moderator\Helpers;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class BundleIdFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property): Builder
    {
        return $query
            ->whereHas('items', function ($query) use ($value): void {
                $query->where(['orders_items.bundle_id' => $value]);
            });
    }
}
