<?php

namespace Modules\Moderator\Helpers;


use App\Models\Vendor\Activity;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class ModelClassNameFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property): Builder
    {
        return $query->where($property, '=', Activity::getClassName($value));
    }
}