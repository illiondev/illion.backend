<?php

namespace Modules\Moderator\Helpers;

class Permissions
{
    const READ_BOOKS = 'read books';
    const EDIT_BOOKS = 'edit books';
    const READ_AUTHORS = 'read authors';
    const EDIT_AUTHORS = 'edit authors';
    const READ_USERS = 'read users';
    const EDIT_USERS = 'edit users';
    const READ_CONTENT = 'read content';
    const EDIT_CONTENT = 'edit content';
    const READ_STATISTICS = 'read statistics';
    const READ_ORDERS = 'read orders';
    const EDIT_ORDERS = 'edit orders';
    const READ_PAYOUTS = 'read payouts';
    const EDIT_PAYOUTS = 'edit payouts';
    const READ_LOGS = 'read logs';
    const READ_EXTERNAL_SALES = 'read external sales';
    const EDIT_EXTERNAL_SALES = 'edit external sales';
    const READ_VACANCIES = 'read vacancies';
    const EDIT_VACANCIES = 'edit vacancies';
    const READ_SOON = 'read soon';
    const EDIT_SOON = 'edit soon';

    /**
     * author's section
     */
    const READ_AUTHORS_STATISTICS = 'read authors statistics';
    const READ_AUTHORS_PAYOUTS = 'read authors payouts';
}
