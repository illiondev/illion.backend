<?php

namespace Modules\Moderator\Helpers;


use Modules\Moderator\Models\Order;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class OnlyWithFailedOrdersFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property): Builder
    {
        $query
            ->where(['status' => Order::STATUS_CANCELED])
            ->whereHas('items', function ($query) use ($property): void {
                $query->where(['orders_items.bundle_id' => $property]);
            })
            ->whereNotIn('email', function (\Illuminate\Database\Query\Builder $query) use ($property) {
                $query
                    ->select('email')
                    ->from('orders_items')
                    ->join('orders', 'orders.id', '=', 'orders_items.order_id')
                    ->whereIn('orders.status', [
                        Order::STATUS_NEW,
                        Order::STATUS_WAITING_FOR_PAYMENT,
                        Order::STATUS_PROCESSING,
                        Order::STATUS_COMPLETED,
                    ])
                    ->where(['orders_items.bundle_id' => $property]);
            });

        return $query;
    }
}
