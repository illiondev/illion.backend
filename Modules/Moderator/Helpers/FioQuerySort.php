<?php

namespace Modules\Moderator\Helpers;


use Spatie\QueryBuilder\Sorts\Sort;
use Illuminate\Database\Eloquent\Builder;

class FioQuerySort implements Sort
{
    public function __invoke(Builder $query, $value, string $property): Builder
    {
        return
            $query->orderBy('surname', $value ? 'desc' : 'asc')
                ->orderBy('name', $value ? 'desc' : 'asc')
                ->orderBy('patronimic', $value ? 'desc' : 'asc');
    }
}