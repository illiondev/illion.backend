<?php

namespace Modules\Moderator\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Modules\Moderator\Models\Author;
use Illuminate\Contracts\Pagination\Paginator;

class AuthorRepository
{
    protected $model;

    public function __construct(Author $model)
    {
        $this->model = $model;
    }

    public function findByUserId(int $userId): Author
    {
        return $this->model
            ->where(['user_id' => $userId])
            ->first();
    }

    public function findOrFail(int $id): Author
    {
        return $this->model->findOrFail($id);
    }

    public function allPaginate(int $perPage): Paginator
    {
        return $this->model
            ->with(['media'])
            ->orderBy('id', 'desc')
            ->paginate($perPage);
    }

    public function create(array $params): Author
    {
        $author = new $this->model($params);

        $author->saveOrFail();

        return $author;
    }

    public function update(Author $author, array $validated): Author
    {
        $author->fill($validated);
        $author->saveOrFail();

        return $author;
    }

    public function autocomplete(string $q, int $limit, bool $usersOnly = false): Collection
    {
        $authors = $this->model
            ->where(function ($query) use ($q): void {
                $query->where('name', 'like', $q . '%');
                $query->orWhere('surname', 'like', $q . '%');
            })
            ->limit($limit);

        if($usersOnly) {
            $authors->whereNotNull('user_id');
        }

        return $authors->get();
    }
}
