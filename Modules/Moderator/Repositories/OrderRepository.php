<?php

namespace Modules\Moderator\Repositories;

use Illuminate\Support\Collection;
use Modules\Moderator\Helpers\BundleIdFilter;
use Modules\Moderator\Helpers\FioQueryFilter;
use Modules\Moderator\Helpers\FioQuerySort;
use Modules\Moderator\Helpers\OnlyWithFailedOrdersFilter;
use Modules\Moderator\Helpers\ShiptorQueryFilter;
use Modules\Moderator\Models\Order;
use Illuminate\Contracts\Pagination\Paginator;
use Request;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;
use Spatie\QueryBuilder\Sort;

class OrderRepository
{
    protected $model;

    public function __construct(Order $model)
    {
        $this->model = $model;
    }

    public function findOrFail(int $id): Order
    {
        return $this->model->findOrFail($id);
    }

    public function all(array $params, ?int $limit = null): Collection
    {
        return ($this->queryAll($params))->limit($limit)->get();
    }

    public function allPaginate(array $params, int $perPage): Paginator
    {
        return ($this->queryAll($params))->paginate($perPage);
    }

    protected function queryAll(array $params): QueryBuilder
    {
        $result = QueryBuilder::for(Order::class);

        //ordery bo created_at desc by default
        if (empty($params['sort'])) {
            $result->orderBy('created_at', 'desc');
        }

        $result->allowedSorts([
            'id',
            'email',
            'status',
            'total_price',
            'shiptor_id',
            'payment_id',
            'shipping_method',
            'created_at',
            Sort::custom('fio', FioQuerySort::class),
            'bundle_id',
        ]);

        $allowedFilters = [
            Filter::exact('id'),
            Filter::partial('email'),
            Filter::exact('status'),
            Filter::scope('price_from'),
            Filter::scope('price_to'),
            Filter::scope('date_from'),
            Filter::scope('date_to'),
            Filter::custom('fio', FioQueryFilter::class),
            Filter::custom('shiptor', ShiptorQueryFilter::class),
            Filter::custom('bundle_id', BundleIdFilter::class),
        ];

        if (!empty(Request::input('filter')['bundle_id'])) {
            $allowedFilters[] = Filter::custom('only_with_failed_orders', OnlyWithFailedOrdersFilter::class, Request::input('filter')['bundle_id']);
        }

        $result->allowedFilters($allowedFilters);

        return $result;
    }

}
