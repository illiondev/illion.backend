<?php

namespace Modules\Moderator\Repositories;

use App\Interfaces\PriceableInterface;
use Modules\Moderator\Models\Price;

class PriceRepository
{
    protected $model;

    public function __construct(Price $model)
    {
        $this->model = $model;
    }

    public function update(PriceableInterface $model, int $type, string $currency, float $amount): Price
    {
        /** @var Price $price */
        $price = $model->prices()->firstOrNew([
            'type' => $type,
            'currency' => $currency,
        ]);

        $price->amount = $amount;

        $price->saveOrFail();

        return $price;
    }
}
