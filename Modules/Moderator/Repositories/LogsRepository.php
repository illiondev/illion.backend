<?php

namespace Modules\Moderator\Repositories;

use App\Models\Vendor\Activity;
use Illuminate\Contracts\Pagination\Paginator;
use Modules\Moderator\Helpers\ModelClassNameFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;
use Illuminate\Support\Collection;

class LogsRepository
{
    public function __construct(Activity $model)
    {
        $this->model = $model;
    }

    public function usersLogs(int $perPage): Paginator
    {
        $result =
            QueryBuilder::for(Activity::class)
                ->join('users', function ($q) {
                    $q->on('causer_id', '=', 'users.id');
                    $q->where('causer_type', '=', 'App\Models\User');
                })
                ->select($this->model->getTable().'.*')
                ->orderBy($this->model->getTable().'.created_at', 'desc');

        $result->allowedFilters([
            Filter::exact('subject_id'),
            'description',
            Filter::exact('user_id', 'causer_id'),
            Filter::partial('email', 'email'),
            Filter::custom('subject_label', ModelClassNameFilter::class, 'subject_type'),
        ]);

        return $result->paginate($perPage);
    }

    public function subjectClasses(): Collection
    {
        return Activity::distinct('subject_type')->get(['subject_type']);
    }
}
