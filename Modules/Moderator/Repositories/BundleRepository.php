<?php

namespace Modules\Moderator\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Modules\Moderator\Models\Bundle;
use Spatie\QueryBuilder\QueryBuilder;

class BundleRepository
{
    protected $model;

    public function __construct(Bundle $model)
    {
        $this->model = $model;
    }

    protected function allQuery(): Builder
    {
        return $this->model
            ->with([
                'media',
            ]);
    }

    public function all(): Collection
    {
        return $this->allQuery()->get();
    }

    public function allPaginate(int $perPage): Paginator
    {
        return $this->allQuery()->paginate($perPage);
    }

    public function findOrFail(int $id): Bundle
    {
        return $this->model->findOrFail($id);
    }

    public function delete(int $id): void
    {
        $this->model->find($id)->delete();
    }

    public function new(array $params): Bundle
    {
        $bundle = new $this->model($params);

        return $bundle;
    }

    public function update(Bundle $bundle, array $params): Bundle
    {
        $bundle->fill($params);

        $bundle->saveOrFail();

        return $bundle;
    }

    public function autocomplete(string $q, int $limit): Collection
    {
        $result = QueryBuilder::for(Bundle::class)
            ->select(['bundles.id', 'bundles.title'])
            ->where(function ($query) use ($q): void {
                $query->orWhere('bundles.title', 'like', $q . '%');
            })
            ->limit($limit);

        return $result->get();
    }
}
