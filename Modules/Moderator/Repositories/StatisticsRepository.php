<?php

namespace Modules\Moderator\Repositories;

use Modules\Moderator\Models\BillProduct;
use Modules\Moderator\Models\Bundle;
use Modules\Moderator\Models\ExternalOrder;
use Modules\Moderator\Models\ExternalOrderItem;
use Modules\Moderator\Models\ExternalSale;
use Modules\Moderator\Models\ExternalSaleProduct;
use Modules\Moderator\Models\Order;
use Illuminate\support\Collection;
use Illuminate\Support\Facades\DB;
use Modules\Moderator\Models\OrderBill;
use Modules\Moderator\Models\OrderItem;
use Modules\Moderator\Models\OrderProduct;
use Modules\Moderator\Models\Statistics;
use Modules\Moderator\Models\StatisticsProfit;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;

class StatisticsRepository
{
    public function salesStatistics(): Collection
    {
        $orders = QueryBuilder::for(OrderItem::class)
            ->join('orders', 'orders.id', '=', 'orders_items.order_id')
            ->leftJoin('bundles', 'orders_items.bundle_id', '=', 'bundles.id')
            ->groupBy('orders_items.bundle_id', 'bundles.title')
            ->select([
                'orders_items.bundle_id',
                'bundles.title',
                'bundles.created_at',
                DB::raw('sum(orders_items.quantity) AS ordered'),
                DB::raw('sum(if(orders.status in (' . Order::STATUS_PROCESSING . ', ' . Order::STATUS_COMPLETED . '), orders_items.quantity, 0)) AS paid'),
                DB::raw('sum(if(orders.status in (' . Order::STATUS_NEW . ', ' . Order::STATUS_WAITING_FOR_PAYMENT . ', ' . Order::STATUS_CANCELED . '), orders_items.quantity, 0)) AS notPaid'),
                DB::raw('sum(if(orders.status in (' . Order::STATUS_PROCESSING . ', ' . Order::STATUS_COMPLETED . '), orders_items.price * orders_items.quantity, 0)) AS sum'),
                DB::raw('sum(if(orders.status = \'' . Order::STATUS_RETURNED . '\', orders_items.quantity, 0)) AS returned'),
                DB::raw('sum(orders_items.quantity) AS delivery'),
            ]);

        $orders->allowedFilters([
            Filter::scope('date_from'),
            Filter::scope('date_to'),
        ]);

        $orders = $orders->get();

        $result = $orders->map(function ($object) {
            /** @var OrderItem $object */
            return (new Statistics([
                'bundle_id' => $object->bundle_id,
                'type' => $object->bundle->getType(),
                'title' => $object->title,
                'ordered' => $object->ordered,
                'paid' => $object->paid,
                'notPaid' => $object->notPaid,
                'sum' => $object->sum,
                'returned' => $object->returned,
                'created_at' => $object->created_at,
            ]));
        });

        $result = $result->keyBy('bundle_id');
        /** @var Statistics[] $result */

        $externalOrdersItems = QueryBuilder::for(ExternalOrderItem::class)
            ->join('external_orders', 'external_orders.external_id', '=',
                'external_orders_items.external_order_id')
            ->leftJoin('bundles', 'external_orders_items.bundle_id', '=', 'bundles.id')
            ->groupBy('external_orders_items.bundle_id', 'bundles.title')
            ->select([
                'external_orders_items.bundle_id',
                'bundles.title',
                'bundles.created_at',
                DB::raw('sum(external_orders_items.quantity) AS ordered'),
                DB::raw('sum(if(external_orders.status in (' . Order::STATUS_PROCESSING . ', ' . Order::STATUS_COMPLETED . '), external_orders_items.quantity, 0)) AS paid'),
                DB::raw('sum(if(external_orders.status in (' . Order::STATUS_CANCELED . '), external_orders_items.quantity, 0)) AS notPaid'),
                DB::raw('sum(if(external_orders.status in (' . Order::STATUS_PROCESSING . ', ' . Order::STATUS_COMPLETED . '), external_orders_items.price * external_orders_items.quantity, 0)) AS sum'),
                DB::raw('sum(if(external_orders.status = \'' . Order::STATUS_RETURNED . '\', external_orders_items.quantity, 0)) AS returned'),
            ]);

        $externalOrdersItems->allowedFilters([
            Filter::scope('date_from'),
            Filter::scope('date_to'),
        ]);

        $externalOrdersItems = $externalOrdersItems->get()->keyBy('bundle_id');

        /** @var ExternalOrderItem[]|Collection $externalOrdersItems */
        foreach ($externalOrdersItems as $externalOrdersItem) {
            if (isset($result[$externalOrdersItem->bundle_id])) {
                $result[$externalOrdersItem->bundle_id]->ordered += $externalOrdersItem->ordered;
                $result[$externalOrdersItem->bundle_id]->paid += $externalOrdersItem->paid;
                $result[$externalOrdersItem->bundle_id]->notPaid += $externalOrdersItem->notPaid;
                $result[$externalOrdersItem->bundle_id]->sum += $externalOrdersItem->sum;
                $result[$externalOrdersItem->bundle_id]->returned += $externalOrdersItem->returned;
            } else {
                $result[$externalOrdersItem->bundle_id] = new Statistics([
                    'bundle_id' => $externalOrdersItem->bundle_id,
                    'type' => $externalOrdersItem->bundle->getType(),
                    'title' => $externalOrdersItem->title,
                    'ordered' => $externalOrdersItem->ordered,
                    'paid' => $externalOrdersItem->paid,
                    'notPaid' => $externalOrdersItem->notPaid,
                    'sum' => $externalOrdersItem->sum,
                    'returned' => $externalOrdersItem->returned,
                    'created_at' => $externalOrdersItem->created_at,
                ]);
            }
        }

        /**
         * External sales
         */
        $externalSales = QueryBuilder::for(ExternalSale::class)
            ->with(['bundle'])
            ->where(['is_paid' => true])
            ->select([
                'bundle_id',
                DB::raw('sum(quantity*price) AS sum'),
                DB::raw('sum(quantity) AS paid'),
            ])
            ->groupBy('bundle_id');

        $externalSales->allowedFilters([
            Filter::scope('date_from'),
            Filter::scope('date_to'),
        ]);

        $externalSales = $externalSales->get()->keyBy('bundle_id');

        /** @var ExternalSale[]|Collection $externalSales */
        foreach ($externalSales as $externalSale) {
            if (isset($result[$externalSale->bundle_id])) {
                $result[$externalSale->bundle_id]->external_sales_sum += $externalSale->sum;
                $result[$externalSale->bundle_id]->paid += $externalSale->paid;
                $result[$externalSale->bundle_id]->ordered += $externalSale->paid;
            } else {
                $result[$externalSale->bundle_id] = new Statistics([
                    'bundle_id' => $externalSale->bundle_id,
                    'type' => $externalSale->bundle->getType(),
                    'title' => $externalSale->bundle->title,
                    'external_sales_sum' => $externalSale->sum,
                    'paid' => $externalSale->paid,
                    'ordered' => $externalSale->paid,
                    'created_at' => $externalSale->bundle->created_at,
                ]);
            }
        }

        /**
         * Bills
         */
        $bills = QueryBuilder::for(OrderBill::class)
            ->with(['bundle'])
            ->where([
                'status' => OrderBill::PAYMENT_STATUS_PAYED,
            ])
            ->whereNotNull('bundle_id')
            ->select([
                'bundle_id',
                DB::raw('sum(price) AS sum'),
                DB::raw('count(*) AS paid'),
            ])
            ->groupBy('bundle_id');

        $bills->allowedFilters([
            Filter::scope('date_from'),
            Filter::scope('date_to'),
        ]);

        $bills = $bills->get()->keyBy('bundle_id');

        /** @var OrderBill[]|Collection $bills */
        foreach ($bills as $bill) {
            if (isset($result[$bill->bundle_id])) {
                $result[$bill->bundle_id]->bills_sales += $bill->paid;
                $result[$bill->bundle_id]->bills_sales_sum += $bill->sum;
            } else {
                $result[$bill->bundle_id] = new Statistics([
                    'bundle_id' => $bill->bundle_id,
                    'type' => $bill->bundle->getType(),
                    'title' => $bill->bundle->title,
                    'bills_sales' => $bill->paid,
                    'bills_sales_sum' => $bill->sum,
                    'created_at' => $bill->bundle->created_at,
                ]);
            }
        }

        $result = $result->sortBy('created_at');

        /**
         * Delivery price
         * приблизительно считает доставку
         */
        foreach ($result as $item) {
            if (in_array($item->type, [Bundle::PRODUCT_TYPE_PAPER_BOOK, Bundle::PRODUCT_TYPE_MIXED])) {
                $bundleId = $item->bundle_id;

                $totalDeliverySumOrder = QueryBuilder::for(Order::class)
                    ->whereIn('status', [
                        Order::STATUS_PROCESSING,
                        Order::STATUS_COMPLETED,
                        Order::STATUS_RETURNED,
                    ])
                    ->whereHas('items', function ($query) use ($bundleId): void {
                        $query->where(['orders_items.bundle_id' => $bundleId]);
                    })
                    ->select([
                        DB::raw("(
                        delivery_price / 
                        (select sum(orders_products.quantity) from 
                        orders_products where orders_products.order_id = orders.id)
                        ) * 
                         (select sum(orders_products.quantity) from 
                        orders_products where orders_products.order_id = orders.id AND orders_products.bundle_id = {$bundleId})
                         AS dp"),
                    ])
                    ->allowedFilters([
                        Filter::scope('date_from'),
                        Filter::scope('date_to'),
                    ])
                    ->get()
                    ->sum('dp');

                $totalDeliverySumOrder2 = QueryBuilder::for(ExternalOrder::class)
                    ->whereIn('status', [
                        Order::STATUS_PROCESSING,
                        Order::STATUS_COMPLETED,
                        Order::STATUS_RETURNED,
                    ])
                    ->select([
                        DB::raw("(
                        delivery_price / 
                        (select sum(external_orders_items.quantity) from 
                        external_orders_items where external_orders_items.external_order_id = external_orders.external_id)
                        ) * 
                         (select sum(external_orders_items.quantity) from 
                        external_orders_items where external_orders_items.external_order_id = external_orders.external_id AND external_orders_items.bundle_id = {$bundleId})
                         AS dp"),
                    ])
                    ->allowedFilters([
                        Filter::scope('date_from'),
                        Filter::scope('date_to'),
                    ])
                    ->whereHas('items', function ($query) use ($bundleId): void {
                        $query->where(['external_orders_items.bundle_id' => $bundleId]);
                    })
                    ->get()
                    ->sum('dp');

                $item->delivery_sum = $totalDeliverySumOrder + $totalDeliverySumOrder2;
            }
        }


        return $result;
    }

    public function summary(): array
    {
        /**
         * Delivery price
         */
        $totalDeliverySumOrder = QueryBuilder::for(Order::class)
            ->whereIn('status', [
                Order::STATUS_PROCESSING,
                Order::STATUS_COMPLETED,
                Order::STATUS_RETURNED,
            ])->allowedFilters([
                Filter::scope('date_from'),
                Filter::scope('date_to'),
            ])->sum('delivery_price');

        $totalDeliverySumExternalOrder = QueryBuilder::for(ExternalOrder::class)
            ->whereIn('status', [
                Order::STATUS_PROCESSING,
                Order::STATUS_COMPLETED,
                Order::STATUS_RETURNED,
            ])->allowedFilters([
                Filter::scope('date_from'),
                Filter::scope('date_to'),
            ])->sum('delivery_price');

        $totalDeliverySum = $totalDeliverySumOrder + $totalDeliverySumExternalOrder;

        return [
            'delivery' => $totalDeliverySum,
        ];
    }

    public function profit()
    {
        $orders = QueryBuilder::for(OrderProduct::class)
            ->join('orders', 'orders.id', '=', 'orders_products.order_id')
            ->select([
                'author_id',
                DB::raw('SUM(orders_products.quantity) AS ordered'),
                DB::raw('SUM(((orders_products.net_profit - orders_products.prime_cost) * orders_products.quantity) * (orders_products.authors_percent / 100)) as authors_profit'),
                DB::raw('SUM(((orders_products.net_profit - orders_products.prime_cost) * orders_products.quantity) * ((100 - orders_products.authors_percent) / 100)) as house_profit'),
            ])
            ->whereIn('orders.status', [Order::STATUS_PROCESSING, Order::STATUS_COMPLETED])
            ->groupBy('orders_products.author_id');

        $orders->allowedFilters([
            Filter::scope('date_from'),
            Filter::scope('date_to'),
        ]);

        $orders = $orders->get();

        $result = $orders->map(function ($object) {
            /** @var OrderItem $object */
            return (new StatisticsProfit([
                'author_id' => $object->author_id,
                'authors_profit' => $object->authors_profit,
                'house_profit' => $object->house_profit,
            ]));
        });

        $result = $result->keyBy('author_id');

        /**
         * External sales
         */
        $externalSales = QueryBuilder::for(ExternalSaleProduct::class)
            ->join('external_sales', 'external_sales.id', '=', 'external_sales_products.external_sale_id')
            ->where(['external_sales.is_paid' => true])
            ->select([
                'author_id',
                DB::raw('SUM(external_sales_products.quantity) AS ordered'),
                DB::raw('SUM(((external_sales_products.net_profit - external_sales_products.prime_cost) * external_sales_products.quantity) * (external_sales_products.authors_percent / 100)) as authors_profit'),
                DB::raw('SUM(((external_sales_products.net_profit - external_sales_products.prime_cost) * external_sales_products.quantity) * ((100 - external_sales_products.authors_percent) / 100)) as house_profit'),
            ])
            ->groupBy('author_id');

        $externalSales->allowedFilters([
            Filter::scope('date_from'),
            Filter::scope('date_to'),
        ]);

        $externalSales = $externalSales->get()->keyBy('author_id');

        /** @var ExternalSale[]|Collection $externalSales */
        foreach ($externalSales as $object) {
            $key = $object->author_id;
            if (isset($result[$key])) {
                $result[$key]->authors_profit += $object->authors_profit;
                $result[$key]->house_profit += $object->house_profit;
            } else {
                $result[$key] = new StatisticsProfit([
                    'author_id' => $object->author_id,
                    'authors_profit' => $object->authors_profit,
                    'house_profit' => $object->house_profit,
                ]);
            }
        }

        /**
         * Bills
         */
        $bills = QueryBuilder::for(BillProduct::class)
            ->join('bills', 'bills.id', '=', 'bills_products.bill_id')
            ->where(['bills.status' => OrderBill::PAYMENT_STATUS_PAYED])
            ->whereNotNull('bills.bundle_id')
            ->select([
                'author_id',
                DB::raw('count(*) AS ordered'),
                DB::raw('SUM(((bills_products.net_profit - bills_products.prime_cost) * bills_products.quantity) * (bills_products.authors_percent / 100)) as authors_profit'),
                DB::raw('SUM(((bills_products.net_profit - bills_products.prime_cost) * bills_products.quantity) * ((100 - bills_products.authors_percent) / 100)) as house_profit'),
            ])
            ->groupBy('author_id');

        $bills->allowedFilters([
            Filter::scope('date_from'),
            Filter::scope('date_to'),
        ]);

        $bills = $bills->get()->keyBy('author_id');

        /** @var OrderBill[]|Collection $bills */
        foreach ($bills as $object) {
            $key = $object->author_id;
            if (isset($result[$key])) {
                $result[$key]->authors_profit += $object->authors_profit;
                $result[$key]->house_profit += $object->house_profit;
            } else {
                $result[$key] = new Statistics([
                    'author_id' => $object->author_id,
                    'authors_profit' => $object->authors_profit,
                    'house_profit' => $object->house_profit,
                ]);
            }
        }

        return $result;
    }
}
