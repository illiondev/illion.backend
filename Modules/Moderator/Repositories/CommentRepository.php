<?php

namespace Modules\Moderator\Repositories;

use App\Models\Comment;
use App\Models\User;

class CommentRepository
{
    protected $model;

    public function __construct(Comment $model)
    {
        $this->model = $model;
    }

    public function update(User $model, string $text, int $moderatorId): ?Comment
    {
        /** @var Comment $comment */
        $comment = $model->comment;

        if (!$text) {
            if ($model->comment) {
                $model->comment->delete();
            }
            return null;
        }

        if (!$comment) {
            $comment = new Comment();
        }

        $comment->text = $text;
        $comment->user_id = $moderatorId;

        $model->comment()->save($comment);

        return $comment;
    }
}
