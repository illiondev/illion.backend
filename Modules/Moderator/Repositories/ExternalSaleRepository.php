<?php

namespace Modules\Moderator\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Modules\Moderator\Models\ExternalSale;
use Spatie\QueryBuilder\Filter;
use Spatie\QueryBuilder\QueryBuilder;

class ExternalSaleRepository
{
    protected $model;

    public function __construct(ExternalSale $model)
    {
        $this->model = $model;
    }

    public function findOrFail(int $id): ExternalSale
    {
        return $this->model->findOrFail($id);
    }

    public function allPaginate(int $perPage): Paginator
    {
        $result = QueryBuilder::for(ExternalSale::class)
            ->orderBy('created_at', 'desc');

        $result->allowedFilters([
            Filter::exact('bundle_id'),
        ]);

        return $result->paginate($perPage);

    }

    public function create(int $moderatorId, array $validated): ExternalSale
    {
        $externalSale = new $this->model($validated);

        $externalSale->user_id = $moderatorId;

        $externalSale->saveOrFail();

        return $externalSale;
    }

    public function update(ExternalSale $externalSale, int $moderatorId, array $validated): ExternalSale
    {
        $externalSale->fill($validated);

        $externalSale->user_id = $moderatorId;

        $externalSale->saveOrFail();

        return $externalSale;
    }
}
