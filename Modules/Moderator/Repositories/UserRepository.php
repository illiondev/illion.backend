<?php

namespace Modules\Moderator\Repositories;

use App\Models\User;
use Illuminate\Contracts\Pagination\Paginator;

class UserRepository
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function getModeratorsPaginate(int $perPage): Paginator
    {
        return $this->model
            ->with(['permissions'])
            ->role(User::ROLE_MODERATOR)
            ->paginate($perPage);
    }

    public function getModerator(int $id): User
    {
        return $this->model
            ->with(['permissions'])
            ->role(User::ROLE_MODERATOR)
            ->where(['id' => $id])
            ->first();
    }
}
