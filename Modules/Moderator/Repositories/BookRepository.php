<?php

namespace Modules\Moderator\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Modules\Moderator\Models\Book;

class BookRepository
{
    protected $model;

    public function __construct(Book $model)
    {
        $this->model = $model;
    }

    protected function queryAll(): Builder
    {
        return $this->model->orderBy('id', 'desc');
    }

    protected function queryById(int $id): Builder
    {
        return $this->model->where([
            'id' => $id,
        ]);
    }

    public function allPaginate(int $perPage): Paginator
    {
        return $this->queryAll()
            ->with([
                'bookChapters',
                'author',
                'media',
            ])
            ->paginate($perPage);
    }

    public function findOrFail(int $id): Book
    {
        return $this->queryById($id)->firstOrFail();
    }

    public function create(int $userId, array $params): Book
    {
        $book = new $this->model($params);
        $book->user_id = $userId;

        $book->saveOrFail();

        return $book;
    }

    public function update(Book $book, array $params): Book
    {
        $book->fill($params);

        $book->saveOrFail();

        return $book;
    }
}
