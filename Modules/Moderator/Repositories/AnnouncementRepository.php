<?php

namespace Modules\Moderator\Repositories;

use Modules\Moderator\Models\Announcement;

class AnnouncementRepository
{
    protected $model;

    public function __construct(Announcement $model)
    {
        $this->model = $model;
    }

    public function firstOrNew(): ?Announcement
    {
        $announcement = $this->model->first();

        if (!$announcement) {
            $announcement = new $this->model(['enabled' => false]);
        }

        return $announcement;
    }

    public function firstOrFail(): Announcement
    {
        return $this->model->firstOrFail();
    }

    public function create(array $params): Announcement
    {
        $announcement = new $this->model($params);

        $announcement->saveOrFail();

        return $announcement;
    }

    public function update(Announcement $announcement, array $validated): Announcement
    {
        $announcement->fill($validated);
        $announcement->saveOrFail();

        return $announcement;
    }
}
