<?php

namespace Modules\Moderator\Repositories;

use Modules\Moderator\Models\Vacancy;
use Illuminate\Support\Collection;

class VacancyRepository
{
    protected $model;

    public function __construct(Vacancy $model)
    {
        $this->model = $model;
    }

    public function findOrFail(int $id): Vacancy
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $params): Vacancy
    {
        $vacancy = new $this->model($params);

        $vacancy->saveOrFail();

        return $vacancy;
    }

    public function update(Vacancy $vacancy, array $validated): Vacancy
    {
        $vacancy->fill($validated);
        $vacancy->saveOrFail();

        return $vacancy;
    }

    public function all(): Collection
    {
        return $this->model->orderBy('priority', 'desc')->get();
    }
}
