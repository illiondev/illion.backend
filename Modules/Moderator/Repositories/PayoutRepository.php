<?php

namespace Modules\Moderator\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Modules\Moderator\Models\Payout;

class PayoutRepository
{
    protected $model;

    public function __construct(Payout $model)
    {
        $this->model = $model;
    }

    public function findOrFail(int $id): Payout
    {
        return $this->model->findOrFail($id);
    }

    public function allPaginate(int $perPage): Paginator
    {
        return $this->model
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    public function create(int $authorId, int $moderatorId, float $amount, string $payoutDate): Payout
    {
        $payout = new $this->model([
            'author_id' => $authorId,
            'payout_date' => $payoutDate,
            'amount' => $amount,
        ]);

        $payout->moderator_id = $moderatorId;

        $payout->saveOrFail();

        return $payout;
    }

    public function update(Payout $payout, int $authorId, int $moderatorId, float $amount, string $payoutDate): Payout
    {
        $payout->fill([
            'author_id' => $authorId,
            'payout_date' => $payoutDate,
            'amount' => $amount,
        ]);

        $payout->moderator_id = $moderatorId;

        $payout->saveOrFail();

        return $payout;
    }
}
