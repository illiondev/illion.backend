<?php

namespace Modules\Moderator\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Modules\Moderator\Models\Soon;

class SoonRepository
{
    protected $model;

    public function __construct(Soon $model)
    {
        $this->model = $model;
    }

    protected function queryAll(): Builder
    {
        return $this->model->orderBy('id', 'desc');
    }

    public function all(): Collection
    {
        return $this->queryAll()->get();
    }

    public function findOrFail(int $id): Soon
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $params): Soon
    {
        $soon = new $this->model($params);

        $soon->saveOrFail();

        return $soon;
    }

    public function update(Soon $soon, array $params): Soon
    {
        $soon->fill($params);

        $soon->saveOrFail();

        return $soon;
    }
}
