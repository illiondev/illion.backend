<?php

namespace Modules\Moderator\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Modules\Moderator\Models\BookChapter;

class BookChapterRepository
{
    protected $model;

    public function __construct(BookChapter $model)
    {
        $this->model = $model;
    }

    protected function getByBookIdQuery(int $bookId): Builder
    {
        return $this->model
            ->where(['book_id' => $bookId])
            ->orderBy('number');
    }

    protected function getNextChapterNumber(int $bookId): int
    {
        $prevNumber = $this->getByBookIdQuery($bookId)->max('number');

        return $prevNumber + 1;
    }

    public function allByBookId(int $bookId): Collection
    {
        return $this->getByBookIdQuery($bookId)->get();
    }

    public function findOrFailByBookId(int $bookId, int $id): BookChapter
    {
        return $this
            ->getByBookIdQuery($bookId)
            ->where(['id' => $id])
            ->firstOrFail();
    }

    public function create(int $bookId, array $params): BookChapter
    {
        $bookChapter = new $this->model($params);

        $bookChapter->number = $this->getNextChapterNumber($bookId);

        $bookChapter->book_id = $bookId;

        if ($bookChapter->content === null) {
            $bookChapter->content = '';
        }

        $bookChapter->saveOrFail();

        return $bookChapter;
    }

    public function update(BookChapter $bookChapter, array $params): BookChapter
    {
        $bookChapter->fill($params);

        $bookChapter->number = $params['number'] ?? $bookChapter->number;

        $bookChapter->saveOrFail();

        return $bookChapter;
    }

    public function getChaptersCount(int $bookId): int
    {
        return $this->getByBookIdQuery($bookId)->count();
    }

    public function getUncompletedChaptersCount(int $bookId): int
    {
        return $this->getByBookIdQuery($bookId)
            ->where(['completed' => false])
            ->count();
    }
}
