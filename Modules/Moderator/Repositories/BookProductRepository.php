<?php

namespace Modules\Moderator\Repositories;

use Illuminate\Support\Collection;
use Modules\Moderator\Models\Product;
use Spatie\QueryBuilder\QueryBuilder;

class BookProductRepository
{
    protected $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function autocomplete(string $q, int $limit): Collection
    {
        $result = QueryBuilder::for(Product::class)
            ->join('books', 'books.id', '=', 'book_products.book_id')
            ->join('authors', 'authors.id', '=', 'books.author_id')
            ->select(['book_products.id', 'book_products.type', 'book_products.prime_cost', 'book_products.book_id'])
            ->where(function ($query) use ($q): void {
                $query->where('books.title', 'like', $q . '%');
                $query->orWhere('name', 'like', $q . '%');
                $query->orWhere('surname', 'like', $q . '%');
            })
            ->limit($limit);

        return $result->get();
    }
}
