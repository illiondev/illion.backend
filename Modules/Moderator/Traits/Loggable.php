<?php

namespace Modules\Moderator\Traits;

use Spatie\Activitylog\Traits\LogsActivity;

trait Loggable
{
    use LogsActivity;

    protected static $logName = 'moderator_model_observer';
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public static function getLogName() {
        return self::$logName;
    }
}
