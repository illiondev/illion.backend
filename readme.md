# House of Illion

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

For now we have only Unit tests

### How to run tests

To run Unit tests next command should be executed:

```
./vendor/bin/codecept run unit
```

### Code coverage

Normally coverage of tested code should be more than 70% of code.

To build Unit tests coverage map just run:

```
./vendor/bin/codecept run unit --coverage-html
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Laravel](https://laravel.com/docs/5.7) - The web framework used

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **John Doe** - *Initial work* - [IllionDev](https://bitbucket.org/illiondev/)

See also the list of [commit authors](https://bitbucket.org/illiondev/illion.backend/commits/all) who participated in this project.

## License

This project is licensed under the *** License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
